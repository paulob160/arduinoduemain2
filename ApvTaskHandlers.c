/******************************************************************************/
/* (C) PulsingCoreSoftware Limited 2018 (C)                                   */
/******************************************************************************/
/*                                                                            */
/* ApvTaskHandlers.c                                                          */
/* 31.10.18                                                                   */
/* Paul O'Brien                                                               */
/*                                                                            */
/* - definition of the cooperative ("mosaic") scheduling structure. This is   */
/*   a task-based structure but there is NO requirement for tasks to be re-   */
/*   entrant. The use of reentrancy is a decision for the end-user            */
/*                                                                            */
/******************************************************************************/
/* Include Files :                                                            */
/******************************************************************************/

#include <stdio.h>
#include "ApvError.h"
#include "ApvUtilities.h"
#include "ApvTaskHandlers.h"
#include "ApvTaskPriorities.h"

/******************************************************************************/
/* Global Variables :                                                         */
/******************************************************************************/
/* The "task control block" array serves to contain the task control blocks   */
/* and to drive the scheduler/dispatcher                                      */
/******************************************************************************/

apvTaskControlBlock_t  apvTaskControlBlocks[APV_MAXIMUM_TASK_CONTROL_BLOCKS];

/******************************************************************************/
/* The "task run list" is a temporary list of tasks that are candidates to    */
/* run in the current scheduling slot index                                   */
/******************************************************************************/

apvTaskControlBlock_t *apvTaskRunList[APV_MAXIMUM_TASK_CONTROL_BLOCKS];

/******************************************************************************/
/* Global Function Definitions :                                              */
/******************************************************************************/
/* apvInitialiseTaskControlBlocks() :                                         */
/*  --> taskControlBlocks      : address of a list of task control blocks     */
/*  <-- taskControlBlockErrors : error codes                                  */
/*                                                                            */
/* = initialise a list of task control block                                  */
/*                                                                            */
/******************************************************************************/

APV_ERROR_CODE apvInitialiseTaskControlBlocks(apvTaskControlBlock_t *taskControlBlocks)
  {
/******************************************************************************/
  
  APV_ERROR_CODE taskControlBlockErrors = APV_ERROR_CODE_NONE;

  uint8_t        taskControlBlockIndex = 0;

/******************************************************************************/

  if (taskControlBlocks == NULL)
    {
    taskControlBlockErrors = APV_ERROR_CODE_NULL_PARAMETER;
    }
  else
    {
    for (taskControlBlockIndex = 0; taskControlBlockIndex < APV_MAXIMUM_TASK_CONTROL_BLOCKS; taskControlBlockIndex++)
      {
      apvInitialiseTaskControlBlock(&apvTaskControlBlocks[taskControlBlockIndex]);
      }
    }

/******************************************************************************/

  return(taskControlBlockErrors);

/******************************************************************************/
  } /* end of apvInitialiseTaskControlBlocks                                  */

/******************************************************************************/
/* apvInitialiseTaskControlBlock() :                                          */
/*  --> taskControlBlock       : address of a task control block              */
/*  <-- taskControlBlockErrors : error codes                                  */
/*                                                                            */
/* = initialise a task control block                                          */
/*                                                                            */
/******************************************************************************/

APV_ERROR_CODE apvInitialiseTaskControlBlock(apvTaskControlBlock_t *taskControlBlock)
  {
/******************************************************************************/

  APV_ERROR_CODE taskControlBlockErrors    = APV_ERROR_CODE_NONE;

  uint8_t        taskScratchWorkspaceIndex = 0;

/******************************************************************************/

  if (taskControlBlock == NULL)
    {
    taskControlBlockErrors = APV_ERROR_CODE_NULL_PARAMETER;
    }
  else
    {
    taskControlBlock->apvTaskControlBlockAllocated   = FALSE;
    taskControlBlock->apvTaskFunction                = NULL;
    taskControlBlock->apvTaskParameters              = NULL;
    taskControlBlock->apvTaskScheduleType            = APV_TASK_SCHEDULE_NONE;
    taskControlBlock->apvTaskSchedulePriorityClass   = APV_TASK_SCHEDULE_PRIORITY_CLASS_NONE;
    taskControlBlock->apvTaskSchedulePriority        = APV_TASK_SCHEDULE_PRIORITY_HIGHEST;
    taskControlBlock->apvTaskSelfSchedulingFlag      = FALSE;
    taskControlBlock->apvTaskSchedulingEventComplete = FALSE;
    taskControlBlock->apvTaskSchedulingEventInterval = 0;
    taskControlBlock->apvTaskSchedulingEventNext     = 0;
  
    for (taskScratchWorkspaceIndex = 0; taskScratchWorkspaceIndex < APV_TASK_SCRATCH_WORKSPACE_SIZE; taskScratchWorkspaceIndex++)
      {
      taskControlBlock->apvTaskScratchWorkspace[taskScratchWorkspaceIndex] = 0;
      }
    }

/******************************************************************************/

  return(taskControlBlockErrors);

/******************************************************************************/
  } /* end of apvInitialiseTaskControlBlock                                   */

/******************************************************************************/
/* apvCreateTask() :                                                          */
/*  --> taskFunction           : address of a function to run on a scheduled  */
/*                               interval                                     */
/*  --> taskParameters         : address of a parameter block to pass to the  */
/*                               scheduled function                           */
/*  --> taskScheduleType       : the task scheduling type                     */
/*  --> taskSchedulingInterval : the task scheduling interval                 */
/*  <-- taskErrors             : error codes                                  */
/*                                                                            */
/* - populate a task control block in the list of task control blocks         */
/*                                                                            */
/******************************************************************************/

APV_ERROR_CODE apvCreateTask(APV_ERROR_CODE        (*taskFunction)(void      *taskParameters,
		                                                                 void      *taskWorkspace,
                                                                   boolean_t *taskSelfSchedulingFlag),
                             void                  *taskParameters,
                             apvTaskScheduleType_t  taskScheduleType,
                             uint16_t               taskSchedulingInterval)
  {
/******************************************************************************/

  APV_ERROR_CODE taskErrors            = APV_ERROR_CODE_NONE;

  uint8_t        taskControlBlockIndex = 0;

/******************************************************************************/

  if ((taskFunction == NULL) || (taskParameters == NULL))
    {
    taskErrors = APV_ERROR_CODE_NULL_PARAMETER;
    }
  else
    {
    if (taskSchedulingInterval > (APV_MAXIMUM_TASK_SCHEDULING_TICKS - 1))
      {
      taskErrors = APV_ERROR_CODE_PARAMETER_OUT_OF_RANGE;
      }
    else
      {
      if (taskScheduleType > APV_TASK_SCHEDULE_NEVER)
        {
        taskErrors = APV_ERROR_CODE_PARAMETER_OUT_OF_RANGE;
        }
      else
        {
        // Try and find a free slot in the task control block list
        do 
          {
          if (apvTaskControlBlocks[taskControlBlockIndex].apvTaskControlBlockAllocated == TRUE)
            {
            taskControlBlockIndex = taskControlBlockIndex + 1;
            }
          else
            {
            break;
            }
          }
        while (taskControlBlockIndex != APV_MAXIMUM_TASK_CONTROL_BLOCKS);
  
        if (taskControlBlockIndex == APV_MAXIMUM_TASK_CONTROL_BLOCKS)
          {
          taskErrors = APV_ERROR_CODE_CONFIGURATION_ERROR;
          }
        else
          {
          apvInitialiseTaskControlBlock(&apvTaskControlBlocks[taskControlBlockIndex]);

          apvTaskControlBlocks[taskControlBlockIndex].apvTaskControlBlockAllocated   = TRUE;
          apvTaskControlBlocks[taskControlBlockIndex].apvTaskFunction                = taskFunction;
          apvTaskControlBlocks[taskControlBlockIndex].apvTaskParameters              = taskParameters;
          apvTaskControlBlocks[taskControlBlockIndex].apvTaskScheduleType            = taskScheduleType;
          apvTaskControlBlocks[taskControlBlockIndex].apvTaskSchedulePriorityClass   = APV_TASK_SCHEDULE_PRIORITY_CLASS_LOW;
          apvTaskControlBlocks[taskControlBlockIndex].apvTaskSchedulePriority        = APV_TASK_SCHEDULE_PRIORITY_LSM9DS1;
          apvTaskControlBlocks[taskControlBlockIndex].apvTaskSchedulingEventInterval = taskSchedulingInterval;
          }
        }
      }
    }

/******************************************************************************/

  return(taskErrors);

/******************************************************************************/
  } /* end of apvCreateTask                                                   */

/******************************************************************************/
/* apvTaskSchedulerDispatcher() :                                             */
/*  --> taskControlBlocks : address of a list of task control blocks          */
/*  --> taskSlotNumber    : current index of the scheduling slots :           */
/*                            0 { index } 1023                                */
/*  <-- schedulerErrors   : shceduler error codes                             */
/*                                                                            */
/* - arbitrate and run any tasks due in the current slot                      */
/*                                                                            */
/******************************************************************************/

APV_ERROR_CODE apvTaskSchedulerDispatcher(apvTaskControlBlock_t  *taskControlBlocks,
                                          apvTaskControlBlock_t **taskRunList,
                                          uint16_t                taskSlotNumber)
  {
/******************************************************************************/

  APV_ERROR_CODE schedulerErrors       = APV_ERROR_CODE_NONE;

  uint8_t        taskControlBlockIndex = 0,
                 taskRunListIndex      = 0;

/******************************************************************************/

  if (taskSlotNumber >= APV_MAXIMUM_TASK_SCHEDULING_TICKS)
    {
    // This count should be impossible - something has gone very wrong!
    schedulerErrors = APV_ERROR_CODE_PARAMETER_OUT_OF_RANGE;
    }
  else
    {
    if (taskControlBlocks == NULL)
      {
      // Another catastrophe!
      schedulerErrors = APV_ERROR_CODE_NULL_PARAMETER;
      }
    else
      {
      // Empty any remnants from an old run list
      for (taskRunListIndex = 0; taskRunListIndex < APV_MAXIMUM_TASK_CONTROL_BLOCKS; taskRunListIndex++)
        {
        *(taskRunList + taskRunListIndex) = NULL;
        }

      // Ready to try to kick off one or more task functions
      taskRunList = 0;

      for (taskControlBlockIndex = 0; taskControlBlockIndex < APV_MAXIMUM_TASK_CONTROL_BLOCKS; taskControlBlockIndex++)
        {
        // Put any (possible) run candidates on the (possible) run-list : 
        // - there are a number of ways a task can become a running candidate
        if ((taskControlBlocks + taskControlBlockIndex)->apvTaskControlBlockAllocated == TRUE)
          {
          // Does the event time match the scheduling time ?
          if ((taskControlBlocks + taskControlBlockIndex)->apvTaskSchedulingEventNext == taskSlotNumber)
            {
            *(taskRunList + taskRunListIndex) = (taskControlBlocks + taskControlBlockIndex);
             taskRunList = taskRunList + 1;
            }
          else
            {
            // Is the task run at intervals based on the current scheduling time ?
           if (1)
              {
              *(taskRunList + taskRunListIndex) = (taskControlBlocks + taskControlBlockIndex);
               taskRunList = taskRunList + 1;
              }
            else
              {
              // Is the task currently self-scheduled ?
              if (0)
                {
                *(taskRunList + taskRunListIndex) = (taskControlBlocks + taskControlBlockIndex);
                 taskRunList = taskRunList + 1;
                }
              }
            }
          }
        }
      }
    }

/******************************************************************************/

  return(schedulerErrors);

/******************************************************************************/
  } /* end of apvTaskSchedulerDispatcher                                      */

/******************************************************************************/
/* (C) PulsingCoreSoftware Limited 2018 (C)                                   */
/******************************************************************************/
