/******************************************************************************/
/* (C) PulsingCoreSoftware Limited 2020 (C)                                   */
/******************************************************************************/
/*                                                                            */
/* ApvPeripheralControlUSART.h                                                */
/* 03.05.20                                                                   */
/* Paul O'Brien                                                               */
/*                                                                            */
/*  - initialisation and setup of the USART chip peripheral behaviour         */
/*                                                                            */
/******************************************************************************/

#ifndef _APV_PERIPHERAL_CONTROL_USART_H_
#define _APV_PERIPHERAL_CONTROL_USART_H_

/******************************************************************************/

// The length of the GPS receiver ring buffer needs to be able to cope with a 
// "fast" data stream occuring once every second
#define APV_GPS_RECEIVER_RING_BUFFER_LENGTH APV_COMMS_RING_BUFFER_MAXIMUM_LENGTH

#define APV_GPS_RECEIVER_BAUD_RATE          (9600)
#define APV_GPS_RECEIVER_OVERSAMPLING_8        (8)
#define APV_GPS_RECEIVER_OVERSAMPLING_16      (16)
#define APV_GPS_RECEIVER_OVERSAMPLING_x_2      (2)

/******************************************************************************/

typedef enum apvGpsReceiverRingBufferSet_tTag
  {
  APV_GPS_RECEIVER_RING_BUFFER_INDEX_0 = 0,
  APV_GPS_RECEIVER_RING_BUFFER_INDEX_1,
  APV_GPS_RECEIVER_RING_BUFFER_SET
  } apvGpsReceiverRingBufferSet_t;

typedef enum apvUsartNumber_tTag
  {
  APV_USART_0 = 0,
  APV_USART_1,
  APV_USART_2,
  APV_USART_3,
  APV_USARTS
  } apvUsartNumber_t;
  
/******************************************************************************/

extern apvRingBuffer_t  apvGpsReceiverPortRingBuffer[APV_GPS_RECEIVER_RING_BUFFER_SET],
                       *apvGpsReceiverPortRingBuffer_p[APV_GPS_RECEIVER_RING_BUFFER_SET],
                       *apvGpsReceiverPortReceiveBuffer;

extern          Usart   apvUsart0ControlBlock;
extern volatile Usart  *apvUsart0ControlBlock_p;

extern          Usart   apvGpsReceiverControlBlock; 
extern volatile Usart  *apvGpsReceiverControlBlock_p;
extern          uint8_t ID_GPS;

/******************************************************************************/

extern void                  USART0_Handler(void);
extern void                  USART1_Handler(void);
extern void                  USART2_Handler(void);
extern void                  USART3_Handler(void);
extern APV_SERIAL_ERROR_CODE apvGpsReceiverCommsSetup(apvUsartNumber_t usartNumber);
extern void                  apsGpsReceiverCommsHandler(uint8_t usartNumber);

/******************************************************************************/

#endif

/******************************************************************************/
/* (C) PulsingCoreSoftware Limited 2020 (C)                                   */
/******************************************************************************/
