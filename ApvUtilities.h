/******************************************************************************/
/* (C) PulsingCoreSoftware Limited 2018 (C)                                   */
/******************************************************************************/
/*                                                                            */
/* ApvUtilities.h                                                             */
/* 22.03.18                                                                   */
/* Paul O'Brien                                                               */
/*                                                                            */
/* - a set of functions to add some helpers                                   */
/*                                                                            */
/******************************************************************************/

#ifndef _APV_UTILITIES_
#define _APV_UTILITIES_

/******************************************************************************/
/* Include Files :                                                            */
/******************************************************************************/

#include <stdint.h>
#ifndef WIN32
#include <__armlib.h>
#include <sam3x8e.h>
#endif
#include "ApvError.h"
#include "APVTypes.h"
#include "ApvCommsUtilities.h"

/******************************************************************************/
/* Definitions :                                                              */
/******************************************************************************/

#define APV_INITIAL_INTERRUPT_NESTING_COUNT ((int16_t)0)

#define APV_RESOURCE_ID_BASE                (64) // a number of pins etc.,. can be dedicated to development, debug...
                                                 // and start at an "ID" offset to avoid the Atmel MCU ids (sam3x8e.h)


// The physical addresses of the four peripheral I/O controller blocks
#define APV_PIO_BLOCK_A ((Pio *)0x400E0E00U)
#define APV_PIO_BLOCK_B ((Pio *)0x400E1000U)
#define APV_PIO_BLOCK_C ((Pio *)0x400E1200U)
#define APV_PIO_BLOCK_D ((Pio *)0x400E1400U)


#define  APV_SYSTEM_INTERRUPT_ID_NMI                      ((int16_t)-14)
#define  APV_SYSTEM_INTERRUPT_ID_HARD_FAULT               ((int16_t)-13)
#define  APV_SYSTEM_INTERRUPT_ID_MEMORY_MANAGEMENT_FAULT  ((int16_t)-12)
#define  APV_SYSTEM_INTERRUPT_ID_BUS_FAULT                ((int16_t)-11)
#define  APV_SYSTEM_INTERRUPT_ID_USAGE_FAULT              ((int16_t)-10)
#define  APV_SYSTEM_INTERRUPT_ID_SUPERVISOR_CALL           ((int16_t)-5)
#define  APV_SYSTEM_INTERRUPT_ID_PEND_SUPERVISORY          ((int16_t)-2)
#define  APV_SYSTEM_INTERRUPT_ID_SYSTEM_TICK               ((int16_t)-1)
#define  APV_SYSTEM_INTERRUPT_ID_S                         ((int16_t) 8)

#define APV_16_BIT_BINARY_BIT_POSITIONS                   (16)               // 16-bit hexadecimal bit width
#define APV_16_BIT_BINARY_BIT_POSITION_DECIMAL_WEIGHTS     (5)               // decimal digits spanning 16-bit hex
#define APV_16_BIT_BINARY_BIT_POSITION_DECIMAL_MINIMUM     (1)               // decimal-0 is the sign bit
#define APV_16_BIT_BINARY_BIT_POSITION_SIGN                (0)
#define APV_16_BIT_BINARY_2_DECIMAL_WIDTH                  (6)               // the conversion is the decimal weights plus a sign
#define APV_16_BIT_2_DECIMAL_MODULUS                      (10)               // base-10 arithmetic
#define APV_16_BIT_BINARY_BIT_SIGN_POSITION               ((uint16_t)0x8000) // test the sign if required
#define APV_16_BIT_BINARY_BIT_NEGATIVE_SIGN               '-'
#define APV_16_BIT_BINARY_BIT_POSITIVE_SIGN               '+'

#define APV_16_BIT_BINARY_BIT_CONVERT_TO_ASCII            '0'                // add an ASCII '0' to make the conversion printable

// Apv Arduino System Identifier is at Peripheral I/O Register 'C' bits 12, 13, 14 and 15
#define APV_ARDUINO_SYSTEM_IDENTITY_MASK                  ((1 << APV_ARDUINO_PERIPHERAL_BIT_12) | \
                                                           (1 << APV_ARDUINO_PERIPHERAL_BIT_13) | \
                                                           (1 << APV_ARDUINO_PERIPHERAL_BIT_14) | \
                                                           (1 << APV_ARDUINO_PERIPHERAL_BIT_15))

// Lsm9ds1
#define APV_ARDUINO_LSM9DS1_MASK                            (1 << APV_ARDUINO_PERIPHERAL_BIT_29)

// nRF24L01 Chip Enables are at peripheral I/O Registers 'C' bit 26 and 'B' bit 21 respectively
#define APV_ARDUINO_N_RF24L01_TX_MASK                      (1 << APV_ARDUINO_PERIPHERAL_BIT_26)
#define APV_ARDUINO_N_RF24L01_RX_MASK                      (1 << APV_ARDUINO_PERIPHERAL_BIT_21)

// "Heartbeat" LED chip enable is at peripheral I/O register 'B' bit 27
#define APV_ARDUINO_HEARTBEAT_LED_MASK                     (1 << APV_ARDUINO_PERIPHERAL_BIT_27)

/******************************************************************************/
/* Constant Equivalences :                                                    */
/******************************************************************************/
// Equating Visual C++'s poor treatment of "booleans'
#define FALSE false
#define TRUE  true

/******************************************************************************/
/* Type Definitions :                                                         */
/******************************************************************************/
/* System Identifiers :                                                       */
/* - the system identity scheme is designed to reflect the Tx:Rx mapping of   */
/*   the nRF24L01+ where one "master" can terminate up to six "slaves" using  */
/*   the "Enhanced ShockBurst" data-pipe mechanism                            */
/*   Bits 0 .. 2 are used to encode the identity, bit 3 is used to detect if  */
/*   no code has been set where it should read '1' due to the internal pull-  */
/*   up' resistor                                                             */
/*   Reference : NORDIC SEMICONDUCTOR, "nRF24L01+ Single Chip 2.4GHz          */
/*               Transceiver", Product Specification" v1.0, September 2006,   */
/*               p39, "MultiCeiver(tm)"                                       */
/*                                                                            */
/******************************************************************************/

typedef enum apvArduinoSystemIdentifier_tTag
  {
  APV_ARDUINO_SYSTEM_IDENTITY_NONE     = 0,  
  APV_ARDUINO_SYSTEM_IDENTITY_MASTER,  // master == code '1' 
  APV_ARDUINO_SYSTEM_IDENTITY_SLAVE_1,
  APV_ARDUINO_SYSTEM_IDENTITY_SLAVE_2,
  APV_ARDUINO_SYSTEM_IDENTITY_SLAVE_3,
  APV_ARDUINO_SYSTEM_IDENTITY_SLAVE_4,
  APV_ARDUINO_SYSTEM_IDENTITY_SLAVE_5,
  APV_ARDUINO_SYSTEM_IDENTITY_SLAVE_6,
  APV_ARDUINO_SYSTEM_IDENTITY_NONE_8,
  APV_ARDUINO_SYSTEM_IDENTITY_NONE_9,
  APV_ARDUINO_SYSTEM_IDENTITY_NONE_10,
  APV_ARDUINO_SYSTEM_IDENTITY_NONE_11,
  APV_ARDUINO_SYSTEM_IDENTITY_NONE_12,
  APV_ARDUINO_SYSTEM_IDENTITY_NONE_13,
  APV_ARDUINO_SYSTEM_IDENTITY_NONE_14,
  APV_ARDUINO_SYSTEM_IDENTITY_NONE_15,
  APV_ARDUINO_SYSTEM_IDENTITIES  
  } apvArduinoSystemIdentifier_t;

/******************************************************************************/
/* Arduino I/O Pin Allocation Mapping :                                       */
/******************************************************************************/
// Arduino maps out the Atmel SAM3XA/E PIO Controllers A - D
typedef enum apvArduinoPioSet_tTag
  {
  APV_ARDUINO_PERIPHERAL_SET_A = 0,
  APV_ARDUINO_PERIPHERAL_SET_B,
  APV_ARDUINO_PERIPHERAL_SET_C,
  APV_ARDUINO_PERIPHERAL_SET_D,
  APV_ARDUINO_PERIPHERAL_SETS
  } apvArduinoPioSet_t;

// Arduino Peripheral Controllers potentially map 32 bits/pins 0 { .. } 31
typedef enum apvArduinoPioBits_tTag
  {
  APV_ARDUINO_PERIPHERAL_BIT_00 = 0,
  APV_ARDUINO_PERIPHERAL_BIT_01,
  APV_ARDUINO_PERIPHERAL_BIT_02,
  APV_ARDUINO_PERIPHERAL_BIT_03,
  APV_ARDUINO_PERIPHERAL_BIT_04,
  APV_ARDUINO_PERIPHERAL_BIT_05,
  APV_ARDUINO_PERIPHERAL_BIT_06,
  APV_ARDUINO_PERIPHERAL_BIT_07,
  APV_ARDUINO_PERIPHERAL_BIT_08,
  APV_ARDUINO_PERIPHERAL_BIT_09,
  APV_ARDUINO_PERIPHERAL_BIT_10,
  APV_ARDUINO_PERIPHERAL_BIT_11,
  APV_ARDUINO_PERIPHERAL_BIT_12,
  APV_ARDUINO_PERIPHERAL_BIT_13,
  APV_ARDUINO_PERIPHERAL_BIT_14,
  APV_ARDUINO_PERIPHERAL_BIT_15,
  APV_ARDUINO_PERIPHERAL_BIT_16,
  APV_ARDUINO_PERIPHERAL_BIT_17,
  APV_ARDUINO_PERIPHERAL_BIT_18,
  APV_ARDUINO_PERIPHERAL_BIT_19,
  APV_ARDUINO_PERIPHERAL_BIT_20,
  APV_ARDUINO_PERIPHERAL_BIT_21,
  APV_ARDUINO_PERIPHERAL_BIT_22,
  APV_ARDUINO_PERIPHERAL_BIT_23,
  APV_ARDUINO_PERIPHERAL_BIT_24,
  APV_ARDUINO_PERIPHERAL_BIT_25,
  APV_ARDUINO_PERIPHERAL_BIT_26,
  APV_ARDUINO_PERIPHERAL_BIT_27,
  APV_ARDUINO_PERIPHERAL_BIT_28,
  APV_ARDUINO_PERIPHERAL_BIT_29,
  APV_ARDUINO_PERIPHERAL_BIT_30,
  APV_ARDUINO_PERIPHERAL_BIT_31,
  APV_ARDUINO_PERIPHERAL_BITS
  } apvArduinoPioBits_t;

// Arduino bits can be digital, mapped to a peripheral or not available
typedef enum apvArduinoPioBitsUsage_tTag
  {
  APV_ARDUINO_PERIPHERAL_USAGE_DIGITAL = 0,
  APV_ARDUINO_PERIPHERAL_USAGE_PERIPHERAL,
  APV_ARDUINO_PERIPHERAL_USAGE_NO_ACCESS,
  APV_ARDUINO_PERIPHERAL_USAGES
  } apvArduinoPioBitsUsage_t;

// Arduino Bit Allocations' Descriptor
typedef struct apvArduinoPeripheralBitAllocation_tTag
  {
  apvArduinoPioSet_t       arduinoPioSet;
  apvArduinoPioBits_t      arduinoPioBits;
  apvArduinoPioBitsUsage_t arduinoPioBitsUsage;
  } apvArduinoPeripheralBitAllocation_t;

/******************************************************************************/

typedef enum apvPeripheralLineGroup_tTag
  {
  APV_PERIPHERAL_LINE_GROUP_A = 0,
  APV_PERIPHERAL_LINE_GROUP_B,
  APV_PERIPHERAL_LINE_GROUP_C,
  APV_PERIPHERAL_LINE_GROUP_D,
  APV_PERIPHERAL_LINE_GROUPS
  } apvPeripheralLineGroup_t;

typedef enum apvPeripheralId_tTag
  {
  APV_PERIPHERAL_ID_SUPC   = ID_SUPC,
  APV_PERIPHERAL_ID_RSTC   = ID_RSTC,
  APV_PERIPHERAL_ID_RTC    = ID_RTC,
  APV_PERIPHERAL_ID_RTT    = ID_RTT,
  APV_PERIPHERAL_ID_WDT    = ID_WDT,
  APV_PERIPHERAL_ID_PMC    = ID_PMC,
  APV_PERIPHERAL_ID_EFC0   = ID_EFC0,
  APV_PERIPHERAL_ID_EFC1   = ID_EFC1,
  APV_PERIPHERAL_ID_UART   = ID_UART,
  APV_PERIPHERAL_ID_SMC    = ID_SMC,
  APV_PERIPHERAL_ID_PIOA   = ID_PIOA,
  APV_PERIPHERAL_ID_PIOB   = ID_PIOB,
  APV_PERIPHERAL_ID_PIOC   = ID_PIOC,
  APV_PERIPHERAL_ID_PIOD   = ID_PIOD,
  APV_PERIPHERAL_ID_USART0 = ID_USART0,
  APV_PERIPHERAL_ID_USART1 = ID_USART1,
  APV_PERIPHERAL_ID_USART2 = ID_USART2,
  APV_PERIPHERAL_ID_USART3 = ID_USART3,
  APV_PERIPHERAL_ID_HSMCI  = ID_HSMCI,
  APV_PERIPHERAL_ID_TWI0   = ID_TWI0,
  APV_PERIPHERAL_ID_TWI1   = ID_TWI1,
  APV_PERIPHERAL_ID_SPI0   = ID_SPI0,
  APV_PERIPHERAL_ID_SSC    = ID_SSC,
  APV_PERIPHERAL_ID_TC0    = ID_TC0,
  APV_PERIPHERAL_ID_TC1    = ID_TC1,
  APV_PERIPHERAL_ID_TC2    = ID_TC2,
  APV_PERIPHERAL_ID_TC3    = ID_TC3,
  APV_PERIPHERAL_ID_TC4    = ID_TC4,
  APV_PERIPHERAL_ID_TC5    = ID_TC5,
  APV_PERIPHERAL_ID_TC6    = ID_TC6,
  APV_PERIPHERAL_ID_TC7    = ID_TC7,
  APV_PERIPHERAL_ID_TC8    = ID_TC8,
  APV_PERIPHERAL_ID_PWM    = ID_PWM,
  APV_PERIPHERAL_ID_ADC    = ID_ADC,
  APV_PERIPHERAL_ID_DACC   = ID_DACC,
  APV_PERIPHERAL_ID_DMAC   = ID_DMAC,
  APV_PERIPHERAL_ID_UOTGHS = ID_UOTGHS,
  APV_PERIPHERAL_ID_TRNG   = ID_TRNG,
  APV_PERIPHERAL_ID_EMAC   = ID_EMAC,
  APV_PERIPHERAL_ID_CAN0   = ID_CAN0,
  APV_PERIPHERAL_ID_CAN1   = ID_CAN1,
  APV_PERIPHERAL_IDS
  } apvPeripheralId_t;

/******************************************************************************/
/* Digital I/O Pin Allocation :                                               */
/******************************************************************************/

typedef enum apvDigitalId_tTag
  {
  APV_DIGITAL_ID_IDENTITY_SELECT           = 0,
  APV_DIGITAL_ID_LSM9DS_CHIP_ENABLE_SELECT,
  APV_DIGITAL_RFM69HCW_CHIP_ENABLE_SELECT,
  APV_DIGITAL_NRF24L01_TX_CHIP_ENABLE_SELECT,
  APV_DIGITAL_NRF24L01_RX_CHIP_ENABLE_SELECT,
  APV_DIGITAL_HEARTBEAT_LED_SELECT,
  APV_DIGITAL_RFM69HCW_RESET_SELECT,
  APV_DIGITAL_IDS
  } apvDigitalId_t;

/******************************************************************************/

typedef enum apvPointerConversionWords_tTag
  {
  APV_POINTER_CONVERSION_WORD_LOW = 0,
  APV_POINTER_CONVERSION_WORD_HIGH,
  APV_POINTER_CONVERSION_WORDS
  } apvPointerConversionWords_t;

typedef union apvPointerConversion_tTag
  {
  uint32_t apvPointerConversionWords[APV_POINTER_CONVERSION_WORDS]; // map the low-word to [0], high-word to [1]
  uint32_t apvPointerConversion;
  } apvPointerConversion_t;

typedef enum apvInterruptCounters_tTag
  {
  APV_TRANSMIT_INTERRUPT_COUNTER = 0,
  APV_RECEIVE_INTERRUPT_COUNTER,
  APV_INTERRUPT_COUNTERS
  } apvInterruptCounters_t;

typedef enum apvResourceId_tTag
  {
  APV_RESOURCE_ID_STROBE_0 = APV_RESOURCE_ID_BASE,
  APV_RESOURCE_IDS
  } apvResourceId_t;

typedef struct binary2DecimalConversion16_tTag
  {
  uint16_t binary2DecimalConversion16[APV_16_BIT_BINARY_BIT_POSITION_DECIMAL_WEIGHTS];
  uint8_t  binaryWeightSignificantColumns;
  } binary2DecimalConversion16_t;

/******************************************************************************/
/* Lightweight FIFO Definitions :                                             */
/******************************************************************************/

typedef struct apvFIFOEntry_tTag
  {
  uint8_t  apvFIFOEntryKey;
  uint32_t apvFIFOEntryData;
  } apvFIFOEntry_t;

typedef struct apvFIFOStructure_tTag
  {
  apvFIFOEntry_t *apvFIFO;
  uint8_t         apvFIFOHeadPointer;
  uint8_t         apvFIFODepth;
  uint8_t         apvFIFOTailPointer;
  uint8_t         apvFIFOFillLevel;
  } apvFIFOStructure_t;

// The legal depth choices are just "round" binary values
typedef enum apvFIFOEntryDepths_tTag
  {
  APV_FIFO_ENTRY_DEPTH_8       =  8,
  APV_FIFO_MINIMUM_ENTRY_DEPTH =  APV_FIFO_ENTRY_DEPTH_8,
  APV_FIFO_ENTRY_DEPTH_16      = (APV_FIFO_ENTRY_DEPTH_8  << 1),
  APV_FIFO_ENTRY_DEPTH_32      = (APV_FIFO_ENTRY_DEPTH_16 << 1),
  APV_FIFO_ENTRY_DEPTH_64      = (APV_FIFO_ENTRY_DEPTH_32 << 1),
  APV_FIFO_MAXIMUM_ENTRY_DEPTH =  APV_FIFO_ENTRY_DEPTH_64,
  APV_FIFO_ENTRY_DEPTHS        =  4
  } apvFIFOEntryDepths_t;
  
/******************************************************************************/
/* Typedef Equivalences :                                                     */
/******************************************************************************/
// Equating Visual C++'s poor treatment of "booleans'
typedef bool boolean_t;

/******************************************************************************/
/* Global Variables Declarations :                                            */
/******************************************************************************/

extern int16_t                apvInterruptNestingCount;
extern APV_GLOBAL_ERROR_FLAG  apvGlobalErrorFlags;
extern uint32_t               apvInterruptCounters[APV_INTERRUPT_COUNTERS];

extern Pio                     ApvPeripheralLineControlBlock[APV_PERIPHERAL_LINE_GROUPS];   // shadow PIO control blocks
extern Pio                    *ApvPeripheralLineControlBlock_p[APV_PERIPHERAL_LINE_GROUPS]; // physical block addresses

extern const apvArduinoPeripheralBitAllocation_t apvPeripheralAllocationMap[APV_ARDUINO_PERIPHERAL_SETS][APV_ARDUINO_PERIPHERAL_BITS];
extern       uint32_t                            apvPeripheralUsageMap[APV_ARDUINO_PERIPHERAL_SETS];

/******************************************************************************/
/* Function Declarations :                                                    */
/******************************************************************************/

extern APV_ERROR_CODE apvSwitchResourceLines(apvResourceId_t resourceLineId,
                                             bool            resourceLineSwitch);
extern APV_ERROR_CODE apvDriveResourceIoLine(apvResourceId_t          resourceLineId,
                                              apvPeripheralLineGroup_t resourceLineGroup,
                                              uint32_t                 resourceLine,
                                               bool                     resourceLineSense);
extern APV_ERROR_CODE apvReadSystemIdentity(apvArduinoSystemIdentifier_t *apvIdentity);
extern void           APV_CRITICAL_REGION_ENTRY(void);
extern void           APV_CRITICAL_REGION_EXIT(void);


extern APV_ERROR_CODE apvUnLoadFIFO(apvFIFOStructure_t *fifo,
                                    apvFIFOEntry_t     *fifoEntry,
                                    bool                interruptControl);
extern APV_ERROR_CODE apvLoadFIFO(apvFIFOStructure_t *fifo,
                                  apvFIFOEntry_t     *fifoEntry,
                                  bool                interruptControl);
extern APV_ERROR_CODE apvCreateFIFO(apvFIFOStructure_t *fifo,
                                    APV_TYPE_UINT8      fifoDepth,
                                    apvFIFOEntry_t     *fifoEntries);
extern APV_ERROR_CODE apvResetFIFO(apvFIFOStructure_t *fifo,
                                   bool                interruptControl);
extern APV_ERROR_CODE apvGetFIFOFillLevel(apvFIFOStructure_t *fifo,
                                   uint8_t            *fillLevel,
                                   bool                interruptControl);

extern bool           apvSignedConvert16HexToDecimal(bool           signMode,
                                    uint16_t       hex16Pattern,
                                    unsigned char *decimal16Pattern);

extern void           apvRingBufferPrint(apvRingBuffer_t *ringBuffer);

/******************************************************************************/

#endif

/******************************************************************************/
/* (C) PulsingCoreSoftware Limited 2018 (C)                                   */
/******************************************************************************/
