/******************************************************************************/
/* (C) PulsingCoreSoftware Limited 2018 (C)                                   */
/******************************************************************************/
/*                                                                            */
/* ApvUtilities.c                                                             */
/* 22.03.18                                                                   */
/* Paul O'Brien                                                               */
/*                                                                            */
/* - a set of functions to add some helpers                                   */
/*                                                                            */
/******************************************************************************/
/* Include Files :                                                            */
/******************************************************************************/

#include <stdio.h>
#include <stdint.h>
#ifdef WIN32
#define libarm_disable_irq() //printf("\n DISABLE INTERRUPTS : NESTING LEVEL %05d", apvInterruptNestingCount);
#define libarm_enable_irq()  //printf("\n ENABLE INTERRUPTS  : NESTING LEVEL %05d", apvInterruptNestingCount);
#else
#include <sam3x8e.h>
#include <core_cm3.h>
#endif
#include "ApvUtilities.h"

/******************************************************************************/
/* Global Variables :                                                         */
/******************************************************************************/

int16_t                apvInterruptNestingCount                     = APV_INITIAL_INTERRUPT_NESTING_COUNT;
APV_GLOBAL_ERROR_FLAG  apvGlobalErrorFlags                          = APV_GLOBAL_ERROR_FLAG_NONE;
uint32_t               apvInterruptCounters[APV_INTERRUPT_COUNTERS] = { 0x00000000, 0x00000000 };

// This definition avoids extravagant casting effort from the Atmel constants
Pio  ApvPeripheralLineControlBlock[APV_PERIPHERAL_LINE_GROUPS];    // shadow PIO control blocks
Pio *ApvPeripheralLineControlBlock_p[APV_PERIPHERAL_LINE_GROUPS] = // physical block addresses
  {
  APV_PIO_BLOCK_A,
  APV_PIO_BLOCK_B,
  APV_PIO_BLOCK_C,
  APV_PIO_BLOCK_D
  };

/******************************************************************************/
/* Peripheral I/O Controller Register/Bit Mapping :                           */
/******************************************************************************/

const apvArduinoPeripheralBitAllocation_t apvPeripheralAllocationMap[APV_ARDUINO_PERIPHERAL_SETS][APV_ARDUINO_PERIPHERAL_BITS];
      uint32_t                            apvPeripheralUsageMap[APV_ARDUINO_PERIPHERAL_SETS];

/******************************************************************************/
/* 16-bit hexadecimal to 6-digit decimal conersion :                          */
/******************************************************************************/

static const binary2DecimalConversion16_t binary2DecimalConversion16[APV_16_BIT_BINARY_BIT_POSITIONS] = 
  {
    {
      { 0, 0, 0, 0, 1 }, // 2 ^  0
      1
    },
    {
      { 0, 0, 0, 0, 2 }, // 2 ^  1
      1
    },
    {
      { 0, 0, 0, 0, 4 }, // 2 ^  2
      1
    },
    {
      { 0, 0, 0, 0, 8 }, // 2 ^  3
      1
    },
    {
      { 0, 0, 0, 1, 6 }, // 2 ^  4
      2
    },
    {
      { 0, 0, 0, 3, 2 }, // 2 ^  5
      2
    },
    {
      { 0, 0, 0, 6, 4 }, // 2 ^  6
      2
    },
    {
      { 0, 0, 1, 2, 8 }, // 2 ^  7
      3
    },
    {
      { 0, 0, 2, 5, 6 }, // 2 ^  8
      3
    },
    {
      { 0, 0, 5, 1, 2 }, // 2 ^  9
      3
    },
    {
      { 0, 1, 0, 2, 4 }, // 2 ^ 10
      4
    },
    {
      { 0, 2, 0, 4, 8 }, // 2 ^ 11
      4
    },
    {
      { 0, 4, 0, 9, 6 }, // 2 ^ 12
      4
    },
    {
      { 0, 8, 1, 9, 2 }, // 2 ^ 13
      4
    },
    {
      { 1, 6, 3, 8, 4 }, // 2 ^ 14
      5
    },
    {
      { 3, 2, 7, 6, 8 },  // 2 ^ 15
      5
    }
  };

/******************************************************************************/
/* Function Declarations :                                                    */
/******************************************************************************/
/* apvSwitchPeripheralLines() :                                               */
/*  --> resourceLineId       : resource ID                                    */
/*  --> resourceLineSwitch   : [ false == resource lines disabled |           */
/*                               true  == resource lines enabled ]            */
/*                                                                            */
/*  <-- resourceControlError : error codes                                    */
/*                                                                            */
/*  - enable/disable the PIO lines associated with a resource                 */
/*                                                                            */
/* Reference : "Atmel-11057C-ATARM-SAM3X-SAM3A-Datasheet_23-Mar-15", p40,     */
/*             p643 - 4                                                       */
/*                                                                            */
/******************************************************************************/

APV_ERROR_CODE apvSwitchResourceLines(apvResourceId_t resourceLineId,
                                      bool            resourceLineSwitch)
  {
/******************************************************************************/

  APV_ERROR_CODE resourceControlError = APV_ERROR_CODE_NONE;
  uint32_t       resourceStatus       = 0;

/******************************************************************************/

  if ((resourceLineId < APV_RESOURCE_ID_BASE) || (resourceLineId >= APV_RESOURCE_IDS))
    {
    resourceControlError = APV_ERROR_CODE_PARAMETER_OUT_OF_RANGE;
    }
  else
    {
    if (resourceLineSwitch == true)
      {
      switch(resourceLineId)
        {
        /******************************************************************************/
        /* Project-specific I/O allocations :                                         */
        /******************************************************************************/

        case APV_RESOURCE_ID_STROBE_0 : // The first debug strobe is at (MCU PIN34) PIO C PIN 3 (PC3) : enable the line
                                        ApvPeripheralLineControlBlock_p[APV_PERIPHERAL_LINE_GROUP_C]->PIO_PER  = PIO_PER_P3;

                                        // Enable the pull-ups on Tx and RX
                                        resourceStatus = ApvPeripheralLineControlBlock_p[APV_PERIPHERAL_LINE_GROUP_C]->PIO_PUSR;
                                        resourceStatus = resourceStatus | PIO_PUER_P3;
                                        ApvPeripheralLineControlBlock_p[APV_PERIPHERAL_LINE_GROUP_C]->PIO_PUER = resourceStatus;

                                        //ApvPeripheralLineControlBlock_p[APV_PERIPHERAL_LINE_GROUP_C]->PIO_PUER = PIO_PUER_P3;

                                        // Enable the line output
                                        ApvPeripheralLineControlBlock_p[APV_PERIPHERAL_LINE_GROUP_C]->PIO_OER  = PIO_OER_P3;

                                        // Start with the line low
                                        ApvPeripheralLineControlBlock_p[APV_PERIPHERAL_LINE_GROUP_C]->PIO_CODR = PIO_CODR_P3;

                                        // Check the allocation status - parallel I/O enabled, peripheral disabled
                                        if ((ApvPeripheralLineControlBlock_p[APV_PERIPHERAL_LINE_GROUP_C]->PIO_PSR & PIO_PDR_P3) != PIO_PDR_P3)
                                          {
                                          resourceControlError = APV_ERROR_CODE_CONFIGURATION_ERROR;
                                          }
                                        else
                                          {
                                          if ((ApvPeripheralLineControlBlock_p[APV_PERIPHERAL_LINE_GROUP_C]->PIO_OSR & PIO_PDR_P3) != PIO_PDR_P3)
                                            {
                                            resourceControlError = APV_ERROR_CODE_CONFIGURATION_ERROR;
                                            }
                                          else
                                            {
                                            if ((ApvPeripheralLineControlBlock_p[APV_PERIPHERAL_LINE_GROUP_C]->PIO_PUSR & PIO_PDR_P3) == PIO_PDR_P3)
                                              {
                                              resourceControlError = APV_ERROR_CODE_CONFIGURATION_ERROR;
                                              }
                                            }
                                          }

                                        break;

        default                      :
                                        break;
        }
      }
    else
      {
      switch(resourceLineId)
        {
        /******************************************************************************/
        /* Project-specific I/O allocations :                                         */
        /******************************************************************************/

        case APV_RESOURCE_ID_STROBE_0 : // The first debug strobe is at (MCU PIN34) PIO C PIN 3 (PC3)
                                        ApvPeripheralLineControlBlock_p[APV_PERIPHERAL_LINE_GROUP_C]->PIO_PDR = PIO_PDR_P3;

                                        // Disable the line output
                                        ApvPeripheralLineControlBlock_p[APV_PERIPHERAL_LINE_GROUP_C]->PIO_ODR = PIO_ODR_P3;

                                        // Check the allocation status
                                        if ((ApvPeripheralLineControlBlock_p[APV_PERIPHERAL_LINE_GROUP_C]->PIO_PSR & PIO_PSR_P3) != PIO_PER_P3)
                                          {
                                          resourceControlError = APV_ERROR_CODE_CONFIGURATION_ERROR;
                                          }

                                        break;

         default                      :
                                        break;
        }
      }
    }

/******************************************************************************/

  return(resourceControlError);

/******************************************************************************/
  } /* end of apvSwitchResourceLines                                          */

/******************************************************************************/
/* apvDriveResourceIoLine() :                                                 */
/*                                                                            */
/*  --> resourceLineId    : resource id - not really used here, just a check- */
/*                          reference                                         */
/*  --> resourceLineGroup : [ APV_PERIPHERAL_LINE_GROUP_A = 0 |               */
/*                            APV_PERIPHERAL_LINE_GROUP_B |                   */
/*                            APV_PERIPHERAL_LINE_GROUP_C |                   */
/*                            APV_PERIPHERAL_LINE_GROUP_D ]                   */
/*  --> resourceLine      : [ PIO_CODR_Pxy | PIO_SODR_Pxy ] - not checked!    */
/*  --> resourceLineSense : [ false == CODR | true == SODR ]                  */
/*                                                                            */
/*  <-- resourceControlError : error codes                                    */
/*                                                                            */
/* Reference : "Atmel-11057C-ATARM-SAM3X-SAM3A-Datasheet_23-Mar-15", p40,     */
/*             p643 - 4                                                       */
/*                                                                            */
/******************************************************************************/

APV_ERROR_CODE apvDriveResourceIoLine(apvResourceId_t          resourceLineId,
                                      apvPeripheralLineGroup_t resourceLineGroup,
                                      uint32_t                 resourceLine,
                                      bool                     resourceLineSense)
  {
/******************************************************************************/

  APV_ERROR_CODE resourceControlError = APV_ERROR_CODE_NONE;

/******************************************************************************/

  if ((resourceLineId    <  APV_RESOURCE_ID_BASE)       || (resourceLineId >= APV_RESOURCE_IDS) ||
      (resourceLineGroup >= APV_PERIPHERAL_LINE_GROUPS) || (resourceLine   >  ((uint32_t)PIO_CODR_P31)))
    {
    resourceControlError = APV_ERROR_CODE_PARAMETER_OUT_OF_RANGE;
    }
  else
    {
    if (resourceLineSense == true)
      {
      ApvPeripheralLineControlBlock_p[resourceLineGroup]->PIO_SODR = resourceLine;
      }
    else
      {
      ApvPeripheralLineControlBlock_p[resourceLineGroup]->PIO_CODR = resourceLine;
      }
    }

/******************************************************************************/

  return(resourceControlError);

/******************************************************************************/
  } /* end of apvDriveResourceIoLine                                          */

/******************************************************************************/
/* apvReadSystemIdentity() :                                                  */
/*  <-- apvIdentity   : pointer to the identity code of this system           */
/*  <-- identityError : error codes                                           */
/*                                                                            */
/* - read the identity code of this system                                    */
/*                                                                            */
/******************************************************************************/

APV_ERROR_CODE apvReadSystemIdentity(apvArduinoSystemIdentifier_t *apvIdentity)
  {
/******************************************************************************/

  apvArduinoSystemIdentifier_t systemIdentifier     = APV_ARDUINO_SYSTEM_IDENTITY_NONE;
  uint32_t                     systemIdentifierBits = 0;

  APV_ERROR_CODE               identityError    = APV_ERROR_CODE_NONE;

/******************************************************************************/

  if (apvIdentity != NULL)
    {
    // Are the identity bits switched on ?
    if (apvPeripheralUsageMap[APV_ARDUINO_PERIPHERAL_SET_C] & APV_ARDUINO_SYSTEM_IDENTITY_MASK)
      {
      // Read the identity bits
      systemIdentifierBits = ApvPeripheralLineControlBlock_p[APV_PERIPHERAL_LINE_GROUP_C]->PIO_PDSR & APV_ARDUINO_SYSTEM_IDENTITY_MASK;

      systemIdentifier     = (apvArduinoSystemIdentifier_t)(systemIdentifierBits >> APV_ARDUINO_PERIPHERAL_BIT_12);

      if ((systemIdentifier == APV_ARDUINO_SYSTEM_IDENTITY_NONE) || (systemIdentifier >= APV_ARDUINO_SYSTEM_IDENTITY_NONE_8))
        {
        identityError = APV_ERROR_CODE_PARAMETER_OUT_OF_RANGE;
        }
      else
        {
        *apvIdentity = systemIdentifier;
        }
      }
    else
      {
      identityError = APV_ERROR_CODE_CONFIGURATION_ERROR;
      }
    }
  else
    {
    identityError = APV_ERROR_CODE_NULL_PARAMETER;
    }
  
/******************************************************************************/

  return(identityError);

/******************************************************************************/
  } /* end of apvReadSystemIdentity                                           */

/******************************************************************************/
/* APV_CRITICAL_REGION_ENTRY() :                                              */
/*  - lock a region of code where one or more writers and readers have access */
/*    to the same memory region. Maintains a function-nesting count to        */
/*    determine when to safely release the lock                               */
/*                                                                            */
/******************************************************************************/

void APV_CRITICAL_REGION_ENTRY(void)
  {
/******************************************************************************/

  if (apvInterruptNestingCount < APV_INITIAL_INTERRUPT_NESTING_COUNT)
    {
    apvGlobalErrorFlags = (APV_GLOBAL_ERROR_FLAG)(apvGlobalErrorFlags | APV_GLOBAL_ERROR_FLAG_FUNCTION_INTERRUPT_NESTING);
    }
  else
    {
    __disable_irq(); // the locking mechanism

    apvInterruptNestingCount = apvInterruptNestingCount + 1;
    }

/******************************************************************************/
  } /* end of APV_CRITICAL_REGION_ENTRY                                       */

/******************************************************************************/
/* APV_CRITICAL_REGION_EXIT() :                                               */
/*  - lock a region of code where one or more writers and readers have access */
/*    to the same memory region. Maintains a function-nesting count to        */
/*    determine when to safely release the lock                               */
/*                                                                            */
/******************************************************************************/

void APV_CRITICAL_REGION_EXIT(void)
  {
/******************************************************************************/

  if (apvInterruptNestingCount > APV_INITIAL_INTERRUPT_NESTING_COUNT)
    {
    apvInterruptNestingCount = apvInterruptNestingCount - 1;

    __enable_irq(); // the unlocking mechanism
    }
  else
    {
    apvGlobalErrorFlags = (APV_GLOBAL_ERROR_FLAG)(apvGlobalErrorFlags | APV_GLOBAL_ERROR_FLAG_FUNCTION_INTERRUPT_NESTING);
    }

/******************************************************************************/
  } /* end of APV_CRITICAL_REGION_EXIT                                        */

/******************************************************************************/
/* FIFO Definitions :                                                         */
/* - implements a simple lightweight FIFO mechanism. The key difference with  */
/*   the full-blown ring-buffers is the head pointer ALWAYS advances when new */
/*   values arrive. If the FIFO is full the tail pointer just follows the     */
/*   head overwriting the oldest data. Secondly FIFO entry are inherently     */
/*   two-dimensional - each entry is the data and a possible key. Also, FIFOs */
/*   are intended to be small! "Depth" is forced to a "round" binary value    */
/*                                                                            */
/******************************************************************************/
/* apvCreateFIFO() :                                                          */
/*  --> fifo        : the FIFO                                                */
/*  --< fifoDepth   : the number of entries for the FIFO - will be forced to  */
/*                    "round" binary number                                   */
/*  --> fifoEntries : array of data and keys                                  */
/*  <-- fifoError   : error codes                                             */
/*                                                                            */
/******************************************************************************/

APV_ERROR_CODE apvCreateFIFO(apvFIFOStructure_t *fifo,
                             APV_TYPE_UINT8      fifoDepth,
                             apvFIFOEntry_t     *fifoEntries)
  {
/******************************************************************************/

  APV_ERROR_CODE fifoError        = APV_ERROR_CODE_NONE;

  APV_TYPE_UINT8 minimumFifoDepth = APV_FIFO_ENTRY_DEPTH_8,  // bracket the possible "round" binary values 
                 maximumFifoDepth = APV_FIFO_ENTRY_DEPTH_16; // the FIFO depth can be set to
  
/******************************************************************************/

  if ((fifo == NULL) || (fifoEntries == NULL))
    {
    fifoError = APV_ERROR_CODE_NULL_PARAMETER;
    }
  else
    {
    fifo->apvFIFO            = fifoEntries;

    fifo->apvFIFOHeadPointer = 0;
    fifo->apvFIFOTailPointer = 0;
    fifo->apvFIFOFillLevel   = 0;

    if (fifoDepth < APV_FIFO_MINIMUM_ENTRY_DEPTH)
      {
      fifo->apvFIFODepth = APV_FIFO_MINIMUM_ENTRY_DEPTH;
      }
    else
      {
      if (fifoDepth >= APV_FIFO_MAXIMUM_ENTRY_DEPTH)
        {
        fifo->apvFIFODepth = APV_FIFO_MAXIMUM_ENTRY_DEPTH;
        }
      else
        {
        // Force the FIFO depth to be one of the "round" binary choices
        while (maximumFifoDepth <= APV_FIFO_MAXIMUM_ENTRY_DEPTH)
          {
          if ((fifoDepth >= minimumFifoDepth) && (fifoDepth < maximumFifoDepth))
            {
            fifo->apvFIFODepth = minimumFifoDepth;
            break;
            }
          else
            {
            minimumFifoDepth = minimumFifoDepth << 1;
            maximumFifoDepth = maximumFifoDepth << 1;
            }
          }
        }
      }
    }

/******************************************************************************/

  return(fifoError);

/******************************************************************************/
  } /* end of apvCreateFIFO                                                   */

/******************************************************************************/
/* apvResetFIFO() :                                                           */
/*  --> fifo             : the FIFO                                           */
/*  --> interruptControl : allows the function to be called in foreground or  */
/*                         background                                         */
/*  <-- fifoError        : error codes                                        */
/*                                                                            */
/* - reset the "light" FIFO control parameters to zero                        */
/*                                                                            */
/******************************************************************************/

APV_ERROR_CODE apvResetFIFO(apvFIFOStructure_t *fifo,
                            bool                interruptControl)
  {
/******************************************************************************/

  APV_ERROR_CODE fifoError = APV_ERROR_CODE_NONE;

/******************************************************************************/

  if (fifo == NULL)
    {
    fifoError = APV_ERROR_CODE_NULL_PARAMETER;
    }
  else
    {
    if (interruptControl == true)
      {
      APV_CRITICAL_REGION_ENTRY();
      }
         
    fifo->apvFIFOHeadPointer = 0;
    fifo->apvFIFOTailPointer = 0;
    fifo->apvFIFOFillLevel   = 0;

    if (interruptControl == true)
      {
      APV_CRITICAL_REGION_EXIT();
      }
    }

/******************************************************************************/

  return(fifoError);

/******************************************************************************/
  } /* end of apvResetFIFO                                                    */

/******************************************************************************/
/* apvLoadFIFO() :                                                            */
/*  --> fifo             : the FIFO                                           */
/*  --> fifoEntry        : new data and key                                   */
/*  --> interruptControl : allows the function to be called in foreground or  */
/*                         background                                         */
/*  <-- fifoError        : error codes                                        */
/*                                                                            */
/******************************************************************************/

APV_ERROR_CODE apvLoadFIFO(apvFIFOStructure_t *fifo,
                           apvFIFOEntry_t     *fifoEntry,
                           bool                interruptControl)
  {
/******************************************************************************/

  APV_ERROR_CODE fifoError = APV_ERROR_CODE_NONE;

/******************************************************************************/

  if ((fifo == NULL) || (fifoEntry == NULL))
    {
    fifoError = APV_ERROR_CODE_NULL_PARAMETER;
    }
  else
    {
    if (interruptControl == true)
      {
      APV_CRITICAL_REGION_ENTRY();
      }

    // Load the new entry - if the buffer is full the head and tail move 
    // together. Wrapping is just modulus "...MAXIMUM_DEPTH"
    fifo->apvFIFO[fifo->apvFIFOHeadPointer] = *fifoEntry;

    fifo->apvFIFOHeadPointer = (fifo->apvFIFOHeadPointer + 1) % fifo->apvFIFODepth;

    if (fifo->apvFIFOFillLevel == fifo->apvFIFODepth)
      {
      fifo->apvFIFOTailPointer = (fifo->apvFIFOTailPointer + 1) % fifo->apvFIFODepth;
      }
    else
      {
      fifo->apvFIFOFillLevel = fifo->apvFIFOFillLevel + 1;
      }

    if (interruptControl == true)
      {
      APV_CRITICAL_REGION_EXIT();
      }
    }

/******************************************************************************/

  return(fifoError);

/******************************************************************************/
  } /* end of apvLoadFIFO                                                     */

/******************************************************************************/
/* apvUnLoadFIFO() :                                                          */
/*  --> fifo             : the FIFO                                           */
/*  --> fifoEntry        : new data and key                                   */
/*  --> interruptControl : allows the function to be called in foreground or  */
/*                         background                                         */
/*  <-- fifoError        : error codes                                        */
/*                                                                            */
/******************************************************************************/

APV_ERROR_CODE apvUnLoadFIFO(apvFIFOStructure_t *fifo,
                             apvFIFOEntry_t     *fifoEntry,
                             bool                interruptControl)
  {
/******************************************************************************/

  APV_ERROR_CODE fifoError = APV_ERROR_CODE_NONE;

/******************************************************************************/

  if ((fifo == NULL) || (fifoEntry == NULL))
    {
    fifoError = APV_ERROR_CODE_NULL_PARAMETER;
    }
  else
    {
    if (interruptControl == true)
      {
      APV_CRITICAL_REGION_ENTRY();
      }

    if (fifo->apvFIFOFillLevel != 0)
      {
      *fifoEntry = fifo->apvFIFO[fifo->apvFIFOTailPointer];

      fifo->apvFIFOFillLevel   = fifo->apvFIFOFillLevel - 1;
      fifo->apvFIFOTailPointer = (fifo->apvFIFOTailPointer + 1) % fifo->apvFIFODepth;
      }

    if (interruptControl == true)
      {
      APV_CRITICAL_REGION_EXIT();
      }
    }

/******************************************************************************/

  return(fifoError);

/******************************************************************************/
  } /* end of apvUnLoadFIFO                                                   */

/******************************************************************************/
/* apvGetFIFOFillLevel() :                                                    */
/*  --> fifo             : the FIFO                                           */
/*  --> fillLevel        : number of filled entries in the FIFO buffer        */
/*  --> interruptControl : allows the function to be called in foreground or  */
/*                         background                                         */
/*  <-- fifoError        : error codes                                        */
/*                                                                            */
/******************************************************************************/

APV_ERROR_CODE apvGetFIFOFillLevel(apvFIFOStructure_t *fifo,
                                   uint8_t            *fillLevel,
                                   bool                interruptControl)
  {
/******************************************************************************/

  APV_ERROR_CODE fifoError = APV_ERROR_CODE_NONE;

/******************************************************************************/

  if ((fifo == NULL) || (fillLevel == NULL))
    {
    fifoError = APV_ERROR_CODE_NULL_PARAMETER;
    }
  else
    {
    if (interruptControl == true)
      {
      APV_CRITICAL_REGION_ENTRY();
      }

    *fillLevel = fifo->apvFIFOFillLevel;

    if (interruptControl == true)
      {
      APV_CRITICAL_REGION_EXIT();
      }
    }

/******************************************************************************/

  return(fifoError);

/******************************************************************************/
  } /* end of apvGetFIFOFillLevel                                             */

/******************************************************************************/
/* apvSignedConvert16HexToDecimal() :                                         */
/*  --> signMode         : [ false == unsigned | true == signed ]             */
/*  --> hex16Pattern     : 16-bit hexadeciaml bit-pattern                     */
/*  --> decimal16Pattern : 6-digit (5 + sign) 0 { decimal number } 65535      */
/*                                                                            */
/* - convert a signed- or unsigned 16-bit hex pattern to a printable decimal  */
/*   string. This is a table-driven conversion trying for maximum execution   */
/*   speed. The resulting number format is <sign> + <five-digit-decimal>. The */
/*   format is signed-positive i.e. positive numbers are "+xxxxx", negative   */
/*   numbers are "-xxxxx". As the hex-to-decimal conversion table is all      */
/*   positive, decimal entries with leading zeros do not require any work so  */
/*   on the average the formatter conversion time is reduced.                 */
/*                                                                            */
/******************************************************************************/

boolean_t apvSignedConvert16HexToDecimal(boolean_t      signMode,
                                         uint16_t       hex16Pattern,
                                         unsigned char *decimal16Pattern)
  {
/******************************************************************************/

  boolean_t      conversionSuccess                                   = TRUE;

  uint16_t      conversionBit                                        = (uint16_t)(0x0001),
                conversionDec                                        = 0,
                conversionWeight                                     = 0,
                conversionCarry                                      = 0;
  unsigned char decimalConversion[APV_16_BIT_BINARY_2_DECIMAL_WIDTH] = { 0, 0, 0, 0, 0, 0 };
  
/******************************************************************************/

  if (decimal16Pattern != NULL)
    {
    if (hex16Pattern != 0)
      {
      // Deal with the signed or unsigned-ness. Note that for a positive value with the 
      // sign character set converting "hexPattern" will always return -32768 {} 32767
      if (signMode == TRUE)
        { // If the bit-pattern has the sign-bit set convert to positive and fill-in the sign character
        if (hex16Pattern & APV_16_BIT_BINARY_BIT_SIGN_POSITION)
          {
          hex16Pattern = (uint16_t)(-((int16_t)hex16Pattern));
  
          decimal16Pattern[APV_16_BIT_BINARY_BIT_POSITION_SIGN] = APV_16_BIT_BINARY_BIT_NEGATIVE_SIGN;
          }
        else
          {
          decimal16Pattern[APV_16_BIT_BINARY_BIT_POSITION_SIGN] = APV_16_BIT_BINARY_BIT_POSITIVE_SIGN;
          }
        }
      else
        {
        decimal16Pattern[APV_16_BIT_BINARY_BIT_POSITION_SIGN] = APV_16_BIT_BINARY_BIT_POSITIVE_SIGN;
        }
  
      // Examine each bit position until the bit falls off the end...
      while (conversionBit != 0)
        {
        if (conversionBit & hex16Pattern)
          {
          for (conversionDec = APV_16_BIT_BINARY_BIT_POSITION_DECIMAL_WEIGHTS; conversionDec > (APV_16_BIT_BINARY_BIT_POSITION_DECIMAL_WEIGHTS - binary2DecimalConversion16[conversionWeight].binaryWeightSignificantColumns); conversionDec--)
            {
            decimalConversion[conversionDec] = decimalConversion[conversionDec] + binary2DecimalConversion16[conversionWeight].binary2DecimalConversion16[(conversionDec - 1)] + conversionCarry;
  
            if (decimalConversion[conversionDec] >= APV_16_BIT_2_DECIMAL_MODULUS)
              {
              decimalConversion[conversionDec] = decimalConversion[conversionDec] - APV_16_BIT_2_DECIMAL_MODULUS;
              conversionCarry                  = 1;
              }
            else
              {
              conversionCarry                  = 0;
              }
            }
          }
  
        /******************************************************************************/
        /* Eliminating the higher-weight columns that are zero means a trailing carry */
        /* may need to be handled before moving on to the next bit-weight decimal-    */
        /* equivalent                                                                 */
        /******************************************************************************/

        if (conversionCarry == 1)
          {
          decimalConversion[conversionDec] = 1;

          conversionCarry = 0;
          }

        /******************************************************************************/

        conversionBit    = conversionBit    << 1;
        conversionWeight = conversionWeight  + 1;
        }
      }
    else
      {
      decimal16Pattern[APV_16_BIT_BINARY_BIT_POSITION_SIGN] = APV_16_BIT_BINARY_BIT_POSITIVE_SIGN;
      }

    // Finally make the conversion printable ASCII
    for (conversionDec = APV_16_BIT_BINARY_BIT_POSITION_DECIMAL_WEIGHTS; conversionDec >= APV_16_BIT_BINARY_BIT_POSITION_DECIMAL_MINIMUM; conversionDec--)
      {
      decimal16Pattern[conversionDec] = decimalConversion[conversionDec] + APV_16_BIT_BINARY_BIT_CONVERT_TO_ASCII;
      }
    }
  else
    {
    conversionSuccess = FALSE;
    }
  
/******************************************************************************/

  return(conversionSuccess);

/******************************************************************************/
  } /* end of apvSignedConvert16HexToDecimal                                  */



/******************************************************************************/
/* (C) PulsingCoreSoftware Limited 2018 (C)                                   */
/******************************************************************************/
