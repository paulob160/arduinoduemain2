/******************************************************************************/
/* (C) PulsingCoreSoftware Limited 2018 (C)                                   */
/******************************************************************************/
/*                                                                            */
/* nFR24L01+.h                                                                */
/* 23.08.18                                                                   */
/* Paul O'Brien                                                               */
/*                                                                            */
/******************************************************************************/

#ifndef _APV_NRF24L01_H_
#define _APV_NRF24L01_H_

/******************************************************************************/
/* Include Files :                                                            */
/******************************************************************************/

#include <stdint.h>
#include "stdbool.h"

/******************************************************************************/
/* Constant Definitions :                                                     */
/******************************************************************************/
/* SPI Register Access Definitions :                                          */
/* Reference : "nRF24L01+ Product Specification", p50+                        */
/******************************************************************************/

#define APV_NRF24L01_NULL_CHARACTER              ((APV_NRF24L01_FIELD_SIZE)0x00) // the low-byte of a register read is a dummy

#define APV_NRF24L01_REGISTER_SHIFT              (5)
#define APV_NRF24L01_REGISTER_MASK               ((uint8_t)0x1F) // register address == 0bxxx11111)
                                                 
#define APV_NRF24L01_COMMAND_R_REGISTER          ((uint8_t)(0x00 << APV_NRF24L01_REGISTER_SHIFT)) // "read  register" command word == 0b000xxxxx
#define APV_NRF24L01_COMMAND_W_REGISTER          ((uint8_t)(0x01 << APV_NRF24L01_REGISTER_SHIFT)) // "write register" command word == 0b001xxxxx
                                                 
#define APV_NRF24L01_COMMAND_R_REGISTER_PAYLOAD  ((uint8_t)0x61) // "read rx payload"  command word == 0b01100001
#define APV_NRF24L01_COMMAND_W_REGISTER_PAYLOAD  ((uint8_t)0xa0) // "write rx payload" command word == 0b10100000
                                                 
#define APV_NRF24L01_COMMAND_FLUSH_TX            ((uint8_t)0xe1) // "flush Tx FIFO" command word == 0b11100001
#define APV_NRF24L01_COMMAND_FLUSH_RX            ((uint8_t)0xe2) // "flush Rx FIFO" command word == 0b11100010
                                                 
#define APV_NRF24L01_COMMAND_REUSE_TX_PL         ((uint8_t)0xe3) // "reuse Tx payload" command word == 0b11100011
                                                 
#define APV_NRF24L01_COMMAND_R_RX_PL_WID         ((uint8_t)0x60) // "read the top Rx FIFO payload 'width'" command word == 0b01100000
                                                 
#define APV_NRF24L01_PIPE_SHIFT                  (3)
#define APV_NRF24L01_PIPE_MASK                   ((uint8_t)0x07) // pipe address = 0bxxxxx111
                                                 
#define APV_NRF24L01_COMMAND_W_ACK_PAYLOAD       ((uint8_t)(0x15 << APV_NRF24L01_PIPE_SHIFT)) // "write ACK + payload on 0 { pipe } 5" command word == 0b10101xxx

#define APV_NRF24L01_COMMAND_W_TX_PAYLOAD_NO_ACK ((uint8_t)0xb0)                              // "write Tx payload no auto-ACK" command word == 0b10110000

#define APV_NRF24L01_COMMAND_NOP                 ((uint8_t)0xff)

/******************************************************************************/
/* Definition of the TX, Rx and Ack FIFOs                                     */
/******************************************************************************/

#define APV_NRF24L01_PAYLOAD_BYTE_WIDTH           ((uint8_t)8) // in bits!
#define APV_NRF24L01_PAYLOAD_LENGTH              ((uint8_t)32) // the maximum payload length in bytes sets the depth of the FIFOs

#define APV_NRF24L01_WIDTH_IN_BYTES_ACK_PLD      APV_NRF24L01_PAYLOAD_LENGTH
#define APV_NRF24L01_WIDTH_IN_BITS_ACK_PLD       (((uint16_t)APV_NRF24L01_WIDTH_IN_BYTES_ACK_PLD) * ((uint16_t)APV_NRF24L01_PAYLOAD_BYTE_WIDTH))

#define APV_NRF24L01_WIDTH_IN_BYTES_TX_PLD       APV_NRF24L01_PAYLOAD_LENGTH
#define APV_NRF24L01_WIDTH_IN_BITS_TX_PLD        (((uint16_t)APV_NRF24L01_WIDTH_IN_BYTES_TX_PLD)  * ((uint16_t)APV_NRF24L01_PAYLOAD_BYTE_WIDTH))

#define APV_NRF24L01_WIDTH_IN_BYTES_RX_PLD       APV_NRF24L01_PAYLOAD_LENGTH
#define APV_NRF24L01_WIDTH_IN_BITS_RX_PLD        (((uint16_t)APV_NRF24L01_WIDTH_IN_BYTES_RX_PLD)  * ((uint16_t)APV_NRF24L01_PAYLOAD_BYTE_WIDTH))

/******************************************************************************/
/* SPI Register Definitions :                                                 */
/* Reference : "nRF24L01+ Product Specification", p57+                        */
/******************************************************************************/
/* "CONFIG" Register :                                                        */
/******************************************************************************/

//#define APV_NRF24L01_REGISTER_ADDRESS_CONFIG       ((uint8_t)0x00) // "CONFIG" register address == 0x00
                                                   
#define APV_NRF24L01_REGISTER_CONFIG_PRIM_TX       ((uint8_t)0x00)
#define APV_NRF24L01_REGISTER_CONFIG_PRIM_RX       ((uint8_t)0x01)                                   // [ 0 == PTX | 1 == PRX ]
                                                                                                    
#define APV_NRF24L01_REGISTER_CONFIG_POWER_DOWN    ((uint8_t)0x00)                                  
#define APV_NRF24L01_REGISTER_CONFIG_POWER_UP      (APV_NRF24L01_REGISTER_CONFIG_PRIM_RX  << 1)      // [ 0 == power-down | 1 == power-up ]
                                                                                                    
#define APV_NRF24L01_REGISTER_CONFIG_CRC0_1        ((uint8_t)0x00)                                  
#define APV_NRF24L01_REGISTER_CONFIG_CRC0_2        (APV_NRF24L01_REGISTER_CONFIG_POWER_UP << 1)      // [ 0 == 1-byte crc | 1 == 2-byte crc ]
                                                                                                    
#define APV_NRF24L01_REGISTER_CONFIG_EN_CRC_N      ((uint8_t)0x00)                                  
#define APV_NRF24L01_REGISTER_CONFIG_EN_CRC        (APV_NRF24L01_REGISTER_CONFIG_CRC0_2 << 1)        // [ 0 == crc disabled | 1 == crc enabled ]
                                                                                                    
#define APV_NRF24L01_REGISTER_CONFIG_MASK_MAX_RT   ((uint8_t)0x00)                                  
#define APV_NRF24L01_REGISTER_CONFIG_MASK_MAX_RT_N (APV_NRF24L01_REGISTER_CONFIG_EN_CRC << 1)        // [ 0 == enable | 1 == disable ] "MAX_RT" interrupt (low)

#define APV_NRF24L01_REGISTER_CONFIG_MASK_TX_DS    ((uint8_t)0x00)
#define APV_NRF24L01_REGISTER_CONFIG_MASK_TX_DS_N  (APV_NRF24L01_REGISTER_CONFIG_MASK_MAX_RT_N << 1) // [ 0 == enable | 1 == disable ] "TX_DS" interrupt (low)

#define APV_NRF24L01_REGISTER_CONFIG_MASK_RX_DR    ((uint8_t)0x00)
#define APV_NRF24L01_REGISTER_CONFIG_MASK_RX_DR_N  (APV_NRF24L01_REGISTER_CONFIG_MASK_TX_DS_N << 1)  // [ 0 == enable | 1 == disable ] "RX_DR" interrupt (low)

#define APV_NRF24L01_REGISTER_CONFIG_USE_MASK      ((uint8_t)0xff)                                // allowable ('1') R/W bit-pattern == 0b11111111

// Default at RESET
#define APV_NRF24L01_REGISTER_CONFIG_DEFAULT       (APV_NRF24L01_REGISTER_CONFIG_EN_CRC)

/******************************************************************************/
/* "EN_AA" Enhanced ShockBurst AutoAcknowledge Enable Register :              */
/******************************************************************************/

//#define APV_NRF24L01_REGISTER_ADDRESS_EN_AA        ((uint8_t)0x01)                                // "EN_AA" register address == 0x01
                                                                                                  
#define APV_NRF24L01_REGISTER_EN_AA_DATA_PIPE_0    ((uint8_t)0x01)                                // enable auto-ACK on data-pipe 0
#define APV_NRF24L01_REGISTER_EN_AA_DATA_PIPE_0_N  ((uint8_t)0x00)

#define APV_NRF24L01_REGISTER_EN_AA_DATA_PIPE_1    (APV_NRF24L01_REGISTER_EN_AA_DATA_PIPE_0 << 1) // enable auto-ACK on data-pipe 1
#define APV_NRF24L01_REGISTER_EN_AA_DATA_PIPE_1_N  ((uint8_t)0x00)

#define APV_NRF24L01_REGISTER_EN_AA_DATA_PIPE_2    (APV_NRF24L01_REGISTER_EN_AA_DATA_PIPE_1 << 1) // enable auto-ACK on data-pipe 2
#define APV_NRF24L01_REGISTER_EN_AA_DATA_PIPE_2_N  ((uint8_t)0x00)

#define APV_NRF24L01_REGISTER_EN_AA_DATA_PIPE_3    (APV_NRF24L01_REGISTER_EN_AA_DATA_PIPE_2 << 1) // enable auto-ACK on data-pipe 3
#define APV_NRF24L01_REGISTER_EN_AA_DATA_PIPE_3_N  ((uint8_t)0x00)

#define APV_NRF24L01_REGISTER_EN_AA_DATA_PIPE_4    (APV_NRF24L01_REGISTER_EN_AA_DATA_PIPE_3 << 1) // enable auto-ACK on data-pipe 4
#define APV_NRF24L01_REGISTER_EN_AA_DATA_PIPE_4_N  ((uint8_t)0x00)

#define APV_NRF24L01_REGISTER_EN_AA_DATA_PIPE_5    (APV_NRF24L01_REGISTER_EN_AA_DATA_PIPE_4 << 1) // enable auto-ACK on data-pipe 5
#define APV_NRF24L01_REGISTER_EN_AA_DATA_PIPE_5_N  ((uint8_t)0x00)

#define APV_NRF24L01_REGISTER_EN_AA_USE_MASK       ((uint8_t)0x3f)                                // allowable ('1') R/W bit-pattern == 0b00111111

// Default at RESET
#define APV_NRF24L01_REGISTER_EN_AA_DEFAULT (APV_NRF24L01_REGISTER_EN_AA_DATA_PIPE_0 | \
                                             APV_NRF24L01_REGISTER_EN_AA_DATA_PIPE_1 | \
                                             APV_NRF24L01_REGISTER_EN_AA_DATA_PIPE_2 | \
                                             APV_NRF24L01_REGISTER_EN_AA_DATA_PIPE_3 | \
                                             APV_NRF24L01_REGISTER_EN_AA_DATA_PIPE_4 | \
                                             APV_NRF24L01_REGISTER_EN_AA_DATA_PIPE_5)

// "nRF24L01+" documentation equivalents :
#define ENAA_P0 APV_NRF24L01_REGISTER_EN_AA_DATA_PIPE_0
#define ENAA_P1 APV_NRF24L01_REGISTER_EN_AA_DATA_PIPE_1
#define ENAA_P2 APV_NRF24L01_REGISTER_EN_AA_DATA_PIPE_2
#define ENAA_P3 APV_NRF24L01_REGISTER_EN_AA_DATA_PIPE_3
#define ENAA_P4 APV_NRF24L01_REGISTER_EN_AA_DATA_PIPE_4
#define ENAA_P5 APV_NRF24L01_REGISTER_EN_AA_DATA_PIPE_5

/******************************************************************************/
/* "EN_RXADDR" Data-pipe (Rx Address) Enable Register :                       */
/******************************************************************************/

//#define APV_NRF24L01_REGISTER_ADDRESS_EN_RXADDR       ((uint8_t)0x02)                                    // "EN_RXADDR" register address == 0x02

#define APV_NRF24L01_REGISTER_EN_RXADDR_DATA_PIPE_0   ((uint8_t)0x01)                                    // enable Rx Address for data-pipe 0
#define APV_NRF24L01_REGISTER_EN_RXADDR_DATA_PIPE_0_N ((uint8_t)0x00)

#define APV_NRF24L01_REGISTER_EN_RXADDR_DATA_PIPE_1   (APV_NRF24L01_REGISTER_EN_RXADDR_DATA_PIPE_0 << 1) // enable Rx Address for data-pipe 1
#define APV_NRF24L01_REGISTER_EN_RXADDR_DATA_PIPE_1_N ((uint8_t)0x00)

#define APV_NRF24L01_REGISTER_EN_RXADDR_DATA_PIPE_2   (APV_NRF24L01_REGISTER_EN_RXADDR_DATA_PIPE_1 << 1) // enable Rx Address for data-pipe 2
#define APV_NRF24L01_REGISTER_EN_RXADDR_DATA_PIPE_2_N ((uint8_t)0x00)

#define APV_NRF24L01_REGISTER_EN_RXADDR_DATA_PIPE_3   (APV_NRF24L01_REGISTER_EN_RXADDR_DATA_PIPE_2 << 1) // enable Rx Address for data-pipe 3
#define APV_NRF24L01_REGISTER_EN_RXADDR_DATA_PIPE_3_N ((uint8_t)0x00)

#define APV_NRF24L01_REGISTER_EN_RXADDR_DATA_PIPE_4   (APV_NRF24L01_REGISTER_EN_RXADDR_DATA_PIPE_3 << 1) // enable Rx Address for data-pipe 4
#define APV_NRF24L01_REGISTER_EN_RXADDR_DATA_PIPE_4_N ((uint8_t)0x00)

#define APV_NRF24L01_REGISTER_EN_RXADDR_DATA_PIPE_5   (APV_NRF24L01_REGISTER_EN_RXADDR_DATA_PIPE_4 << 1) // enable Rx Address for data-pipe 5
#define APV_NRF24L01_REGISTER_EN_RXADDR_DATA_PIPE_5_N ((uint8_t)0x00)

#define APV_NRF24L01_REGISTER_EN_RXADDR_USE_MASK      ((uint8_t)0x3f)                                   // allowable ('1') R/W bit-pattern == 0b00111111

// Default at RESET
#define APV_NRF24L01_REGISTER_EN_RXADDR_DEFAULT       ((uint8_t)(APV_NRF24L01_REGISTER_EN_RXADDR_DATA_PIPE_0 | APV_NRF24L01_REGISTER_EN_RXADDR_DATA_PIPE_1))

// "nRF24L01+" documentation equivalents :
#define ERX_P0 APV_NRF24L01_REGISTER_EN_RXADDR_DATA_PIPE_0
#define ERX_P1 APV_NRF24L01_REGISTER_EN_RXADDR_DATA_PIPE_1
#define ERX_P2 APV_NRF24L01_REGISTER_EN_RXADDR_DATA_PIPE_2
#define ERX_P3 APV_NRF24L01_REGISTER_EN_RXADDR_DATA_PIPE_3
#define ERX_P4 APV_NRF24L01_REGISTER_EN_RXADDR_DATA_PIPE_4
#define ERX_P5 APV_NRF24L01_REGISTER_EN_RXADDR_DATA_PIPE_5

/******************************************************************************/
/* "SETUP_AW" Data-pipe Address Width :                                       */
/*  NOTE - addresses are enumerated low-byte to high-byte. The address-width  */
/*         patterns are thus :                                                */
/*                                                                            */
/* WIDTH : PATTERN                                                            */
/* ------| BYTE 4 | BYTE 3 | BYTE 2 | BYTE 1 | BYTE 0                         */
/*   5      0xXY  |  0xXY  |  0xXY  |  0xXY  |  0xXY                          */
/*   4      ----  |  0xXY  |  0xXY  |  0xXY  |  0xXY                          */
/*   3      ----  |  ----  |  0xXY  |  0xXY  |  0xXY                          */
/*                                                                            */
/*       - address of data-pipe 0 is UNIQUE width 5                           */
/*       - address of data-pipes 1-5 is COMMON for bytes <4,3,2,1> width 5    */
/*                                                 bytes   <3,2,1> width 4    */
/*                                                 bytes     <2,1> width 3    */
/******************************************************************************/

//#define APV_NRF24L01_REGISTER_ADDRESS_SETUP_AW       ((uint8_t)0x03) // "SETUP_AW" register address == 0x03

#define APV_NRF24L01_REGISTER_SETUP_AW_MASK          ((uint8_t)0x03) // 1 { address width } 3

#define APV_NRF24L01_REGISTER_SETUP_AW_03_BYTES      ((uint8_t)0x01) // address-width == 3 bytes
#define APV_NRF24L01_REGISTER_SETUP_AW_04_BYTES      ((uint8_t)0x02) // address-width == 4 bytes
#define APV_NRF24L01_REGISTER_SETUP_AW_05_BYTES      ((uint8_t)0x03) // address-width == 5 bytes

#define APV_NRF24L01_REGISTER_SETUP_AW_MAXIMUM_WIDTH (5)
#define APV_NRF24L01_REGISTER_SETUP_AW_MINIMUM_WIDTH (3)

#define APV_NRF24L01_REGISTER_SETUP_AW_USE_MASK      ((uint8_t)0x03) // allowable R/W ('1') bit-pattern == 0b00000011
 // Default at RESET
#define APV_NRF24L01_REGISTER_SETUP_AW_DEFAULT       APV_NRF24L01_REGISTER_SETUP_AW_05_BYTES

/******************************************************************************/
/* "SETUP_RETR" Automatic Retransmission :                                    */
/******************************************************************************/

//#define APV_NRF24L01_REGISTER_ADDRESS_SETUP_RETR     ((uint8_t)0x04) // "SETUP_RETR" register address == 0x04

#define APV_NRF24L01_REGISTER_SETUP_RETR_ARC_MASK    ((uint8_t)0x0f) // auto-retransmit 0 { count } 15 mask
#define APV_NRF24L01_REGISTER_SETUP_RETR_ARC_DISABLE ((uint8_t)0x00) // auto-retranmit disabled

#define APV_NRF24L01_REGISTER_SETUP_RETR_ARD_SHIFT   (4)                                                                                      // auto-retransmit delay bit shift

#define APV_NRF24L01_REGISTER_SETUP_RETR_ARD_MASK    (APV_NRF24L01_REGISTER_SETUP_RETR_ARC_MASK << APV_NRF24L01_REGISTER_SETUP_RETR_ARD_SHIFT) // auto-retransmit delay mask

#define APV_NRF24L01_REGISTER_SETUP_RETR_ARD_PERIOD  (250)           // auto-retransmit delay is in multiples of 250us

#define APV_NRF24L01_REGISTER_SETUP_RETR_USE_MASK    ((uint8_t)0xff) // allowable R/W ('1') bit-pattern == 0b11111111
// Default at RESET
#define APV_NRF24L01_REGISTER_SETUP_RETR_DEFAULT     ((uint8_t)0x03)

/******************************************************************************/
/* "RF_CH" RF Channel                                                         */
/*  NOTE - channel spacing is defined in steps of 1MHz. Transmission speeds   */
/*         are 250kbps, 1Mbps and 2Mbps. Bandwidth occupancy is < 1MHz at 250 */
/*         and 1Mbps but > 1MHz at 2Mbps. So at 2Mbps channel spacing MUST be */
/*         >= 2MHz!                                                           */
/******************************************************************************/

//#define APV_NRF24L01_REGISTER_ADDRESS_RF_CH            ((uint8_t)0x05)  // "RF_CH" register address == 0x05

#define APV_NRF24L01_REGISTER_RF_CH_MASK               ((uint8_t)0x7f)  // 0 { RF Channel } 127

#define APV_NRF24L01_REGISTER_RF_CH_BASE_FREQUENCY_MHz ((uint16_t)2400) // RF Channel Base Frequency == 2400MHz (2.4GHz)
#define APV_NRF24L01_REGISTER_RF_CH_RESOLUTION_MHz     ((uint8_t)0x01)  // RF Channel Resolution     == 1MHz

#define APV_NRF24L01_REGISTER_RF_CH_SPACING_250kbps    ((uint8_t)0x01)  // RF channels can be  spaced at 1MHz    at 250kbps transmission speed
#define APV_NRF24L01_REGISTER_RF_CH_SPACING_1Mbps      ((uint8_t)0x01)  // RF channels can be  spaced at 1MHz    at 1Mbps   transmission speed
#define APV_NRF24L01_REGISTER_RF_CH_SPACING_2Mbps      ((uint8_t)0x02)  // RF channels MUST be spaced at >= 2MHz at 2Mbps   transmission speed

#define APV_NRF24L01_REGISTER_RF_CH_USE_MASK           ((uint8_t)0x7f)  // allowable R/W ('1') bit-pattern == 0b01111111
// Default at RESET   
#define APV_NRF24L01_REGISTER_RF_CH_DEFAULT            ((uint8_t)0x02)  // default RF channel        == 2402MHz

/******************************************************************************/
/* "RF_SETUP" RF Setup                                                        */
/******************************************************************************/

//#define APV_NRF24L01_REGISTER_ADDRESS_RF_SETUP          ((uint8_t)0x06) // "RF_SETUP" register address  == 0x06

#define APV_NRF24L01_REGISTER_RF_SETUP_RF_PWR_SHIFT     (1)
#define APV_NRF24L01_REGISTER_RF_SETUP_RF_PWR_MASK      ((uint8_t)(0x03))

#define APV_NRF24L01_REGISTER_RF_SETUP_RF_PWR_TX_m18    (0x00)                                                // RF Tx power == -18dBm
#define APV_NRF24L01_REGISTER_RF_SETUP_RF_PWR_TX_m12    (0x01 << APV_NRF24L01_REGISTER_RF_SETUP_RF_PWR_SHIFT) // RF Tx power == -12dBm
#define APV_NRF24L01_REGISTER_RF_SETUP_RF_PWR_TX_m06    (0x02 << APV_NRF24L01_REGISTER_RF_SETUP_RF_PWR_SHIFT) // RF Tx power == -6dBm
#define APV_NRF24L01_REGISTER_RF_SETUP_RF_PWR_TX_00     (0x03 << APV_NRF24L01_REGISTER_RF_SETUP_RF_PWR_SHIFT) // RF Tx power ==  0dBm

// RF Data-rate high- and low-bits are NOT contiguous!
#define APV_NRF24L01_REGISTER_RF_SETUP_RF_DR_HIGH_SHIFT (APV_NRF24L01_REGISTER_RF_SETUP_RF_PWR_SHIFT + 2) // bit 3
#define APV_NRF24L01_REGISTER_RF_SETUP_RF_DR_LOW_SHIFT  (APV_NRF24L01_REGISTER_RF_SETUP_RF_PWR_SHIFT + 4) // bit 5

#define APV_NRF24L01_REGISTER_RF_SETUP_RF_DR_250kbps    (0x01 << APV_NRF24L01_REGISTER_RF_SETUP_RF_DR_LOW_SHIFT)
#define APV_NRF24L01_REGISTER_RF_SETUP_RF_DR_2Mbps      (0x01 << APV_NRF24L01_REGISTER_RF_SETUP_RF_DR_HIGH_SHIFT)
#define APV_NRF24L01_REGISTER_RF_SETUP_RF_DR_1Mbps      ((uint8_t)(~(APV_NRF24L01_REGISTER_RF_SETUP_RF_DR_250kbps | APV_NRF24L01_REGISTER_RF_SETUP_RF_DR_2Mbps)))

#define APV_NRF24L01_REGISTER_RF_SETUP_PLL_LOCK         (0x00) // !!! ONLY USED FOR TEST !!!

#define APV_NRF24L01_REGISTER_RF_SETUP_CONT_WAVE_SHIFT  (APV_NRF24L01_REGISTER_RF_SETUP_RF_DR_LOW_SHIFT + 2) // bit 7
#define APV_NRF24L01_REGISTER_RF_SETUP_CONT_WAVE        (0x01 << APV_NRF24L01_REGISTER_RF_SETUP_CONT_WAVE_SHIFT)

#define APV_NRF24L01_REGISTER_RF_SETUP_USE_MASK         ((uint8_t)0xbe) // allowable R/W ('1') bit-pattern == 0b10111110

// Default after RESET
#define APV_NRF24L01_REGISTER_RF_SETUP_DEFAULT          (APV_NRF24L01_REGISTER_RF_SETUP_RF_PWR_TX_00 | APV_NRF24L01_REGISTER_RF_SETUP_RF_DR_2Mbps)

/******************************************************************************/
/* "STATUS" Status Register                                                   */
/*  NOTE - VERY IMPORTANT! See the comment re: "MAX_RT"                       */
/******************************************************************************/

//#define APV_NRF24L01_REGISTER_ADDRESS_STATUS            ((uint8_t)0x07) // "STATUS" register address == 0x07

#define APV_NRF24L01_REGISTER_STATUS_TX_FULL            ((uint8_t)0x01) // [ 0 == Tx FIFO not full | 1 == Tx FIFO full ]

#define APV_NRF24L01_REGISTER_STATUS_RX_P_NO_MASK       ((uint8_t)0x03)
#define APV_NRF24L01_REGISTER_STATUS_RX_P_NO_SHIFT      (1)

// Data pipe identifiers
#define APV_NRF24L01_REGISTER_STATUS_RX_P_NO_0          (APV_nRF24L01_DATA_PIPE_0 << APV_NRF24L01_REGISTER_STATUS_RX_P_NO_SHIFT)
#define APV_NRF24L01_REGISTER_STATUS_RX_P_NO_1          (APV_nRF24L01_DATA_PIPE_1 << APV_NRF24L01_REGISTER_STATUS_RX_P_NO_SHIFT)
#define APV_NRF24L01_REGISTER_STATUS_RX_P_NO_2          (APV_nRF24L01_DATA_PIPE_2 << APV_NRF24L01_REGISTER_STATUS_RX_P_NO_SHIFT)
#define APV_NRF24L01_REGISTER_STATUS_RX_P_NO_3          (APV_nRF24L01_DATA_PIPE_3 << APV_NRF24L01_REGISTER_STATUS_RX_P_NO_SHIFT)
#define APV_NRF24L01_REGISTER_STATUS_RX_P_NO_4          (APV_nRF24L01_DATA_PIPE_4 << APV_NRF24L01_REGISTER_STATUS_RX_P_NO_SHIFT)
#define APV_NRF24L01_REGISTER_STATUS_RX_P_NO_5          (APV_nRF24L01_DATA_PIPE_5 << APV_NRF24L01_REGISTER_STATUS_RX_P_NO_SHIFT)

// Rx FIFO Empty (bad coupling!)
#define APV_NRF24L01_REGISTER_STATUS_RX_EMPTY_CODE      ((uint8_t)0x07)
#define APV_NRF24L01_REGISTER_STATUS_RX_EMPTY           (APV_NRF24L01_REGISTER_STATUS_RX_EMPTY_CODE << APV_NRF24L01_REGISTER_STATUS_RX_P_NO_SHIFT)

#define APV_NRF24L01_REGISTER_STATUS_MAX_RT_SHIFT       (APV_NRF24L01_REGISTER_STATUS_RX_P_NO_SHIFT + 2)
#define APV_NRF24L01_REGISTER_STATUS_MAX_RT             (0x01 << APV_NRF24L01_REGISTER_STATUS_MAX_RT_SHIFT) // flags a maximum number of Tx retransmit interrupts have occurred - 
                                                                                                            // MUST write this bit to clear and re-enable communications!

#define APV_NRF24L01_REGISTER_STATUS_TX_DS              (APV_NRF24L01_REGISTER_STATUS_MAX_RT << 1)          // "Data sent" Tx FIFO interrupt - write to clear

#define APV_NRF24L01_REGISTER_STATUS_RX_DR              (APV_NRF24L01_REGISTER_STATUS_TX_DS << 1)           // "Data ready" Rx FIFO interrupt - write to clear

#define APV_NRF24L01_REGISTER_STATUS_USE_MASK           ((uint8_t)0x70)                                     // allowable R/W ('1') bit pattern == 0b01110000

// Default after RESET
#define APV_NRF24L01_REGISTER_STATUS_DEFAULT            (APV_NRF24L01_REGISTER_STATUS_RX_EMPTY)

/******************************************************************************/
/* "OBSERVE_TX" Transmit Operation Status                                     */
/******************************************************************************/

//#define APV_NRF24L01_REGISTER_ADDRESS_OBSERVE_TX        ((uint8_t)0x08)                               // "OBSERVE_TX" register address == 0x08

#define APV_NRF24L01_REGISTER_OBSERVE_TX_ARC_CNT_MASK   ((uint8_t)0x0f)                               // Count of retransmitted packets
#define APV_NRF24L01_REGISTER_OBSERVE_TX_ARC_CNT        APV_NRF24L01_REGISTER_OBSERVE_TX_ARC_CNT_MASK

#define APV_NRF24L01_REGISTER_OBSERVE_TX_PLOS_CNT_MASK  APV_NRF24L01_REGISTER_OBSERVE_TX_ARC_CNT_MASK // Count of lost packets
#define APV_NRF24L01_REGISTER_OBSERVE_TX_PLOS_CNT_SHIFT (4)
#define APV_NRF24L01_REGISTER_OBSERVE_TX_PLOS_CNT       (APV_NRF24L01_REGISTER_OBSERVE_TX_PLOS_CNT_MASK << APV_NRF24L01_REGISTER_OBSERVE_TX_PLOS_CNT_SHIFT)

#define APV_NRF24L01_REGISTER_OBSERVE_TX_USE_MASK       ((uint8_t)0x00)                               // allowable R/W ('1') bit-pattern == 0x00000000

// Default after RESET
#define APV_NRF24L01_REGISTER_OBSERVE_TX_DEFAULT        ((uint8_t)0x00)

/******************************************************************************/
/* "RPD" Received Power Detector                                              */
/******************************************************************************/

//#define APV_NRF24L01_REGISTER_ADDRESS_RPD               ((uint8_t)0x09) // "RPD" register address == 0x09

#define APV_NRF24L01_REGISTER_RPD                       ((uint8_t)0x01) // received power >= -64dBm

#define APV_NRF24L01_REGISTER_RPD_USE_MASK              ((uint8_t)0x00) // allowable R/W ('1') bit-pattern == 0x00000000

// Default after RESET
#define APV_NRF24L01_REGISTER_RPD_DEFAULT               ((uint8_t)0x00)

/******************************************************************************/
/* "RX_ADDR_Pn" Data-pipe n Receiver Address                                  */
/******************************************************************************/

//#define APV_NRF24L01_REGISTER_ADDRESS_RX_ADDR_P0       ((uint8_t)0x0a)                                // "RX_ADDR_P0" register address == 0x0a
//#define APV_NRF24L01_REGISTER_ADDRESS_RX_ADDR_P1       (APV_NRF24L01_REGISTER_ADDRESS_RX_ADDR_P0 + 1) // "RX_ADDR_P1" register address == 0x0b
//#define APV_NRF24L01_REGISTER_ADDRESS_RX_ADDR_P2       (APV_NRF24L01_REGISTER_ADDRESS_RX_ADDR_P1 + 1) // "RX_ADDR_P2" register address == 0x0c
//#define APV_NRF24L01_REGISTER_ADDRESS_RX_ADDR_P3       (APV_NRF24L01_REGISTER_ADDRESS_RX_ADDR_P2 + 1) // "RX_ADDR_P3" register address == 0x0d
//#define APV_NRF24L01_REGISTER_ADDRESS_RX_ADDR_P4       (APV_NRF24L01_REGISTER_ADDRESS_RX_ADDR_P3 + 1) // "RX_ADDR_P4" register address == 0x0e
//#define APV_NRF24L01_REGISTER_ADDRESS_RX_ADDR_P5       (APV_NRF24L01_REGISTER_ADDRESS_RX_ADDR_P4 + 1) // "RX_ADDR_P5" register address == 0x0f

#define APV_NRF24L01_REGISTER_RX_ADDR_Pn_MAXIMUM_WIDTH APV_NRF24L01_REGISTER_SETUP_AW_MAXIMUM_WIDTH
#define APV_NRF24L01_REGISTER_RX_ADDR_Pn_MINIMUM_WIDTH APV_NRF24L01_REGISTER_SETUP_AW_MINIMUM_WIDTH

// Default after RESET
// - data-pipe 0 Receive Address
#define APV_NRF24L01_REGISTER_RX_ADDR_P0_DEFAULT_OCTET ((uint8_t)0xe7)
#define APV_NRF24L01_REGISTER_RX_ADDR_P0_DEFAULT_SHIFT (8)
#define APV_NRF24L01_REGISTER_RX_ADDR_P0_DEFAULT       ((((uint64_t)APV_NRF24L01_REGISTER_RX_ADDR_P0_DEFAULT_OCTET) << (APV_NRF24L01_REGISTER_RX_ADDR_P0_DEFAULT_SHIFT * 0)) | \
                                                        (((uint64_t)APV_NRF24L01_REGISTER_RX_ADDR_P0_DEFAULT_OCTET) << (APV_NRF24L01_REGISTER_RX_ADDR_P0_DEFAULT_SHIFT * 1)) | \
                                                        (((uint64_t)APV_NRF24L01_REGISTER_RX_ADDR_P0_DEFAULT_OCTET) << (APV_NRF24L01_REGISTER_RX_ADDR_P0_DEFAULT_SHIFT * 2)) | \
                                                        (((uint64_t)APV_NRF24L01_REGISTER_RX_ADDR_P0_DEFAULT_OCTET) << (APV_NRF24L01_REGISTER_RX_ADDR_P0_DEFAULT_SHIFT * 3))| \
                                                        (((uint64_t)APV_NRF24L01_REGISTER_RX_ADDR_P0_DEFAULT_OCTET) << (APV_NRF24L01_REGISTER_RX_ADDR_P0_DEFAULT_SHIFT * 4)))

// - data-pipe 1 - 5 Receive Address octets 1 .. 4
#define APV_NRF24L01_REGISTER_RX_ADDR_Pn_DEFAULT_OCTET ((uint8_t)0xc2)
#define APV_NRF24L01_REGISTER_RX_ADDR_Pn_DEFAULT       ((((uint64_t)APV_NRF24L01_REGISTER_RX_ADDR_Pn_DEFAULT_OCTET) << (APV_NRF24L01_REGISTER_RX_ADDR_P0_DEFAULT_SHIFT * 1)) | \
                                                        (((uint64_t)APV_NRF24L01_REGISTER_RX_ADDR_Pn_DEFAULT_OCTET) << (APV_NRF24L01_REGISTER_RX_ADDR_P0_DEFAULT_SHIFT * 2)) | \
                                                        (((uint64_t)APV_NRF24L01_REGISTER_RX_ADDR_Pn_DEFAULT_OCTET) << (APV_NRF24L01_REGISTER_RX_ADDR_P0_DEFAULT_SHIFT * 3)) | \
                                                        (((uint64_t)APV_NRF24L01_REGISTER_RX_ADDR_Pn_DEFAULT_OCTET) << (APV_NRF24L01_REGISTER_RX_ADDR_P0_DEFAULT_SHIFT * 4)))

// - data-pipe 1 - 5 Receive Address octet 0
#define APV_NRF24L01_REGISTER_RX_ADDR_Pn_DEFAULT_WIDTH (1) // receive data-pipes 2 - 5 addresses are only unique at the least-significant byte, sharing the other address 
                                                           // bytes of receive data-pipe 1

#define APV_NRF24L01_REGISTER_RX_ADDR_P1_DEFAULT_OCTET ((uint8_t)0xc2)
#define APV_NRF24L01_REGISTER_RX_ADDR_P2_DEFAULT_OCTET ((uint8_t)0xc3)
#define APV_NRF24L01_REGISTER_RX_ADDR_P3_DEFAULT_OCTET ((uint8_t)0xc4)
#define APV_NRF24L01_REGISTER_RX_ADDR_P4_DEFAULT_OCTET ((uint8_t)0xc5)
#define APV_NRF24L01_REGISTER_RX_ADDR_P5_DEFAULT_OCTET ((uint8_t)0xc6)

#define APV_NRF24L01_REGISTER_RX_ADDR_P1_DEFAULT       (APV_NRF24L01_REGISTER_RX_ADDR_Pn_DEFAULT | ((uint64_t)APV_NRF24L01_REGISTER_RX_ADDR_P1_DEFAULT_OCTET))
#define APV_NRF24L01_REGISTER_RX_ADDR_P2_DEFAULT       (APV_NRF24L01_REGISTER_RX_ADDR_Pn_DEFAULT | ((uint64_t)APV_NRF24L01_REGISTER_RX_ADDR_P2_DEFAULT_OCTET))
#define APV_NRF24L01_REGISTER_RX_ADDR_P3_DEFAULT       (APV_NRF24L01_REGISTER_RX_ADDR_Pn_DEFAULT | ((uint64_t)APV_NRF24L01_REGISTER_RX_ADDR_P3_DEFAULT_OCTET))
#define APV_NRF24L01_REGISTER_RX_ADDR_P4_DEFAULT       (APV_NRF24L01_REGISTER_RX_ADDR_Pn_DEFAULT | ((uint64_t)APV_NRF24L01_REGISTER_RX_ADDR_P4_DEFAULT_OCTET))
#define APV_NRF24L01_REGISTER_RX_ADDR_P5_DEFAULT       (APV_NRF24L01_REGISTER_RX_ADDR_Pn_DEFAULT | ((uint64_t)APV_NRF24L01_REGISTER_RX_ADDR_P5_DEFAULT_OCTET))

#define APV_NRF24L01_REGISTER_RX_ADDR_Pn_USE_MASK      ((uint8_t)0xff)

/******************************************************************************/
/* "TX_ADDR" Transmit Address                                                 */
/******************************************************************************/

//#define APV_NRF24L01_REGISTER_ADDRESS_TX_ADDR          ((uint8_t)0x10)                                // "TX_ADDR" register address == 0x10

#define APV_NRF24L01_REGISTER_TX_ADDR_MAXIMUM_WIDTH    APV_NRF24L01_REGISTER_SETUP_AW_MAXIMUM_WIDTH
#define APV_NRF24L01_REGISTER_TX_ADDR_MINIMUM_WIDTH    APV_NRF24L01_REGISTER_SETUP_AW_MINIMUM_WIDTH

// Default after RESET
#define APV_NRF24L01_REGISTER_TX_ADDR_DEFAULT_OCTET    APV_NRF24L01_REGISTER_RX_ADDR_P0_DEFAULT_OCTET // Transmit address default octet == 0xe7
#define APV_NRF24L01_REGISTER_TX_ADDR_DEFAULT          APV_NRF24L01_REGISTER_RX_ADDR_P0_DEFAULT       // Transmit address default         == 0x000000e7e7e7e7e7

#define APV_NRF24L01_REGISTER_TX_ADDR_USE_MASK         ((uint8_t)0xff)

/******************************************************************************/
/* "RX_PW_Pn" Data-pipe 'n' Received Payload Length                           */
/******************************************************************************/

//#define APV_NRF24L01_REGISTER_ADDRESS_RX_PW_P0         ((uint8_t)0x11)                              // "RX_PW_P0" register address == 0x11
//#define APV_NRF24L01_REGISTER_ADDRESS_RX_PW_P1         (APV_NRF24L01_REGISTER_ADDRESS_RX_PW_P0 + 1) // "RX_PW_P1" register address == 0x12
//#define APV_NRF24L01_REGISTER_ADDRESS_RX_PW_P2         (APV_NRF24L01_REGISTER_ADDRESS_RX_PW_P1 + 1) // "RX_PW_P2" register address == 0x13
//#define APV_NRF24L01_REGISTER_ADDRESS_RX_PW_P3         (APV_NRF24L01_REGISTER_ADDRESS_RX_PW_P2 + 1) // "RX_PW_P3" register address == 0x14
//#define APV_NRF24L01_REGISTER_ADDRESS_RX_PW_P4         (APV_NRF24L01_REGISTER_ADDRESS_RX_PW_P3 + 1) // "RX_PW_P4" register address == 0x15
//#define APV_NRF24L01_REGISTER_ADDRESS_RX_PW_P5         (APV_NRF24L01_REGISTER_ADDRESS_RX_PW_P4 + 1) // "RX_PW_P5" register address == 0x16

#define APV_NRF24L01_REGISTER_RX_PW_Pn_UNUSED          ((uint8_t)0x00)                              // IF bit #0 == 0 data-pipe 'n' is not in use

#define APV_NRF24L01_REGISTER_RX_PW_Pn_MASK_SHIFT      (5)
#define APV_NRF24L01_REGISTER_RX_PW_Pn_MAXIMUM_BYTES   (1 << APV_NRF24L01_REGISTER_RX_PW_Pn_MASK_SHIFT)

// Mask bit pattern = 0b00111111
#define APV_NRF24L01_REGISTER_RX_PW_Pn_USE_MASK        ((uint8_t)(APV_NRF24L01_REGISTER_RX_PW_Pn_MAXIMUM_BYTES + (APV_NRF24L01_REGISTER_RX_PW_Pn_MAXIMUM_BYTES - 1))) // allowable ('1') R/W bit-pattern == 0b00111111

// Default after RESET
#define APV_NRF24L01_REGISTER_RX_PW_Pn_DEFAULT         ((uint8_t)0x00)

/******************************************************************************/
/* "FIFO_STATUS" FIFO Status Register                                         */
/******************************************************************************/

//#define APV_NRF24L01_REGISTER_ADDRESS_FIFO_STATUS     ((uint8_t)0x17)                                   // "FIFO_STATUS" register address == 0x17

#define APV_NRF24L01_REGISTER_FIFO_STATUS_RX_EMPTY    ((uint8_t)0x01)                                   // [ 0 == Rx FIFO not empty | 1 == Rx FIFO empty ]
#define APV_NRF24L01_REGISTER_FIFO_STATUS_RX_EMPTY_N  ((uint8_t)0x00)

#define APV_NRF24L01_REGISTER_FIFO_STATUS_RX_FULL     (APV_NRF24L01_REGISTER_FIFO_STATUS_RX_EMPTY << 1) // [ 0 == Rx FIFO not full | 1 == Rx FIFO full ]
#define APV_NRF24L01_REGISTER_FIFO_STATUS_RX_FULL_N   ((uint8_t)0x00)

// Bits 3:2 empty
#define APV_NRF24L01_REGISTER_FIFO_STATUS_TX_EMPTY    (APV_NRF24L01_REGISTER_FIFO_STATUS_RX_FULL  << 3) // [ 0 == Tx FIFO not empty | 1 == Tx FIFO empty ]
#define APV_NRF24L01_REGISTER_FIFO_STATUS_TX_EMPTY_N  ((uint8_t)0x00)

#define APV_NRF24L01_REGISTER_FIFO_STATUS_TX_FULL     (APV_NRF24L01_REGISTER_FIFO_STATUS_TX_EMPTY << 1) // [ 0 == Tx FIFO not full | 1 == Tx FIFO full ]
#define APV_NRF24L01_REGISTER_FIFO_STATUS_TX_FULL_N   ((uint8_t)0x00)

#define APV_NRF24L01_REGISTER_FIFO_STATUS_TX_REUSE    (APV_NRF24L01_REGISTER_FIFO_STATUS_TX_FULL << 1)  // [ 0 == no payload reuse | 1 == payload reuse ]
#define APV_NRF24L01_REGISTER_FIFO_STATUS_TX_REUSE_N  ((uint8_t)0x00)

#define APV_NRF24L01_REGISTER_FIFO_STATUS_USE_MASK    ((uint8_t)0x00)                                   // allowable R/W ('1') bit-pattern == 0b00000000

// Default after RESET
#define APV_NRF24L01_REGISTER_FIFO_STATUS_DEFAULT     (APV_NRF24L01_REGISTER_FIFO_STATUS_RX_EMPTY | APV_NRF24L01_REGISTER_FIFO_STATUS_TX_EMPTY)

/******************************************************************************/
/* "DYNPD" Enable Dynamic Payload Length                                      */
/*  NOTE - enable dynamic payload on data-pipe 'n' requires the "global" flag */
/*         "EN_DPL" in the "FEATURE" register and the matching "ENAA_Pn"      */
/*         (enable auto-acknowledge)                                          */
/******************************************************************************/

//#define APV_NRF24L01_REGISTER_ADDRESS_DYNPD           ((uint8_t)0x1c)                           // "DYNPD" register address == 0x1c
                                                                                                
#define APV_NRF24L01_REGISTER_DYNPD_DPL_P0            ((uint8_t)0x01)                           // [ 0 == don't enable ! 1 == enable ] dynamic payload length on data-pipe 0
#define APV_NRF24L01_REGISTER_DYNPD_DPL_P1            (APV_NRF24L01_REGISTER_DYNPD_DPL_P0 << 1) // [ 0 == don't enable ! 1 == enable ] dynamic payload length on data-pipe 1
#define APV_NRF24L01_REGISTER_DYNPD_DPL_P2            (APV_NRF24L01_REGISTER_DYNPD_DPL_P1 << 1) // [ 0 == don't enable ! 1 == enable ] dynamic payload length on data-pipe 2
#define APV_NRF24L01_REGISTER_DYNPD_DPL_P3            (APV_NRF24L01_REGISTER_DYNPD_DPL_P2 << 1) // [ 0 == don't enable ! 1 == enable ] dynamic payload length on data-pipe 3
#define APV_NRF24L01_REGISTER_DYNPD_DPL_P4            (APV_NRF24L01_REGISTER_DYNPD_DPL_P3 << 1) // [ 0 == don't enable ! 1 == enable ] dynamic payload length on data-pipe 4
#define APV_NRF24L01_REGISTER_DYNPD_DPL_P5            (APV_NRF24L01_REGISTER_DYNPD_DPL_P4 << 1) // [ 0 == don't enable ! 1 == enable ] dynamic payload length on data-pipe 5

#define APV_NRF24L01_REGISTER_DYNPD_USE_MASK          ((uint8_t)0x3f)                           // allowable ('1') R/W bit-pattern == 0b00111111

// Default after RESET
#define APV_NRF24L01_REGISTER_DYNPD_DPL_DEFAULT       ((uint8_t)0x00)

/******************************************************************************/
/* "FEATURE" Feature Register                                                 */
/*  NOTE - on "EN_ACK_PAY" set the rules for ARD are :                        */
/*          - 2Mbps mode : ACK payload > 15 bytes --> ARD >= 500us            */
/*          - 1Mbps mode : ACK payload >  5 bytes --> ARD >= 500us            */
/*         in any "ACK" case :                                                */
/*          - 250kbps    :                            ARD >= 500us            */
/*                                                                            */
/******************************************************************************/

//#define APV_NRF24L01_REGISTER_ADDRESS_FEATURE         ((uint8_t)0x1d) // "FEATURE" register address == 0x1d

#define APV_NRF24L01_REGISTER_FEATURE_EN_DYN_ACK      ((uint8_t)0x01)                                 // [ 0 == disable | 1 == enable ] the "W_TX_PAYLOAD_NOACK" command
#define APV_NRF24L01_REGISTER_FEATURE_EN_ACK_PAY      (APV_NRF24L01_REGISTER_FEATURE_EN_DYN_ACK << 1) // [ 0 == disable | 1 == enable ] payload with ACK
#define APV_NRF24L01_REGISTER_FEATURE_EN_DPL          (APV_NRF24L01_REGISTER_FEATURE_EN_ACK_PAY << 1) // [ 0 == disable | 1 == enable ] dynamic payload length

#define APV_NRF24L01_REGISTER_FEATURE_USE_MASK        ((uint8_t)0x07)                                 // allowable R/W ('1') bit-pattern == 0b00000111
// Default after RESET
#define APV_NRF24L01_REGISTER_FEATURE_DEFAULT         ((uint8_t)0x00)

/******************************************************************************/
/* The master beacon constants :                                              */
/******************************************************************************/

#define APV_NRF24L01_MASTER_BEACON_OUTER_PATTERN   ((uint8_t)0x81)
#define APV_NRF24L01_MASTER_BEACON_MID_PATTERN     ((uint8_t)0x5A)
#define APV_NRF24L01_MASTER_BEACON_INNER_PATTERN   ((uint8_t)0x7E)

#define APV_NRF24L01_MASTER_BEACON_PATTERN_LENGTH             (15)

/******************************************************************************/
/* Type Definitions :                                                         */
/******************************************************************************/

typedef uint8_t APV_NRF24L01_FIELD_SIZE;

typedef enum apv_nRF24L01SystemId_tTag
  {
  APV_nRF24L01_SYSTEM_ID_MASTER = 0,
  APV_nRF24L01_SYSTEM_ID_SLAVE_0,
  APV_nRF24L01_SYSTEM_ID_SLAVE_1,
  APV_nRF24L01_SYSTEM_ID_SLAVE_2,
  APV_nRF24L01_SYSTEM_ID_SLAVE_3,
  APV_nRF24L01_SYSTEM_ID_SLAVE_4,
  APV_nRF24L01_SYSTEM_ID_SLAVE_5,
  APV_nRF24L01_SYSTEM_IDS
  } apv_nRF24L01SystemId_t;

typedef enum apv_nRF24L01DuplexId_tTag
  {
  APV_nRF24L01_DUPLEX_ID_TX = 0,
  APV_nRF24L01_DUPLEX_ID_RX,
  APV_nRF24L01_DUPLEX_IDS
  } apv_nRF24L01DuplexId_t;

typedef enum apv_nRF24L01DataPipe_tTag
  {
  APV_nRF24L01_DATA_PIPE_0 = 0,
  APV_nRF24L01_DATA_PIPE_1,
  APV_nRF24L01_DATA_PIPE_2,
  APV_nRF24L01_DATA_PIPE_3,
  APV_nRF24L01_DATA_PIPE_4,
  APV_nRF24L01_DATA_PIPE_5,
  APV_nRF24L01_DATA_PIPE_SET
  } apv_nRF24L01DataPipe_t;

typedef enum apv_nRF24LO1RegisterAddressSet_tTag
  {
  APV_NRF24L01_REGISTER_ADDRESS_CONFIG = 0,
  APV_NRF24L01_REGISTER_ADDRESS_EN_AA,
  APV_NRF24L01_REGISTER_ADDRESS_EN_RXADDR,
  APV_NRF24L01_REGISTER_ADDRESS_SETUP_AW,
  APV_NRF24L01_REGISTER_ADDRESS_SETUP_RETR,
  APV_NRF24L01_REGISTER_ADDRESS_RF_CH,
  APV_NRF24L01_REGISTER_ADDRESS_RF_SETUP,
  APV_NRF24L01_REGISTER_ADDRESS_STATUS,
  APV_NRF24L01_REGISTER_ADDRESS_OBSERVE_TX,
  APV_NRF24L01_REGISTER_ADDRESS_RPD,
  APV_NRF24L01_REGISTER_ADDRESS_RX_ADDR_P0,
  APV_NRF24L01_REGISTER_ADDRESS_RX_ADDR_P1,
  APV_NRF24L01_REGISTER_ADDRESS_RX_ADDR_P2,
  APV_NRF24L01_REGISTER_ADDRESS_RX_ADDR_P3,
  APV_NRF24L01_REGISTER_ADDRESS_RX_ADDR_P4,
  APV_NRF24L01_REGISTER_ADDRESS_RX_ADDR_P5,
  APV_NRF24L01_REGISTER_ADDRESS_TX_ADDR,
  APV_NRF24L01_REGISTER_ADDRESS_RX_PW_P0,
  APV_NRF24L01_REGISTER_ADDRESS_RX_PW_P1,
  APV_NRF24L01_REGISTER_ADDRESS_RX_PW_P2,
  APV_NRF24L01_REGISTER_ADDRESS_RX_PW_P3,
  APV_NRF24L01_REGISTER_ADDRESS_RX_PW_P4,
  APV_NRF24L01_REGISTER_ADDRESS_RX_PW_P5,
  APV_NRF24L01_REGISTER_ADDRESS_FIFO_STATUS,
  APV_NRF24L01_REGISTER_ADDRESS_0x18, // unused address
  APV_NRF24L01_REGISTER_ADDRESS_0x19, // unused address
  APV_NRF24L01_REGISTER_ADDRESS_0x1a, // unused address
  APV_NRF24L01_REGISTER_ADDRESS_0x1b, // unused address
  APV_NRF24L01_REGISTER_ADDRESS_DYNPD,
  APV_NRF24L01_REGISTER_ADDRESS_FEATURE,
  APV_NRF24L01_REGISTER_ADDRESS_SET
  } apv_nRF24LO1RegisterAddressSet_t;

// The constant part of the register definitions
typedef struct apv_nRF24L01RegisterDefinition_tTag
  {
  apv_nRF24LO1RegisterAddressSet_t apv_nRF24L01RegisterAddress;
  uint8_t                          apv_nRF24L01RegisterAccess;
  uint8_t                          apv_nRF24L01RegisterDefault;
  boolean_t                        apv_nRF24L01RegisterInUse; // flag the register address as [ false == inactive | true == active ]
  } apv_nRF24L01RegisterDefinition_t;

// The variable part of the register definitions
typedef struct apv_nRF24L01Registers_tTag
  {
  const apv_nRF24L01RegisterDefinition_t *apv_nRF24L01RegisterDefinition;
  uint8_t                                 apv_nRF24L01Register; // the register "shadow"
  } apv_nRF24L01Registers_t;

typedef struct apv_nRF24L01DataPipeAddressDefinition_tTag
  {
  uint8_t apv_nRF24L01DataPipeAddress[APV_NRF24L01_REGISTER_SETUP_AW_MAXIMUM_WIDTH];
  uint8_t apv_nRF24L01DataPipeAddressLength;
  } apv_nRF24L01DataPipeAddressDefinition_t;

typedef enum apv_nRF24L01BeaconTransmitState_tTag
  {
  APV_NRF24L01_BEACON_TRANSMIT_STATE_IDLE = 0,
  APV_NRF24L01_BEACON_TRANSMIT_STATE_TX,
  APV_NRF24L01_BEACON_TRANSMIT_STATE_RX,
  APV_NRF24L01_BEACON_TRANSMIT_STATES
  } apv_nRF24L01BeaconTransmitState_t;

/******************************************************************************/
/* Global Function Declarations :                                             */
/******************************************************************************/

extern APV_ERROR_CODE apvInitialisation_nRF24L01(apvCoreTimerBlock_t            *apvCoreTimerBlock,
                                                 apvArduinoSystemIdentifier_t   *apvIdentity,
                                                 apv_nRF24L01DuplexId_t          apv_nRF24L01DuplexId);

extern void           apv_nRF24L01WriteRegisterDefault(apv_nRF24LO1RegisterAddressSet_t apv_nRF24L01RegisterNumberr,
                                                       apv_nRF24L01DuplexId_t           apv_nRF24L01DuplexId);
extern void           apv_nRF24L01ChipEnableControl(apv_nRF24L01DuplexId_t  apv_nRF24L01DuplexId,
                                                    bool                   *chipEnableState);

extern APV_ERROR_CODE apv_nRF24L01StartDataPipeReceiveAddressRead(apv_nRF24L01DataPipe_t                                      dataPipe,
                                                                  apv_nRF24L01DataPipeAddressDefinition_t *dataPipeAddresses,
                                                                  uint8_t                                  dataPipeAddressesLength,
                                                                  apvChipSelectRegisterInstance_t          spiChipSelect);
extern APV_ERROR_CODE apv_nRF24L01WriteRegister(apv_nRF24LO1RegisterAddressSet_t             apv_nRF24L01RegisterAddress,
                                                    uint8_t                          *apv_nRF24L01RegisterPayLoad,
                                                    uint8_t                           apv_NRF24L01RegisterPayLoadLength,
                                                    apvChipSelectRegisterInstance_t   apv_nRf24L01SpiChipSelect);

extern APV_ERROR_CODE apv_nRF24L01MasterTransmitManager(apvArduinoSystemIdentifier_t  masterTransmitClass,
                                                        bool                         *masterSchedulingFlag);
extern APV_ERROR_CODE apv_nRF24L01MasterReceiveManager(apvArduinoSystemIdentifier_t  masterReceiveClass,
                                                       bool                         *masterSchedulingFlag);
extern APV_ERROR_CODE apv_nRF24L01SlaveTransmitManager(apvArduinoSystemIdentifier_t  slaveTransmitClass,
                                                       bool                         *slaveSchedulingFlag);
extern APV_ERROR_CODE apv_nRF24L01SlaveReceiveManager(apvArduinoSystemIdentifier_t  slaveReceiveClass,
                                                      bool                         *slaveSchedulingFlag);

extern void           apv_nRF24L01StateTimer(void *durationEventMessage);

/******************************************************************************/
/* Global Variable Declarations :                                             */
/******************************************************************************/

extern bool     apv_nRF24L01CoreActiveFlag[APV_nRF24L01_DUPLEX_IDS];
extern bool     apv_nRF24L01CoreSchedulingFlag;

extern const    apv_nRF24L01RegisterDefinition_t        apv_nRF24L01RegisterDefinitions[APV_NRF24L01_REGISTER_ADDRESS_SET];
extern          apv_nRF24L01Registers_t                 apv_nRF24L01Registers[APV_nRF24L01_DUPLEX_IDS][APV_NRF24L01_REGISTER_ADDRESS_SET];
extern          apv_nRF24L01DataPipeAddressDefinition_t apv_nRF24L01DataPipeAddresses[APV_nRF24L01_DATA_PIPE_SET];

extern          bool                                    nRF24L01ChipEnableState[APV_nRF24L01_DUPLEX_IDS];

extern volatile bool                                    apv_nRF24L01TimerFlag;

extern const    uint8_t                                 apv_nRF24L01MasterBeaconPattern[APV_NRF24L01_MASTER_BEACON_PATTERN_LENGTH];

/******************************************************************************/

#endif

/******************************************************************************/
/* (C) PulsingCoreSoftware Limited 2018 (C)                                   */
/******************************************************************************/

