/******************************************************************************/
/* (C) PulsingCoreSoftware Limited 2018 (C)                                   */
/******************************************************************************/
/*                                                                            */
/* ApvMain.c                                                                  */
/* 15.03.18                                                                   */
/* Paul O'Brien                                                               */
/*                                                                            */
/******************************************************************************/
/* Include Files :                                                            */
/******************************************************************************/

#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <sam3x8e.h>
#include <core_cm3.h>
#include "ApvSerial.h"
#include "ApvEventTimers.h"
#include "ApvSystemTime.h"
#include "ApvPeripheralControl.h"
#include "ApvPeripheralControlUSART.h"
#include "ApvControlPortProtocol.h"
#include "ApvMessageHandling.h"
#include "ApvMessagingLayerManager.h"
#include "ApvLsm9ds1.h"
#include "ApvUtilities.h"
#include "apv_RFM69HCW.h"
#include "apv_nRF24L01.h"

/******************************************************************************/
/* Constant Definitions :                                                     */
/******************************************************************************/

#define APV_RUN_TIME_TX_MODULUS ((uint64_t)1024)

/******************************************************************************/
/* Local Variable Definitions :                                               */
/******************************************************************************/

static apvArduinoSystemIdentifier_t apvArduinoSystemIdentifier = APV_ARDUINO_SYSTEM_IDENTITY_NONE;

static uint32_t                     apvDurationTimer0Index     = APV_DURATION_TIMER_NULL_INDEX; // the first countdown timer is an 150usecs timer

// The main loop runs at one-millisecond intervals. Processes that do not require 
// high-resolution can subdivide this counter which runs periodically from 0 - 
// 999
static uint16_t                     apvOneMillisecondCounter   = APV_ONE_MILLISECOND_COUNTER_MINIMUM,
                                    apvOneSecondCounter        = APV_ONE_SECOND_COUNTER_MINIMUM;
static bool                         apvHeartbeatPulse          = false;
static volatile uint32_t            apvHeartbeatSelect         = 0;

/******************************************************************************/
/* Local Function Declarations :                                              */
/******************************************************************************/

int main(void);

/******************************************************************************/
/* Function Definitions :                                                     */
/******************************************************************************/

int main(void)
  {
/******************************************************************************/

           APV_SERIAL_ERROR_CODE  apvSerialErrorCode        = APV_SERIAL_ERROR_CODE_NONE;
  volatile uint64_t               apvRunTimeCounter         = 0,
                                  apvRunTimeCounterOld      = 0;

           bool                   apvPrimarySerialPortStart = false;

           uint16_t               components                = 0,
                                  messageCount              = 0;

           int16_t                interruptSource           = 0;
           uint8_t                interruptPriority         = 0;

/******************************************************************************/

  // Create the serial UART inter-messaging layer message buffers
  apvSerialErrorCode = apvCreateMessageBuffers(&apvMessageSerialUartFreeBufferSet,
                                               &apvMessageSerialUartFreeBuffers[0],
                                                APV_MESSAGE_FREE_BUFFER_SET_SIZE);

  // Create messaging layer handler function inter-layer free message buffers
  apvSerialErrorCode = apvCreateMessageBuffers(&apvMessagingLayerFreeBufferSet,
                                               &apvMessagingLayerFreeBuffers[0],
                                                APV_MESSAGING_LAYER_FREE_MESSAGE_BUFFER_SET_SIZE);

  // Initialise the array of message layer handling components
  apvSerialErrorCode = apvMessagingLayerComponentInitialise(&apvMessagingLayerComponents[0],
                                                             APV_MESSAGING_LAYER_COMPONENT_ENTRIES);

  // Load the serial UART messaging layer received message interpreter
  apvSerialErrorCode = apvMessagingLayerComponentLoad( APV_PLANE_SERIAL_UART_CONTROL_0,
                                                      &apvMessagingLayerComponents[0],
                                                       APV_MESSAGING_LAYER_COMPONENT_ENTRIES,
                                                      &apvMessageSerialUartFreeBufferSet,            // serial UART free buffer set/pool
                                                      &apvMessagingLayerFreeBufferSet,               // messaging layer free buffer set/pool
                                                      &apvMessagingLayerComponentSerialUartRxBuffer, // serial UART component input ring
                                                       APV_COMMS_PLANE_SERIAL_UART,
                                                       APV_SIGNAL_PLANE_CONTROL_0,
                                                      &apvMessagingLayerSerialUARTInputHandler);

  // Load the serial UART messaging layer transmit message handler
  apvSerialErrorCode = apvMessagingLayerComponentLoad( APV_PLANE_SERIAL_UART_CONTROL_1,
                                                      &apvMessagingLayerComponents[0],
                                                       APV_MESSAGING_LAYER_COMPONENT_ENTRIES,
                                                      &apvMessagingLayerFreeBufferSet,               // messaging layer free buffer set/pool
                                                      &apvMessageSerialUartFreeBufferSet,            // serial UART free buffer set/pool
                                                      &apvMessagingLayerComponentSerialUartTxBuffer, // serial UART component output ring
                                                       APV_COMMS_PLANE_SERIAL_UART,
                                                       APV_SIGNAL_PLANE_CONTROL_1,
                                                      &apvMessagingLayerSerialUARTOutputHandler);

  // Load the SPI0 Lsm9ds1 messaging layer format handler - this has no (direct) hardware termination
  apvSerialErrorCode = apvMessagingLayerComponentLoad( APV_PLANE_SPI_CONTROL_0_0,
                                                      &apvMessagingLayerComponents[0],
                                                       APV_MESSAGING_LAYER_COMPONENT_ENTRIES,
                                                      &apvMessagingLayerFreeBufferSet,               // messaging layer free buffer set/pool
                                                      &apvMessagingLayerFreeBufferSet,               // messaging layer free buffer set/pool
                                                      &apvMessagingLayerComponentSpi0Buffer,         // SPI0 formatting component input ring
                                                       APV_COMMS_PLANE_SPI_0,
                                                       APV_SIGNAL_PLANE_CONTROL_0,
                                                      &messagingLayerSpi0Lsm9ds1FormatHandler);

  /******************************************************************************/
  /* Set all the interrupt source priorities to the lowest possible, for all    */
  /* configurable priorities                                                    */
  /******************************************************************************/

  for (interruptSource = APV_SYSTEM_INTERRUPT_ID_MEMORY_MANAGEMENT_FAULT; interruptSource < APV_PERIPHERAL_IDS; interruptSource++)
    {
    if (apvGetInterruptPriority( interruptSource,
                                &interruptPriority) == APV_ERROR_CODE_NONE)
      {
      apvSetInterruptPriority( interruptSource,
                               APV_DEVICE_INTERRUPT_PRIORITY_BASE,
                              &apvInterruptPriorities[0]);
      }
    }

  /******************************************************************************/
  /* Elevate the priority of the elapsed timer block core timer "SysTick"       */
  /******************************************************************************/

  apvSetInterruptPriority( APV_SYSTEM_INTERRUPT_ID_SYSTEM_TICK,
                           APV_DEVICE_INTERRUPT_PRIORITY_SYSTICK,
                          &apvInterruptPriorities[0]);

  /******************************************************************************/

  apvSerialErrorCode = apvInitialiseEventTimerBlocks(&apvEventTimerBlock[APV_EVENT_TIMER_0],
                                                      TCCHANNEL_NUMBER);

  apvSerialErrorCode = apvAssignEventTimer( APV_EVENT_TIMER_GENERAL_PURPOSE_ID,
                                           &apvEventTimerBlock[APV_EVENT_TIMER_0], // BASE ADDRESS
                                            apvEventTimerChannel0CallBack);

  // Set the general-purpose (system tick) timebase to 1 millisecond (1000000 nanoseconds)
  apvEventTimerGeneralPurposeTimeBaseTarget = APV_EVENT_TIMER_GENERAL_PURPOSE_TIME_BASE;

  apvSerialErrorCode = apvConfigureWaveformEventTimer( APV_EVENT_TIMER_GENERAL_PURPOSE_ID,
                                                      &apvEventTimerBlock[APV_EVENT_TIMER_0],
                                                       APV_EVENT_TIMER_CHANNEL_TIMER_CLOCK_0,
                                                       apvEventTimerGeneralPurposeTimeBaseTarget, // nanoseconds,
                                                       true,                                      // enable the RC3 interrupt
                                                       APV_EVENT_TIMER_CHANNEL_TIMER_XC0_NONE,
                                                       APV_EVENT_TIMER_CHANNEL_TIMER_XC1_NONE,   
                                                       APV_EVENT_TIMER_CHANNEL_TIMER_XC2_NONE);

  // SWITCH ON THE NVIC/TIMER IRQ
  apvSerialErrorCode = apvSwitchNvicDeviceIrq(APV_EVENT_TIMER_GENERAL_PURPOSE_ID,
                                              true);

  apvSerialErrorCode = apvSwitchPeripheralClock(ID_TC0, // switch on the timebase peripheral clock
                                                true);

  apvSerialErrorCode = apvSwitchWaveformEventTimer( APV_EVENT_TIMER_GENERAL_PURPOSE_ID,
                                                   &apvEventTimerBlock[APV_EVENT_TIMER_0],
                                                    true);

  // Default to using the UART for primary serial comms
  apvSerialErrorCode = apvSerialCommsManager(APV_PRIMARY_SERIAL_PORT_UART,
                                             APV_PRIMARY_SERIAL_RING_BUFFER_SET);

  apvSerialErrorCode = apvSwitchPeripheralLines(ID_UART, // switch on the primary serial port peripheral
                                                true);

  apvSerialErrorCode = apvSwitchPeripheralClock(ID_UART, // switch on the primary serial port peripheral clock
                                                true);

  apvSerialErrorCode = apvControlUart(APV_UART_CONTROL_ACTION_RESET);
  apvSerialErrorCode = apvControlUart(APV_UART_CONTROL_ACTION_RESET_STATUS);
  apvSerialErrorCode = apvControlUart(APV_UART_CONTROL_ACTION_ENABLE);

  apvSerialErrorCode = apvConfigureUart(APV_UART_PARITY_NONE,
                                        APV_UART_CHANNEL_MODE_NORMAL,
                                        APV_UART_BAUD_RATE_SELECT_19200);

  // SWITCH ON THE NVIC/UART IRQ
  apvSerialErrorCode = apvUartSwitchInterrupt(APV_UART_INTERRUPT_SELECT_RECEIVE,
                                              true);

  apvSerialErrorCode = apvSwitchNvicDeviceIrq(APV_PERIPHERAL_ID_UART,
                                              true);

  /******************************************************************************/
  /* GPS Receiver USARTx Setup :                                                */
  /* - setup the USART used for the GPS receiver                                */
  /******************************************************************************/
    
  apvSerialErrorCode = apvGpsReceiverCommsSetup(APV_USART_0);
  
  /******************************************************************************/
  /* SPI0 Setup :                                                               */
  /* - first create the chip-select-related transmit and receive FIFOs          */
  /******************************************************************************/

  apvSerialErrorCode =  apvCreateFIFO(&apvSpi0TxFIFO,
                                       APV_FIFO_MAXIMUM_ENTRY_DEPTH,
                                      &apvSpi0TxFIFOBuffer[0]);

  apvSerialErrorCode =  apvCreateFIFO(&apvSpi0ChipSelectFIFO[APV_SPI_FIXED_CHIP_SELECT_0],
                                       APV_FIFO_MAXIMUM_ENTRY_DEPTH,
                                      &apvSpi0ChipSelectFIFOBuffer[APV_SPI_FIXED_CHIP_SELECT_0][0]);

  apvSerialErrorCode =  apvCreateFIFO(&apvSpi0ChipSelectFIFO[APV_SPI_FIXED_CHIP_SELECT_1],
                                       APV_FIFO_MAXIMUM_ENTRY_DEPTH,
                                      &apvSpi0ChipSelectFIFOBuffer[APV_SPI_FIXED_CHIP_SELECT_1][0]);

  apvSerialErrorCode =  apvCreateFIFO(&apvSpi0ChipSelectFIFO[APV_SPI_FIXED_CHIP_SELECT_2],
                                       APV_FIFO_MAXIMUM_ENTRY_DEPTH,
                                      &apvSpi0ChipSelectFIFOBuffer[APV_SPI_FIXED_CHIP_SELECT_2][0]);

  apvSerialErrorCode = apvSwitchPeripheralClock(APV_PERIPHERAL_ID_SPI0, // switch on SPI0
                                                true);

  apvSerialErrorCode = apvSPIEnable(ApvSpi0ControlBlock_p,
                                    true);

  /******************************************************************************/
  /* SPI0 operations are initially setup for the Lsm9ds1 on chip-select 0       */
  /******************************************************************************/
  // Operatimg mode is :
  //  - MASTER
  //  - chip-select set by registers SPI0_CSR[0 .. 3]
  //  - chip-selects are connected to one device each
  //  - fault detection mode is OFF
  //  - data receiver must NOT be read before the next transmit event
  //  - loopback is OFF
  //  - chip-select delay is 6 * MCK
  apvSerialErrorCode = apvSPISetOperatingMode(ApvSpi0ControlBlock_p,
                                              APV_SPI_MASTER_MODE,
                                              APV_SPI_PERIPHERAL_SELECT_VARIABLE,
                                              APV_SPI_CHIP_SELECT_DECODE_DIRECT,
                                              APV_SPI_MODE_FAULT_DETECTION_DISABLED,
                                              APV_SPI_WAIT_ON_DATA_READ_DISABLED,
                                              APV_SPI_LOOPBACK_DISABLED,
                                              APV_SPI_CHIP_SELECT_DELAY_MINIMUM_nS);

  // Chip-select mode is :
  //  - SPI0_CSR[0]
  //  - chip-select "inactive" is logic '1'
  //  - data CHANGES on the LEADING edge of "SPCK" and is sampled on the edge "FOLLOWING"
  //  - chip-select ALWAYS rises after each data transfer ON THE SAME SLAVE
  //  - chip-select ALWAYS rises on ANY slave
  //  - data bit width   == 16
  //  - serial baud rate == 312 kbps
  //  - pre-SPCK delay   == 4 * MCK
  //  - inter-transfer delay BEFORE chip-select de-assert == 0
  /*apvSerialErrorCode = apvSetChipSelectCharacteristics(ApvSpi0ControlBlock_p,
                                                       APV_SPI_FIXED_PERIPHERAL_CHIP_SELECT_xxx0,
                                                       APV_SPI_SERIAL_CLOCK_POLARITY_ZERO,
                                                       APV_SPI_SERIAL_CLOCK_PHASE_DATA_CHANGE_FOLLOWING,
                                                       APV_SPI_CHIP_SELECT_SINGLE_SLAVE_RISE,
                                                       APV_SPI_CHIP_SELECT_CHANGE_SLAVE_RISE,
                                                       APV_SPI_MAXIMUM_BIT_TRANSFER_WIDTH,
                                                       APV_SPI_BAUD_RATE_SELECT_312K5,
                                                       APV_SPI_FIRST_SPCK_TRANSITION_DELAY_nS,
                                                       APV_SPI_INTER_TRANSFER_DELAY); */

  /******************************************************************************/
  /* Enable all the available SPI0 I/O lines - this includes the three chip-    */
  /* select lines NPCS0 - 2 which are used for Lsm9ds1 (CS0), nRF24L01 Tx (CS1) */
  /* and nRF24L01 Rx (CS2)                                                      */
  /******************************************************************************/

  apvSerialErrorCode = apvSwitchPeripheralLines(ID_SPI0,
                                                true);

  apvSerialErrorCode = apvSPISwitchInterrupt(ApvSpi0ControlBlock_p,
                                             APV_SPI_INTERRUPT_SELECT_RECEIVE_DATA,
                                             true);

  apvSerialErrorCode = apvSwitchNvicDeviceIrq(APV_PERIPHERAL_ID_SPI0,
                                              true);

#if (0)
 while (true)
    {
    volatile uint8_t  spiTransmitPrefix    = 0x8f;
    volatile uint8_t  spiTransmitTraffic   = 0x00; //0x8f; // bit 7 (READ) == 1, address 0:6 0x0f "WHO_AM_I" // 0x00;
    volatile uint8_t  spiChipSelectOut     = 0x00;
    volatile uint16_t spiReceivedCharacter = 0;
    volatile uint8_t  spiChipSelectIn      = 0;

    apvSerialErrorCode = apvSPITransmitCharacter(ApvSpi0ControlBlock_p,
                                                 spiTransmitPrefix,
                                                 spiTransmitTraffic,
                                                 spiChipSelectOut,
                                                 APV_SPI_LAST_TRANSFER_ACTIVE,
                                                 true);

    apvSerialErrorCode = apvSpiReceiveCharacter( ApvSpi0ControlBlock_p,
                                                &spiReceivedCharacter,
                                                &spiChipSelectIn);
    }
#endif

  /******************************************************************************/

  apvSerialErrorCode = apvSwitchPeripheralClock(ID_RTT, // switch on the core timer
                                                true);

  apvSerialErrorCode = apvInitialiseSystemTimer(&apvCoreTimeBaseBlock,
                                                 APV_SYSTEM_TIMER_CLOCK_MINIMUM_PERIOD);

  /* apvSerialErrorCode = apvInitialiseCoreTimer(&apvCoreTimeBaseBlock,
                                               APV_CORE_TIMER_CLOCK_MINIMUM_INTERVAL); */

  // Assign a dummy SPI timer for this test
  /* apvSerialErrorCode = apvCreateDurationTimer(&apvCoreTimeBaseBlock,
                                               apvSpiStateTimer,
                                               APV_DURATION_TIMER_TYPE_PERIODIC,
                                               APV_CORE_TIMER_CLOCK_MINIMUM_INTERVAL,
                                               APV_DURATION_TIMER_SOURCE_RTT,
                                              &spiTimerIndex); */

  apvSerialErrorCode = apvCreateDurationTimer(&apvCoreTimeBaseBlock,
                                               apvDurationStateTimer,
                                               NULL,
                                               APV_DURATION_TIMER_TYPE_PERIODIC,
                                               APV_SYSTEM_TIMER_CLOCK_MINIMUM_PERIOD,
                                               APV_DURATION_TIMER_SOURCE_SYSTICK,
                                               APV_DURATION_TIMER_STATE_RUNNING,
                                              &apvDurationTimer0Index);

  // Switch on the peripheral I/O "C" channel clock
  apvSerialErrorCode = apvSwitchPeripheralClock(ID_PIOC,
                                                true);

  // Switch on the system identity code I/O lines
  apvSerialErrorCode = apvSwitchDigitalLines(APV_DIGITAL_ID_IDENTITY_SELECT,
                                             true);

  // Read the system identity
  apvSerialErrorCode = apvReadSystemIdentity(&apvArduinoSystemIdentifier);

  // If the identifier is unknown we can go no further...
  if ((apvArduinoSystemIdentifier == APV_ARDUINO_SYSTEM_IDENTITY_NONE) || (apvArduinoSystemIdentifier >= APV_ARDUINO_SYSTEM_IDENTITY_NONE_8))
    {
    while (true)
      ;
    }

  /******************************************************************************/

  // Switch on the timer strobe
  apvSerialErrorCode = apvSwitchResourceLines(APV_RESOURCE_ID_STROBE_0,
                                              true);

  // Switch on the core timer interrupt
  /* apvSerialErrorCode = apvSwitchNvicDeviceIrq(APV_CORE_TIMER_ID,
                                              true); */

  apvSerialErrorCode = apvStartSystemTimer(&apvCoreTimeBaseBlock);

  /******************************************************************************/
  /* This delay is to allow the system to settle down...                        */
  /******************************************************************************/

#if (0)
  {
  volatile bool     apvLsm9ds1Delay      = false;
           uint32_t apvLsm9ds1DelayIndex = 0;
  
  apvSerialErrorCode = apvCreateDurationTimer(&apvCoreTimeBaseBlock,
                                               apvLsm9ds1DelayStateTimer,
                                              (void *)&apvLsm9ds1Delay,
                                               APV_DURATION_TIMER_TYPE_ONE_SHOT,
                                               APV_LSM9S1_INITIALISATION_DELAY_PERIOD,
                                               APV_DURATION_TIMER_SOURCE_SYSTICK,
                                               APV_DURATION_TIMER_STATE_RUNNING,
                                              &apvLsm9ds1DelayIndex);
  
  while(apvLsm9ds1Delay == false)
    ;
  
  apvSerialErrorCode = apvDestroyDurationTimer(&apvCoreTimeBaseBlock,
                                               &apvLsm9ds1DelayIndex);
  }
#endif

#if (0)

  /******************************************************************************/
  /* Initialise the nRF24L01 transmit and receive nRF24L01 wireless'            */
  /******************************************************************************/

  {
  volatile bool     apvLsm9ds1Delay      = false;
           uint32_t apvLsm9ds1DelayIndex = 0;

  apvSerialErrorCode = apvCreateDurationTimer(&apvCoreTimeBaseBlock,
                                               apvLsm9ds1DelayStateTimer,
                                              (void *)&apvLsm9ds1Delay,
                                               APV_DURATION_TIMER_TYPE_ONE_SHOT,
                                               APV_LSM9S1_INITIALISATION_DELAY_PERIOD,
                                               APV_DURATION_TIMER_SOURCE_SYSTICK,
                                               APV_DURATION_TIMER_STATE_RUNNING,
                                              &apvLsm9ds1DelayIndex);

  while(apvLsm9ds1Delay == false)
    ;

  apvSerialErrorCode = apvDestroyDurationTimer(&apvCoreTimeBaseBlock,
                                               &apvLsm9ds1DelayIndex);

  apvSerialErrorCode = apvInitialisation_nRF24L01(&apvCoreTimeBaseBlock,
                                                  &apvArduinoSystemIdentifier,
                                                   APV_nRF24L01_DUPLEX_ID_TX);
  }
#endif

#if (0)
  apvSerialErrorCode = apvInitialisation_nRF24L01(&apvCoreTimeBaseBlock,
                                                  &apvArduinoSystemIdentifier,
                                                   APV_nRF24L01_DUPLEX_ID_RX);
#endif

 /******************************************************************************/
  /* Initialise the LDS9DS1 9DOF inertial platform :                            */
  /* - the device has a considerable startup time ~ 110ms. This startup time    */
  /*   is needed AFTER device initialisation!                                   */
  /******************************************************************************/

#if (0)
  apvSerialErrorCode = apvSPISwitchInterrupt(ApvSpi0ControlBlock_p,
                                             APV_SPI_INTERRUPT_SELECT_RECEIVE_DATA,
                                             false);

  apvSerialErrorCode = apvSPIEnable(ApvSpi0ControlBlock_p,
                                    false);

//  apvSerialErrorCode = apvSPIReset(ApvSpi0ControlBlock_p);

  apvSerialErrorCode = apvSPIEnable(ApvSpi0ControlBlock_p,
                                    true);

  apvSerialErrorCode = apvSPISwitchInterrupt(ApvSpi0ControlBlock_p,
                                              APV_SPI_INTERRUPT_SELECT_RECEIVE_DATA,
                                              true);
#endif

  {
  volatile bool     apvLsm9ds1Delay      = false;
           uint32_t apvLsm9ds1DelayIndex = 0;

  apvSerialErrorCode = apvCreateDurationTimer(&apvCoreTimeBaseBlock,
                                               apvLsm9ds1DelayStateTimer,
                                              (void *)&apvLsm9ds1Delay,
                                               APV_DURATION_TIMER_TYPE_ONE_SHOT,
                                               APV_LSM9S1_INITIALISATION_DELAY_PERIOD,
                                               APV_DURATION_TIMER_SOURCE_SYSTICK,
                                               APV_DURATION_TIMER_STATE_RUNNING,
                                              &apvLsm9ds1DelayIndex);

  while(apvLsm9ds1Delay == false)
    ;

  apvSerialErrorCode = apvDestroyDurationTimer(&apvCoreTimeBaseBlock,
                                               &apvLsm9ds1DelayIndex);
  }

  apvSerialErrorCode = apvInitialiseLsm9ds1(&apvCoreTimeBaseBlock,
                                             APV_SPI_FIXED_CHIP_SELECT_0);

  /******************************************************************************/
  /* If the Lsm9ds1 has initialised properly switch on the routine process to   */
  /* read the accelerometers and gyros                                          */
  /******************************************************************************/

  if (apvSerialErrorCode == APV_SERIAL_ERROR_CODE_NONE)
    {
    apvLsm9ds1CoreActiveFlag = true;
    }

  /******************************************************************************/
  /* Initialise the RFM69HCW/SX1231 LoRa radio chip                             */
  /******************************************************************************/

  // Switch on the peripheral I/O "D" channel clock
  //apvSerialErrorCode = apvSwitchPeripheralClock(ID_PIOD,
  //                                              true);

  apvSerialErrorCode = apvInitialiseRfm69Hcw(&apvCoreTimeBaseBlock,
                                              APV_SPI_FIXED_CHIP_SELECT_1);

  /******************************************************************************/
  /* If the Rfm69Hcw has initialised properly enable it's use                   */
  /******************************************************************************/

  if (apvSerialErrorCode == APV_SERIAL_ERROR_CODE_NONE)
    {
    apvRfm69HcwCoreActiveFlag = true;
    }

/******************************************************************************/

/*  apvSerialErrorCode = apvStartDurationTimer(&apvCoreTimeBaseBlock,
                                              apvDurationTimer0Index); */

/******************************************************************************/
/* Enable LED 'L' as the heartbeat indicator                                  */
/******************************************************************************/

  // Switch on the peripheral I/O "B" channel clock
  apvSerialErrorCode = apvSwitchPeripheralClock(ID_PIOB,
                                                true);

  // Switch on the system heartbeat LED lines
  apvSerialErrorCode = apvSwitchDigitalLines(APV_DIGITAL_HEARTBEAT_LED_SELECT,
                                             true);

/******************************************************************************/


  if (apvSerialErrorCode == APV_SERIAL_ERROR_CODE_NONE)
    {
    while (true)
      {
       APV_CRITICAL_REGION_ENTRY();

       /******************************************************************************/
       /* This is the "slow" loop timer event detector                               */
       /******************************************************************************/

       if (apvEventTimerHotShot.Flags.APV_EVENT_TIMER_CHANNEL_0_FLAG == APV_EVENT_TIMER_FLAG_SET)
         {
         apvEventTimerHotShot.Flags.APV_EVENT_TIMER_CHANNEL_0_FLAG = APV_EVENT_TIMER_FLAG_CLEAR;

         apvRunTimeCounterOld = apvRunTimeCounter;

         apvRunTimeCounter    = apvRunTimeCounter + 1;
         }

       /******************************************************************************/
       /* This is the "fast" loop timer core-timer flag detector. The core-timer is  */
       /* used to derive a number of slower "process" timers                         */
       /******************************************************************************/

       if (apvCoreTimerFlag == APV_CORE_TIMER_FLAG_HIGH)
         {
         apvCoreTimerBackgroundFlag = apvCoreTimerFlag;
         apvCoreTimerFlag           = APV_CORE_TIMER_FLAG_LOW;
         }

       APV_CRITICAL_REGION_EXIT();

       /******************************************************************************/
       /* FOR NOW, AT EACH MILLISECOND...                                            */
       /* 27.07.18 : despite the Rowley debugger insisting this event happens every  */
       /*            "1100-(odd)" cycles (~15us ?) this has been timed by hardware   */
       /*            pin strobes at an oscilloscope as occurring bang-on 1ms!        */
       /******************************************************************************/

       if (apvRunTimeCounterOld != apvRunTimeCounter)
         {
         apvRunTimeCounterOld    = apvRunTimeCounter;

       /******************************************************************************/
       /* *** TEST THE SENSE OF THE nRF24L01 chip-enable :                           */
       /******************************************************************************/

         if (nRF24L01ChipEnableState[APV_nRF24L01_DUPLEX_ID_TX] == true)
           {
           nRF24L01ChipEnableState[APV_nRF24L01_DUPLEX_ID_TX] = false;
           nRF24L01ChipEnableState[APV_nRF24L01_DUPLEX_ID_RX] = true;
           }
         else
           {
           nRF24L01ChipEnableState[APV_nRF24L01_DUPLEX_ID_TX] = true;
           nRF24L01ChipEnableState[APV_nRF24L01_DUPLEX_ID_RX] = false;
           }

         apv_nRF24L01ChipEnableControl( APV_nRF24L01_DUPLEX_ID_TX,
                                       &nRF24L01ChipEnableState[APV_nRF24L01_DUPLEX_ID_TX]);

         apv_nRF24L01ChipEnableControl( APV_nRF24L01_DUPLEX_ID_RX,
                                       &nRF24L01ChipEnableState[APV_nRF24L01_DUPLEX_ID_RX]);
       // Alternatively...
       // apv_nRF24L01ChipEnableControl( APV_nRF24L01_DUPLEX_ID_RX,
       //                                &nRF24L01ChipEnableState);

       /******************************************************************************/
       /* *** TEST THE SENSE OF THE nRF24L01 chip-enable :                           */
       /******************************************************************************/

         /******************************************************************************/
         /* Low-level message input de-framing is handled here :                       */
         /*                                                                            */
         /*        *** MESSAGE DEFRAMING : LOW-LEVEL MESSAGE STATE-MACHINES ***        */
         /*                                                                            */
         /******************************************************************************/
         /* The first level of wired (serial port) I/O is a framed message defined by  */
         /* a finite-state-machine.                                                    */
         /******************************************************************************/

         if (apvPrimarySerialPortStart == false)
           {
           apvPrimarySerialPortStart = true;

           // Initialise the lowest-level serial comms frame receiver state machine
           apvSerialErrorCode = apvDeFrameMessageInitialisation( apvPrimarySerialCommsReceiveBuffer,
                                                                &apvMessageSerialUartFreeBufferSet,
                                                                &apvMessagingDeFramingStateMachine[0]);
           }

         /******************************************************************************/
         /* BEWARE THE DEBUGGER! With the optimisation level set to 0 the debugger can */
         /* enable the virtual printf channel. THIS KILLS THE PROCESSOR! The result is */
         /* an apparent inability to keep up with even the slowest messaging. Setting  */
         /* the optimisation level to '1' disables the virtual printf channel. Using a */
         /* couple of "message successful/unsuccessful" counters demonstrates that all */
         /* is well and the processor has no problems with continuous 19.2Kbps message */
         /* arrival                                                                    */
         /******************************************************************************/

         // Run the serial UART comms message state-machine
         if ((apvOneMillisecondCounter & APV_ONE_MILLISECOND_DEFRAMER_TASK_TRIGGER) == APV_ONE_MILLISECOND_DEFRAMER_TASK_TRIGGER)
           {
           apvSerialErrorCode = apvDeFrameMessage(&apvMessagingDeFramingStateMachine[0]);
           }

         /******************************************************************************/
         /* The second level of any message activity is handled here :                 */
         /*                                                                            */
         /*               *** MESSAGING LAYER : COMPONENT HANDLERS ***                 */
         /*                                                                            */
         /******************************************************************************/
         /*               *** ATTENTION - ATTENTION - ATTENTION ***                    */
         /* THE DEFRAMER HAS DIRECT ACCESS TO THE MESSAGING COMPONENT ARRAY. MESSAGES  */
         /* DO NOT HAVE TO GO THROUGH THE SERIAL UART COMPONENTS! IF A COMPONENT IS TO */
         /* DIRECTLY TERMINATE THE DEFRAMER IT MUST HAVE IT'S INPUT BUFFER SOURCE SET  */
         /* TO THE SERIAL UART BUFFER SET, NOT THE FREE SET! OTHERWISE THE SERIAL      */
         /* UART BUFFER SET GETS CONSUMED IF THEY ARE NOT RELEASED IN THE COMPONENT!   */
         /*  - what this means in practise is that a message with a legal source plane */
         /*    and control index can be directed straight to the component without an  */
         /*    intermediary decode in the serial UART input-handling component         */
         /*               *** ATTENTION - ATTENTION - ATTENTION ***                    */
         /******************************************************************************/
         /* All messaging-layer component handling is data-driven. Each component      */
         /* handler has in input port and an output port - activity is triggered by    */
         /* messages arriving at the input ports :                                     */
         /******************************************************************************/
         /* A communications channel is usually two components, one input and one      */
         /* output. To remove the effect of the linear search on a channel i.e. if the */
         /* input component is earlier than the output component the channel gets two  */
         /* bites of the cherry because the input component sends a message to the     */
         /* output component, the "ready" state is read first for the whole list       */
         /* before acting on it                                                        */
         /******************************************************************************/

         for (components = 0; components < APV_MESSAGING_LAYER_COMPONENT_ENTRIES_SIZE; components++)
           { // Visit each CHANNEL once at each pass
           if (apvMessagingLayerComponents[components].messagingLayerComponentLoaded == true)
             { // Check if the component is loaded
             if (apvRingBufferReportFillState( apvMessagingLayerComponents[components].messagingLayerInputBuffers,
                                              &messageCount,
                                               true) == APV_ERROR_CODE_NONE)
               { // Only service components that have messages waiting at their input ports
               if (messageCount != 0)
                 {
                 apvMessagingLayerComponentReady[components] = true;
                 }
               else
                 {
                 apvMessagingLayerComponentReady[components] = false;
                 }
               }
             }
           }

         for (components = 0; components < APV_MESSAGING_LAYER_COMPONENT_ENTRIES_SIZE; components++)
           {
           if (apvMessagingLayerComponentReady[components] == true)
             {
             apvMessagingLayerComponents[components].messagingLayerServiceManager(&apvMessagingLayerComponents[components],
                                                                                  (apvMessagingLayerComponent_t *)&apvMessagingLayerComponents);
             }
           }

         /******************************************************************************/

#if (0)
       if (receiveInterrupt == true)
         {
         // Try and get a new receive ring-buffer from the queue
         if (apvRingBufferUnLoad( apvUartPortPrimaryReceiveRingBuffer_p,
                                 (uint32_t *)&apvReceiveBuffer_p,
                                  sizeof(uint8_t),
                                  true) != 0)
           {
           // Put the buffer on the transmit queue
           apvRingBufferLoad( apvUartPortPrimaryTransmitRingBuffer_p,
                             (uint32_t *)&apvReceiveBuffer_p,
                              sizeof(uint8_t),
                              true);

           // Flag the transmitter new characters are available
           transmitInterrupt = true;
           }

         receiveInterrupt = false;
         }

       if (transmitInterrupt == true)
         {
         if (transmitInterruptTrigger == false)
           {
           transmitInterruptTrigger = true;

           if (apvUartBufferTransmitPrime( ApvUartControlBlock_p,
                                           apvUartPortPrimaryTransmitRingBuffer_p,
                                          &apvPrimarySerialCommsTransmitBuffer) != APV_ERROR_CODE_NONE)
             {
             // That didn't go well!
             transmitInterrupt        = false;
             transmitInterruptTrigger = false;
             }
           }
         }
       else
         {
         transmitInterruptTrigger = false;
         }

       __enable_irq();
#endif

         /******************************************************************************/
         /* Run scheduled tasks in the background                                      */
         /******************************************************************************/
         /* TASK 1 : run the Lsm9ds1 accelerometer and gyro process : the process runs */
         /*          as a multi-stage state machine and the initiating flag can remain */
         /*          active for a number of loop iterations                            */
         /******************************************************************************/

         // Set the task trigger
         if ((apvOneMillisecondCounter == APV_ONE_MILLISECOND_LSM9DS1_TASK_TRIGGER) && (apvLsm9ds1CoreActiveFlag == true))
           {
           apvLsm9ds1CoreSchedulingFlag = true;
           }

         // Now as long as the trigger stays active the task will run. Internally the task will decide 
         // when the state-machine has completed one round of processing and will de-schedule itself
         if (apvLsm9ds1CoreSchedulingFlag == true)
           {
           apvSerialErrorCode = apvServiceLsm9ds1( ApvSpi0ControlBlock_p,
                                                   APV_SPI_FIXED_CHIP_SELECT_0,
                                                  &apvLsm9ds1MeasurementStateMachine[APV_LSM9DS1_MEASUREMENT_STATE_INITIALISE],
                                                  &apvLsm9ds1MeasurementState,
                                                  &apvLsm9ds1Measurements[APV_LSM9DS1_MEASUREMENT_ACCELEROMETER_X],
                                                  &apvLsm9ds1MeasurementIndex,
                                                  &apvLsm9ds1CoreSchedulingFlag);
           }
         
         /******************************************************************************/
         /* TASK 2 : run the NMEA message decoder : the process runs as a multi-stage  */
         /*          autonomous state machine updating GPS fix and navigation data.    */
         /*          Incoming messaging can be considered to be continuous requiring   */
         /*          the decoder to run at short regular intervals                     */
         /******************************************************************************/

         if (apvOneMillisecondCounter == apvNmeaDecoderCoreSchedule)
            {
            apvNmeaDecoderCoreSchedule = apvNmeaDecoderCoreSchedule + APV_ONE_MILLISECOND_UBLOX6_TASK_TRIGGER;

            apvExecuteNMEAStateMachine(apvGpsReceiverPortReceiveBuffer);
            
            // Roll over the next scheduling interval
            if (apvNmeaDecoderCoreSchedule >= APV_ONE_MILLISECOND_COUNTER_MAXIMUM)
              {
              apvNmeaDecoderCoreSchedule = APV_ONE_MILLISECOND_UBLOX6_TASK_TRIGGER - (apvNmeaDecoderCoreSchedule - APV_ONE_MILLISECOND_COUNTER_MAXIMUM);
              }
            }

         /******************************************************************************/
         /* TASK 3 : run the RFM69HCW housekeeping process once per scheduling loop    */
         /******************************************************************************/

         // Set the (possible) task trigger
         if ((apvOneMillisecondCounter  == APV_ONE_MILLISECOND_RFM69HCW_TASK_TRIGGER) && 
             (apvRfm69HcwCoreActiveFlag == true))
           {
           apvRfm69HcwCoreSchedulingFlag = true;
           }
             
         if ((apvTerminateRfm69HcwManualReset(NULL) == true) && (apvRfm69HcwCoreSchedulingFlag == true))
           {
           }
         
         /******************************************************************************/
         /* Increment and wrap the one-millisecond timer coarse subdivision counter    */
         /******************************************************************************/

         apvOneMillisecondCounter = apvOneMillisecondCounter + 1;

         if (apvOneMillisecondCounter >= APV_ONE_MILLISECOND_COUNTER_MAXIMUM)
           {
           apvOneMillisecondCounter = APV_ONE_MILLISECOND_COUNTER_MINIMUM;

                   /******************************************************************************/
           /* Increment and wrap the one-second timer coarse subdivision counter         */
           /******************************************************************************/

                   apvOneSecondCounter = apvOneSecondCounter + 1;

                   if (apvOneSecondCounter >= APV_ONE_SECOND_COUNTER_MAXIMUM)
                          {
                          apvOneSecondCounter = APV_ONE_SECOND_COUNTER_MINIMUM;

              apvHeartbeatSelect = (uint32_t)(1 << APV_ARDUINO_PERIPHERAL_BIT_27);
                          
                          // Flip the "heartbeat" indicator LED
                          if (apvHeartbeatPulse == true)
                                {
                                apvToggleSingleDigitalLine( APV_ARDUINO_PERIPHERAL_SET_B,
                                            apvHeartbeatSelect,
                                           &apvHeartbeatPulse);
                                
                                apvHeartbeatPulse = false;
                                }
                          else
                                {
                                apvToggleSingleDigitalLine( APV_ARDUINO_PERIPHERAL_SET_B,
                                            apvHeartbeatSelect,
                                           &apvHeartbeatPulse);
                                
                                apvHeartbeatPulse = true;
                                }
                          }

                   /******************************************************************************/
           }

         /******************************************************************************/
         /*                     END OF THE ONE-MILLISECOND LOOP                        */
         /******************************************************************************/
         }
      }
    }

/******************************************************************************/

  return(0);

/******************************************************************************/
  } /* end of main                                                            */

/******************************************************************************/
/* (C) PulsingCoreSoftware Limited 2018 (C)                                   */
/******************************************************************************/
