/******************************************************************************/
/* (C) PulsingCoreSoftware Limited 2018 (C)                                   */
/******************************************************************************/
/*                                                                            */
/* nFR24L01+.c                                                                */
/* 23.08.18                                                                   */
/* Paul O'Brien                                                               */
/*                                                                            */
/* - control and status for the nRF24L01 2.4GHz wireless. The APV project     */
/*   uses two radios per system allowing full-duplex communications between   */
/*   the master and the slaves. This requires two SPI0 chip-select lines      */
/*                                                                            */
/******************************************************************************/
/* Include Files :                                                            */
/******************************************************************************/

#include "ApvError.h"
#include "ApvPeripheralControl.h"
#include "ApvEventTimers.h"
#include "apv_nRF24L01.h"

/******************************************************************************/
/* Global Variable Definitions :                                              */
/******************************************************************************/
// Keep track of the transmit and receive chip-select states
bool nRF24L01ChipEnableState[APV_nRF24L01_DUPLEX_IDS] = { false, false };

/******************************************************************************/
/* the nRF24L01 global interrupt flags :                                      */
/******************************************************************************/

bool apv_nRF24L01CoreActiveFlag[APV_nRF24L01_DUPLEX_IDS] = { false, false }; // if the core is detected this flag is set
bool apv_nRF24L01CoreSchedulingFlag                      = false;            // process scheduling flag

/******************************************************************************/
/* Timing flag :                                                              */
/******************************************************************************/

volatile bool apv_nRF24L01TimerFlag  = false;

/******************************************************************************/
/* nRF24L01+ Radio Control and Status Registers :                             */
/******************************************************************************/
// Define the constant part of the nRF24L01+ control and status registers
const apv_nRF24L01RegisterDefinition_t apv_nRF24L01RegisterDefinitions[APV_NRF24L01_REGISTER_ADDRESS_SET] = 
  {
    {
    APV_NRF24L01_REGISTER_ADDRESS_CONFIG,
    APV_NRF24L01_REGISTER_CONFIG_USE_MASK,
    APV_NRF24L01_REGISTER_CONFIG_DEFAULT,
    TRUE
    },
    {
    APV_NRF24L01_REGISTER_ADDRESS_EN_AA,
    APV_NRF24L01_REGISTER_EN_AA_USE_MASK,
    APV_NRF24L01_REGISTER_EN_AA_DEFAULT,
    TRUE
    },
    {
    APV_NRF24L01_REGISTER_ADDRESS_EN_RXADDR,
    APV_NRF24L01_REGISTER_EN_RXADDR_USE_MASK,
    APV_NRF24L01_REGISTER_EN_RXADDR_DEFAULT,
    TRUE
    },
    {
    APV_NRF24L01_REGISTER_ADDRESS_SETUP_AW,
    APV_NRF24L01_REGISTER_SETUP_AW_USE_MASK,
    APV_NRF24L01_REGISTER_SETUP_AW_DEFAULT,
    TRUE
    },
    {
    APV_NRF24L01_REGISTER_ADDRESS_SETUP_RETR,
    APV_NRF24L01_REGISTER_SETUP_RETR_USE_MASK,
    APV_NRF24L01_REGISTER_SETUP_RETR_DEFAULT,
    TRUE
    },
    {
    APV_NRF24L01_REGISTER_ADDRESS_RF_CH,
    APV_NRF24L01_REGISTER_RF_CH_USE_MASK,
    APV_NRF24L01_REGISTER_RF_CH_DEFAULT,
    TRUE
    },
    {
    APV_NRF24L01_REGISTER_ADDRESS_RF_SETUP,
    APV_NRF24L01_REGISTER_RF_SETUP_USE_MASK,
    APV_NRF24L01_REGISTER_RF_SETUP_DEFAULT,
    TRUE
    },
    {
    APV_NRF24L01_REGISTER_ADDRESS_STATUS,
    APV_NRF24L01_REGISTER_STATUS_USE_MASK,
    APV_NRF24L01_REGISTER_STATUS_DEFAULT,
    TRUE
    },
    {
    APV_NRF24L01_REGISTER_ADDRESS_OBSERVE_TX,
    APV_NRF24L01_REGISTER_OBSERVE_TX_USE_MASK,
    APV_NRF24L01_REGISTER_OBSERVE_TX_DEFAULT,
    TRUE
    },
    {
    APV_NRF24L01_REGISTER_ADDRESS_RPD,
    APV_NRF24L01_REGISTER_RPD_USE_MASK,
    APV_NRF24L01_REGISTER_RPD_DEFAULT,
    TRUE
    },
    {
    APV_NRF24L01_REGISTER_ADDRESS_RX_ADDR_P0,
    APV_NRF24L01_REGISTER_RX_ADDR_Pn_USE_MASK,
    APV_NRF24L01_REGISTER_RX_ADDR_P0_DEFAULT_OCTET,
    TRUE
    },
    {
    APV_NRF24L01_REGISTER_ADDRESS_RX_ADDR_P1,
    APV_NRF24L01_REGISTER_RX_ADDR_Pn_USE_MASK,
    APV_NRF24L01_REGISTER_RX_ADDR_P1_DEFAULT_OCTET,
    TRUE
    },
    {
    APV_NRF24L01_REGISTER_ADDRESS_RX_ADDR_P2,
    APV_NRF24L01_REGISTER_RX_ADDR_Pn_USE_MASK,
    APV_NRF24L01_REGISTER_RX_ADDR_P2_DEFAULT_OCTET,
    TRUE
    },
    {
    APV_NRF24L01_REGISTER_ADDRESS_RX_ADDR_P3,
    APV_NRF24L01_REGISTER_RX_ADDR_Pn_USE_MASK,
    APV_NRF24L01_REGISTER_RX_ADDR_P3_DEFAULT_OCTET,
    TRUE
    },
    {
    APV_NRF24L01_REGISTER_ADDRESS_RX_ADDR_P4,
    APV_NRF24L01_REGISTER_RX_ADDR_Pn_USE_MASK,
    APV_NRF24L01_REGISTER_RX_ADDR_P4_DEFAULT_OCTET,
    TRUE
    },
    {
    APV_NRF24L01_REGISTER_ADDRESS_RX_ADDR_P5,
    APV_NRF24L01_REGISTER_RX_ADDR_Pn_USE_MASK,
    APV_NRF24L01_REGISTER_RX_ADDR_P5_DEFAULT_OCTET,
    TRUE
    },
    {
    APV_NRF24L01_REGISTER_ADDRESS_TX_ADDR,
    APV_NRF24L01_REGISTER_TX_ADDR_USE_MASK,
    APV_NRF24L01_REGISTER_TX_ADDR_DEFAULT_OCTET,
    TRUE
    },
    {
    APV_NRF24L01_REGISTER_ADDRESS_RX_PW_P0,
    APV_NRF24L01_REGISTER_RX_PW_Pn_USE_MASK,
    APV_NRF24L01_REGISTER_RX_PW_Pn_DEFAULT,
    TRUE
    },
    {
    APV_NRF24L01_REGISTER_ADDRESS_RX_PW_P1,
    APV_NRF24L01_REGISTER_RX_PW_Pn_USE_MASK,
    APV_NRF24L01_REGISTER_RX_PW_Pn_DEFAULT,
    TRUE
    },
    {
    APV_NRF24L01_REGISTER_ADDRESS_RX_PW_P2,
    APV_NRF24L01_REGISTER_RX_PW_Pn_USE_MASK,
    APV_NRF24L01_REGISTER_RX_PW_Pn_DEFAULT,
    TRUE
    },
    {
    APV_NRF24L01_REGISTER_ADDRESS_RX_PW_P3,
    APV_NRF24L01_REGISTER_RX_PW_Pn_USE_MASK,
    APV_NRF24L01_REGISTER_RX_PW_Pn_DEFAULT,
    TRUE
    },
    {
    APV_NRF24L01_REGISTER_ADDRESS_RX_PW_P4,
    APV_NRF24L01_REGISTER_RX_PW_Pn_USE_MASK,
    APV_NRF24L01_REGISTER_RX_PW_Pn_DEFAULT,
    TRUE
    },
    {
    APV_NRF24L01_REGISTER_ADDRESS_RX_PW_P5,
    APV_NRF24L01_REGISTER_RX_PW_Pn_USE_MASK,
    APV_NRF24L01_REGISTER_RX_PW_Pn_DEFAULT,
    TRUE
    },
    {
    APV_NRF24L01_REGISTER_ADDRESS_FIFO_STATUS,
    APV_NRF24L01_REGISTER_FIFO_STATUS_USE_MASK,
    APV_NRF24L01_REGISTER_FIFO_STATUS_DEFAULT,
    TRUE
    },
    {
    APV_NRF24L01_REGISTER_ADDRESS_0x18,
    0,
    0,
    FALSE
    },
    {
    APV_NRF24L01_REGISTER_ADDRESS_0x19,
    0,
    0,
    FALSE
    },
    {
    APV_NRF24L01_REGISTER_ADDRESS_0x1a,
    0,
    0,
    FALSE
    },
    {
    APV_NRF24L01_REGISTER_ADDRESS_0x1b,
    0,
    0,
    FALSE
    },
    {
    APV_NRF24L01_REGISTER_ADDRESS_DYNPD,
    APV_NRF24L01_REGISTER_DYNPD_USE_MASK,
    APV_NRF24L01_REGISTER_DYNPD_DPL_DEFAULT,
    TRUE
    },
    {
    APV_NRF24L01_REGISTER_ADDRESS_FEATURE,
    APV_NRF24L01_REGISTER_FEATURE_USE_MASK,
    APV_NRF24L01_REGISTER_FEATURE_DEFAULT,
    TRUE
    }
  };

// Definition of the variable part of the nRF24L01+ control and status registers for full duplex operation
apv_nRF24L01Registers_t apv_nRF24L01Registers[APV_nRF24L01_DUPLEX_IDS][APV_NRF24L01_REGISTER_ADDRESS_SET] = 
  {
    {
      {
      &apv_nRF24L01RegisterDefinitions[APV_NRF24L01_REGISTER_ADDRESS_CONFIG],
       APV_NRF24L01_REGISTER_CONFIG_DEFAULT
      },
      {
      &apv_nRF24L01RegisterDefinitions[APV_NRF24L01_REGISTER_ADDRESS_EN_AA],
       APV_NRF24L01_REGISTER_EN_AA_DEFAULT
      },
      {
      &apv_nRF24L01RegisterDefinitions[APV_NRF24L01_REGISTER_ADDRESS_EN_RXADDR],
       APV_NRF24L01_REGISTER_EN_RXADDR_DEFAULT
      },
      {
      &apv_nRF24L01RegisterDefinitions[APV_NRF24L01_REGISTER_ADDRESS_SETUP_AW],
       APV_NRF24L01_REGISTER_SETUP_AW_DEFAULT
      },
      {
      &apv_nRF24L01RegisterDefinitions[APV_NRF24L01_REGISTER_ADDRESS_SETUP_RETR],
       APV_NRF24L01_REGISTER_SETUP_RETR_DEFAULT
      },
      {
      &apv_nRF24L01RegisterDefinitions[APV_NRF24L01_REGISTER_ADDRESS_RF_CH],
       APV_NRF24L01_REGISTER_RF_CH_DEFAULT
      },
      {
      &apv_nRF24L01RegisterDefinitions[APV_NRF24L01_REGISTER_ADDRESS_RF_SETUP],
       APV_NRF24L01_REGISTER_RF_SETUP_DEFAULT
      },
      {
      &apv_nRF24L01RegisterDefinitions[APV_NRF24L01_REGISTER_ADDRESS_STATUS],
       APV_NRF24L01_REGISTER_STATUS_DEFAULT
      },
      {
      &apv_nRF24L01RegisterDefinitions[APV_NRF24L01_REGISTER_ADDRESS_OBSERVE_TX],
       APV_NRF24L01_REGISTER_OBSERVE_TX_DEFAULT
      },
      {
      &apv_nRF24L01RegisterDefinitions[APV_NRF24L01_REGISTER_ADDRESS_RPD],
       APV_NRF24L01_REGISTER_RPD_DEFAULT
      },
      {
      &apv_nRF24L01RegisterDefinitions[APV_NRF24L01_REGISTER_ADDRESS_RX_ADDR_P0],
       APV_NRF24L01_REGISTER_RX_ADDR_P0_DEFAULT_OCTET
      },
      {
      &apv_nRF24L01RegisterDefinitions[APV_NRF24L01_REGISTER_ADDRESS_RX_ADDR_P1],
       APV_NRF24L01_REGISTER_RX_ADDR_P1_DEFAULT_OCTET
      },
      {
      &apv_nRF24L01RegisterDefinitions[APV_NRF24L01_REGISTER_ADDRESS_RX_ADDR_P2],
       APV_NRF24L01_REGISTER_RX_ADDR_P2_DEFAULT_OCTET
      },
      {
      &apv_nRF24L01RegisterDefinitions[APV_NRF24L01_REGISTER_ADDRESS_RX_ADDR_P3],
       APV_NRF24L01_REGISTER_RX_ADDR_P3_DEFAULT_OCTET
      },
      {
      &apv_nRF24L01RegisterDefinitions[APV_NRF24L01_REGISTER_ADDRESS_RX_ADDR_P4],
       APV_NRF24L01_REGISTER_RX_ADDR_P4_DEFAULT_OCTET
      },
      {
      &apv_nRF24L01RegisterDefinitions[APV_NRF24L01_REGISTER_ADDRESS_RX_ADDR_P5],
       APV_NRF24L01_REGISTER_RX_ADDR_P5_DEFAULT_OCTET
      },
      {
      &apv_nRF24L01RegisterDefinitions[APV_NRF24L01_REGISTER_ADDRESS_TX_ADDR],
       APV_NRF24L01_REGISTER_TX_ADDR_DEFAULT_OCTET
      },
      {
      &apv_nRF24L01RegisterDefinitions[APV_NRF24L01_REGISTER_ADDRESS_RX_PW_P0],
       APV_NRF24L01_REGISTER_RX_PW_Pn_DEFAULT
      },
      {
      &apv_nRF24L01RegisterDefinitions[APV_NRF24L01_REGISTER_ADDRESS_RX_PW_P1],
       APV_NRF24L01_REGISTER_RX_PW_Pn_DEFAULT
      },
      {
      &apv_nRF24L01RegisterDefinitions[APV_NRF24L01_REGISTER_ADDRESS_RX_PW_P2],
       APV_NRF24L01_REGISTER_RX_PW_Pn_DEFAULT
      },
      {
      &apv_nRF24L01RegisterDefinitions[APV_NRF24L01_REGISTER_ADDRESS_RX_PW_P3],
       APV_NRF24L01_REGISTER_RX_PW_Pn_DEFAULT
      },
      {
      &apv_nRF24L01RegisterDefinitions[APV_NRF24L01_REGISTER_ADDRESS_RX_PW_P4],
       APV_NRF24L01_REGISTER_RX_PW_Pn_DEFAULT
      },
      {
      &apv_nRF24L01RegisterDefinitions[APV_NRF24L01_REGISTER_ADDRESS_RX_PW_P5],
       APV_NRF24L01_REGISTER_RX_PW_Pn_DEFAULT
      },
      {
      &apv_nRF24L01RegisterDefinitions[APV_NRF24L01_REGISTER_ADDRESS_FIFO_STATUS],
       APV_NRF24L01_REGISTER_FIFO_STATUS_DEFAULT
      },
      {
      &apv_nRF24L01RegisterDefinitions[APV_NRF24L01_REGISTER_ADDRESS_0x18],
       0
      },
      {
      &apv_nRF24L01RegisterDefinitions[APV_NRF24L01_REGISTER_ADDRESS_0x19],
       0
      },
      {
      &apv_nRF24L01RegisterDefinitions[APV_NRF24L01_REGISTER_ADDRESS_0x1a],
       0
      },
      {
      &apv_nRF24L01RegisterDefinitions[APV_NRF24L01_REGISTER_ADDRESS_0x1b],
       0
      },
      {
      &apv_nRF24L01RegisterDefinitions[APV_NRF24L01_REGISTER_ADDRESS_DYNPD],
       APV_NRF24L01_REGISTER_DYNPD_DPL_DEFAULT
      },
      {
      &apv_nRF24L01RegisterDefinitions[APV_NRF24L01_REGISTER_ADDRESS_FEATURE],
       APV_NRF24L01_REGISTER_FEATURE_DEFAULT
      }
    },
    {
      {
      &apv_nRF24L01RegisterDefinitions[APV_NRF24L01_REGISTER_ADDRESS_CONFIG],
       APV_NRF24L01_REGISTER_CONFIG_DEFAULT
      },
      {
      &apv_nRF24L01RegisterDefinitions[APV_NRF24L01_REGISTER_ADDRESS_EN_AA],
       APV_NRF24L01_REGISTER_EN_AA_DEFAULT
      },
      {
      &apv_nRF24L01RegisterDefinitions[APV_NRF24L01_REGISTER_ADDRESS_EN_RXADDR],
       APV_NRF24L01_REGISTER_EN_RXADDR_DEFAULT
      },
      {
      &apv_nRF24L01RegisterDefinitions[APV_NRF24L01_REGISTER_ADDRESS_SETUP_AW],
       APV_NRF24L01_REGISTER_SETUP_AW_DEFAULT
      },
      {
      &apv_nRF24L01RegisterDefinitions[APV_NRF24L01_REGISTER_ADDRESS_SETUP_RETR],
       APV_NRF24L01_REGISTER_SETUP_RETR_DEFAULT
      },
      {
      &apv_nRF24L01RegisterDefinitions[APV_NRF24L01_REGISTER_ADDRESS_RF_CH],
       APV_NRF24L01_REGISTER_RF_CH_DEFAULT
      },
      {
      &apv_nRF24L01RegisterDefinitions[APV_NRF24L01_REGISTER_ADDRESS_RF_SETUP],
       APV_NRF24L01_REGISTER_RF_SETUP_DEFAULT
      },
      {
      &apv_nRF24L01RegisterDefinitions[APV_NRF24L01_REGISTER_ADDRESS_STATUS],
       APV_NRF24L01_REGISTER_STATUS_DEFAULT
      },
      {
      &apv_nRF24L01RegisterDefinitions[APV_NRF24L01_REGISTER_ADDRESS_OBSERVE_TX],
       APV_NRF24L01_REGISTER_OBSERVE_TX_DEFAULT
      },
      {
      &apv_nRF24L01RegisterDefinitions[APV_NRF24L01_REGISTER_ADDRESS_RPD],
       APV_NRF24L01_REGISTER_RPD_DEFAULT
      },
      {
      &apv_nRF24L01RegisterDefinitions[APV_NRF24L01_REGISTER_ADDRESS_RX_ADDR_P0],
       APV_NRF24L01_REGISTER_RX_ADDR_P0_DEFAULT_OCTET
      },
      {
      &apv_nRF24L01RegisterDefinitions[APV_NRF24L01_REGISTER_ADDRESS_RX_ADDR_P1],
       APV_NRF24L01_REGISTER_RX_ADDR_P1_DEFAULT_OCTET
      },
      {
      &apv_nRF24L01RegisterDefinitions[APV_NRF24L01_REGISTER_ADDRESS_RX_ADDR_P2],
       APV_NRF24L01_REGISTER_RX_ADDR_P2_DEFAULT_OCTET
      },
      {
      &apv_nRF24L01RegisterDefinitions[APV_NRF24L01_REGISTER_ADDRESS_RX_ADDR_P3],
       APV_NRF24L01_REGISTER_RX_ADDR_P3_DEFAULT_OCTET
      },
      {
      &apv_nRF24L01RegisterDefinitions[APV_NRF24L01_REGISTER_ADDRESS_RX_ADDR_P4],
       APV_NRF24L01_REGISTER_RX_ADDR_P4_DEFAULT_OCTET
      },
      {
      &apv_nRF24L01RegisterDefinitions[APV_NRF24L01_REGISTER_ADDRESS_RX_ADDR_P5],
       APV_NRF24L01_REGISTER_RX_ADDR_P5_DEFAULT_OCTET
      },
      {
      &apv_nRF24L01RegisterDefinitions[APV_NRF24L01_REGISTER_ADDRESS_TX_ADDR],
       APV_NRF24L01_REGISTER_TX_ADDR_DEFAULT_OCTET
      },
      {
      &apv_nRF24L01RegisterDefinitions[APV_NRF24L01_REGISTER_ADDRESS_RX_PW_P0],
       APV_NRF24L01_REGISTER_RX_PW_Pn_DEFAULT
      },
      {
      &apv_nRF24L01RegisterDefinitions[APV_NRF24L01_REGISTER_ADDRESS_RX_PW_P1],
       APV_NRF24L01_REGISTER_RX_PW_Pn_DEFAULT
      },
      {
      &apv_nRF24L01RegisterDefinitions[APV_NRF24L01_REGISTER_ADDRESS_RX_PW_P2],
       APV_NRF24L01_REGISTER_RX_PW_Pn_DEFAULT
      },
      {
      &apv_nRF24L01RegisterDefinitions[APV_NRF24L01_REGISTER_ADDRESS_RX_PW_P3],
       APV_NRF24L01_REGISTER_RX_PW_Pn_DEFAULT
      },
      {
      &apv_nRF24L01RegisterDefinitions[APV_NRF24L01_REGISTER_ADDRESS_RX_PW_P4],
       APV_NRF24L01_REGISTER_RX_PW_Pn_DEFAULT
      },
      {
      &apv_nRF24L01RegisterDefinitions[APV_NRF24L01_REGISTER_ADDRESS_RX_PW_P5],
       APV_NRF24L01_REGISTER_RX_PW_Pn_DEFAULT
      },
      {
      &apv_nRF24L01RegisterDefinitions[APV_NRF24L01_REGISTER_ADDRESS_FIFO_STATUS],
       APV_NRF24L01_REGISTER_FIFO_STATUS_DEFAULT
      },
      {
      &apv_nRF24L01RegisterDefinitions[APV_NRF24L01_REGISTER_ADDRESS_0x18],
       0
      },
      {
      &apv_nRF24L01RegisterDefinitions[APV_NRF24L01_REGISTER_ADDRESS_0x19],
       0
      },
      {
      &apv_nRF24L01RegisterDefinitions[APV_NRF24L01_REGISTER_ADDRESS_0x1a],
       0
      },
      {
      &apv_nRF24L01RegisterDefinitions[APV_NRF24L01_REGISTER_ADDRESS_0x1b],
       0
      },
      {
      &apv_nRF24L01RegisterDefinitions[APV_NRF24L01_REGISTER_ADDRESS_DYNPD],
       APV_NRF24L01_REGISTER_DYNPD_DPL_DEFAULT
      },
      {
      &apv_nRF24L01RegisterDefinitions[APV_NRF24L01_REGISTER_ADDRESS_FEATURE],
       APV_NRF24L01_REGISTER_FEATURE_DEFAULT
      }
    }    
  };

/******************************************************************************/
/* Transmit and receive data-pipe device addresses are initialised by default */
/* and can be set to application-specific values :                            */
/******************************************************************************/

apv_nRF24L01DataPipeAddressDefinition_t apv_nRF24L01TxDataPipeAddress = 
  {
    {
    APV_NRF24L01_REGISTER_TX_ADDR_DEFAULT_OCTET, 
    APV_NRF24L01_REGISTER_TX_ADDR_DEFAULT_OCTET,
    APV_NRF24L01_REGISTER_TX_ADDR_DEFAULT_OCTET,
    APV_NRF24L01_REGISTER_TX_ADDR_DEFAULT_OCTET,
    APV_NRF24L01_REGISTER_TX_ADDR_DEFAULT_OCTET 
    },
  APV_NRF24L01_REGISTER_TX_ADDR_MAXIMUM_WIDTH
  };

apv_nRF24L01DataPipeAddressDefinition_t apv_nRF24L01RxDataPipeAddresses[APV_nRF24L01_DATA_PIPE_SET] = 
  {
    {
      { 
      APV_NRF24L01_REGISTER_RX_ADDR_P0_DEFAULT_OCTET,
      APV_NRF24L01_REGISTER_RX_ADDR_P0_DEFAULT_OCTET,
      APV_NRF24L01_REGISTER_RX_ADDR_P0_DEFAULT_OCTET,
      APV_NRF24L01_REGISTER_RX_ADDR_P0_DEFAULT_OCTET,
      APV_NRF24L01_REGISTER_RX_ADDR_P0_DEFAULT_OCTET 
      },
    APV_NRF24L01_REGISTER_RX_ADDR_Pn_MAXIMUM_WIDTH
    },
    {
      { 
      APV_NRF24L01_REGISTER_RX_ADDR_P1_DEFAULT_OCTET,
      APV_NRF24L01_REGISTER_RX_ADDR_P1_DEFAULT_OCTET,
      APV_NRF24L01_REGISTER_RX_ADDR_P1_DEFAULT_OCTET,
      APV_NRF24L01_REGISTER_RX_ADDR_P1_DEFAULT_OCTET,
      APV_NRF24L01_REGISTER_RX_ADDR_P1_DEFAULT_OCTET    
      },
    APV_NRF24L01_REGISTER_RX_ADDR_Pn_MAXIMUM_WIDTH
    },
    {
      { 
      APV_NRF24L01_REGISTER_RX_ADDR_P2_DEFAULT_OCTET 
      },
    APV_NRF24L01_REGISTER_RX_ADDR_Pn_DEFAULT_WIDTH
    },
    {
      { 
      APV_NRF24L01_REGISTER_RX_ADDR_P3_DEFAULT_OCTET 
      },
    APV_NRF24L01_REGISTER_RX_ADDR_Pn_DEFAULT_WIDTH
    },
    {
      { 
      APV_NRF24L01_REGISTER_RX_ADDR_P4_DEFAULT_OCTET 
      },
    APV_NRF24L01_REGISTER_RX_ADDR_Pn_DEFAULT_WIDTH
    },
    {
      { 
      APV_NRF24L01_REGISTER_RX_ADDR_P5_DEFAULT_OCTET 
      },
    APV_NRF24L01_REGISTER_RX_ADDR_Pn_DEFAULT_WIDTH
    },
  };

/******************************************************************************/
/* The master beacon definitions :                                            */
/******************************************************************************/

const uint8_t apv_nRF24L01MasterBeaconPattern[APV_NRF24L01_MASTER_BEACON_PATTERN_LENGTH] = 
  {
  APV_NRF24L01_MASTER_BEACON_OUTER_PATTERN,
  APV_NRF24L01_MASTER_BEACON_MID_PATTERN,  
  APV_NRF24L01_MASTER_BEACON_INNER_PATTERN,
  APV_NRF24L01_MASTER_BEACON_MID_PATTERN,
  APV_NRF24L01_MASTER_BEACON_OUTER_PATTERN,
  APV_NRF24L01_MASTER_BEACON_OUTER_PATTERN,
  APV_NRF24L01_MASTER_BEACON_MID_PATTERN,  
  APV_NRF24L01_MASTER_BEACON_INNER_PATTERN,
  APV_NRF24L01_MASTER_BEACON_MID_PATTERN,
  APV_NRF24L01_MASTER_BEACON_OUTER_PATTERN,
  APV_NRF24L01_MASTER_BEACON_OUTER_PATTERN,
  APV_NRF24L01_MASTER_BEACON_MID_PATTERN,  
  APV_NRF24L01_MASTER_BEACON_INNER_PATTERN,
  APV_NRF24L01_MASTER_BEACON_MID_PATTERN,
  APV_NRF24L01_MASTER_BEACON_OUTER_PATTERN
  };

/******************************************************************************/
/* Global Function Definitions :                                              */
/******************************************************************************/

#if (1)
/******************************************************************************/

/* apvInitialisation_nRF24L01() :                                             */
/*  --> apvCoreTimerBlock    : duration timers block                          */
/*  --> apvIdentity          : [ 1 == master | 2 { slave 'n - 1' } 7 ]        */
/*  --> apv_nRF24L01DuplexId : [ APV_nRF24L01_DUPLEX_ID_TX |                  */
/*                               APV_nRF24L01_DUPLEX_ID_RX]                   */
/*  <-- initialisationError  : error codes                                    */
/******************************************************************************/

APV_ERROR_CODE apvInitialisation_nRF24L01(apvCoreTimerBlock_t      *apvCoreTimerBlock,
                                          apvArduinoSystemIdentifier_t *apvIdentity,
                                          apv_nRF24L01DuplexId_t        apv_nRF24L01DuplexId)
  {
/******************************************************************************/

  APV_ERROR_CODE                   initialisationError          = APV_ERROR_CODE_NONE;

  apv_nRF24LO1RegisterAddressSet_t registerSet                  = APV_NRF24L01_REGISTER_ADDRESS_CONFIG;

  uint32_t                         apv_nRF24L01TimerIndex       =  APV_DURATION_TIMER_NULL_INDEX;
  bool                             apv_nRF24L01TimerExpiredFlag = true;

  uint8_t                          apvSpi0FillLevel             = 0;
  apvFIFOEntry_t                   apv_nRF24L01DataWord;

  // Initialises as Tx Channel Settings
  apv_nRF24L01DataPipe_t           apv_nRF24L01DataPipe         = APV_nRF24L01_DATA_PIPE_0;
  apvChipSelectRegisterInstance_t  apv_nRF24L01ChipSelect       = APV_SPI_CHIP_SELECT_REGISTER_1;
  
/******************************************************************************/

  // Write the nRF24L01 register set defaults into their "shadows"
  for (registerSet = APV_NRF24L01_REGISTER_ADDRESS_CONFIG; registerSet < APV_NRF24L01_REGISTER_ADDRESS_SET; registerSet++)
    {
    apv_nRF24L01WriteRegisterDefault(registerSet,
                                     apv_nRF24L01DuplexId);
    }

  // Switch on the nRF24L01 chip-enable I/O lines
  if (apv_nRF24L01DuplexId == APV_nRF24L01_DUPLEX_ID_TX)
    {
    initialisationError = apvSwitchDigitalLines(APV_DIGITAL_NRF24L01_TX_CHIP_ENABLE_SELECT,
                                                true);
    }
  else
    {
    initialisationError = apvSwitchDigitalLines(APV_DIGITAL_NRF24L01_RX_CHIP_ENABLE_SELECT,
                                                true);    
    }

  // Start with chip-enable inactive
  nRF24L01ChipEnableState[apv_nRF24L01DuplexId] = false;

  apv_nRF24L01ChipEnableControl( apv_nRF24L01DuplexId,
                                &nRF24L01ChipEnableState[apv_nRF24L01DuplexId]);

  /******************************************************************************/
  /* Firstly try to read back the transmit pipe address register to check the   */
  /* devices are present and active                                             */
  /******************************************************************************/

  // Setup the chip-select and operating mode
  // Pass this to the timer callback function
  apv_nRF24L01TimerFlag = false;
  
  initialisationError = apvCreateDurationTimer( apvCoreTimerBlock,
                                                apv_nRF24L01StateTimer,              // callback function
                                               (void *)&apv_nRF24L01TimerFlag,       // timer expiry flag passed to callback function
                                                APV_DURATION_TIMER_TYPE_ONE_SHOT,    // single-shot
                                                APV_EVENT_TIMER_INVERSE_NANOSECONDS, // one second period
                                                APV_DURATION_TIMER_SOURCE_SYSTICK,
                                                APV_DURATION_TIMER_STATE_STOPPED,
                                               &apv_nRF24L01TimerIndex);  

  if (apv_nRF24L01DuplexId == APV_nRF24L01_DUPLEX_ID_TX)
    {
    apvSpi0Cs1nRF24L01Assignment();

    // Restart the one-shot timer delay waiting for a character
    initialisationError = apvReTriggerDurationTimer(apvCoreTimerBlock,
                                                    apv_nRF24L01TimerIndex,
                                                    APV_EVENT_TIMER_INVERSE_NANOSECONDS);
    
    /******************************************************************************/
    /* Default to expiry - this will be cancelled if the interrupt turns up first */
    /******************************************************************************/
    
    apv_nRF24L01TimerExpiredFlag = true; 

#if (1)
    initialisationError = apv_nRF24L01StartDataPipeReceiveAddressRead( apv_nRF24L01DataPipe,
                                                                      &apv_nRF24L01RxDataPipeAddresses[0],
                                                                       APV_nRF24L01_DATA_PIPE_SET,
                                                                       apv_nRF24L01ChipSelect);
#endif

    /******************************************************************************/
    /* Wait for the SPI0 receive interrupt or the duration timer expiry,          */
    /* whichever happens first                                                    */
    /******************************************************************************/

    while (apv_nRF24L01TimerFlag == false)
      {
      if (apvGetFIFOFillLevel(&apvSpi0ChipSelectFIFO[apv_nRF24L01ChipSelect],
                              &apvSpi0FillLevel,
                               true) == APV_ERROR_CODE_NONE)
        {
        if (apvSpi0FillLevel != 0)
          {
          apv_nRF24L01TimerExpiredFlag = false;
          }
        }
      }

    if (apv_nRF24L01TimerExpiredFlag == false)
      {
      apv_nRF24L01TimerExpiredFlag = true;

      // Finished with the duration timer
      apvDestroyDurationTimer( apvCoreTimerBlock, 
                              &apv_nRF24L01TimerIndex);
          
      //CHECK THE CONTENTS OF THE RX SPI BUFFER FOR CS[1]
      if (apvSpi0FillLevel == 0)
        {
        // This shouldn't even be possible, but...
        initialisationError = APV_ERROR_CODE_MESSAGE_BUFFER_FAULTY;
        }
      else
        {
        // The first word back is always the status word - discard it for now...
        apvUnLoadFIFO(&apvSpi0ChipSelectFIFO[apv_nRF24L01ChipSelect],
                      &apv_nRF24L01DataWord,
                       true);

        // Unload and chack the remaining address characters - finished with the timer index, repurpose here
        for (apv_nRF24L01TimerIndex = 0; apv_nRF24L01TimerIndex < apv_nRF24L01RxDataPipeAddresses[apv_nRF24L01DataPipe].apv_nRF24L01DataPipeAddressLength; apv_nRF24L01TimerIndex++)
          {
          apvUnLoadFIFO(&apvSpi0ChipSelectFIFO[apv_nRF24L01ChipSelect],
                        &apv_nRF24L01DataWord,
                         true);

          if (apv_nRF24L01DataWord.apvFIFOEntryData != apv_nRF24L01RxDataPipeAddresses[apv_nRF24L01DataPipe].apv_nRF24L01DataPipeAddress[apv_nRF24L01TimerIndex])
            {
            // Looks like the nRF24L01 is not connected or has failed
            while(true)
              ;
            }
          }
        }
      }
    else
      {
      initialisationError = APV_ERROR_CODE_EVENT_TIMEOUT_ERROR;
      }
    }
  else
    {
    apvSpi0Cs2nRF24L01Assignment();

    apv_nRF24L01DataPipe   = APV_nRF24L01_DATA_PIPE_1;
    apv_nRF24L01ChipSelect = APV_SPI_CHIP_SELECT_REGISTER_2;
    
    // Restart the one-shot timer delay waiting for a character
    initialisationError = apvReTriggerDurationTimer(apvCoreTimerBlock,
                                                    apv_nRF24L01TimerIndex,
                                                    APV_EVENT_TIMER_INVERSE_NANOSECONDS);
    
    /******************************************************************************/
    /* Default to expiry - this will be cancelled if the interrupt turns up first */
    /******************************************************************************/
    
    apv_nRF24L01TimerExpiredFlag = true; 

#if (1)
    initialisationError = apv_nRF24L01StartDataPipeReceiveAddressRead( apv_nRF24L01DataPipe,
                                                                      &apv_nRF24L01RxDataPipeAddresses[0],
                                                                       APV_nRF24L01_DATA_PIPE_SET,
                                                                       apv_nRF24L01ChipSelect);
#endif

    /******************************************************************************/
    /* Wait for the SPI0 receive interrupt or the duration timer expiry,          */
    /* whichever happens first                                                    */
    /******************************************************************************/

    while (apv_nRF24L01TimerFlag == false)
      {
      if (apvGetFIFOFillLevel(&apvSpi0ChipSelectFIFO[apv_nRF24L01ChipSelect],
                              &apvSpi0FillLevel,
                               true) == APV_ERROR_CODE_NONE)
        {
        if (apvSpi0FillLevel != 0)
          {
          apv_nRF24L01TimerExpiredFlag = false;
          }
        }
      }

    if (apv_nRF24L01TimerExpiredFlag == false)
      {
      apv_nRF24L01TimerExpiredFlag = true;

      // Finished with the duration timer
      apvDestroyDurationTimer( apvCoreTimerBlock, 
                              &apv_nRF24L01TimerIndex);
      
      //CHECK THE CONTENTS OF THE RX SPI BUFFER FOR CS[2]
      if (apvSpi0FillLevel == 0)
        {
        // This shouldn't even be possible, but...
        initialisationError = APV_ERROR_CODE_MESSAGE_BUFFER_FAULTY;
        }
      else
        {
        // The first word back is always the status word - discard it for now...
        apvUnLoadFIFO(&apvSpi0ChipSelectFIFO[apv_nRF24L01ChipSelect],
                      &apv_nRF24L01DataWord,
                       true);

        // Unload and chack the remaining address characters - finished with the timer index, repurpose here
        for (apv_nRF24L01TimerIndex = 0; apv_nRF24L01TimerIndex < apv_nRF24L01RxDataPipeAddresses[apv_nRF24L01DataPipe].apv_nRF24L01DataPipeAddressLength; apv_nRF24L01TimerIndex++)
          {
          apvUnLoadFIFO(&apvSpi0ChipSelectFIFO[apv_nRF24L01ChipSelect],
                        &apv_nRF24L01DataWord,
                         true);

          if (apv_nRF24L01DataWord.apvFIFOEntryData != apv_nRF24L01RxDataPipeAddresses[apv_nRF24L01DataPipe].apv_nRF24L01DataPipeAddress[apv_nRF24L01TimerIndex])
            {
            // Looks like the nRF24L01 is not connected or has failed
            while(true)
              ;
            }
          }
        }      
      }
    else
      {
      initialisationError = APV_ERROR_CODE_EVENT_TIMEOUT_ERROR;
      }    
    }

/******************************************************************************/

  return(initialisationError);

/******************************************************************************/
  } /* end of apvInitialisation_nRF24L01                                      */

#endif

/******************************************************************************/
/* apv_nRF24L01WriteRegisterDefault() :                                       */
/*  --> apv_nRF24L01RegisterNumber : nRF24L01 register number                 */
/*  --> apv_nRF24L01DuplexId       : [ APV_nRF24L01_DUPLEX_ID_TX |            */
/*                                     APV_nRF24L01_DUPLEX_ID_RX]             */
/*                                                                            */
/* - write the default value into a nRF24L01 register "shadow"                */
/*                                                                            */
/******************************************************************************/

void apv_nRF24L01WriteRegisterDefault(apv_nRF24LO1RegisterAddressSet_t apv_nRF24L01RegisterNumber,
                                      apv_nRF24L01DuplexId_t           apv_nRF24L01DuplexId)
  {
/******************************************************************************/

  if (apv_nRF24L01RegisterNumber >= APV_NRF24L01_REGISTER_ADDRESS_SET)
    {
    return;
    }
  else
    {
    apv_nRF24L01Registers[apv_nRF24L01DuplexId][apv_nRF24L01RegisterNumber].apv_nRF24L01Register = apv_nRF24L01Registers[apv_nRF24L01DuplexId][apv_nRF24L01RegisterNumber].apv_nRF24L01RegisterDefinition->apv_nRF24L01RegisterDefault;
    }

/******************************************************************************/
  } /* end of apv_nRF24L01WriteRegisterDefault                                */

/******************************************************************************/
/* apv_nRF24L01ChipEnableControl() :                                          */
/*  --> apv_nRF24L01DuplexId : [ APV_nRF24L01_DUPLEX_ID_TX |                  */
/*                               APV_nRF24L01_DUPLEX_ID_RX]                   */
/*  --> chipEnableState      : [ false == chip-enable disabled |              */
/*                               true  == chip-enable ]                       */
/*                                                                            */
/* - the nRF24L01 chip-enable drives the Tx and Rx states. The Arduino pin    */
/*   I/O drive is inverted - "set" results in electrical '0', "clear" in      */
/*   electrical '1'                                                           */
/*                                                                            */
/******************************************************************************/

void apv_nRF24L01ChipEnableControl(apv_nRF24L01DuplexId_t  apv_nRF24L01DuplexId,
                                   bool                   *chipEnableState)
  {
/******************************************************************************/

  if (*chipEnableState == false)
    {
    if (apv_nRF24L01DuplexId == APV_nRF24L01_DUPLEX_ID_TX)
      {
      // Drive low so chip-select is inactive
      ApvPeripheralLineControlBlock_p[APV_PERIPHERAL_LINE_GROUP_C]->PIO_SODR = PIO_SODR_P16;
      }
    else
      {
      ApvPeripheralLineControlBlock_p[APV_PERIPHERAL_LINE_GROUP_C]->PIO_SODR = PIO_SODR_P17;
      }
    }
  else
    {
    if (apv_nRF24L01DuplexId == APV_nRF24L01_DUPLEX_ID_TX)
      {
      // Drive high so chip-select is active
      ApvPeripheralLineControlBlock_p[APV_PERIPHERAL_LINE_GROUP_C]->PIO_CODR = PIO_CODR_P16;
      }
    else
      {
      ApvPeripheralLineControlBlock_p[APV_PERIPHERAL_LINE_GROUP_C]->PIO_CODR = PIO_CODR_P17;
      }
    }

/******************************************************************************/
  } /* end of apv_nRF24L01ChipEnableControl                                   */


/******************************************************************************/
/* apv_nRF24L01StartDataPipeReceiveAddressRead() :                            */
/*  --> dataPipe              : APV_nRF24L01_DATA_PIPE_0 { ... }              */
/*                                                  APV_nRF24L01_DATA_PIPE_5  */
/*  --> dataPipeAddressLength : [ 1 | 3 { ... } 5 ]                           */
/*  --> spiChipSelect         : [ 0 .. 3 ]                                    */
/*  <-- readError             : error codes                                   */
/*                                                                            */
/* - load the SPI0 transmit buffer with the required command to read a data-  */
/*   pipe receive address. If the transmit interrupt is dormant prime the     */
/*   interrupt-driven transmit and receive mechanism. The transmit FIFO must  */
/*   be pre-loaded with the correct number of dummy bytes to clock out the    */
/*   address from the nRF24L01.                                               */
/*   NOTE : although the nRF24L01 SPI bus is 8-bits wide the value to be      */
/*          transferred to the 16-bit transmit register must be right-        */
/*          justified. This means a single byte or the low-byte of a 16-bit   */
/*          value load in the low-byte of the transmit register               */
/*                                                                            */
/******************************************************************************/

APV_ERROR_CODE apv_nRF24L01StartDataPipeReceiveAddressRead(apv_nRF24L01DataPipe_t                                      dataPipe,
                                                                              apv_nRF24L01DataPipeAddressDefinition_t *dataPipeAddresses,
                                                                              uint8_t                                  dataPipeAddressesLength,
                                                                              apvChipSelectRegisterInstance_t          spiChipSelect)
  {
/******************************************************************************/

  uint8_t                          apv_nRF24L01ReadCommand             = 0,
                                   apv_nRF24L01RegisterAccessIndex     = 0,
                                   apv_nRF24L01RegisterAccessPayload[APV_NRF24L01_REGISTER_SETUP_AW_MAXIMUM_WIDTH + 1]; // data-pipe register accesses are a command-byte and (a maximum) 
                                                                                                                        // five significant register address bytes

  apvSPILastTransfer_t             apv_nRF24L01LastTransfer            = APV_SPI_LAST_TRANSFER_NONE;
  apv_nRF24LO1RegisterAddressSet_t apv_nRF24L01DataPipeRegisterAddress = APV_NRF24L01_REGISTER_ADDRESS_RX_ADDR_P0;
  uint8_t                          dataPipeAddressLength               = 0;
  bool                             apv_nRF24L01Prime                   = false;
  
  APV_ERROR_CODE                   readError                           = APV_ERROR_CODE_NONE;

/******************************************************************************/

  if (dataPipe < APV_nRF24L01_DATA_PIPE_SET)
    {
    // Preload the "read-register" command prefix
    apv_nRF24L01ReadCommand = APV_NRF24L01_COMMAND_R_REGISTER;

    // Turn the requested data-pipe index into a receive address register
    switch(dataPipe)
      {
      case   APV_nRF24L01_DATA_PIPE_1 : apv_nRF24L01DataPipeRegisterAddress = APV_NRF24L01_REGISTER_ADDRESS_RX_ADDR_P1;
                                        break;
      case   APV_nRF24L01_DATA_PIPE_2 : apv_nRF24L01DataPipeRegisterAddress = APV_NRF24L01_REGISTER_ADDRESS_RX_ADDR_P2;
                                        break;
      case   APV_nRF24L01_DATA_PIPE_3 : apv_nRF24L01DataPipeRegisterAddress = APV_NRF24L01_REGISTER_ADDRESS_RX_ADDR_P3;
                                        break;
      case   APV_nRF24L01_DATA_PIPE_4 : apv_nRF24L01DataPipeRegisterAddress = APV_NRF24L01_REGISTER_ADDRESS_RX_ADDR_P4;
                                        break;
      case   APV_nRF24L01_DATA_PIPE_5 : apv_nRF24L01DataPipeRegisterAddress = APV_NRF24L01_REGISTER_ADDRESS_RX_ADDR_P5;
                                        break;
      case   APV_nRF24L01_DATA_PIPE_0 : 
      default                         : break;
      }

    dataPipeAddressLength = (dataPipeAddresses + dataPipe)->apv_nRF24L01DataPipeAddressLength;
    
    apv_nRF24L01ReadCommand = apv_nRF24L01ReadCommand | ((uint8_t)apv_nRF24L01DataPipeRegisterAddress);

    // Build the register access payload
    apv_nRF24L01RegisterAccessPayload[0] = apv_nRF24L01ReadCommand;

    for (apv_nRF24L01RegisterAccessIndex = 0; apv_nRF24L01RegisterAccessIndex < dataPipeAddressLength; apv_nRF24L01RegisterAccessIndex++)
      {
      apv_nRF24L01RegisterAccessPayload[apv_nRF24L01RegisterAccessIndex + 1] = APV_NRF24L01_NULL_CHARACTER;
      }

    readError = apv_nRF24L01WriteRegister( apv_nRF24L01DataPipeRegisterAddress,
                                          &apv_nRF24L01RegisterAccessPayload[0],
                                           ((dataPipeAddresses + dataPipe)->apv_nRF24L01DataPipeAddressLength + 1),
                                           spiChipSelect);
    
    /******************************************************************************/
    /*          ??? COMPRESS THIS USING apv_nRF24L01WriteRegister() ???           */
    /******************************************************************************/

#if (0)
    readError = apvSPITransmitCharacter(APV_NRF24L01_NULL_CHARACTER,
                                        apv_nRF24L01ReadCommand,
                                        spiChipSelect,
                                        APV_SPI_LAST_TRANSFER_NONE,
                                        false);      

    if (dataPipe >= APV_nRF24L01_DATA_PIPE_2)
      {
      // The stored receive addresses of data pipes 2 - 5 are one-byte only. The single dummy byte
      // must terminate the SPI transfer
      readError = apvSPITransmitCharacter(APV_NRF24L01_NULL_CHARACTER,
                                          APV_NRF24L01_NULL_CHARACTER,
                                          spiChipSelect,
                                          APV_SPI_LAST_TRANSFER_ACTIVE,
                                          true);      
      }
    else
      {
      // The stored receive addresses of data pipes 0 and 1 are 3 - 5 bytes wide
      if ((dataPipeAddressLength >= APV_NRF24L01_REGISTER_RX_ADDR_Pn_MINIMUM_WIDTH) && (dataPipeAddressLength <= APV_NRF24L01_REGISTER_RX_ADDR_Pn_MAXIMUM_WIDTH))
        {
        do
          {
          dataPipeAddressLength = dataPipeAddressLength - 1;

          if (dataPipeAddressLength == 0)
            {
            apv_nRF24L01LastTransfer = APV_SPI_LAST_TRANSFER_ACTIVE;
            apv_nRF24L01Prime        = true;
            }
          
          // Preload the transmit FIFO with "clocking" null characters
          readError = apvSPITransmitCharacter(APV_NRF24L01_NULL_CHARACTER,
                                              APV_NRF24L01_NULL_CHARACTER,
                                              spiChipSelect,
                                              apv_nRF24L01LastTransfer,
                                              apv_nRF24L01Prime);
          }
        while (dataPipeAddressLength > 0);
        }
      else
        {
        readError = APV_ERROR_CODE_PARAMETER_OUT_OF_RANGE;
        }
      }
#endif    
    }
  else
    {
    readError = APV_ERROR_CODE_PARAMETER_OUT_OF_RANGE;
    }
  
/******************************************************************************/

  return(readError);

/******************************************************************************/
  } /* end of apv_nRF24L01StartDataPipeReceiveAddressRead                     */

/******************************************************************************/
/* apv_nRF24L01WriteRegister() :                                              */
/*  --> apv_nRF24L01RegisterAddress : [ CONFIG      |                         */
/*                                      EN_AA       |                         */
/*                                      EN_RXADDR   |                         */
/*                                      SETUP_AW    |                         */
/*                                      SETUP_RETR  |                         */
/*                                      RF_CH       |                         */
/*                                      RF_SETUP    |                         */
/*                                      STATUS      |                         */
/*                                      OBSERVE_TX  |                         */
/*                                      RPD         |                         */
/*                                      RX_ADDR_P0  |                         */
/*                                      RX_ADDR_P1  |                         */
/*                                      RX_ADDR_P2  |                         */
/*                                      RX_ADDR_P3  |                         */
/*                                      RX_ADDR_P4  |                         */
/*                                      RX_ADDR_P5  |                         */
/*                                      RX_ADDR_P0  |                         */
/*                                      TX_ADDR     |                         */
/*                                      RX_PW_P0    |                         */
/*                                      RX_PW_P1    |                         */
/*                                      RX_PW_P2    |                         */
/*                                      RX_PW_P3    |                         */
/*                                      RX_PW_P4    |                         */
/*                                      RX_PW_P5    |                         */
/*                                      FIFO_STATUS |                         */
/*                                      DYNPD       |                         */
/*                                      FEATURE ]                             */
/*  --> apv_nRF24L01RegisterPayLoad : <command-word> + 1 { <data-word> } n    */
/*      - multiple-word payloads (0 .. <n - 1>) can be two types :            */
/*          REGISTER_READ  : payload[0]     == <command-word>                 */
/*                           payload[1]     == APV_NRF24L01_NULL_CHARACTER    */
/*                           payload[n - 1] == APV_NRF24L01_NULL_CHARACTER    */
/*          REGISTER_WRITE : payload[0]     == <command-word>                 */
/*                           payload[1]     == <first-significant-word>       */
/*                           payload[n - 1] == <last-significant-word>        */
/*      - this follows the order the transmit FIFO is filled during the calls */
/*        to "apvSPITransmitCharacter()"                                      */
/*                                                                            */
/*  --> apv_NRF24L01RegisterPayLoadLength : 1 { <number-of-payload-words> n   */
/*  --> apv_nRf24L01SpiChipSelect   : [ APV_SPI_CHIP_SELECT_REGISTER_1 |      */
/*                                      APV_SPI_CHIP_SELECT_REGISTER_2 ]      */
/*                                                                            */
/* - write a command and optionally zero or more additional payload words.    */
/*   The SPI0 receive interrupt will be started to capture any characters     */
/*   returning from the device                                                */
/*                                                                            */
/* Reference : Nordic Semiconductor, "nRF24L01 Product Specification", v1.0,  */
/*             September 2008, Section 9 "Register Map", p57                  */
/*                                                                            */
/******************************************************************************/

APV_ERROR_CODE apv_nRF24L01WriteRegister(apv_nRF24LO1RegisterAddressSet_t             apv_nRF24L01RegisterAddress,
                                                    uint8_t                          *apv_nRF24L01RegisterPayLoad,
                                                    uint8_t                           apv_NRF24L01RegisterPayLoadLength,
                                                    apvChipSelectRegisterInstance_t   apv_nRf24L01SpiChipSelect)
  {
/******************************************************************************/

  uint8_t              apv_nRF24L01RegisterIndex = 0;
  apvSPILastTransfer_t apv_nRF24L01LastTransfer  = APV_SPI_LAST_TRANSFER_NONE;
  bool                 apv_nRF24L01Prime         = false;
  
  APV_ERROR_CODE       writeError                = APV_ERROR_CODE_NONE;

/******************************************************************************/

  if (apv_nRF24L01RegisterAddress < APV_NRF24L01_REGISTER_ADDRESS_SET)
    {
    if (apv_nRF24L01RegisterPayLoad != NULL)
      {
      if (apv_NRF24L01RegisterPayLoadLength != 0)
        {
        if ((apv_nRf24L01SpiChipSelect == APV_SPI_CHIP_SELECT_REGISTER_1) || (apv_nRf24L01SpiChipSelect == APV_SPI_CHIP_SELECT_REGISTER_2))
          {
          for (apv_nRF24L01RegisterIndex = 0; apv_nRF24L01RegisterIndex < apv_NRF24L01RegisterPayLoadLength; apv_nRF24L01RegisterIndex++)
            {
            // The final word in the payload buffer is marked by CSN rising and the transmit interrupt priming
            if (apv_nRF24L01RegisterIndex == (apv_NRF24L01RegisterPayLoadLength - 1))
              {
              apv_nRF24L01LastTransfer  = APV_SPI_LAST_TRANSFER_ACTIVE;
              apv_nRF24L01Prime         = true;
              }
            
            writeError = apvSPITransmitCharacter(  APV_NRF24L01_NULL_CHARACTER,
                                                 *(apv_nRF24L01RegisterPayLoad + apv_nRF24L01RegisterIndex),
                                                  apv_nRf24L01SpiChipSelect,
                                                  apv_nRF24L01LastTransfer,
                                                  apv_nRF24L01Prime);      
            }
          }
        else
          {
          writeError = APV_ERROR_CODE_PARAMETER_OUT_OF_RANGE;
          }
        }
      else
        {
        writeError = APV_ERROR_CODE_PARAMETER_OUT_OF_RANGE;
        }
      }
    else
      {
      writeError = APV_ERROR_CODE_NULL_PARAMETER;
      }
    }
  else
    {
    writeError = APV_ERROR_CODE_PARAMETER_OUT_OF_RANGE;
    }
  
/******************************************************************************/

  return(writeError);

/******************************************************************************/
  } /* end of apv_nRF24L01WriteRegister                                       */

/******************************************************************************/
/* apv_nRF24L01MasterTransmitBeacon() :                                       */
/*  --> masterBeaconSignal       : { <beacon_pattern_words> }                 */
/*  --> masterBeaconSignalLength : beacon length 0 { ... } n                  */
/*  --> masterBeaconTransmitState            :                                */
/*                                [ APV_NRF24L01_BEACON_TRANSMIT_STATE_IDLE | */
/*                                  APV_NRF24L01_BEACON_TRANSMIT_STATE_TX   | */
/*                                  APV_NRF24L01_BEACON_TRANSMIT_STATE_RX ]   */
/*  <-- beaconError                          : error codes                    */
/*                                                                            */
/* - the beacon transmission is used principally to synchronise the slaves to */
/*   the transmit-receive cycle. Secondarily it might carry some useful data! */
/*   The beacon timing is crucial but not critical. The idea is the slaves    */
/*   will not do anything until they have detected the beacon. Afterwards the */
/*   beacon timing is used to determine the slave/master:receive/transmit     */
/*   slot :                                                                   */
/*                                                                            */
/*  ------------------------------------------------------------------------  */
/*  | MASTER |  SLAVE(0) |               ...                 | SLAVE(N-1)  |  */
/*  ------------------------------------------------------------------------  */
/*  |   B    |  ^  |  |  |                                   |  ^   |  |   |  */
/*  |   E    |  |  |  V  |                                   |  |   |  V   |  */
/*  |   A    |  R  |  T  |                                   |  R   |  T   |  */
/*  |   C    |  X  |  X  |                                   |  X   |  X   |  */
/*  |   O    |     |     |                                   |      |      |  */
/*  |   N    |     |     |                                   |      |      |  */
/*  ------------------------------------------------------------------------  */
/*                                                                            */
/* - the beacon cannot be used for "fine-timing", this would require a more   */
/*   sophisticated receiver capable of finer pattern correlation. Instead it  */
/*   should establish the transmit/receive window to a couple of milliseconds */
/*   with no actual guarantee of some circumstance where it canot be found.   */
/*   The pattern used is designed to try to capture enough of the signal to   */
/*   have a high confidence. If the signal is "straddled" it may be deemed to */
/*   lie in +/- one 1 millisecond slot :                                      */
/*                                                                            */
/*   t = 0 : 1 { 0x81 | 0x5A | 0x7E | 0x5A | 0x81 } 3                         */
/*                                                                            */
/******************************************************************************/

APV_ERROR_CODE apv_nRF24L01MasterTransmitBeacon(uint8_t                                         *masterBeaconSignal,
                                                              uint8_t                            masterBeaconSignalLength,
                                                              apv_nRF24L01BeaconTransmitState_t *masterBeaconTransmitState)
  {
/******************************************************************************/

  APV_ERROR_CODE beaconError = APV_ERROR_CODE_NONE;

/******************************************************************************/

  if (masterBeaconSignal != NULL)
    {
    if (masterBeaconSignalLength != 0)
      {
      switch(*masterBeaconTransmitState)
        {
        case APV_NRF24L01_BEACON_TRANSMIT_STATE_IDLE : 
                                                       break;
    
        case APV_NRF24L01_BEACON_TRANSMIT_STATE_TX   :
                                                       break;
    
        case APV_NRF24L01_BEACON_TRANSMIT_STATE_RX   :
                                                       break;
    
        default                                      :
                                                       break;    
        }
      }
    else
      {
      beaconError = APV_ERROR_CODE_PARAMETER_OUT_OF_RANGE;
      }
    }
  else
    {
    beaconError = APV_ERROR_CODE_NULL_PARAMETER;
    }
  
/******************************************************************************/

  return(beaconError);

/******************************************************************************/
  } /* end of apv_nRF24L01MasterTransmitBeacon                                */

/******************************************************************************/
/* apv_nRF24L01SlaveReceiveBeacon() :                                         */
/*  --> masterBeaconSignal          : { <beacon_pattern_words> }              */
/*  --> masterBeaconSignalLength    : beacon length 0 { ... } n               */
/*  --> synchronisationFineInterval :                                         */
/*  --> synchronisationFullPeriod   :                                         */
/******************************************************************************/

APV_ERROR_CODE apv_nRF24L01SlaveReceiveBeacon(uint8_t *masterBeaconSignal,
                                                           uint8_t  masterBeaconSignalLength,
                                                           uint16_t synchronisationFineInterval,
                                                           uint16_t synchronisationFullPeriod)
  {
/******************************************************************************/

  APV_ERROR_CODE slaveReceiveErrors = APV_ERROR_CODE_NONE;

/******************************************************************************/
/******************************************************************************/

  return(slaveReceiveErrors);

/******************************************************************************/
  } /* end of apv_nRF24L01SlaveReceiveBeacon                                  */
  
/******************************************************************************/
/* apv_nRF24L01MasterTransmitManager() :                                      */
/*  --> masterTransmitClass  : [ MASTER == beacon transmission | SLAVE_n ]    */
/*  --> masterSchedulingFlag : task internal scheduling flag                  */
/*  <-- masterTransmitErrors : error codes                                    */
/*                                                                            */
/* - high-level task manager for master transmission tasks                    */
/*                                                                            */
/******************************************************************************/

APV_ERROR_CODE apv_nRF24L01MasterTransmitManager(apvArduinoSystemIdentifier_t  masterTransmitClass,
                                                 bool                         *masterSchedulingFlag)
  {
/******************************************************************************/

  APV_ERROR_CODE masterTransmitErrors = APV_ERROR_CODE_NONE;

/******************************************************************************/

  switch(masterTransmitClass)
    {
    case APV_ARDUINO_SYSTEM_IDENTITY_MASTER  : // beacon tranmission
                                               break;
    case APV_ARDUINO_SYSTEM_IDENTITY_SLAVE_1 : // per-slave transmission
    case APV_ARDUINO_SYSTEM_IDENTITY_SLAVE_2 :
    case APV_ARDUINO_SYSTEM_IDENTITY_SLAVE_3 :
    case APV_ARDUINO_SYSTEM_IDENTITY_SLAVE_4 :
    case APV_ARDUINO_SYSTEM_IDENTITY_SLAVE_5 :
    case APV_ARDUINO_SYSTEM_IDENTITY_SLAVE_6 :
                                               break;
    default                                  :
                                               masterTransmitErrors = APV_ERROR_CODE_EVENT_UNRECOGNISED_PARAMETER;
                                               break;
    }

  // Temporarily cancel any further scheduling
  *masterSchedulingFlag = false;

/******************************************************************************/

  return(masterTransmitErrors);

/******************************************************************************/
  } /* end of apv_nRF24L01MasterTransmitManager                               */

/******************************************************************************/
/* apv_nRF24L01MasterReceiveManager() :                                       */
/*  --> masterReceiveClass   : [ SLAVE_n ]                                    */
/*  --> masterSchedulingFlag : task internal scheduling flag                  */
/*  <-- masterReceiveErrors  : error codes                                    */
/*                                                                            */
/* - high-level task manager for master reception tasks                       */
/*                                                                            */
/******************************************************************************/

APV_ERROR_CODE apv_nRF24L01MasterReceiveManager(apvArduinoSystemIdentifier_t  masterReceiveClass,
                                                bool                         *masterSchedulingFlag)
  {
/******************************************************************************/

  APV_ERROR_CODE masterReceiveErrors = APV_ERROR_CODE_NONE;

/******************************************************************************/

  switch(masterReceiveClass)
    {
    case APV_ARDUINO_SYSTEM_IDENTITY_SLAVE_1 : // per-slave reception
    case APV_ARDUINO_SYSTEM_IDENTITY_SLAVE_2 :
    case APV_ARDUINO_SYSTEM_IDENTITY_SLAVE_3 :
    case APV_ARDUINO_SYSTEM_IDENTITY_SLAVE_4 :
    case APV_ARDUINO_SYSTEM_IDENTITY_SLAVE_5 :
    case APV_ARDUINO_SYSTEM_IDENTITY_SLAVE_6 :
                                               break;
    default                                  :
                                               masterReceiveErrors = APV_ERROR_CODE_EVENT_UNRECOGNISED_PARAMETER;
                                               break;
    }

  // Temporarily cancel any further scheduling
  *masterSchedulingFlag = false;

/******************************************************************************/

  return(masterReceiveErrors);

/******************************************************************************/
  } /* end of apv_nRF24L01MasterReceiveManager                                */

/******************************************************************************/
/* apv_nRF24L01SlaveTransmitManager() :                                       */
/*  --> slaveTransmitClass  : [ SLAVE_n ]                                     */
/*  --> slaveSchedulingFlag : task internal scheduling flag                   */
/*  <-- slaveReceiveErrors  : error codes                                     */
/*                                                                            */
/* - slave '1 . n' can only transmit to the master                            */
/*                                                                            */
/******************************************************************************/

APV_ERROR_CODE apv_nRF24L01SlaveTransmitManager(apvArduinoSystemIdentifier_t  slaveTransmitClass,
                                                bool                         *slaveSchedulingFlag)
  {
/******************************************************************************/

  APV_ERROR_CODE slaveTransmitErrors = APV_ERROR_CODE_NONE;

/******************************************************************************/

  switch(slaveTransmitClass)
    {
    case APV_ARDUINO_SYSTEM_IDENTITY_SLAVE_1 : // single-slave transmission
    case APV_ARDUINO_SYSTEM_IDENTITY_SLAVE_2 :
    case APV_ARDUINO_SYSTEM_IDENTITY_SLAVE_3 :
    case APV_ARDUINO_SYSTEM_IDENTITY_SLAVE_4 :
    case APV_ARDUINO_SYSTEM_IDENTITY_SLAVE_5 :
    case APV_ARDUINO_SYSTEM_IDENTITY_SLAVE_6 :
    default                                  :
                                               slaveTransmitErrors = APV_ERROR_CODE_EVENT_UNRECOGNISED_PARAMETER;
                                               break;
    }

  // Temporarily cancel any further scheduling
  *slaveSchedulingFlag = false;

/******************************************************************************/

  return(slaveTransmitErrors);

/******************************************************************************/
  } /* end of apv_nRF24L01SlaveTransmitManager                                */

/******************************************************************************/
/* apv_nRF24L01SlaveReceiveManager() :                                        */
/*  --> slaveReceiveClass   : [ SLAVE_n ]                                     */
/*  --> slaveSchedulingFlag : task internal scheduling flag                   */
/*  <-- slaveReceiveErrors  : error codes                                     */
/*                                                                            */
/* - slave '1 . n' can only receive from the master                           */
/*                                                                            */
/******************************************************************************/

APV_ERROR_CODE apv_nRF24L01SlaveReceiveManager(apvArduinoSystemIdentifier_t  slaveReceiveClass,
                                               bool                         *slaveSchedulingFlag)
  {
/******************************************************************************/

  APV_ERROR_CODE slaveReceiveErrors = APV_ERROR_CODE_NONE;

/******************************************************************************/

  switch(slaveReceiveClass)
    {
    case APV_ARDUINO_SYSTEM_IDENTITY_SLAVE_1 : // single-slave reception
    case APV_ARDUINO_SYSTEM_IDENTITY_SLAVE_2 :
    case APV_ARDUINO_SYSTEM_IDENTITY_SLAVE_3 :
    case APV_ARDUINO_SYSTEM_IDENTITY_SLAVE_4 :
    case APV_ARDUINO_SYSTEM_IDENTITY_SLAVE_5 :
    case APV_ARDUINO_SYSTEM_IDENTITY_SLAVE_6 :
    default                                  :
                                               slaveReceiveErrors = APV_ERROR_CODE_EVENT_UNRECOGNISED_PARAMETER;
                                               break;
    }

  // Temporarily cancel any further scheduling
  *slaveSchedulingFlag = false;

/******************************************************************************/

  return(slaveReceiveErrors);

/******************************************************************************/
  } /* end of apv_nRF24L01SlaveReceiveManager                                 */

/******************************************************************************/
/* apv_nRF24L01StateTimer() :                                                 */
/*  --> durationEventMessage : possible callback parameter - in this case a   */
/*                             boolean flag signalling timer expiry           */
/*                                                                            */
/* - device timing callback - set the "timeout" flag                          */
/*                                                                            */
/******************************************************************************/

void apv_nRF24L01StateTimer(void *durationEventMessage)
  {
/******************************************************************************/

  *(bool *)durationEventMessage = true;

/******************************************************************************/
  } /* end of apv_nRF24L01StateTimer                                          */

/******************************************************************************/
/* (C) PulsingCoreSoftware Limited 2018 (C)                                   */
/******************************************************************************/

