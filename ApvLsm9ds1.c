/******************************************************************************/
/* (C) PulsingCoreSoftware Limited 2018 (C)                                   */
/******************************************************************************/
/*                                                                            */
/*  Lsm9ds1.h                                                                 */
/*  09.07.18                                                                  */
/*  Paul O'Brien                                                              */
/*                                                                            */
/*  - definition of communications with the STM LSM9DS1 combined 3-axis       */
/*    gyroscope, accelerometer and magnetometer SPI SLAVE chip                */
/*    Reference : STMicroelectronics NV, "iNEMO inertial module", DocId025715 */
/*    Rev 3, 2015                                                             */
/*                                                                            */
/******************************************************************************/
/* Includes :                                                                 */
/******************************************************************************/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include "sam3x8e.h"
#include "ApvUtilities.h"
#include "ApvError.h"
#include "ApvSerial.h"
#include "ApvPeripheralControl.h"
#include "ApvCommsUtilities.h"
#include "ApvEventTimers.h"
#include "ApvLsm9ds1.h"
#include "ApvMessagingLayerManager.h"
#include "ApvTaskHandlers.h"

/******************************************************************************/
/* Global Variables :                                                         */
/******************************************************************************/
/* The task parameter control block                                           */
/******************************************************************************/

apvLsm9ds1TaskParameterBlock_t apvLsm9ds1TaskParameterBlock;

/******************************************************************************/
/* The SPI0 global interrupt flags :                                          */
/******************************************************************************/

bool apvLsm9ds1CoreActiveFlag     = false; // if the Lsm9ds1 is detected this flag is set to 'true'
bool apvLsm9ds1CoreSchedulingFlag = false; // process scheduling flag
         
/******************************************************************************/
/* Timing flag :                                                              */
/******************************************************************************/

volatile bool apvLsm9ds1TimerFlag  = false;

/******************************************************************************/
/* Local Measurement Report Buffer :                                          */
/******************************************************************************/

static volatile apvLsm9ds1MeasurementReport_t measurementMessageBuffer;

/******************************************************************************/
/* Default measurement parameters :                                           */
/******************************************************************************/

apvLsm9ds1RegisterRanges_t apvLsm9ds1RegisterRanges = 
  {
  APV_LSM9DS1_CTRL_REG6_XL_ODR_XL_50_Hz,
  APV_LSM9DS1_CTRL_REG6_XL_FS_XL_2g,
  APV_LSM9DS1_CTRL_REG1_G_59p5_Hz,
  APV_LSM9DS1_CTRL_REG1_G_245_dps
  };

/******************************************************************************/
/* Measurement Stack Definition :                                             */
/******************************************************************************/

       apvLsm9ds1Measurement_t   apvLsm9ds1Measurements[APV_LSM9DS1_MEASUREMENT_SET];
       apvLsm9ds1MeasurementId_t apvLsm9ds1MeasurementIndex = APV_LSM9DS1_MEASUREMENT_ACCELEROMETER_X;

static unsigned char             apvLsm9ds1DecimalMeasurement[APV_16_BIT_BINARY_2_DECIMAL_WIDTH];

/******************************************************************************/
/* Measurement State Machine Definition :                                     */
/******************************************************************************/

const apvLsm9ds1MeasurementStateDescriptor_t *apvLsm9ds1MeasurementState = &apvLsm9ds1MeasurementStateMachine[APV_LSM9DS1_MEASUREMENT_STATE_INITIALISE];

      // The measurement state-machine variables are kept seperately in RAM
      apvLsm9ds1MeasurementVariable_t         apvLsm9ds1MeasurementVariables[APV_LSM9DS1_MEASUREMENT_STATE_SET];

// The measurement state-machine constants are kept seperately in FLASH-ROM
const apvLsm9ds1MeasurementStateDescriptor_t  apvLsm9ds1MeasurementStateMachine[APV_LSM9DS1_MEASUREMENT_STATE_SET] = 
  {
    { // State : initialisation (general : not associated with a register)
     APV_LSM9DS1_MEASUREMENT_STATE_INITIALISE,                                                         // state ID
     APV_LSM9DS1_REGISTER_ADDRESS_RESERVED_M_00,                                                       // Lsm9ds1 register address
     NULL,                                                                                             // points to the low-register of a 16-bit register access
     APV_LSM9DS1_REGISTER_NO_ACTION,                                                                   // [ READ_DATA == state reads | WRITE_DATA == state writes | NO_ACTION == utility state ] : register access
     APV_LSM9DS1_REGISTER_ACCESS_SIZE_ONE_BYTE,                                                        // registers are one- or two-bytes wide
     0,                                                                                                // the constant part of this state
    &apvLsm9ds1MeasurementVariables[APV_LSM9DS1_MEASUREMENT_STATE_INITIALISE],                         // points to the variable part of this state
     false,                                                                                            // register signed/unsigned : [ false == unsigned | true == signed ]
    &apvLsm9ds1MeasurementStateMachine[APV_LSM9DS1_MEASUREMENT_STATE_ACCELEROMETER_OUTPUT_INITIALISE], // points to the next state
     false                                                                                             // progress to the next state in this scheduling loop instance
    },
    { // State : switch on 3DOF accelerometer
     APV_LSM9DS1_MEASUREMENT_STATE_ACCELEROMETER_OUTPUT_INITIALISE,
     APV_LSM9DS1_REGISTER_ADDRESS_CTRL_REG5_XL,
     NULL,
     APV_LSM9DS1_REGISTER_WRITE_DATA,
     APV_LSM9DS1_REGISTER_ACCESS_SIZE_ONE_BYTE,
     (APV_LSM9DS1_CTRL_REG5_XL_ZEN_XL_MASK | APV_LSM9DS1_CTRL_REG5_XL_YEN_XL_MASK | APV_LSM9DS1_CTRL_REG5_XL_XEN_XL_MASK),
    &apvLsm9ds1MeasurementVariables[APV_LSM9DS1_MEASUREMENT_STATE_ACCELEROMETER_OUTPUT_INITIALISE],
     false,
    &apvLsm9ds1MeasurementStateMachine[APV_LSM9DS1_MEASUREMENT_STATE_GYROSCOPE_OUTPUT_INITIALISE],
     false                                                                                             // progress to the next state in this scheduling loop instance    
    },
    { // State : switch on 3DOF gyroscope
     APV_LSM9DS1_MEASUREMENT_STATE_GYROSCOPE_OUTPUT_INITIALISE,
     APV_LSM9DS1_REGISTER_ADDRESS_CTRL_REG4,
     NULL,
     APV_LSM9DS1_REGISTER_WRITE_DATA,
     APV_LSM9DS1_REGISTER_ACCESS_SIZE_ONE_BYTE,
     (APV_LSM9DS1_CTRL_REG4_ZEN_G_MASK | APV_LSM9DS1_CTRL_REG4_YEN_G_MASK | APV_LSM9DS1_CTRL_REG4_XEN_G_MASK),
    &apvLsm9ds1MeasurementVariables[APV_LSM9DS1_MEASUREMENT_STATE_GYROSCOPE_OUTPUT_INITIALISE],
     false,
    &apvLsm9ds1MeasurementStateMachine[APV_LSM9DS1_MEASUREMENT_STATE_ACCELEROMETER_DATA_RATE_INITIALISE],
     false                                                                                             // progress to the next state in this scheduling loop instance    
    },
    { // State : set the accelerometer output data rate
     APV_LSM9DS1_MEASUREMENT_STATE_ACCELEROMETER_DATA_RATE_INITIALISE,
     APV_LSM9DS1_REGISTER_ADDRESS_CTRL_REG6_XL,
     NULL,
     APV_LSM9DS1_REGISTER_WRITE_DATA,
     APV_LSM9DS1_REGISTER_ACCESS_SIZE_ONE_BYTE,
     APV_LSM9DS1_CTRL_REG6_XL_ODR_XL_50_Hz, // start with a moderate (50Hz) data-rate
    &apvLsm9ds1MeasurementVariables[APV_LSM9DS1_MEASUREMENT_STATE_ACCELEROMETER_DATA_RATE_INITIALISE],
     false,
    &apvLsm9ds1MeasurementStateMachine[APV_LSM9DS1_MEASUREMENT_STATE_GYROSCOPE_DATA_RATE_INITIALISE],
     false                                                                                             // progress to the next state in this scheduling loop instance    
    },
    { // State : set the gyroscope output data rate
     APV_LSM9DS1_MEASUREMENT_STATE_GYROSCOPE_DATA_RATE_INITIALISE,
     APV_LSM9DS1_REGISTER_ADDRESS_CTRL_REG1_G,
     NULL,
     APV_LSM9DS1_REGISTER_WRITE_DATA,
     APV_LSM9DS1_REGISTER_ACCESS_SIZE_ONE_BYTE,
     APV_LSM9DS1_CTRL_REG1_G_59p5_Hz, // start with a moderate (59.5Hz) data-rate
    &apvLsm9ds1MeasurementVariables[APV_LSM9DS1_MEASUREMENT_STATE_GYROSCOPE_DATA_RATE_INITIALISE],
     false,
    &apvLsm9ds1MeasurementStateMachine[APV_LSM9DS1_MEASUREMENT_STATE_READ_ACCELEROMETER_X_LOW],
     true                                                                                             // wait for the next scheduling loop before proceeding - this 
                                                                                                      // gives the device a chance to produce some data!
    },
    { // State :  read the accelerometer 'X' axis low register
     APV_LSM9DS1_MEASUREMENT_STATE_READ_ACCELEROMETER_X_LOW,
     APV_LSM9DS1_REGISTER_ADDRESS_OUT_X_L_XL,
     NULL,
     APV_LSM9DS1_REGISTER_READ_DATA,
     APV_LSM9DS1_REGISTER_ACCESS_SIZE_TWO_BYTES,
     APV_LSM9DS1_MEASUREMENT_AVERAGING_LENGTH,
    &apvLsm9ds1MeasurementVariables[APV_LSM9DS1_MEASUREMENT_STATE_READ_ACCELEROMETER_X_LOW],
     true,
    &apvLsm9ds1MeasurementStateMachine[APV_LSM9DS1_MEASUREMENT_STATE_READ_ACCELEROMETER_X_HIGH],
     false                                                                                             // progress to the next state in this scheduling loop instance    
    },
    { // State :  read the accelerometer 'X' axis high register
     APV_LSM9DS1_MEASUREMENT_STATE_READ_ACCELEROMETER_X_HIGH,
     APV_LSM9DS1_REGISTER_ADDRESS_OUT_X_H_XL,
    &apvLsm9ds1MeasurementStateMachine[APV_LSM9DS1_MEASUREMENT_STATE_READ_ACCELEROMETER_X_LOW],
     APV_LSM9DS1_REGISTER_READ_DATA,
     APV_LSM9DS1_REGISTER_ACCESS_SIZE_TWO_BYTES,
     APV_LSM9DS1_MEASUREMENT_AVERAGING_LENGTH,
    &apvLsm9ds1MeasurementVariables[APV_LSM9DS1_MEASUREMENT_STATE_READ_ACCELEROMETER_X_HIGH],
     true,
    &apvLsm9ds1MeasurementStateMachine[APV_LSM9DS1_MEASUREMENT_STATE_READ_ACCELEROMETER_Y_LOW],
     false                                                                                             // progress to the next state in this scheduling loop instance    
    },
    { // State :  read the accelerometer 'Y' axis low register
     APV_LSM9DS1_MEASUREMENT_STATE_READ_ACCELEROMETER_Y_LOW,
     APV_LSM9DS1_REGISTER_ADDRESS_OUT_Y_L_XL,
     NULL,
     APV_LSM9DS1_REGISTER_READ_DATA,
     APV_LSM9DS1_REGISTER_ACCESS_SIZE_TWO_BYTES,
     APV_LSM9DS1_MEASUREMENT_AVERAGING_LENGTH,
    &apvLsm9ds1MeasurementVariables[APV_LSM9DS1_MEASUREMENT_STATE_READ_ACCELEROMETER_Y_LOW],
     true,
    &apvLsm9ds1MeasurementStateMachine[APV_LSM9DS1_MEASUREMENT_STATE_READ_ACCELEROMETER_Y_HIGH],
     false                                                                                             // progress to the next state in this scheduling loop instance    
    },
    { // State :  read the accelerometer 'Y' axis high register
     APV_LSM9DS1_MEASUREMENT_STATE_READ_ACCELEROMETER_Y_HIGH,
     APV_LSM9DS1_REGISTER_ADDRESS_OUT_Y_H_XL,
    &apvLsm9ds1MeasurementStateMachine[APV_LSM9DS1_MEASUREMENT_STATE_READ_ACCELEROMETER_Y_LOW],
     APV_LSM9DS1_REGISTER_READ_DATA,
     APV_LSM9DS1_REGISTER_ACCESS_SIZE_TWO_BYTES,
     APV_LSM9DS1_MEASUREMENT_AVERAGING_LENGTH,
    &apvLsm9ds1MeasurementVariables[APV_LSM9DS1_MEASUREMENT_STATE_READ_ACCELEROMETER_Y_HIGH],
     true,
    &apvLsm9ds1MeasurementStateMachine[APV_LSM9DS1_MEASUREMENT_STATE_READ_ACCELEROMETER_Z_LOW],
     false                                                                                             // progress to the next state in this scheduling loop instance    
    },
    { // State :  read the accelerometer 'Z' axis low register
     APV_LSM9DS1_MEASUREMENT_STATE_READ_ACCELEROMETER_Z_LOW,
     APV_LSM9DS1_REGISTER_ADDRESS_OUT_Z_L_XL,
     NULL,
     APV_LSM9DS1_REGISTER_READ_DATA,
     APV_LSM9DS1_REGISTER_ACCESS_SIZE_TWO_BYTES,
     APV_LSM9DS1_MEASUREMENT_AVERAGING_LENGTH,
    &apvLsm9ds1MeasurementVariables[APV_LSM9DS1_MEASUREMENT_STATE_READ_ACCELEROMETER_Z_LOW],
     true,
    &apvLsm9ds1MeasurementStateMachine[APV_LSM9DS1_MEASUREMENT_STATE_READ_ACCELEROMETER_Z_HIGH],
     false                                                                                             // progress to the next state in this scheduling loop instance    
    },
    { // State :  read the accelerometer 'Z' axis high register
     APV_LSM9DS1_MEASUREMENT_STATE_READ_ACCELEROMETER_Z_HIGH,
     APV_LSM9DS1_REGISTER_ADDRESS_OUT_Z_H_XL,
    &apvLsm9ds1MeasurementStateMachine[APV_LSM9DS1_MEASUREMENT_STATE_READ_ACCELEROMETER_Z_LOW],
     APV_LSM9DS1_REGISTER_READ_DATA,
     APV_LSM9DS1_REGISTER_ACCESS_SIZE_TWO_BYTES,
     APV_LSM9DS1_MEASUREMENT_AVERAGING_LENGTH,
    &apvLsm9ds1MeasurementVariables[APV_LSM9DS1_MEASUREMENT_STATE_READ_ACCELEROMETER_Z_HIGH],
     true,
    &apvLsm9ds1MeasurementStateMachine[APV_LSM9DS1_MEASUREMENT_STATE_READ_GYROSCOPE_X_LOW],
     false                                                                                             // progress to the next state in this scheduling loop instance    
    },
    { // State :  read the gyroscope 'X' axis low register
     APV_LSM9DS1_MEASUREMENT_STATE_READ_GYROSCOPE_X_LOW,
     APV_LSM9DS1_REGISTER_ADDRESS_OUT_X_L_G,
     NULL,
     APV_LSM9DS1_REGISTER_READ_DATA,
     APV_LSM9DS1_REGISTER_ACCESS_SIZE_TWO_BYTES,
     APV_LSM9DS1_MEASUREMENT_AVERAGING_LENGTH,
    &apvLsm9ds1MeasurementVariables[APV_LSM9DS1_MEASUREMENT_STATE_READ_GYROSCOPE_X_LOW],
     true,
    &apvLsm9ds1MeasurementStateMachine[APV_LSM9DS1_MEASUREMENT_STATE_READ_GYROSCOPE_X_HIGH],
     false                                                                                             // progress to the next state in this scheduling loop instance    
    },
    { // State :  read the gyroscope 'X' axis high register
     APV_LSM9DS1_MEASUREMENT_STATE_READ_GYROSCOPE_X_HIGH,
     APV_LSM9DS1_REGISTER_ADDRESS_OUT_X_H_G,
    &apvLsm9ds1MeasurementStateMachine[APV_LSM9DS1_MEASUREMENT_STATE_READ_GYROSCOPE_X_LOW],
     APV_LSM9DS1_REGISTER_READ_DATA,
     APV_LSM9DS1_REGISTER_ACCESS_SIZE_TWO_BYTES,
     APV_LSM9DS1_MEASUREMENT_AVERAGING_LENGTH,
    &apvLsm9ds1MeasurementVariables[APV_LSM9DS1_MEASUREMENT_STATE_READ_GYROSCOPE_X_HIGH],
     true,
    &apvLsm9ds1MeasurementStateMachine[APV_LSM9DS1_MEASUREMENT_STATE_READ_GYROSCOPE_Y_LOW],
     false                                                                                             // progress to the next state in this scheduling loop instance    
    },
    { // State :  read the gyroscope 'Y' axis low register
     APV_LSM9DS1_MEASUREMENT_STATE_READ_GYROSCOPE_Y_LOW,
     APV_LSM9DS1_REGISTER_ADDRESS_OUT_Y_L_G,
     NULL,
     APV_LSM9DS1_REGISTER_READ_DATA,
     APV_LSM9DS1_REGISTER_ACCESS_SIZE_TWO_BYTES,
     APV_LSM9DS1_MEASUREMENT_AVERAGING_LENGTH,
    &apvLsm9ds1MeasurementVariables[APV_LSM9DS1_MEASUREMENT_STATE_READ_GYROSCOPE_Y_LOW],
     true,
    &apvLsm9ds1MeasurementStateMachine[APV_LSM9DS1_MEASUREMENT_STATE_READ_GYROSCOPE_Y_HIGH],
     false                                                                                             // progress to the next state in this scheduling loop instance    
    },
    { // State :  read the gyroscope 'Y' axis high register
     APV_LSM9DS1_MEASUREMENT_STATE_READ_GYROSCOPE_Y_HIGH,
     APV_LSM9DS1_REGISTER_ADDRESS_OUT_Y_H_G,
    &apvLsm9ds1MeasurementStateMachine[APV_LSM9DS1_MEASUREMENT_STATE_READ_GYROSCOPE_Y_LOW],
     APV_LSM9DS1_REGISTER_READ_DATA,
     APV_LSM9DS1_REGISTER_ACCESS_SIZE_TWO_BYTES,
     APV_LSM9DS1_MEASUREMENT_AVERAGING_LENGTH,
    &apvLsm9ds1MeasurementVariables[APV_LSM9DS1_MEASUREMENT_STATE_READ_GYROSCOPE_Y_HIGH],
     true,
    &apvLsm9ds1MeasurementStateMachine[APV_LSM9DS1_MEASUREMENT_STATE_READ_GYROSCOPE_Z_LOW],
     false                                                                                             // progress to the next state in this scheduling loop instance    
    },
    { // State :  read the gyroscope 'Z' axis low register
     APV_LSM9DS1_MEASUREMENT_STATE_READ_GYROSCOPE_Z_LOW,
     APV_LSM9DS1_REGISTER_ADDRESS_OUT_Z_L_G,
     NULL,
     APV_LSM9DS1_REGISTER_READ_DATA,
     APV_LSM9DS1_REGISTER_ACCESS_SIZE_TWO_BYTES,
     APV_LSM9DS1_MEASUREMENT_AVERAGING_LENGTH,
    &apvLsm9ds1MeasurementVariables[APV_LSM9DS1_MEASUREMENT_STATE_READ_GYROSCOPE_Z_LOW],
     true,
    &apvLsm9ds1MeasurementStateMachine[APV_LSM9DS1_MEASUREMENT_STATE_READ_GYROSCOPE_Z_HIGH],
     false                                                                                             // progress to the next state in this scheduling loop instance    
    },
    { // State :  read the gyroscope 'Z' axis high register
     APV_LSM9DS1_MEASUREMENT_STATE_READ_GYROSCOPE_Z_HIGH,
     APV_LSM9DS1_REGISTER_ADDRESS_OUT_Z_H_G,
    &apvLsm9ds1MeasurementStateMachine[APV_LSM9DS1_MEASUREMENT_STATE_READ_GYROSCOPE_Z_LOW],
     APV_LSM9DS1_REGISTER_READ_DATA,
     APV_LSM9DS1_REGISTER_ACCESS_SIZE_TWO_BYTES,
     APV_LSM9DS1_MEASUREMENT_AVERAGING_LENGTH,
    &apvLsm9ds1MeasurementVariables[APV_LSM9DS1_MEASUREMENT_STATE_READ_GYROSCOPE_Z_HIGH],
     true,
    &apvLsm9ds1MeasurementStateMachine[APV_LSM9DS1_MEASUREMENT_STATE_READ_TEMPERATURE_LOW],
     false                                                                                             // progress to the next state in this scheduling loop instance    
    },
    { // State :  read the temperature low register
     APV_LSM9DS1_MEASUREMENT_STATE_READ_TEMPERATURE_LOW,
     APV_LSM9DS1_REGISTER_ADDRESS_OUT_TEMP_L,
     NULL,
     APV_LSM9DS1_REGISTER_READ_DATA,
     APV_LSM9DS1_REGISTER_ACCESS_SIZE_TWO_BYTES,
     APV_LSM9DS1_MEASUREMENT_AVERAGING_LENGTH,
    &apvLsm9ds1MeasurementVariables[APV_LSM9DS1_MEASUREMENT_STATE_READ_TEMPERATURE_LOW],
     true,
    &apvLsm9ds1MeasurementStateMachine[APV_LSM9DS1_MEASUREMENT_STATE_READ_TEMPERATURE_HIGH],
     false                                                                                             // progress to the next state in this scheduling loop instance    
    },
    { // State :  read the temperature high register
     APV_LSM9DS1_MEASUREMENT_STATE_READ_TEMPERATURE_HIGH,
     APV_LSM9DS1_REGISTER_ADDRESS_OUT_TEMP_H,
    &apvLsm9ds1MeasurementStateMachine[APV_LSM9DS1_MEASUREMENT_STATE_READ_TEMPERATURE_LOW],
     APV_LSM9DS1_REGISTER_READ_DATA,
     APV_LSM9DS1_REGISTER_ACCESS_SIZE_TWO_BYTES,
     APV_LSM9DS1_MEASUREMENT_AVERAGING_LENGTH,
    &apvLsm9ds1MeasurementVariables[APV_LSM9DS1_MEASUREMENT_STATE_READ_TEMPERATURE_HIGH],
     true,
    &apvLsm9ds1MeasurementStateMachine[APV_LSM9DS1_MEASUREMENT_STATE_WRITE_RESULTS],
     false                                                                                             // progress to the next state in this scheduling loop instance
    },
    { // State : (if enabled) successively write Lsm9ds1 mweasurements to the UART output serial port
     APV_LSM9DS1_MEASUREMENT_STATE_WRITE_RESULTS,
     APV_LSM9DS1_REGISTER_ADDRESS_RESERVED_M_00,                                                       // Lsm9ds1 register address
     NULL,                                                                                             // points to the low-register of a 16-bit register access
     APV_LSM9DS1_REGISTER_STREAM_DATA,                                                                 // [ READ_DATA == state reads | WRITE_DATA == state writes | NO_ACTION == utility state ] : register access
     APV_LSM9DS1_REGISTER_ACCESS_SIZE_ONE_BYTE,                                                        // registers are one- or two-bytes wide
     0,                                                                                                // the constant part of this state
    &apvLsm9ds1MeasurementVariables[APV_LSM9DS1_MEASUREMENT_STATE_READ_ACCELEROMETER_X_LOW],           // points to the variable part of this state
     false,                                                                                            // register signed/unsigned : [ false == unsigned | true == signed ]
    &apvLsm9ds1MeasurementStateMachine[APV_LSM9DS1_MEASUREMENT_STATE_INITIALISE],                      // points to the next state
     true                                                                                              // exit the measurement loop after this state completes  
    },
    { // State : error - going nowhere!
     APV_LSM9DS1_MEASUREMENT_STATE_ERROR,                                                    
     APV_LSM9DS1_REGISTER_ADDRESS_RESERVED_M_00,                                                      
     NULL,                                                                                            
     APV_LSM9DS1_REGISTER_NO_ACTION,                                                                  
     APV_LSM9DS1_REGISTER_ACCESS_SIZE_ONE_BYTE,                                                       
     0,                                                                                               
   &apvLsm9ds1MeasurementVariables[APV_LSM9DS1_MEASUREMENT_STATE_ERROR],                        
     false,                                                                                           
    &apvLsm9ds1MeasurementStateMachine[APV_LSM9DS1_MEASUREMENT_STATE_ERROR],
     true
     }
  };

/******************************************************************************/

// Section 6, "Register Mapping", Table 21
const apvLsm9ds1RegisterDescriptor_t apvLsm9ds1AccelerometerAndGyroRegisters[APV_LSM9DS1_ACCELEROMETER_GYRO_REGISTER_SET_SIZE] = 
  {
    { // Reserved
      APV_LSM9DS1_REGISTER_ADDRESS_RESERVED_00,
      APV_LSM9DS1_REGISTER_RESERVED,
      APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    { // Reserved
      APV_LSM9DS1_REGISTER_ADDRESS_RESERVED_01,
      APV_LSM9DS1_REGISTER_RESERVED,
      APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    { // Reserved
      APV_LSM9DS1_REGISTER_ADDRESS_RESERVED_02,
      APV_LSM9DS1_REGISTER_RESERVED,
      APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    { // Reserved
      APV_LSM9DS1_REGISTER_ADDRESS_RESERVED_03,
      APV_LSM9DS1_REGISTER_RESERVED,
      APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    { // Inactivity threshold : 0:6 --> inactivity threshold default 0x00
      // Inactivity mode      : 7   --> [ 0 == power-down | 1 == sleep ] default 0
    APV_LSM9DS1_REGISTER_ADDRESS_ACT_THS,
    APV_LSM9DS1_REGISTER_READ_WRITE,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    { // Inactivity duration : 0:7 --> inactivity duration default 0x00
    APV_LSM9DS1_REGISTER_ADDRESS_ACT_DUR,
    APV_LSM9DS1_REGISTER_READ_WRITE,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    { // Accelerometer interrupt configuration :
      // 0x01 : enable XLIE
      // 0x02 : enable XHIE
      // 0x04 : enable YLIE
      // 0x08 : enable YHIE
      // 0x10 : enable ZLIE
      // 0x20 : enable ZHIE
      // 0x40 : enable 6D detection
      // 0x80 : [ 0 == OR | 1 == AND ] interrupt event combination
    APV_LSM9DS1_REGISTER_ADDRESS_INT_GEN_CFG_XL,
    APV_LSM9DS1_REGISTER_READ_WRITE,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    { // Accelerometer X-axis interrupt threshold : 0:7 interrupt threshold default 0x00
    APV_LSM9DS1_REGISTER_ADDRESS_INT_GEN_THS_X_XL,
    APV_LSM9DS1_REGISTER_READ_WRITE,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    { // Accelerometer Y-axis interrupt threshold : 0:7 interrupt threshold default 0x00
    APV_LSM9DS1_REGISTER_ADDRESS_INT_GEN_THS_Y_XL,
    APV_LSM9DS1_REGISTER_READ_WRITE,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    { // Accelerometer Z-axis interrupt threshold : 0:7 interrupt threshold default 0x00
    APV_LSM9DS1_REGISTER_ADDRESS_INT_GEN_THS_Z_XL,
    APV_LSM9DS1_REGISTER_READ_WRITE,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    { // Accelerometer interrupt duration : 0:6 --> interrupt duration default 0x00
      // Wait on interrupt                : 7   --> [ 0 == no wait on interrupt exit | 1 == wait for "duration" on interrupt exit ]
    APV_LSM9DS1_REGISTER_ADDRESS_INT_GEN_DUR_XL,
    APV_LSM9DS1_REGISTER_READ_WRITE,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    { // Gyro high-pass filter reference : 0:7 --> hp filter reference default 0x00
    APV_LSM9DS1_REGISTER_ADDRESS_REFERENCE_G,
    APV_LSM9DS1_REGISTER_READ_WRITE,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    { // Accelerometer/gyro pin INT1_A/G control : default 0x00
      // 0x01 : enable accelerometer "data-ready" interrupt on INT1_A/G
      // 0x02 : enable gyroscope "data-ready" interrupt on INT1_A/G
      // 0x04 : enable "boot status ready" interrupt on INT1_A/G 
      // 0x08 : enable FIFO threshold interrupt on INT1_A/G
      // 0x10 : enable overrun interrupt on INT1_A/G
      // 0x20 : enable FSS5 interrupt on INT1_A/G
      // 0x40 : enable accelerometer interrupt on INT1_A/G
      // 0x80 : enable gyro interrupt on INT1_A/G
    APV_LSM9DS1_REGISTER_ADDRESS_INT1_CTRL,
    APV_LSM9DS1_REGISTER_READ_WRITE,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    { // Accelerometer/gyro pin INT2_A/G control : default 0x00
      // 0x01 : enable accelerometer "data-ready" interrupt on INT2_A/G
      // 0x02 : enable gyroscope "data-ready" interrupt on INT2_A/G
      // 0x04 : enable "temperature ready" interrupt on INT2_A/G
      // 0x08 : enable FIFO threshold interrupt on INT2_A/G
      // 0x10 : enable overrun interrupt on INT2_A/G
      // 0x20 : enable FSS5 interrupt on INT2_A/G
      // 0x80 : enable inactivity interrupt on INT2_A/G
    APV_LSM9DS1_REGISTER_ADDRESS_INT2_CTRL,
    APV_LSM9DS1_REGISTER_READ_WRITE,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    { // Reserved
    APV_LSM9DS1_REGISTER_ADDRESS_RESERVED_0D,
    APV_LSM9DS1_REGISTER_RESERVED,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    { // Who am I ? : 0:7 --> accelerometer.gyro register set id default 0x68
    APV_LSM9DS1_REGISTER_ADDRESS_WHO_AM_I,
    APV_LSM9DS1_REGISTER_READ_ONLY,
    APV_LSM9DS1_REGISTER_WHO_AM_I_DEFAULT
    },
    { // Gyro control register : default 0x00
      // 0x03 : bandwidth selection
      // 0x18 : full-scale selection
      // 0xe0 : output data-rate selection
    APV_LSM9DS1_REGISTER_ADDRESS_CTRL_REG1_G,
    APV_LSM9DS1_REGISTER_READ_WRITE,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    { // Gyro control register : default 0x00
      // 0x03 : output selector
      // 0xC0 : interrupt selector
    APV_LSM9DS1_REGISTER_ADDRESS_CTRL_REG2_G,
    APV_LSM9DS1_REGISTER_READ_WRITE,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    { // Gyro control register :  : default 0x00
      // 0x0F : high-pass filter cutoff selection (output data-rate dependent)
      // 0x40 : high-pass filter enable [ 0 == disable | 1 == enable ]
      // 0x80 : low pass-filter enable  [ 0 == disable | 1 == enable ]
    APV_LSM9DS1_REGISTER_ADDRESS_CTRL_REG3_G,
    APV_LSM9DS1_REGISTER_READ_WRITE,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    { // Gyro orientation/sign control : default 0x00
      // 0x07 : directional user orientation
      // 0x08 : yaw angular rate sign   [ 0 == '+' | 1 == '-' ]
      // 0x10 : roll angular rate sign  [ 0 == '+' | 1 == '-' ]
      // 0x20 : pitch angular rate sign [ 0 == '+' | 1 == '-' ]
    APV_LSM9DS1_REGISTER_ADDRESS_ORIENT_CFG_G,
    APV_LSM9DS1_REGISTER_READ_WRITE,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    { // Gyro interrupt configuration : default n/a
      // 0x01 : pitch low interrupt has occurred
      // 0x02 : pitch high interrupt has occurred
      // 0x04 : roll low interrupt has occurred
      // 0x08 : roll high interrupt has occurred
      // 0x10 : yaw low interrupt has occurred
      // 0x20 : yaw high interrupt has occurred
      // 0x40 : [ 0 == no  interrupt has occurred | 1 == interrupt has occurred ]
    APV_LSM9DS1_REGISTER_ADDRESS_INT_GEN_SRC_G,
    APV_LSM9DS1_REGISTER_READ_ONLY,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    { // Temperature data low : 2's complement temperature low : default n/a
    APV_LSM9DS1_REGISTER_ADDRESS_OUT_TEMP_L,
    APV_LSM9DS1_REGISTER_READ_ONLY,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    { // Temperature data low : 2's complement temperature high : default n/a
    APV_LSM9DS1_REGISTER_ADDRESS_OUT_TEMP_H,
    APV_LSM9DS1_REGISTER_READ_ONLY,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    { // Accelerometer + gyro status register : default n/a
      // 0x01 : accelerometer data available
      // 0x02 : gyro data available
      // 0x04 : temperature data available
      // 0x08 : boot atatus : [ 0 == no boot | 1 == booting ]
      // 0x10 : inactivity interrupt status : [ 0 == no interrupt | 1 == interrupt ]
      // 0x20 : gyro interrupt status : [ 0 == no interrupt | 1 == interrupt ]
      // 0x40 : accelerometer interrupt status : [ 0 == no interrupt | 1 == interrupt ]
    APV_LSM9DS1_REGISTER_ADDRESS_STATUS_17,
    APV_LSM9DS1_REGISTER_READ_ONLY,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    { // Gyro 2's complement X-rate low : default n/a
    APV_LSM9DS1_REGISTER_ADDRESS_OUT_X_L_G,
    APV_LSM9DS1_REGISTER_READ_ONLY,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    { // Gyro 2's complement X-rate high : default n/a
    APV_LSM9DS1_REGISTER_ADDRESS_OUT_X_H_G,
    APV_LSM9DS1_REGISTER_READ_ONLY,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    { // Gyro 2's complement Y-rate low : default n/a
    APV_LSM9DS1_REGISTER_ADDRESS_OUT_Y_L_G,
    APV_LSM9DS1_REGISTER_READ_ONLY,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    { // Gyro 2's complement Y-rate high : default n/a
    APV_LSM9DS1_REGISTER_ADDRESS_OUT_Y_H_G,
    APV_LSM9DS1_REGISTER_READ_ONLY,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    { // Gyro 2's complement Z-rate low : default n/a
    APV_LSM9DS1_REGISTER_ADDRESS_OUT_Z_L_G,
    APV_LSM9DS1_REGISTER_READ_ONLY,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    { // Gyro 2's complement Z-rate high : default n/a
    APV_LSM9DS1_REGISTER_ADDRESS_OUT_Z_H_G,
    APV_LSM9DS1_REGISTER_READ_ONLY,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    { // Accelerometer + gyro control register : default 0x38
      // 0x01 : position recognition interrupt : [ 0 == 6D | 1 == 4D ]
      // 0x02 : interrupt latch : [ 0 == no latch | 1 == latch ]
      // 0x08 : gyro pitch axis output enable [ 0 == not enabled | 1 = enabled ]
      // 0x10 : gyro roll axis output enable  [ 0 == not enabled | 1 = enabled ]
      // 0x20 : gyro yaw axis output enable   [ 0 == not enabled | 1 = enabled ]
    APV_LSM9DS1_REGISTER_ADDRESS_CTRL_REG4,
    APV_LSM9DS1_REGISTER_READ_WRITE,
    APV_LSM9DS1_REGISTER_CTRL_REG4_DEFAULT
    },
    { // Accelerometer control register : default 0x38
      // 0x08 : x-axis output enable [ 0 == not enabled | 1 = enabled ]
      // 0x10 : y-axis output enable [ 0 == not enabled | 1 = enabled ]
      // 0x20 : z-axis output enable [ 0 == not enabled | 1 = enabled ]
      // 0xC0 : sampling rate decimation
    APV_LSM9DS1_REGISTER_ADDRESS_CTRL_REG5_XL,
    APV_LSM9DS1_REGISTER_READ_WRITE,
    APV_LSM9DS1_REGISTER_CTRL_REG5_XL_DEFAULT
    },
    { // Accelerometer control register : default 0x00
      // 0x03 : anti-aliasing filter bandwidth
      // 0x04 : bandwidth selection
      // 0x18 : full-scale selection
      // 0xE0 : output data rate selection
    APV_LSM9DS1_REGISTER_ADDRESS_CTRL_REG6_XL,
    APV_LSM9DS1_REGISTER_READ_WRITE,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    { // Accelerometer control register : default 0x00
      // 0x01 : high-pass filter enable [ 0 == bypassed | 1 = enabled ]
      // 0x04 : filtered data enable    [ 0 == bypassed | 1 = enabled ]
      // 0x60 : low- and high-pass filters' bandwidth select
      // 0x80 : high-resolution mode [ 0 == bypassed | 1 = enabled ]
    APV_LSM9DS1_REGISTER_ADDRESS_CTRL_REG7_XL,
    APV_LSM9DS1_REGISTER_READ_WRITE,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    { // Device control register : default 0x04
      // 0x01 : softwaze reset [ 0 == normal | 1 == reset ]
      // 0x02 : endian-ness [ 0 == little | 1 == big ]
      // 0x04 : register address auto-increment [ 0 == no | 1 == yes ]
      // 0x08 : SPI interface mode [ 0 == 4-wire | 1 == 3-wire ]
      // 0x10 : INT1_A/G and INT2_A/G drive mode [ 0 == push/pull | 1 == open-drain ]
      // 0x20 : interrupt active level [ 0 == active high | 1 == active low ]
      // 0x40 : block data update mode [ 0 == continuous | 1 == wait on MSB/LSB read ]
      // 0x80 : memory reboot [ 0 == norma | 1 == reboot ]
    APV_LSM9DS1_REGISTER_ADDRESS_CTRL_REG8,
    APV_LSM9DS1_REGISTER_READ_WRITE,
    APV_LSM9DS1_REGISTER_CTRL_REG8_DEFAULT
    },
    { // Device control register : default 0x00
      // 0x01 : enable FIFO threshold [ 0 == not enabled | 1 == enable ]
      // 0x02 : FIFO enable           [ 0 == not enabled | 1 == enable ]
      // 0x04 : enable/disable I2C    [ 0 == I2C enabled | 1 == I2C not enabled ]
      // 0x08 : data available timer  [ 0 == not enabled | 1 == enable ]
      // 0x10 : store temperature data in FIFO [ 0 == not enabled | 1 == enable ]
      // 0x40 : gyro sleep mode [ 0 == not enabled | 1 == enable ]
    APV_LSM9DS1_REGISTER_ADDRESS_CTRL_REG9,
    APV_LSM9DS1_REGISTER_READ_WRITE,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    { // Accelerometer + gyro control register : default 0x00
      // 0x01 : accelerometer self-test [ 0 == not enabled | 1 == enable ]
      // 0x04 : gyo self-test           [ 0 == not enabled | 1 == enable ]
    APV_LSM9DS1_REGISTER_ADDRESS_CTRL_REG10,
    APV_LSM9DS1_REGISTER_READ_WRITE,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    { // Reserved
    APV_LSM9DS1_REGISTER_ADDRESS_RESERVED_25,
    APV_LSM9DS1_REGISTER_RESERVED,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    { // Accelerometer interrupt source : default n/a
      // 0x01 : X-low event interrupt has occurred
      // 0x02 : X-high event interrupt has occurred
      // 0x04 : Y-low event interrupt has occurred
      // 0x08 : Y-high event interrupt has occurred
      // 0x10 : Z-low event interrupt has occurred
      // 0x20 : Z-high event interrupt has occurred
      // 0x40 : interrupt state [ 0 == no interrupt | 1 == interrupt ]
    APV_LSM9DS1_REGISTER_ADDRESS_INT_GEN_SRC_XL,
    APV_LSM9DS1_REGISTER_READ_ONLY,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    { // Device status register : default n/a
      // 0x01 : accelerometer new data available [ 0 == none | 1 == new data ]
      // 0x02 : gyro new data available          [ 0 == none | 1 == new data ]
      // 0x04 : temperature new data available   [ 0 == none | 1 == new data ]
      // 0x08 : boot status [ 0 == no boot | 1 == boot ]
      // 0x10 : inactivity interrupt [ 0 == no interrupt | 1 == interrupt ]
      // 0x20 : gyro interrupt          [ 0 == no interrupt | 1 == interrupt ]
      // 0x40 : accelerometer interrupt [ 0 == no interrupt | 1 == interrupt ]
    APV_LSM9DS1_REGISTER_ADDRESS_STATUS_27,
    APV_LSM9DS1_REGISTER_READ_ONLY,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    { // Accelerometer 2's complement X-axis low : default n/a
    APV_LSM9DS1_REGISTER_ADDRESS_OUT_X_L_XL,
    APV_LSM9DS1_REGISTER_READ_ONLY,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    { // Accelerometer 2's complement X-axis high : default n/a
    APV_LSM9DS1_REGISTER_ADDRESS_OUT_X_H_XL,
    APV_LSM9DS1_REGISTER_READ_ONLY,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    { // Accelerometer 2's complement Y-axis low : default n/a
    APV_LSM9DS1_REGISTER_ADDRESS_OUT_Y_L_XL,
    APV_LSM9DS1_REGISTER_READ_ONLY,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    { // Accelerometer 2's complement Y-axis high : default n/a
    APV_LSM9DS1_REGISTER_ADDRESS_OUT_Y_H_XL,
    APV_LSM9DS1_REGISTER_READ_ONLY,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    { // Accelerometer 2's complement Z-axis low : default n/a
    APV_LSM9DS1_REGISTER_ADDRESS_OUT_Z_L_XL,
    APV_LSM9DS1_REGISTER_READ_ONLY,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    { // Accelerometer 2's complement Z-axis high : default n/a
    APV_LSM9DS1_REGISTER_ADDRESS_OUT_Z_H_XL,
    APV_LSM9DS1_REGISTER_READ_ONLY,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    { // FIFO control : default 0x00
      // 0x1F : threshold
      // 0xE0 : mode selection
    APV_LSM9DS1_REGISTER_ADDRESS_FIFO_CTRL,
    APV_LSM9DS1_REGISTER_READ_WRITE,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    { // FIFO source : default n/a
      // 0x3F : FIFO sample occupancy
      // 0x40 : overrun               [ 0 == FIFO not full | FIFO full ]
      // 0x80 : FIFO threshold status [ 0 == FIFO < threshold | FIFO >= threshold ]
    APV_LSM9DS1_REGISTER_ADDRESS_FIFO_SRC,
    APV_LSM9DS1_REGISTER_READ_ONLY,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    { // Gyro interrupt source configuration : default 0x00
      // 0x01 : enable X-low event
      // 0x02 : enable X-high event
      // 0x04 : enable Y-low event
      // 0x08 : enable Y-high event
      // 0x10 : enable Z-low event
      // 0x20 : enable Z-high event
      // 0x40 : enable interrupt latch [ 0 == no latch | 1 == latch ]
      // 0x80 : [ 0 == OR interrupt events | 1 == AND interrupt events ]
    APV_LSM9DS1_REGISTER_ADDRESS_INT_GEN_CFG_G,
    APV_LSM9DS1_REGISTER_READ_WRITE,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    { // Gyro 2's complement X-rate interrupt threshold low : default 0x00
    APV_LSM9DS1_REGISTER_ADDRESS_INT_GEN_THS_XH_G,
    APV_LSM9DS1_REGISTER_READ_WRITE,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    { // Gyro 2's complement X-rate interrupt threshold high + counter mode : default 0x00
      // 0x7F : threshold high
      // 0x80 : counter mode [ 0 == reset | 1 == decrement ]
    APV_LSM9DS1_REGISTER_ADDRESS_INT_GEN_THS_XL_G,
    APV_LSM9DS1_REGISTER_READ_WRITE,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    { // Gyro 2's complement Y-rate interrupt threshold low : default 0x00
    APV_LSM9DS1_REGISTER_ADDRESS_INT_GEN_THS_YH_G,
    APV_LSM9DS1_REGISTER_READ_WRITE,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    { // Gyro 2's complement Y-rate interrupt threshold high : default 0x00
      // 0x7F : threshold high
    APV_LSM9DS1_REGISTER_ADDRESS_INT_GEN_THS_YL_G,
    APV_LSM9DS1_REGISTER_READ_WRITE,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    { // Gyro 2's complement Z-rate interrupt threshold low : default 0x00
    APV_LSM9DS1_REGISTER_ADDRESS_INT_GEN_THS_ZH_G,
    APV_LSM9DS1_REGISTER_READ_WRITE,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    { // Gyro 2's complement Z-rate interrupt threshold high : default 0x00
      // 0x7F : threshold high
    APV_LSM9DS1_REGISTER_ADDRESS_INT_GEN_THS_ZL_G,
    APV_LSM9DS1_REGISTER_READ_WRITE,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    { // Gyro interrupt exit wait : default 0x00
      // 0x7F : wait duration (see "Gyro 2's complement X-rate interrupt threshold high + counter mode")
      // 0x80 : wait function enable [ 0 == no wait | 1 == wait ]
    APV_LSM9DS1_REGISTER_ADDRESS_INT_GEN_DUR_G,
    APV_LSM9DS1_REGISTER_READ_WRITE,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    }
  };

// Section 6, "Register Mapping", Table 22
const apvLsm9ds1RegisterDescriptor_t apvLsm9ds1MagnetometerRegisters[APV_LSM9DS1_MAGNETOMETER_REGISTER_SET_SIZE] = 
  {
    {  
    APV_LSM9DS1_REGISTER_ADDRESS_RESERVED_M_00,
    APV_LSM9DS1_REGISTER_RESERVED,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    {  
    APV_LSM9DS1_REGISTER_ADDRESS_RESERVED_M_01,
    APV_LSM9DS1_REGISTER_RESERVED,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    {  
    APV_LSM9DS1_REGISTER_ADDRESS_RESERVED_M_02,
    APV_LSM9DS1_REGISTER_RESERVED,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    {  
    APV_LSM9DS1_REGISTER_ADDRESS_RESERVED_M_03,
    APV_LSM9DS1_REGISTER_RESERVED,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    {  
    APV_LSM9DS1_REGISTER_ADDRESS_RESERVED_M_04,
    APV_LSM9DS1_REGISTER_RESERVED,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    {  
    APV_LSM9DS1_REGISTER_ADDRESS_OFFSET_X_REG_L_M,
    APV_LSM9DS1_REGISTER_READ_WRITE,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    {  
    APV_LSM9DS1_REGISTER_ADDRESS_OFFSET_X_REG_H_M,
    APV_LSM9DS1_REGISTER_READ_WRITE,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    {  
    APV_LSM9DS1_REGISTER_ADDRESS_OFFSET_Y_REG_L_M,
    APV_LSM9DS1_REGISTER_READ_WRITE,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    {  
    APV_LSM9DS1_REGISTER_ADDRESS_OFFSET_Y_REG_H_M,
    APV_LSM9DS1_REGISTER_READ_WRITE,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    {  
    APV_LSM9DS1_REGISTER_ADDRESS_OFFSET_Z_REG_L_M,
    APV_LSM9DS1_REGISTER_READ_WRITE,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    {  
    APV_LSM9DS1_REGISTER_ADDRESS_OFFSET_Z_REG_H_M,
    APV_LSM9DS1_REGISTER_READ_WRITE,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    {  
    APV_LSM9DS1_REGISTER_ADDRESS_RESERVED_M_0B,
    APV_LSM9DS1_REGISTER_RESERVED,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    {  
    APV_LSM9DS1_REGISTER_ADDRESS_RESERVED_M_0C,
    APV_LSM9DS1_REGISTER_RESERVED,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    {  
    APV_LSM9DS1_REGISTER_ADDRESS_RESERVED_M_0D,
    APV_LSM9DS1_REGISTER_RESERVED,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    {  
    APV_LSM9DS1_REGISTER_ADDRESS_RESERVED_M_0E,
    APV_LSM9DS1_REGISTER_RESERVED,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    {  
    APV_LSM9DS1_REGISTER_ADDRESS_WHO_AM_I_M,
    APV_LSM9DS1_REGISTER_READ_ONLY,
    APV_LSM9DS1_REGISTER_M_WHO_AM_I_DEFAULT
    },
    {  
    APV_LSM9DS1_REGISTER_ADDRESS_RESERVED_M_10,
    APV_LSM9DS1_REGISTER_RESERVED,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    {  
    APV_LSM9DS1_REGISTER_ADDRESS_RESERVED_M_11,
    APV_LSM9DS1_REGISTER_RESERVED,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    {  
    APV_LSM9DS1_REGISTER_ADDRESS_RESERVED_M_12,
    APV_LSM9DS1_REGISTER_RESERVED,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    {  
    APV_LSM9DS1_REGISTER_ADDRESS_RESERVED_M_13,
    APV_LSM9DS1_REGISTER_RESERVED,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    {  
    APV_LSM9DS1_REGISTER_ADDRESS_RESERVED_M_14,
    APV_LSM9DS1_REGISTER_RESERVED,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    {  
    APV_LSM9DS1_REGISTER_ADDRESS_RESERVED_M_15,
    APV_LSM9DS1_REGISTER_RESERVED,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    {  
    APV_LSM9DS1_REGISTER_ADDRESS_RESERVED_M_16,
    APV_LSM9DS1_REGISTER_RESERVED,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    {  
    APV_LSM9DS1_REGISTER_ADDRESS_RESERVED_M_17,
    APV_LSM9DS1_REGISTER_RESERVED,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    {  
    APV_LSM9DS1_REGISTER_ADDRESS_RESERVED_M_18,
    APV_LSM9DS1_REGISTER_RESERVED,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    {  
    APV_LSM9DS1_REGISTER_ADDRESS_RESERVED_M_19,
    APV_LSM9DS1_REGISTER_RESERVED,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    {  
    APV_LSM9DS1_REGISTER_ADDRESS_RESERVED_M_1A,
    APV_LSM9DS1_REGISTER_RESERVED,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    {  
    APV_LSM9DS1_REGISTER_ADDRESS_RESERVED_M_1B,
    APV_LSM9DS1_REGISTER_RESERVED,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    {  
    APV_LSM9DS1_REGISTER_ADDRESS_RESERVED_M_1C,
    APV_LSM9DS1_REGISTER_RESERVED,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    {  
    APV_LSM9DS1_REGISTER_ADDRESS_RESERVED_M_1D,
    APV_LSM9DS1_REGISTER_RESERVED,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    {  
    APV_LSM9DS1_REGISTER_ADDRESS_RESERVED_M_1E,
    APV_LSM9DS1_REGISTER_RESERVED,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    {  
    APV_LSM9DS1_REGISTER_ADDRESS_RESERVED_M_1F,
    APV_LSM9DS1_REGISTER_RESERVED,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    {  
    APV_LSM9DS1_REGISTER_ADDRESS_CTRL_REG1_M,
    APV_LSM9DS1_REGISTER_READ_WRITE,
    APV_LSM9DS1_REGISTER_M_CTRL_REG1_M_DEFAULT
    },
    {  
    APV_LSM9DS1_REGISTER_ADDRESS_CTRL_REG2_M,
    APV_LSM9DS1_REGISTER_READ_WRITE,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    {  
    APV_LSM9DS1_REGISTER_ADDRESS_CTRL_REG3_M,
    APV_LSM9DS1_REGISTER_READ_WRITE,
    APV_LSM9DS1_REGISTER_M_CTRL_REG3_M_DEFAULT
    },
    {  
    APV_LSM9DS1_REGISTER_ADDRESS_CTRL_REG4_M,
    APV_LSM9DS1_REGISTER_READ_WRITE,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    {  
    APV_LSM9DS1_REGISTER_ADDRESS_CTRL_REG5_M,
    APV_LSM9DS1_REGISTER_READ_WRITE,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    {  
    APV_LSM9DS1_REGISTER_ADDRESS_RESERVED_M_25,
    APV_LSM9DS1_REGISTER_RESERVED,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    {  
    APV_LSM9DS1_REGISTER_ADDRESS_RESERVED_M_26,
    APV_LSM9DS1_REGISTER_RESERVED,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    {  
    APV_LSM9DS1_REGISTER_ADDRESS_STATUS_REG_M,
    APV_LSM9DS1_REGISTER_READ_ONLY,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    {  
    APV_LSM9DS1_REGISTER_ADDRESS_OUT_X_L_M,
    APV_LSM9DS1_REGISTER_READ_ONLY,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    {  
    APV_LSM9DS1_REGISTER_ADDRESS_OUT_X_H_M,
    APV_LSM9DS1_REGISTER_READ_ONLY,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    {  
    APV_LSM9DS1_REGISTER_ADDRESS_OUT_Y_L_M,
    APV_LSM9DS1_REGISTER_READ_ONLY,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    {  
    APV_LSM9DS1_REGISTER_ADDRESS_OUT_Y_H_M,
    APV_LSM9DS1_REGISTER_READ_ONLY,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    {  
    APV_LSM9DS1_REGISTER_ADDRESS_OUT_Z_L_M,
    APV_LSM9DS1_REGISTER_READ_ONLY,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    {  
    APV_LSM9DS1_REGISTER_ADDRESS_OUT_Z_H_M,
    APV_LSM9DS1_REGISTER_READ_ONLY,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    {  
    APV_LSM9DS1_REGISTER_ADDRESS_RESERVED_M_2E,
    APV_LSM9DS1_REGISTER_RESERVED,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    {  
    APV_LSM9DS1_REGISTER_ADDRESS_RESERVED_M_2F,
    APV_LSM9DS1_REGISTER_RESERVED,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    {  
    APV_LSM9DS1_REGISTER_ADDRESS_INT_CFG_M,
    APV_LSM9DS1_REGISTER_READ_WRITE,
    APV_LSM9DS1_REGISTER_M_INT_CFG_M_DEFAULT
    },
    {  
    APV_LSM9DS1_REGISTER_ADDRESS_INT_SRC_M,
    APV_LSM9DS1_REGISTER_READ_ONLY,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    {  
    APV_LSM9DS1_REGISTER_ADDRESS_INT_THS_L_M,
    APV_LSM9DS1_REGISTER_READ_ONLY,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    },
    {  
    APV_LSM9DS1_REGISTER_ADDRESS_INT_THS_H_M,
    APV_LSM9DS1_REGISTER_READ_ONLY,
    APV_LSM9DS1_REGISTER_COMMON_DEFAULT
    }
  };

/******************************************************************************/
/* apvInitialiseLsm9ds1() :                                                   */
/*  --> apvCoreTimerBlock : duration timers block                             */
/*  --> spiChipSelect     : SPI slave device chip select                      */
/*                                                                            */
/* - enable the gyroscopes and accelerometers. Set the output data rates and  */
/*   bandwidths                                                               */
/*                                                                            */
/******************************************************************************/

APV_ERROR_CODE apvInitialiseLsm9ds1(      apvCoreTimerBlock_t      *apvCoreTimerBlock,
                                    const apvSPIFixedChipSelects_t  spiChipSelect)
  {
/******************************************************************************/

  volatile APV_ERROR_CODE  lsm9ds1Error               = APV_ERROR_CODE_NONE;
  uint32_t                 apvLsm9ds1TimerIndex       = APV_DURATION_TIMER_NULL_INDEX;
  bool                     apvLsm9ds1TimerExpiredFlag = true;

  uint16_t                 spiReceivedCharacter       = 0;
  apvSPIFixedChipSelects_t spiChipSelectIn            = 0;

  uint8_t                  apvSpi0FillLevel           = 0;

  volatile uint32_t        statusRegister = 0;

/******************************************************************************/
/* Check the accelerometer and gyroscope device is alive by requesting the    */
/* "WHO_AM_I" code                                                            */
/******************************************************************************/
/* The device startup is a simple state-machine of timed events - get a timer */
/******************************************************************************/

  // Set the right chip-select and operating mode
  lsm9ds1Error = apvSpi0Cs0Lsm9ds1Assignment();

  // Enable the chip-select line
  apvSwitchDigitalLines(APV_DIGITAL_ID_LSM9DS_CHIP_ENABLE_SELECT, 
                        true);
  
  // "apvLsm9ds1TimerFlag" is the duration timer callback flag
  apvLsm9ds1TimerFlag = false;

  lsm9ds1Error = apvCreateDurationTimer( apvCoreTimerBlock,
                                         apvLsm9ds1StateTimer,                // callback function
                                        (void *)&apvLsm9ds1TimerFlag,         // timer expiry flag passed to callback function
                                         APV_DURATION_TIMER_TYPE_ONE_SHOT,    // single-shot
                                         APV_EVENT_TIMER_INVERSE_NANOSECONDS, // one second period
                                         APV_DURATION_TIMER_SOURCE_SYSTICK,
                                         APV_DURATION_TIMER_STATE_STOPPED,
                                        &apvLsm9ds1TimerIndex);
                                           
  // Restart the one-shot timer delay waiting for a character
  lsm9ds1Error = apvReTriggerDurationTimer(apvCoreTimerBlock,
                                           apvLsm9ds1TimerIndex,
                                           APV_EVENT_TIMER_INVERSE_NANOSECONDS);

  // Do the character read request - note the "READ_DATA" prefix character flag is Lsm9ds1-only!
  lsm9ds1Error = apvSPITransmitCharacter((APV_LSM9DS1_REGISTER_READ_DATA | APV_LSM9DS1_REGISTER_ADDRESS_WHO_AM_I),
                                         APV_LSM9DS1_NULL_CHARACTER,
                                         spiChipSelect,
                                         APV_SPI_LAST_TRANSFER_ACTIVE,
                                         true);

  /******************************************************************************/
  /* Default to expiry - this will be cancelled if the interrupt turns up first */
  /******************************************************************************/

  apvLsm9ds1TimerExpiredFlag = true; 

  /******************************************************************************/
  /* Wait for the SPI0 receive interrupt or the duration timer expiry,          */
  /* whichever happens first                                                    */
  /******************************************************************************/

  while (apvLsm9ds1TimerFlag == false)
    {
    if (apvGetFIFOFillLevel(&apvSpi0ChipSelectFIFO[spiChipSelect],
                            &apvSpi0FillLevel,
                             true) == APV_ERROR_CODE_NONE)
      {
      if (apvSpi0FillLevel != 0)
        {
        apvLsm9ds1TimerExpiredFlag = false;
        }
      }
    }

  if (apvLsm9ds1TimerExpiredFlag == false)
    {
    apvLsm9ds1TimerExpiredFlag = true;

    lsm9ds1Error = apvSpiReceiveCharacter(&spiReceivedCharacter,
                                          &spiChipSelectIn);

    /******************************************************************************/

    if (((spiReceivedCharacter &  APV_LSM9DS1_WHO_AM_I_MASK) != APV_LSM9DS1_WHO_AM_I) &&
         (spiChipSelectIn      != APV_SPI_FIXED_CHIP_SELECTS))
      {
      lsm9ds1Error = APV_ERROR_CODE_EVENT_UNRECOGNISED_PARAMETER;
      }
    else
      {
      }
    }
  else
    {
    lsm9ds1Error = APV_ERROR_CODE_EVENT_TIMEOUT_ERROR;
    }

/******************************************************************************/

  return(lsm9ds1Error);

/******************************************************************************/
  } /* end of apvInitialiseLsm9ds1                                            */

/******************************************************************************/
/* apvServiceLsm9ds1() :                                                      */
/*  --> spiControlBlock_p             : SPI0 hardware register block          */
/*  --> spiChipSelect                 : [ 0 .. 3 ]                            */
/*  --> apvLsm9ds1ProcessStateMachine : the Lsm9ds1 state-machine             */
/*  --> apvLsm9ds1State               : the current state                     */
/*  --> apvLsm9ds1Measurements        : measurement results stack             */
/*  --> apvLsm9ds1MeasurementIndex    : index into the measurements results   */
/*                                      stack                                 */
/*  --> apvLsm9ds1SchedulingFlag      : self-scheduling flag                  */
/*  <-- lsm9ds1Error      : error codes                                       */
/*                                                                            */
/* - read the accelerometer, gyro and temperature measurements from the       */
/*   Lsm9ds1. Readings are taken over a number of milliseconds driven round   */
/*   by a state machine. Provision is made foe multiple readings of each DOF  */
/*   for averaging/filtering. Although multiple readings are made they are    */
/*   interleaved to prevent data clumping                                     */
/*                                                                            */
/******************************************************************************/
                                    
APV_ERROR_CODE apvServiceLsm9ds1(      Spi                                     *spiControlBlock_p,
                                       apvSPIFixedChipSelects_t                 spiChipSelect,
                                 const apvLsm9ds1MeasurementStateDescriptor_t  *apvLsm9ds1ProcessStateMachine,
                                       apvLsm9ds1MeasurementStateDescriptor_t **apvLsm9ds1State,
                                       apvLsm9ds1Measurement_t                 *apvLsm9ds1Measurements,
                                       apvLsm9ds1MeasurementId_t               *apvLsm9ds1MeasurementIndex,
                                       bool                                    *apvLsm9ds1SchedulingFlag)
  {
/******************************************************************************/

         APV_ERROR_CODE lsm9ds1Error             = APV_ERROR_CODE_NONE;

  /******************************************************************************/
  /* Read states are not complete until the requested data has been received -  */
  /* the task does not wait for data but checks the interrupt flag at the next  */
  /* invocation. A coarse "data expiry" flag is used to terminate processing    */
  /* and flag an error if the data is unreasonably late. Note this (and other)  */
  /* tasks are NOT re-entrant but rely on "static" variables for persistent     */
  /* state-related behaviour!                                                   */
  /******************************************************************************/

  static bool                          lsm9ds1DataPending       = false;
  static uint8_t                       lsm9ds1DataPendingExpiry = 0;

         uint16_t                      spiReceivedCharacter     = 0;
  static apvLsm9ds1RegisterReadWrite_t precedingAction          = APV_LSM9DS1_REGISTER_NO_ACTION;        
         uint8_t                       fillLevel                = 0,
                                       characterIndex           = 0;

         apvFIFOEntry_t                fifoEntry;

  static uint8_t                       measurementIndex        = 0;

/******************************************************************************/

  if (( spiControlBlock_p          != NULL) && (apvLsm9ds1ProcessStateMachine != NULL) &&
      (*apvLsm9ds1State            != NULL) && (apvLsm9ds1Measurements        != NULL) &&
      ( apvLsm9ds1MeasurementIndex != NULL) && (apvLsm9ds1SchedulingFlag      != NULL))
    {
    if ((*apvLsm9ds1State)->lsm9ds1RegisterReadWrite == APV_LSM9DS1_REGISTER_NO_ACTION)
      { // Set the right chip-select and operating mode for this SPI0 state-machine
      apvSpi0Cs0Lsm9ds1Assignment();

       lsm9ds1DataPending       = false;
       lsm9ds1DataPendingExpiry = 0;

       precedingAction          = APV_LSM9DS1_REGISTER_NO_ACTION;

    // If this is the end of the scheduled states for this loop signal an exit
    if ((*apvLsm9ds1State)->lds9ds1ExitState == true)
      {
      *apvLsm9ds1SchedulingFlag = false;
      }
          
      *apvLsm9ds1State = (apvLsm9ds1MeasurementStateDescriptor_t *)(*apvLsm9ds1State)->lsm9ds1NextMeasurementState;
      }
    else
      {
      if ((*apvLsm9ds1State)->lsm9ds1RegisterReadWrite == APV_LSM9DS1_REGISTER_READ_DATA)
        { 
        // If the preceding action was a "write" flush any concommitant "read"
        if (precedingAction == APV_LSM9DS1_REGISTER_WRITE_DATA)
          {
          lsm9ds1Error = apvSpiReceiveCharacter(&spiReceivedCharacter,
                                                &spiChipSelect);
          }

        precedingAction = APV_LSM9DS1_REGISTER_READ_DATA;
        
        // Is this a fresh read ?
        if (lsm9ds1DataPendingExpiry == 0)
          {
          // Do the character read request
          lsm9ds1Error = apvSPITransmitCharacter(APV_LSM9DS1_REGISTER_READ_DATA | (*apvLsm9ds1State)->lsm9ds1Register,
                                                 APV_LSM9DS1_NULL_CHARACTER,
                                                 spiChipSelect,
                                                 APV_SPI_LAST_TRANSFER_ACTIVE,
                                                 true);

          // Flag "read-in-progress" to the next invocation
          lsm9ds1DataPendingExpiry = 1;
          }
        else
          { // A new data byte is expected
          if (apvGetFIFOFillLevel(&apvSpi0ChipSelectFIFO[spiChipSelect],
                                 &fillLevel,
                                  true) == APV_ERROR_CODE_NONE)
            {
            if (fillLevel > 0)
              {
              apvUnLoadFIFO(&apvSpi0ChipSelectFIFO[spiChipSelect],
                            &fifoEntry,
                             true);

              // All done for this entry
              lsm9ds1DataPendingExpiry = 0;

              // Save the new value
              if (((*apvLsm9ds1State)->ldsm9ds1RegisterAccessSize == APV_LSM9DS1_REGISTER_ACCESS_SIZE_TWO_BYTES))
                {
                if ((*apvLsm9ds1State)->lsm9ds1RegisterLowLink == NULL)
                  {
                  // Keep the low 8-bits
                  (*apvLsm9ds1State)->lsm9ds1RegisterVariable->lsm9ds1RegisterMeasurement = (fifoEntry.apvFIFOEntryData & APV_LSM9DS1_LOW_WORD_MASK);
                  }
                else
                  {
                  // Get the high 8-bits
                  (*apvLsm9ds1State)->lsm9ds1RegisterVariable->lsm9ds1RegisterMeasurement = (fifoEntry.apvFIFOEntryData << APV_LSM9DS1_WORD_SHIFT);

                  // Splice in the low bits
                  (*apvLsm9ds1State)->lsm9ds1RegisterVariable->lsm9ds1RegisterMeasurement = 
                                              (*apvLsm9ds1State)->lsm9ds1RegisterVariable->lsm9ds1RegisterMeasurement | ((*apvLsm9ds1State)->lsm9ds1RegisterLowLink)->lsm9ds1RegisterVariable->lsm9ds1RegisterMeasurement;

                  // Save the measurement result
                  (apvLsm9ds1Measurements + *apvLsm9ds1MeasurementIndex)->lsm9ds1RegisterMeasurement =  (*apvLsm9ds1State)->lsm9ds1RegisterVariable->lsm9ds1RegisterMeasurement;
                  (apvLsm9ds1Measurements + *apvLsm9ds1MeasurementIndex)->lsm9ds1MeasurementId       = *apvLsm9ds1MeasurementIndex;
                  (apvLsm9ds1Measurements + *apvLsm9ds1MeasurementIndex)->lsm9ds1RegisterSign        =  (*apvLsm9ds1State)->lds9ds1RegisterSign;

                  // Increment and wrap the measurement index
                  *apvLsm9ds1MeasurementIndex = *apvLsm9ds1MeasurementIndex + 1;

                  if (*apvLsm9ds1MeasurementIndex >= APV_LSM9DS1_MEASUREMENT_SET)
                    {
                    *apvLsm9ds1MeasurementIndex = APV_LSM9DS1_MEASUREMENT_SET_START;
                    }
                  }
                }
              else
                {
                // This is a single-byte read, keep the low 8-bits
                (*apvLsm9ds1State)->lsm9ds1RegisterVariable->lsm9ds1RegisterMeasurement = (fifoEntry.apvFIFOEntryData & APV_LSM9DS1_LOW_WORD_MASK);

                // Save the measurement result
                (apvLsm9ds1Measurements + *apvLsm9ds1MeasurementIndex)->lsm9ds1RegisterMeasurement = (*apvLsm9ds1State)->lsm9ds1RegisterVariable->lsm9ds1RegisterMeasurement;
                (apvLsm9ds1Measurements + *apvLsm9ds1MeasurementIndex)->lsm9ds1MeasurementId       = *apvLsm9ds1MeasurementIndex;
                (apvLsm9ds1Measurements + *apvLsm9ds1MeasurementIndex)->lsm9ds1RegisterSign        =  (*apvLsm9ds1State)->lds9ds1RegisterSign;

                // Increment and wrap the measurement index
                *apvLsm9ds1MeasurementIndex = *apvLsm9ds1MeasurementIndex + 1;
                
                if (*apvLsm9ds1MeasurementIndex >= APV_LSM9DS1_MEASUREMENT_SET)
                  {
                  *apvLsm9ds1MeasurementIndex = APV_LSM9DS1_MEASUREMENT_SET_START;
                  }
                }

              // If this is the end of the scheduled states for this loop signal an exit
              if ((*apvLsm9ds1State)->lds9ds1ExitState == true)
                {
                *apvLsm9ds1SchedulingFlag = false;
                }

              // Move to the next state
               precedingAction = APV_LSM9DS1_REGISTER_READ_DATA;
              *apvLsm9ds1State = (apvLsm9ds1MeasurementStateDescriptor_t *)((*apvLsm9ds1State)->lsm9ds1NextMeasurementState);
              }
            else
              { // Something has gone very wrong - pack up and go home!
              *apvLsm9ds1SchedulingFlag = false;
               lsm9ds1Error             = APV_ERROR_CODE_EVENT_UNRECOGNISED_PARAMETER;
              }
            }

          }
        }
      else
        {
        if ((*apvLsm9ds1State)->lsm9ds1RegisterReadWrite == APV_LSM9DS1_REGISTER_WRITE_DATA)
          { // Writes are one-way SPI transactions, assume it has completed before the next 
            // state is entered, do not wait for any flags

          // If the preceding action was a "write" flush any concommitant "read"
          if (precedingAction == APV_LSM9DS1_REGISTER_WRITE_DATA)
            {
            lsm9ds1Error = apvSpiReceiveCharacter(&spiReceivedCharacter,
                                                  &spiChipSelect);
            }
            
          // Do the character write request
          lsm9ds1Error = apvSPITransmitCharacter(APV_LSM9DS1_REGISTER_WRITE_DATA | (*apvLsm9ds1State)->lsm9ds1Register,
                                                 (*apvLsm9ds1State)->lsm9ds1RegisterConstant,
                                                 spiChipSelect,
                                                 APV_SPI_LAST_TRANSFER_ACTIVE,
                                                 true);

          // If this is the end of the scheduled states for this loop signal an exit
          if ((*apvLsm9ds1State)->lds9ds1ExitState == true)
            {
            *apvLsm9ds1SchedulingFlag = false;
            }

          // Move to the next state
          if (lsm9ds1Error == APV_ERROR_CODE_NONE)
            {
             precedingAction = APV_LSM9DS1_REGISTER_WRITE_DATA;
            *apvLsm9ds1State = (apvLsm9ds1MeasurementStateDescriptor_t *)((*apvLsm9ds1State)->lsm9ds1NextMeasurementState);
            }
          else
            {
            *apvLsm9ds1State = ((apvLsm9ds1MeasurementStateDescriptor_t *)apvLsm9ds1ProcessStateMachine) + APV_LSM9DS1_MEASUREMENT_STATE_ERROR;
            }
          }
        else
          { 
          /******************************************************************************/
          /* If enabled, stream the measurements results to the UART serial port one    */
          /* per scheduling loop                                                        */
          /******************************************************************************/

          if ((*apvLsm9ds1State)->lsm9ds1RegisterReadWrite == APV_LSM9DS1_REGISTER_STREAM_DATA)
            {    
            if (apvLsm9ds1InertialDataStreaming == true)
              {
              // Build a message for the output serial port - initialise the fixed bits
              measurementMessageBuffer = apvLsm9ds1MeasurementReport;
            
              // Convert the measurement from hex to printable ASCII decimal
              apvSignedConvert16HexToDecimal( (apvLsm9ds1Measurements + measurementIndex)->lsm9ds1RegisterSign,
                                              (apvLsm9ds1Measurements + measurementIndex)->lsm9ds1RegisterMeasurement,
                                             &apvLsm9ds1DecimalMeasurement[0]);
            
              for (characterIndex = 0; characterIndex < APV_16_BIT_BINARY_2_DECIMAL_WIDTH; characterIndex++)
                {
                // Load the converted measurement string into the results buffer
                measurementMessageBuffer.apvLsm9ds1Measurements[measurementIndex].apvLsm9ds1TextDigits[characterIndex] = apvLsm9ds1DecimalMeasurement[characterIndex];
                }
            
              // Find the target messaging component and try to transmit the measurement set
              apvComponentMessageDispatcher(&measurementMessageBuffer.apvLsm9ds1MeasurementInstance[measurementIndex * sizeof(apvLsm9ds1MeasurementText_t)],
                                             sizeof(apvLsm9ds1MeasurementText_t),
                                              APV_COMMS_PLANE_SERIAL_UART,
                                              APV_SIGNAL_PLANE_CONTROL_1);

              measurementIndex = measurementIndex + 1;
              }
            else
              {
              measurementIndex = APV_LSM9DS1_MEASUREMENT_SET;
              }
            
            /******************************************************************************/
            
            // After the final measurement has been streamed out change state
            if (measurementIndex == APV_LSM9DS1_MEASUREMENT_SET)
              {
              measurementIndex = APV_LSM9DS1_MEASUREMENT_SET_START;

              // If this is the end of the scheduled states for this loop signal an exit
              if ((*apvLsm9ds1State)->lds9ds1ExitState == true)
                {
                *apvLsm9ds1SchedulingFlag = false;
                }
            
              // Move to the next state
              if (lsm9ds1Error == APV_ERROR_CODE_NONE)
                {
                 precedingAction = APV_LSM9DS1_REGISTER_NO_ACTION;
                *apvLsm9ds1State = (apvLsm9ds1MeasurementStateDescriptor_t *)((*apvLsm9ds1State)->lsm9ds1NextMeasurementState);
                }
              else
                {
                *apvLsm9ds1State = ((apvLsm9ds1MeasurementStateDescriptor_t *)apvLsm9ds1ProcessStateMachine) + APV_LSM9DS1_MEASUREMENT_STATE_ERROR;
                }
              }
            }
          else
            { // Something has gone very wrong - pack up and go home!
            *apvLsm9ds1SchedulingFlag = false;
             lsm9ds1Error             = APV_ERROR_CODE_EVENT_UNRECOGNISED_PARAMETER;
            }
          }
        }
      }
    }
  else
    {
    lsm9ds1Error = APV_ERROR_CODE_NULL_PARAMETER;
    }

/******************************************************************************/

  return(lsm9ds1Error);

/******************************************************************************/
  } /* end of apvServiceLsm9ds1                                               */

/******************************************************************************/
/* apvLsm9ds1InitialiseTaskParameterBlock() :                                 */
/*  - see "apvServiceLsm9ds1()"                                               */
/******************************************************************************/

APV_ERROR_CODE apvLsm9ds1InitialiseTaskParameterBlock(      apvLsm9ds1TaskParameterBlock_t          *taskParameterBlock,
                                                            Spi                                     *spiControlBlock_p,
                                                            apvSPIFixedChipSelects_t                 spiChipSelect,
                                                      const apvLsm9ds1MeasurementStateDescriptor_t  *apvLsm9ds1ProcessStateMachine,
                                                            apvLsm9ds1MeasurementStateDescriptor_t **apvLsm9ds1State,
                                                            apvLsm9ds1Measurement_t                 *apvLsm9ds1Measurements,
                                                            apvLsm9ds1MeasurementId_t               *apvLsm9ds1MeasurementIndex)
  {
/******************************************************************************/

  APV_ERROR_CODE blockErrors = APV_ERROR_CODE_NONE;

/******************************************************************************/

  if ((taskParameterBlock            == NULL) || (spiControlBlock_p          == NULL) ||
      (apvLsm9ds1ProcessStateMachine == NULL) || (apvLsm9ds1State            == NULL) ||
      (apvLsm9ds1Measurements        == NULL) || (apvLsm9ds1MeasurementIndex == NULL) ||
      (spiChipSelect                 >  APV_SPI_FIXED_CHIP_SELECT_3))
    {
    blockErrors = APV_ERROR_CODE_NULL_PARAMETER;
    }
  else
    {
    taskParameterBlock->spiControlBlock_p             = spiControlBlock_p;
    taskParameterBlock->spiChipSelect                 = spiChipSelect;
    taskParameterBlock->apvLsm9ds1ProcessStateMachine = apvLsm9ds1ProcessStateMachine;
    taskParameterBlock->apvLsm9ds1State               = apvLsm9ds1State;
    taskParameterBlock->apvLsm9ds1Measurements        = apvLsm9ds1Measurements;
    taskParameterBlock->apvLsm9ds1MeasurementIndex    = apvLsm9ds1MeasurementIndex;
    }

/******************************************************************************/

  return(blockErrors);

/******************************************************************************/
  } /* end of apvLsm9ds1InitialiseTaskParameterBlock                          */

/******************************************************************************/
/* apvLsm9ds1Task() :                                                         */
/*  -->  taskParameters         : address of the task/function parameter      */
/*                                block                                       */
/*  -->  taskWorkspace          : address of the task-specific scratch        */
/*                                workspace                                   */
/*  <--> taskSelfSchedulingFlag : address of the self-scheduling flag         */
/*                                                                            */
/* - the task function connects the Lsm9ds1 servicing function to the co-     */
/*   operative scheduler/dispatcher mechanism                                 */
/*                                                                            */
/******************************************************************************/

APV_ERROR_CODE apvLsm9ds1Task(void      *taskParameters,
		                            void      *taskWorkspace,
                              boolean_t *taskSelfSchedulingFlag)
  {
/******************************************************************************/

  APV_ERROR_CODE taskErrors = APV_ERROR_CODE_NONE;

/******************************************************************************/

  taskErrors = apvServiceLsm9ds1(((apvLsm9ds1TaskParameterBlock_t *)taskParameters)->spiControlBlock_p,
                                 ((apvLsm9ds1TaskParameterBlock_t *)taskParameters)->spiChipSelect,
                                 ((apvLsm9ds1TaskParameterBlock_t *)taskParameters)->apvLsm9ds1ProcessStateMachine,
                                 ((apvLsm9ds1TaskParameterBlock_t *)taskParameters)->apvLsm9ds1State,
                                 ((apvLsm9ds1TaskParameterBlock_t *)taskParameters)->apvLsm9ds1Measurements,
                                 ((apvLsm9ds1TaskParameterBlock_t *)taskParameters)->apvLsm9ds1MeasurementIndex,
                                 taskSelfSchedulingFlag);

/******************************************************************************/

  return(taskErrors);

/******************************************************************************/
  } /* end of apvLsm9ds1Task                                                  */

/******************************************************************************/
/* apvLsm9ds1StateTimer() :                                                   */
/*  --> durationEventMessage : possible callback parameter - in this case a   */
/*                             boolean flag signalling timer expiry           */
/*                                                                            */
/* - device timing callback - set the "timeout" flag                          */
/*                                                                            */
/******************************************************************************/

void apvLsm9ds1StateTimer(void *durationEventMessage)
  {
/******************************************************************************/

  *(bool *)durationEventMessage = true;

/******************************************************************************/
  } /* end of apvLsm9ds1StateTimer                                            */

/******************************************************************************/
/* apvLsm9ds1DelayStateTimer() :                                              */
/*  --> durationEventMessage : possible callback parameter - in this case a   */
/*                             boolean flag signalling timer expiry           */
/*                                                                            */
/* - device timing callback - set the "timeout" flag                          */
/*                                                                            */
/******************************************************************************/

void apvLsm9ds1DelayStateTimer(void *durationEventMessage)
  {
/******************************************************************************/

  *(bool *)durationEventMessage = true;

/******************************************************************************/
  } /* end of apvLsm9ds1DelayStateTimer                                       */

/******************************************************************************/
/* (C) PulsingCoreSoftware Limited 2018 (C)                                   */
/******************************************************************************/
