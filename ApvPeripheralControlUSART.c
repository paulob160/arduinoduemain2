/******************************************************************************/
/* (C) PulsingCoreSoftware Limited 2020 (C)                                   */
/******************************************************************************/
/*                                                                            */
/* ApvPeripheralControlUSART.c                                                */
/* 03.05.20                                                                   */
/* Paul O'Brien                                                               */
/*                                                                            */
/*  - initialisation and setup of the USART chip peripheral behaviour         */
/*                                                                            */
/******************************************************************************/
/* Include Files :                                                            */
/******************************************************************************/

#include <stdio.h>
#include <stdint.h>
#include <sam3x8e.h>
#include "ApvError.h"
#include "ApvUtilities.h"
#include "ApvSystemTime.h"
#include "ApvEventTimers.h"
#include "ApvCommsUtilities.h"
#include "ApvPeripheralControl.h"
#include "ApvPeripheralControlUsart.h"

/******************************************************************************/
/* Global Variable Definitions :                                              */
/******************************************************************************/

// USART0 is dedicated to the GPS receiver : this is a constant-speed continual-
// feed input-only source, so use a small ping-pong pair of buffers
apvRingBuffer_t  apvGpsReceiverPortRingBuffer[APV_GPS_RECEIVER_RING_BUFFER_SET],
                *apvGpsReceiverPortRingBuffer_p[APV_GPS_RECEIVER_RING_BUFFER_SET] = 
  {
  APV_RING_BUFFER_LIST_EMPTY_POINTER,
  APV_RING_BUFFER_LIST_EMPTY_POINTER
  };

apvRingBuffer_t *apvGpsReceiverPortReceiveBuffer  = NULL;

          Usart   apvUsart0ControlBlock;                    // "shadow" USART0 control block
 volatile Usart  *apvUsart0ControlBlock_p         = USART0; // USART0 control block physical address
 
          Usart   apvGpsReceiverControlBlock;               // "shadow" GPS USARTx control block
 volatile Usart  *apvGpsReceiverControlBlock_p    = NULL;   // GPS USARTx control block physical address

         uint8_t  ID_GPS                          = ID_USART0;
         
/******************************************************************************/
/* apvGpsReceiverCommsSetup() :                                               */
/*  --> usartNumber : [ 0 | 3 ]                                               */
/*                                                                            */
/* - setup one of the four USARTS as the port for the GPS receiver. The       */
/*   serial protocol for the receiver is fixed at 9600,n,8,1                  */
/******************************************************************************/

APV_SERIAL_ERROR_CODE apvGpsReceiverCommsSetup(apvUsartNumber_t usartNumber)
  {
/******************************************************************************/

  APV_SERIAL_ERROR_CODE apvErrorCode    = APV_SERIAL_ERROR_CODE_NONE;

  uint32_t              baudRateDivider = 0;
  
/******************************************************************************/
  
  // Create the "free" ring-buffer set for this port
  if ((apvErrorCode = apvRingBufferSetInitialise(&apvGpsReceiverPortRingBuffer_p[0],
                                                  &apvGpsReceiverPortRingBuffer[0],
                                                   APV_GPS_RECEIVER_RING_BUFFER_SET,
                                                   APV_GPS_RECEIVER_RING_BUFFER_LENGTH)) == APV_ERROR_CODE_NONE)
    {
    if ((apvErrorCode = apvRingBufferSetPullBuffer(&apvGpsReceiverPortRingBuffer_p[0],
                                                   &apvGpsReceiverPortReceiveBuffer,
                                                    APV_GPS_RECEIVER_RING_BUFFER_SET,
                                                    false)) == APV_ERROR_CODE_NONE)
      {
      // Point to the beginning of the required USART instance register block
      switch(usartNumber)
        {
        case APV_USART_3 : apvGpsReceiverControlBlock_p = (Usart *)USART3;
                           ID_GPS                       = ID_USART3;
                           break;    
        case APV_USART_2 : apvGpsReceiverControlBlock_p = (Usart *)USART2;
                           ID_GPS                       = ID_USART2;
                           break;
        case APV_USART_1 : apvGpsReceiverControlBlock_p = (Usart *)USART1;
                           ID_GPS                       = ID_USART1;
                           break;    
        case APV_USART_0 : apvGpsReceiverControlBlock_p = (Usart *)USART0;
                           ID_GPS                       = ID_USART0;
                           break;
        default          : apvErrorCode                 = APV_ERROR_CODE_MESSAGE_DEFINITION_ERROR;
                           break;
        }

      if (apvErrorCode != APV_ERROR_CODE_CONFIGURATION_ERROR)
        {
        if ((apvErrorCode = apvSwitchPeripheralLines(ID_GPS,
                                                      true))  == APV_ERROR_CODE_NONE)
          {
          if ((apvErrorCode = apvSwitchPeripheralClock(ID_GPS,
                                                        true))  == APV_ERROR_CODE_NONE)
            {
            // Set up the GPS USARTx control register
            apvGpsReceiverControlBlock_p->US_CR = US_CR_RSTRX | US_CR_RSTTX; // reset the receiver and transmitter
            apvGpsReceiverControlBlock_p->US_CR = US_CR_RSTSTA;              // reset the status bits
            apvGpsReceiverControlBlock_p->US_CR = US_CR_TXDIS;               // disable the transmitter
            apvGpsReceiverControlBlock_p->US_CR = US_CR_RXEN;                // enable the receiver

            // Set the mode register : NORMAL mode (0 << 0) 
            // - asynchronous mode           (0 <<  8)
            //     9600 baud
            //     no parity                  (4 <<  9)
            //     eight bit characters       (3 <<  6)
            //     one stop bit               (0 << 12)
            // - MCLK selected                (0 <<  4)
            // - 16x oversampling             (0 << 19)
            // - least significant bit first  (0 << 16)
            // - no loopback ("channel mode") (0 << 14)
            apvGpsReceiverControlBlock_p->US_MR =US_MR_CHRL_8_BIT | US_MR_PAR_NO;

            // Compute the baud rate divider for the generator register
            //
            //  divider = MCLK / ( 8 * (2 - (oversampling bit == 0)) * baudrate )
            //   - '1' is added to round up the divider value'
            baudRateDivider = ((uint32_t)APV_EVENT_TIMER_TIMEBASE_BASECLOCK) / (((uint32_t)APV_GPS_RECEIVER_OVERSAMPLING_16) * ((uint32_t)APV_GPS_RECEIVER_BAUD_RATE)) + ((uint32_t)1);

            apvGpsReceiverControlBlock_p->US_BRGR = baudRateDivider;

            // Switch on the receiver interrupt
            apvGpsReceiverControlBlock_p->US_IER  = US_IER_RXRDY;

            // Route the interrupt through the NVIC
            apvErrorCode = apvSwitchNvicDeviceIrq(ID_GPS,
                                                  true);
            }
          else
            {
            apvErrorCode = APV_ERROR_CODE_CONFIGURATION_ERROR;
            }
          }
        else
          {
          apvErrorCode = APV_ERROR_CODE_CONFIGURATION_ERROR;
          }
        }
      else
        {
        apvErrorCode = APV_ERROR_CODE_CONFIGURATION_ERROR;
        }
      }
    else
      {
      apvErrorCode = APV_ERROR_CODE_CONFIGURATION_ERROR;
      }
    }
  else
    {
    apvErrorCode = APV_ERROR_CODE_CONFIGURATION_ERROR;
    }
  
/******************************************************************************/

  return(apvErrorCode);

/******************************************************************************/
  } /* end of apvGpsReceiverCommsSetup                                        */

/******************************************************************************/
/* apsGpsReceiverCommsHandler() :                                             */
/*  --> usartNumber : [ ID_USART0 | ID_USART1 | ID_USART2 | ID_USART3 ]       */
/*                                                                            */
/* - handle the GPS receiver interrupt source                                 */
/******************************************************************************/

void apsGpsReceiverCommsHandler(uint8_t usartNumber)
  {
/******************************************************************************/

  uint32_t rxBuffer       = 0,
           statusRegister = 0;

/******************************************************************************/

  if (usartNumber == ID_GPS)
    {
    // Read the USART channel status for "RXRDY"
    statusRegister = apvGpsReceiverControlBlock_p->US_CSR;
  
    // Check the interrupt is not spurious
    if ((statusRegister & US_CSR_RXRDY) == US_CSR_RXRDY)
      {
      apvInterruptCounters[APV_RECEIVE_INTERRUPT_COUNTER] = apvInterruptCounters[APV_RECEIVE_INTERRUPT_COUNTER] + 1;
  
      // Read the received character
      rxBuffer = apvGpsReceiverControlBlock_p->US_RHR;
  
      // Put it onto the receiver ring-buffer
      if (apvRingBufferLoad( apvGpsReceiverPortReceiveBuffer,
                             APV_RING_BUFFER_TOKEN_TYPE_LONG_WORD,
                            (uint32_t *)&rxBuffer,
                             sizeof(uint8_t),
                             false) == 0)
        {
        // Ring-buffer overflow!
        while (true)
          ;
        }
      }
    }
  
/******************************************************************************/
  } /* end of apsGpsReceiverCommsHandler                                      */

/******************************************************************************/
/* USART0_Handler() :                                                         */
/******************************************************************************/

void USART0_Handler(void)
  {
/******************************************************************************/

  // Which protocol/device is USART0 handling ?
  if (ID_USART0 == ID_GPS)
    {
    apsGpsReceiverCommsHandler(ID_GPS);
    }

  NVIC_ClearPendingIRQ(USART0_IRQn);
  
/******************************************************************************/  
  } /* end of USART0_Handler                                                  */

/******************************************************************************/
/* USART1_Handler() :                                                         */
/******************************************************************************/

void USART1_Handler(void)
  {
/******************************************************************************/

  // Which protocol/device is USART1 handling ?
  if (ID_USART1 == ID_GPS)
    {
    apsGpsReceiverCommsHandler(ID_GPS);
    }

  NVIC_ClearPendingIRQ(USART1_IRQn);
  
/******************************************************************************/  
  } /* end of USART1_Handler                                                  */

/******************************************************************************/
/* USART2_Handler() :                                                         */
/******************************************************************************/

void USART2_Handler(void)
  {
/******************************************************************************/

  // Which protocol/device is USART2 handling ?
  if (ID_USART2 == ID_GPS)
    {
    apsGpsReceiverCommsHandler(ID_GPS);    
    }

  NVIC_ClearPendingIRQ(USART2_IRQn);
  
/******************************************************************************/  
  } /* end of USART2_Handler                                                  */

/******************************************************************************/
/* USART3_Handler() :                                                         */
/******************************************************************************/

void USART3_Handler(void)
  {
/******************************************************************************/

  // Which protocol/device is USART3 handling ?
  if (ID_USART3 == ID_GPS)
    {
    apsGpsReceiverCommsHandler(ID_GPS);    
    }

  NVIC_ClearPendingIRQ(USART3_IRQn);
  
/******************************************************************************/  
  } /* end of USART3_Handler                                                  */

/******************************************************************************/
/* (C) PulsingCoreSoftware Limited 2020 (C)                                   */
/******************************************************************************/
