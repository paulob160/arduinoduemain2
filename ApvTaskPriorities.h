/******************************************************************************/
/* (C) PulsingCoreSoftware Limited 2018 (C)                                   */
/******************************************************************************/
/*                                                                            */
/* ApvTaskPriorities.h                                                        */
/* 01.11.18                                                                   */
/* Paul O'Brien                                                               */
/*                                                                            */
/* - definition of the cooperative ("mosaic") scheduling structure            */
/*                                                                            */
/******************************************************************************/

#ifndef _APV_TASK_PRIORITIES_H_
#define _APV_TASK_PRIORITIES_H_

/******************************************************************************/
/* Definitions :                                                              */
/******************************************************************************/

#define APV_TASK_SCHEDULE_PRIORITY_HIGHEST  ((APV_TASK_SCHEDULE_PRIORITY)0x00)
#define APV_TASK_SCHEDULE_PRIORITY_LOWEST   ((APV_TASK_SCHEDULE_PRIORITY)0xFE)
#define APV_TASK_SCHEDULE_PRIORITY_RESERVED ((APV_TASK_SCHEDULE_PRIORITY)0xFF)

/******************************************************************************/
/* Task intra-class priorities                                                */
/******************************************************************************/

#define APV_TASK_SCHEDULE_PRIORITY_LSM9DS1 (0x04) // run the Lsm9ds1 task at a "quite a high priority" in it's class

/******************************************************************************/
/* Type Definitions :                                                         */
/******************************************************************************/

typedef uint8_t APV_TASK_SCHEDULE_PRIORITY;

/******************************************************************************/
/* A high priority task CAN block a low priority task even if there is no     */
/* preemption! If two tasks have the same event time or self-schedule the     */
/* higher priority task will still run first! "Sole" priority tasks will be   */
/* the only task to run in that slot; any other tasks will (i) have their     */
/* event-time incremented (next slot) or (ii) self-scheduled tasks will just  */
/* not run until a clear slot occurs!                                         */
/******************************************************************************/

typedef enum apvTaskSchedulePriorityClass_t
  {
  APV_TASK_SCHEDULE_PRIORITY_CLASS_SOLO = 0,
  APV_TASK_SCHEDULE_PRIORITY_CLASS_HIGH,
  APV_TASK_SCHEDULE_PRIORITY_CLASS_MID,
  APV_TASK_SCHEDULE_PRIORITY_CLASS_LOW,
  APV_TASK_SCHEDULE_PRIORITY_CLASS_NONE,
  APV_TASK_SCHEDULE_PRIORITY_CLASSES
  } apvTaskSchedulePriorityClass_t;

/******************************************************************************/

#endif

/******************************************************************************/
/* Include Files :                                                            */
/******************************************************************************/