/******************************************************************************/
/* (C) PulsingCoreSoftware Limited 2020 (C)                                   */
/******************************************************************************/
/*                                                                            */
/* apv_RFM69HCW.c                                                             */
/* 11.06.20                                                                   */
/* Paul O'Brien                                                               */
/*                                                                            */
/* - control and status for the Semtech RFM69HCW 868 - 915MHz wireless        */
/*                                                                            */
/*   Ref: SEMTECH, "SX1231 Transceiver Low Power Integrated UHF Transceiver", */
/*        Rev. 7, June 2013                                                   */
/*                                                                            */
/******************************************************************************/

#include "ApvPeripheralControl.h"
#include "apvUtilities.h"
#include "ApvEventTimers.h"
#include "apv_RFM69HCW.h"

/******************************************************************************/

// Definition of the RFM69HCW/SX1231 register layout, default/reset values and 
// read/write masks
const apv_rfm69hcwRegisterDefinition_t apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REGISTER_ADDRESS_SET] =
  {
    {
    APV_RFM69HCW_REG_FIFO,
    APV_RFM69HCW_RF_FIFO_DEFAULT,
    APV_RFM69HCW_RF_FIFO_RESET,
    APV_RFM69HCW_RF_FIFO_WRITE_MASK,
    APV_RFM69HCW_RF_FIFO_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_OPMODE,
    APV_RFM69HCW_RF_OPMODE_DEFAULT,
    APV_RFM69HCW_RF_OPMODE_RESET,
    APV_RFM69HCW_RF_OPMODE_WRITE_MASK,
    APV_RFM69HCW_RF_OPMODE_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_DATAMODUL,
    APV_RFM69HCW_RF_DATAMODUL_DEFAULT,
    APV_RFM69HCW_RF_DATAMODUL_RESET,
    APV_RFM69HCW_RF_DATAMODUL_WRITE_MASK,
    APV_RFM69HCW_RF_DATAMODUL_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_BITRATEMSB,
    APV_RFM69HCW_RF_BITRATEMSB_DEFAULT,
    APV_RFM69HCW_RF_BITRATEMSB_RESET,
    APV_RFM69HCW_RF_BITRATE_WRITE_MASK,
    APV_RFM69HCW_RF_BITRATE_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_BITRATELSB,
    APV_RFM69HCW_RF_BITRATELSB_DEFAULT,
    APV_RFM69HCW_RF_BITRATELSB_RESET,
    APV_RFM69HCW_RF_BITRATE_WRITE_MASK,
    APV_RFM69HCW_RF_BITRATE_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_FDEVMSB,
    APV_RFM69HCW_RF_FDEVMSB_DEFAULT,
    APV_RFM69HCW_RF_FDEVMSB_RESET,
    APV_RFM69HCW_RF_FDEVMSB_WRITE_MASK,
    APV_RFM69HCW_RF_FDEVMSB_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_FDEVLSB,
    APV_RFM69HCW_RF_FDEVLSB_DEFAULT,
    APV_RFM69HCW_RF_FDEVLSB_RESET,
    APV_RFM69HCW_RF_FDEVLSB_WRITE_MASK,
    APV_RFM69HCW_RF_FDEVLSB_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_FRFMSB,
    APV_RFM69HCW_RF_FRFMSB_DEFAULT,
    APV_RFM69HCW_RF_FRFMSB_RESET,
    APV_RFM69HCW_RF_FRF_WRITE_MASK,
    APV_RFM69HCW_RF_FRF_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_FRFMID,
    APV_RFM69HCW_RF_FRFMID_DEFAULT,
    APV_RFM69HCW_RF_FRFMID_RESET,
    APV_RFM69HCW_RF_FRF_WRITE_MASK,
    APV_RFM69HCW_RF_FRF_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_FRFLSB,
    APV_RFM69HCW_RF_FRFLSB_DEFAULT,
    APV_RFM69HCW_RF_FRFLSB_RESET,
    APV_RFM69HCW_RF_FRF_WRITE_MASK,
    APV_RFM69HCW_RF_FRF_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_OSC1,
    APV_RFM69HCW_RF_OSC1_DEFAULT,
    APV_RFM69HCW_RF_OSC1_RESET,
    APV_RFM69HCW_RF_OSC1_WRITE_MASK,
    APV_RFM69HCW_RF_OSC1_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_AFCCTRL,
    APV_RFM69HCW_RF_AFCCTRL_DEFAULT,
    APV_RFM69HCW_RF_AFCCTRL_RESET,
    APV_RFM69HCW_RF_AFCCTRL_WRITE_MASK,
    APV_RFM69HCW_RF_AFCCTRL_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_LOWBAT,
    APV_RFM69HCW_RF_LOWBAT_DEFAULT,
    APV_RFM69HCW_RF_LOWBAT_RESET,
    APV_RFM69HCW_RF_LOWBAT_WRITE_MASK,
    APV_RFM69HCW_RF_LOWBAT_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_LISTEN1,
    APV_RFM69HCW_RF_LISTEN1_DEFAULT,
    APV_RFM69HCW_RF_LISTEN1_RESET,
    APV_RFM69HCW_RF_LISTEN1_WRITE_MASK,
    APV_RFM69HCW_RF_LISTEN1_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_LISTEN2,
    APV_RFM69HCW_RF_LISTEN2_DEFAULT,
    APV_RFM69HCW_RF_LISTEN2_RESET,
    APV_RFM69HCW_RF_LISTEN2_WRITE_MASK,
    APV_RFM69HCW_RF_LISTEN2_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_LISTEN3,
    APV_RFM69HCW_RF_LISTEN3_DEFAULT,
    APV_RFM69HCW_RF_LISTEN3_RESET,
    APV_RFM69HCW_RF_LISTEN3_WRITE_MASK,
    APV_RFM69HCW_RF_LISTEN3_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_VERSION,
    APV_RFM69HCW_RF_VERSION_DEFAULT,
    APV_RFM69HCW_RF_VERSION_RESET,
    APV_RFM69HCW_RF_VERSION_WRITE_MASK,
    APV_RFM69HCW_RF_VERSION_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_PALEVEL,
    APV_RFM69HCW_RF_PALEVEL_DEFAULT,
    APV_RFM69HCW_RF_PALEVEL_RESET,
    APV_RFM69HCW_RF_PALEVEL_WRITE_MASK,
    APV_RFM69HCW_RF_PALEVEL_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_PARAMP,
    APV_RFM69HCW_RF_PARAMP_DEFAULT,
    APV_RFM69HCW_RF_PARAMP_RESET,
    APV_RFM69HCW_RF_PARAMP_WRITE_MASK,
    APV_RFM69HCW_RF_PARAMP_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_OCP,
    APV_RFM69HCW_RF_OCP_DEFAULT,
    APV_RFM69HCW_RF_OCP_RESET,
    APV_RFM69HCW_RF_OCP_WRITE_MASK,
    APV_RFM69HCW_RF_OCP_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_AGCREF,         // not present on RFM69/SX1231
    APV_RFM69HCW_RF_AGCREF_DEFAULT,
    APV_RFM69HCW_RF_AGCREF_RESET,
    APV_RFM69HCW_RF_AGCREF_WRITE_MASK,
    APV_RFM69HCW_RF_AGCREF_READ_MASK,
    FALSE
    },
    {
    APV_RFM69HCW_REG_AGCTHRESH1,     // not present on RFM69/SX1231
    APV_RFM69HCW_RF_AGCTHRESH1_DEFAULT,
    APV_RFM69HCW_RF_AGCTHRESH1_RESET,
    APV_RFM69HCW_RF_AGCTHRESH1_WRITE_MASK,
    APV_RFM69HCW_RF_AGCTHRESH1_READ_MASK,
    FALSE
    },
    {
    APV_RFM69HCW_REG_AGCTHRESH2,     // not present on RFM69/SX1231
    APV_RFM69HCW_RF_AGCTHRESH2_DEFAULT,
    APV_RFM69HCW_RF_AGCTHRESH2_RESET,
    APV_RFM69HCW_RF_AGCTHRESH2_WRITE_MASK,
    APV_RFM69HCW_RF_AGCTHRESH2_READ_MASK,
    FALSE
    },
    {
    APV_RFM69HCW_REG_AGCTHRESH3,     // not present on RFM69/SX1231
    APV_RFM69HCW_RF_AGCTHRESH2_DEFAULT,
    APV_RFM69HCW_RF_AGCTHRESH2_RESET,
    APV_RFM69HCW_RF_AGCTHRESH2_WRITE_MASK,
    APV_RFM69HCW_RF_AGCTHRESH2_READ_MASK,
    FALSE
    },
    {
    APV_RFM69HCW_REG_LNA,
    APV_RFM69HCW_RF_LNA_DEFAULT,
    APV_RFM69HCW_RF_LNA_RESET,
    APV_RFM69HCW_RF_LNA_WRITE_MASK,
    APV_RFM69HCW_RF_LNA_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_RXBW,
    APV_RFM69HCW_RF_RXBW_DEFAULT,
    APV_RFM69HCW_RF_RXBW_RESET,
    APV_RFM69HCW_RF_RXBW_WRITE_MASK,
    APV_RFM69HCW_RF_RXBW_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_AFCBW,
    APV_RFM69HCW_RF_AFCBW_DEFAULT,
    APV_RFM69HCW_RF_AFCBW_RESET,
    APV_RFM69HCW_RF_AFCBW_WRITE_MASK,
    APV_RFM69HCW_RF_ARCBW_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_OOKPEAK,
    APV_RFM69HCW_RF_OOKPEAK_DEFAULT,
    APV_RFM69HCW_RF_OOKPEAK_RESET,
    APV_RFM69HCW_RF_OOKPEAK_WRITE_MASK,
    APV_RFM69HCW_RF_OOKPEAK_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_OOKAVG,
    APV_RFM69HCW_RF_OOKAVG_DEFAULT,
    APV_RFM69HCW_RF_OOKAVG_RESET,
    APV_RFM69HCW_RF_OOKAVG_WRITE_MASK,
    APV_RFM69HCW_RF_OOKAVG_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_OOKFIX,
    APV_RFM69HCW_RF_OOKFIX_DEFAULT,
    APV_RFM69HCW_RF_OOKFIX_RESET,
    APV_RFM69HCW_RF_OOKFIX_WRITE_MASK,
    APV_RFM69HCW_RF_OOKFIX_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_AFCFEI,
    APV_RFM69HCW_RF_AFCFEI_DEFAULT,
    APV_RFM69HCW_RF_AFCFEI_RESET,
    APV_RFM69HCW_RF_AFCFEI_WRITE_MASK,
    APV_RFM69HCW_RF_AFCFEI_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_AFCMSB,
    APV_RFM69HCW_RF_AFCMSB_DEFAULT,
    APV_RFM69HCW_RF_AFCMSB_RESET,
    APV_RFM69HCW_RF_AFC_WRITE_MASK,
    APV_RFM69HCW_RF_AFC_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_AFCLSB,
    APV_RFM69HCW_RF_AFCLSB_DEFAULT,
    APV_RFM69HCW_RF_AFCLSB_RESET,
    APV_RFM69HCW_RF_AFC_WRITE_MASK,
    APV_RFM69HCW_RF_AFC_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_FEIMSB,
    APV_RFM69HCW_RF_FEIMSB_DEFAULT,
    APV_RFM69HCW_RF_FEIMSB_RESET,
    APV_RFM69HCW_RF_FEI_WRITE_MASK,
    APV_RFM69HCW_RF_FEI_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_FEILSB,
    APV_RFM69HCW_RF_FEILSB_DEFAULT,
    APV_RFM69HCW_RF_FEILSB_RESET,
    APV_RFM69HCW_RF_FEI_WRITE_MASK,
    APV_RFM69HCW_RF_FEI_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_RSSICONFIG,
    APV_RFM69HCW_RF_RSSI_CONFIG_DEFAULT,
    APV_RFM69HCW_RF_RSSI_CONFIG_RESET,
    APV_RFM69HCW_RF_RSSI_CONFIG_WRITE_MASK,
    APV_RFM69HCW_RF_RSSI_CONFIG_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_RSSIVALUE,
    APV_RFM69HCW_RF_RSSI_VALUE_DEFAULT,
    APV_RFM69HCW_RF_RSSI_VALUE_RESET,
    APV_RFM69HCW_RF_RSSI_VALUE_WRITE_MASK,
    APV_RFM69HCW_RF_RSSI_VALUE_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_DIOMAPPING1,
    APV_RFM69HCW_RF_DIOMAPPING1_DEFAULT,
    APV_RFM69HCW_RF_DIOMAPPING1_RESET,
    APV_RFM69HCW_RF_DIOMAPPING1_WRITE_MASK,
    APV_RFM69HCW_RF_DIOMAPPING1_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_DIOMAPPING2,
    APV_RFM69HCW_RF_DIOMAPPING2_DEFAULT,
    APV_RFM69HCW_RF_DIOMAPPING2_RESET,
    APV_RFM69HCW_RF_DIOMAPPING2_WRITE_MASK,
    APV_RFM69HCW_RF_DIOMAPPING2_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_IRQFLAGS1,
    APV_RFM69HCW_RF_IRQFLAGS1_DEFAULT,
    APV_RFM69HCW_RF_IRQFLAGS1_RESET,
    APV_RFM69HCW_RF_IRQFLAGS1_WRITE_MASK,
    APV_RFM69HCW_RF_IRQFLAGS1_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_IRQFLAGS2,
    APV_RFM69HCW_RF_IRQFLAGS2_DEFAULT,
    APV_RFM69HCW_RF_IRQFLAGS2_RESET,
    APV_RFM69HCW_RF_IRQFLAGS2_WRITE_MASK,
    APV_RFM69HCW_RF_IRQFLAGS2_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_RSSITHRESH,
    APV_RFM69HCW_RF_RSSITHRESH_DEFAULT,
    APV_RFM69HCW_RF_RSSITHRESH_RESET,
    APV_RFM69HCW_RF_RSSITHRESH_WRITE_MASK,
    APV_RFM69HCW_RF_RSSITHRESH_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_RXTIMEOUT1,
    APV_RFM69HCW_RF_RXTIMEOUT1_DEFAULT,
    APV_RFM69HCW_RF_RXTIMEOUT1_RESET,
    APV_RFM69HCW_RF_RXTIMEOUT_WRITE_MASK,
    APV_RFM69HCW_RF_RXTIMEOUT_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_RXTIMEOUT2,
    APV_RFM69HCW_RF_RXTIMEOUT2_DEFAULT,
    APV_RFM69HCW_RF_RXTIMEOUT2_RESET,
    APV_RFM69HCW_RF_RXTIMEOUT_WRITE_MASK,
    APV_RFM69HCW_RF_RXTIMEOUT_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_PREAMBLEMSB,
    APV_RFM69HCW_RF_PREAMBLESIZE_MSB_DEFAULT,
    APV_RFM69HCW_RF_PREAMBLESIZE_MSB_RESET,
    APV_RFM69HCW_RF_PREAMBLESIZE_WRITE_MASK,
    APV_RFM69HCW_RF_PREAMBLESIZE_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_PREAMBLELSB,
    APV_RFM69HCW_RF_PREAMBLESIZE_LSB_DEFAULT,
    APV_RFM69HCW_RF_PREAMBLESIZE_LSB_RESET,
    APV_RFM69HCW_RF_PREAMBLESIZE_WRITE_MASK,
    APV_RFM69HCW_RF_PREAMBLESIZE_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_SYNCCONFIG,
    APV_RFM69HCW_RF_SYNC_DEFAULT,
    APV_RFM69HCW_RF_SYNC_RESET,
    APV_RFM69HCW_RF_SYNC_WRITE_MASK,
    APV_RFM69HCW_RF_SYNC_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_SYNCVALUE1,
    APV_RFM69HCW_RF_SYNC_BYTE_DEFAULT,
    APV_RFM69HCW_RF_SYNC_BYTE_RESET,
    APV_RFM69HCW_RF_SYNC_BYTE_WRITE_MASK,
    APV_RFM69HCW_RF_SYNC_BYTE_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_SYNCVALUE2,
    APV_RFM69HCW_RF_SYNC_BYTE_DEFAULT,
    APV_RFM69HCW_RF_SYNC_BYTE_RESET,
    APV_RFM69HCW_RF_SYNC_BYTE_WRITE_MASK,
    APV_RFM69HCW_RF_SYNC_BYTE_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_SYNCVALUE3,
    APV_RFM69HCW_RF_SYNC_BYTE_DEFAULT,
    APV_RFM69HCW_RF_SYNC_BYTE_RESET,
    APV_RFM69HCW_RF_SYNC_BYTE_WRITE_MASK,
    APV_RFM69HCW_RF_SYNC_BYTE_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_SYNCVALUE4,
    APV_RFM69HCW_RF_SYNC_BYTE_DEFAULT,
    APV_RFM69HCW_RF_SYNC_BYTE_RESET,
    APV_RFM69HCW_RF_SYNC_BYTE_WRITE_MASK,
    APV_RFM69HCW_RF_SYNC_BYTE_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_SYNCVALUE5,
    APV_RFM69HCW_RF_SYNC_BYTE_DEFAULT,
    APV_RFM69HCW_RF_SYNC_BYTE_RESET,
    APV_RFM69HCW_RF_SYNC_BYTE_WRITE_MASK,
    APV_RFM69HCW_RF_SYNC_BYTE_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_SYNCVALUE6,
    APV_RFM69HCW_RF_SYNC_BYTE_DEFAULT,
    APV_RFM69HCW_RF_SYNC_BYTE_RESET,
    APV_RFM69HCW_RF_SYNC_BYTE_WRITE_MASK,
    APV_RFM69HCW_RF_SYNC_BYTE_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_SYNCVALUE7,
    APV_RFM69HCW_RF_SYNC_BYTE_DEFAULT,
    APV_RFM69HCW_RF_SYNC_BYTE_RESET,
    APV_RFM69HCW_RF_SYNC_BYTE_WRITE_MASK,
    APV_RFM69HCW_RF_SYNC_BYTE_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_SYNCVALUE8,
    APV_RFM69HCW_RF_SYNC_BYTE_DEFAULT,
    APV_RFM69HCW_RF_SYNC_BYTE_RESET,
    APV_RFM69HCW_RF_SYNC_BYTE_WRITE_MASK,
    APV_RFM69HCW_RF_SYNC_BYTE_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_PACKETCONFIG1,
    APV_RFM69HCW_RF_PACKETCONFIG1_DEFAULT,
    APV_RFM69HCW_RF_PACKETCONFIG1_RESET,
    APV_RFM69HCW_RF_PACKETCONFIG1_WRITE_MASK,
    APV_RFM69HCW_RF_PACKETCONFIG1_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_PAYLOADLENGTH,
    APV_RFM69HCW_RF_PAYLOADLENGTH_DEFAULT,
    APV_RFM69HCW_RF_PAYLOADLENGTH_RESET,
    APV_RFM69HCW_RF_PAYLOADLENGTH_WRITE_MASK,
    APV_RFM69HCW_RF_PAYLOADLENGTH_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_NODEADRS,
    APV_RFM69HCW_RF_NODEADDRESS_DEFAULT,
    APV_RFM69HCW_RF_NODEADDRESS_RESET,
    APV_RFM69HCW_RF_NODEADDRESS_WRITE_MASK,
    APV_RFM69HCW_RF_NODEADDRESS_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_BROADCASTADRS,
    APV_RFM69HCW_RF_BROADCASTADDRESS_DEFAULT,
    APV_RFM69HCW_RF_BROADCASTADDRESS_RESET,
    APV_RFM69HCW_RF_BROADCASTADDRESS_WRITE_MASK,
    APV_RFM69HCW_RF_BROADCASTADDRESS_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_AUTOMODES,
    APV_RFM69HCW_RF_AUTOMODES_DEFAULT,
    APV_RFM69HCW_RF_AUTOMODES_RESET,
    APV_RFM69HCW_RF_AUTOMODES_WRITE_MASK,
    APV_RFM69HCW_RF_AUTOMODES_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_FIFOTHRESH,
    APV_RFM69HCW_RF_FIFOTHRESH_DEFAULT,
    APV_RFM69HCW_RF_FIFOTHRESH_RESET,
    APV_RFM69HCW_RF_FIFOTHRESH_WRITE_MASK,
    APV_RFM69HCW_RF_FIFOTHRESH_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_PACKETCONFIG2,
    APV_RFM69HCW_RF_PACKETCONFIG2_DEFAULT,
    APV_RFM69HCW_RF_PACKETCONFIG2_RESET,
    APV_RFM69HCW_RF_PACKETCONFIG2_WRITE_MASK,
    APV_RFM69HCW_RF_PACKETCONFIG2_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_AESKEY1,
    APV_RFM69HCW_RF_AESKEY_DEFAULT,
    APV_RFM69HCW_RF_AESKEY_RESET,
    APV_RFM69HCW_RF_AESKEY_WRITE_MASK,
    APV_RFM69HCW_RF_AESKEY_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_AESKEY2,
    APV_RFM69HCW_RF_AESKEY_DEFAULT,
    APV_RFM69HCW_RF_AESKEY_RESET,
    APV_RFM69HCW_RF_AESKEY_WRITE_MASK,
    APV_RFM69HCW_RF_AESKEY_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_AESKEY3,
    APV_RFM69HCW_RF_AESKEY_DEFAULT,
    APV_RFM69HCW_RF_AESKEY_RESET,
    APV_RFM69HCW_RF_AESKEY_WRITE_MASK,
    APV_RFM69HCW_RF_AESKEY_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_AESKEY4,
    APV_RFM69HCW_RF_AESKEY_DEFAULT,
    APV_RFM69HCW_RF_AESKEY_RESET,
    APV_RFM69HCW_RF_AESKEY_WRITE_MASK,
    APV_RFM69HCW_RF_AESKEY_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_AESKEY5,
    APV_RFM69HCW_RF_AESKEY_DEFAULT,
    APV_RFM69HCW_RF_AESKEY_RESET,
    APV_RFM69HCW_RF_AESKEY_WRITE_MASK,
    APV_RFM69HCW_RF_AESKEY_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_AESKEY6,
    APV_RFM69HCW_RF_AESKEY_DEFAULT,
    APV_RFM69HCW_RF_AESKEY_RESET,
    APV_RFM69HCW_RF_AESKEY_WRITE_MASK,
    APV_RFM69HCW_RF_AESKEY_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_AESKEY7,
    APV_RFM69HCW_RF_AESKEY_DEFAULT,
    APV_RFM69HCW_RF_AESKEY_RESET,
    APV_RFM69HCW_RF_AESKEY_WRITE_MASK,
    APV_RFM69HCW_RF_AESKEY_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_AESKEY8,
    APV_RFM69HCW_RF_AESKEY_DEFAULT,
    APV_RFM69HCW_RF_AESKEY_RESET,
    APV_RFM69HCW_RF_AESKEY_WRITE_MASK,
    APV_RFM69HCW_RF_AESKEY_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_AESKEY9,
    APV_RFM69HCW_RF_AESKEY_DEFAULT,
    APV_RFM69HCW_RF_AESKEY_RESET,
    APV_RFM69HCW_RF_AESKEY_WRITE_MASK,
    APV_RFM69HCW_RF_AESKEY_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_AESKEY10,
    APV_RFM69HCW_RF_AESKEY_DEFAULT,
    APV_RFM69HCW_RF_AESKEY_RESET,
    APV_RFM69HCW_RF_AESKEY_WRITE_MASK,
    APV_RFM69HCW_RF_AESKEY_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_AESKEY11,
    APV_RFM69HCW_RF_AESKEY_DEFAULT,
    APV_RFM69HCW_RF_AESKEY_RESET,
    APV_RFM69HCW_RF_AESKEY_WRITE_MASK,
    APV_RFM69HCW_RF_AESKEY_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_AESKEY12,
    APV_RFM69HCW_RF_AESKEY_DEFAULT,
    APV_RFM69HCW_RF_AESKEY_RESET,
    APV_RFM69HCW_RF_AESKEY_WRITE_MASK,
    APV_RFM69HCW_RF_AESKEY_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_AESKEY13,
    APV_RFM69HCW_RF_AESKEY_DEFAULT,
    APV_RFM69HCW_RF_AESKEY_RESET,
    APV_RFM69HCW_RF_AESKEY_WRITE_MASK,
    APV_RFM69HCW_RF_AESKEY_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_AESKEY14,
    APV_RFM69HCW_RF_AESKEY_DEFAULT,
    APV_RFM69HCW_RF_AESKEY_RESET,
    APV_RFM69HCW_RF_AESKEY_WRITE_MASK,
    APV_RFM69HCW_RF_AESKEY_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_AESKEY15,
    APV_RFM69HCW_RF_AESKEY_DEFAULT,
    APV_RFM69HCW_RF_AESKEY_RESET,
    APV_RFM69HCW_RF_AESKEY_WRITE_MASK,
    APV_RFM69HCW_RF_AESKEY_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_AESKEY16,
    APV_RFM69HCW_RF_AESKEY_DEFAULT,
    APV_RFM69HCW_RF_AESKEY_RESET,
    APV_RFM69HCW_RF_AESKEY_WRITE_MASK,
    APV_RFM69HCW_RF_AESKEY_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_TEMP1,
    APV_RFM69HCW_RF_TEMP1_MEAS_DEFAULT,
    APV_RFM69HCW_RF_TEMP1_MEAS_RESET,
    APV_RFM69HCW_RF_TEMP1_MEAS_WRITE_MASK,
    APV_RFM69HCW_RF_TEMP1_MEAS_READ_MASK,
    TRUE
    },
    {
    APV_RFM69HCW_REG_TEMP2,
    APV_RFM69HCW_RF_TEMP2_DEFAULT,
    APV_RFM69HCW_RF_TEMP2_RESET,
    APV_RFM69HCW_RF_TEMP2_WRITE_MASK,
    APV_RFM69HCW_RF_TEMP2_READ_MASK,
    TRUE
    }
  };

// The variable aper of the register defintion - "shadow" registers
apv_rfm69hcwRegisters_t apv_rfm69hcwRegisters[APV_RFM69HCW_REGISTER_ADDRESS_SET] = 
  {
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_FIFO],
     APV_RFM69HCW_RF_FIFO_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_OPMODE],
     APV_RFM69HCW_RF_OPMODE_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_DATAMODUL],
     APV_RFM69HCW_RF_DATAMODUL_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_BITRATEMSB],
     APV_RFM69HCW_RF_BITRATEMSB_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_BITRATELSB],
     APV_RFM69HCW_RF_BITRATELSB_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_FDEVMSB],
     APV_RFM69HCW_RF_FDEVMSB_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_FDEVLSB],
     APV_RFM69HCW_RF_FDEVLSB_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_FRFMSB],
     APV_RFM69HCW_RF_FRFMSB_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_FRFMID],
     APV_RFM69HCW_RF_FRFMID_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_FRFLSB],
     APV_RFM69HCW_RF_FRFLSB_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_OSC1],
     APV_RFM69HCW_RF_OSC1_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_AFCCTRL],
     APV_RFM69HCW_RF_AFCCTRL_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_LOWBAT],
     APV_RFM69HCW_RF_LOWBAT_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_LISTEN1],
     APV_RFM69HCW_RF_LISTEN1_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_LISTEN2],
     APV_RFM69HCW_RF_LISTEN2_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_LISTEN3],
     APV_RFM69HCW_RF_LISTEN3_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_VERSION],
     APV_RFM69HCW_RF_VERSION_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_PALEVEL],
     APV_RFM69HCW_RF_PALEVEL_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_PARAMP],
     APV_RFM69HCW_RF_PARAMP_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_OCP],
     APV_RFM69HCW_RF_OCP_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_AGCREF],         // not present on RFM69/SX1231
     APV_RFM69HCW_RF_AGCREF_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_AGCTHRESH1],     // not present on RFM69/SX1231
     APV_RFM69HCW_RF_AGCTHRESH1_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_AGCTHRESH2],     // not present on RFM69/SX1231
     APV_RFM69HCW_RF_AGCTHRESH2_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_AGCTHRESH3],     // not present on RFM69/SX1231
     APV_RFM69HCW_RF_AGCTHRESH2_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_LNA],
     APV_RFM69HCW_RF_LNA_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_RXBW],
     APV_RFM69HCW_RF_RXBW_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_AFCBW],
     APV_RFM69HCW_RF_AFCBW_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_OOKPEAK],
     APV_RFM69HCW_RF_OOKPEAK_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_OOKAVG],
     APV_RFM69HCW_RF_OOKAVG_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_OOKFIX],
     APV_RFM69HCW_RF_OOKFIX_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_AFCFEI],
     APV_RFM69HCW_RF_AFCFEI_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_AFCMSB],
     APV_RFM69HCW_RF_AFCMSB_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_AFCLSB],
     APV_RFM69HCW_RF_AFCLSB_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_FEIMSB],
     APV_RFM69HCW_RF_FEIMSB_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_FEILSB],
     APV_RFM69HCW_RF_FEILSB_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_RSSICONFIG],
     APV_RFM69HCW_RF_RSSI_CONFIG_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_RSSIVALUE],
     APV_RFM69HCW_RF_RSSI_VALUE_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_DIOMAPPING1],
     APV_RFM69HCW_RF_DIOMAPPING1_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_DIOMAPPING2],
     APV_RFM69HCW_RF_DIOMAPPING2_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_IRQFLAGS1],
     APV_RFM69HCW_RF_IRQFLAGS1_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_IRQFLAGS2],
     APV_RFM69HCW_RF_IRQFLAGS2_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_RSSITHRESH],
     APV_RFM69HCW_RF_RSSITHRESH_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_RXTIMEOUT1],
     APV_RFM69HCW_RF_RXTIMEOUT1_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_RXTIMEOUT2],
     APV_RFM69HCW_RF_RXTIMEOUT2_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_PREAMBLEMSB],
     APV_RFM69HCW_RF_PREAMBLESIZE_MSB_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_PREAMBLELSB],
     APV_RFM69HCW_RF_PREAMBLESIZE_LSB_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_SYNCCONFIG],
     APV_RFM69HCW_RF_SYNC_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_SYNCVALUE1],
     APV_RFM69HCW_RF_SYNC_BYTE_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_SYNCVALUE2],
     APV_RFM69HCW_RF_SYNC_BYTE_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_SYNCVALUE3],
     APV_RFM69HCW_RF_SYNC_BYTE_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_SYNCVALUE4],
     APV_RFM69HCW_RF_SYNC_BYTE_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_SYNCVALUE5],
     APV_RFM69HCW_RF_SYNC_BYTE_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_SYNCVALUE6],
     APV_RFM69HCW_RF_SYNC_BYTE_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_SYNCVALUE7],
     APV_RFM69HCW_RF_SYNC_BYTE_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_SYNCVALUE8],
     APV_RFM69HCW_RF_SYNC_BYTE_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_PACKETCONFIG1],
     APV_RFM69HCW_RF_PACKETCONFIG1_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_PAYLOADLENGTH],
     APV_RFM69HCW_RF_PAYLOADLENGTH_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_NODEADRS],
     APV_RFM69HCW_RF_NODEADDRESS_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_BROADCASTADRS],
     APV_RFM69HCW_RF_BROADCASTADDRESS_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_AUTOMODES],
     APV_RFM69HCW_RF_AUTOMODES_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_FIFOTHRESH],
     APV_RFM69HCW_RF_FIFOTHRESH_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_PACKETCONFIG2],
     APV_RFM69HCW_RF_PACKETCONFIG2_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_AESKEY1],
     APV_RFM69HCW_RF_AESKEY_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_AESKEY2],
     APV_RFM69HCW_RF_AESKEY_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_AESKEY3],
     APV_RFM69HCW_RF_AESKEY_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_AESKEY4],
     APV_RFM69HCW_RF_AESKEY_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_AESKEY5],
     APV_RFM69HCW_RF_AESKEY_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_AESKEY6],
     APV_RFM69HCW_RF_AESKEY_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_AESKEY7],
     APV_RFM69HCW_RF_AESKEY_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_AESKEY8],
     APV_RFM69HCW_RF_AESKEY_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_AESKEY9],
     APV_RFM69HCW_RF_AESKEY_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_AESKEY10],
     APV_RFM69HCW_RF_AESKEY_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_AESKEY11],
     APV_RFM69HCW_RF_AESKEY_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_AESKEY12],
     APV_RFM69HCW_RF_AESKEY_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_AESKEY13],
     APV_RFM69HCW_RF_AESKEY_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_AESKEY14],
     APV_RFM69HCW_RF_AESKEY_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_AESKEY15],
     APV_RFM69HCW_RF_AESKEY_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_AESKEY16],
     APV_RFM69HCW_RF_AESKEY_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_TEMP1],
     APV_RFM69HCW_RF_TEMP1_MEAS_DEFAULT
    },
    {
    &apv_rfm69hcwRegisterDefinitions[APV_RFM69HCW_REG_TEMP2],
     APV_RFM69HCW_RF_TEMP2_DEFAULT
    }  
  };

/******************************************************************************/

// At startup no SPI read/write accesses have taken place
apv_rfm69hcwRegisterReadWrite_t apvRfm69HcwPrecedingAccess    = APV_RFM69HCW_REGISTER_NO_ACTION;
// Register access scheduling flag
apv_rfm69hcwRegisterSchedule_t rfm69HcwRegisterScheduleAccess = APV_RFM69HCW_REGISTER_ACCESS_IN_PROGRESS_NONE;

// Rfm69Hcw operations callback timer flag
APV_TYPE_BOOLEAN                apvRfm69HcwTimerFlag          = false,
// Timer expired (or not) flag
                                apvRfm69HcwTimerExpiredFlag   = false,
// Enable the RFM69HCW use in the main loop
                                apvRfm69HcwCoreActiveFlag     = false,
// Initialise the RFM69HCW core scheduling flag
                                apvRfm69HcwCoreSchedulingFlag = false,
// Flag the manual RESET of the RFM69HCW active
                                apvRfm69HcwManualResetFlag    = false;
// Manual reset countdown counter; derives millisecond resolution 
// from the main loop
APV_TYPE_UINT8                  apvRfm69HcwManualResetCounter = 0;
// Index of the (created) duration timer
APV_TYPE_UINT32                 apvRfm69HcwTimerIndex          = APV_DURATION_TIMER_NULL_INDEX;
// Function error codes
APV_ERROR_CODE                  initialisationError            = APV_ERROR_CODE_NONE;

APV_TYPE_UCHAR                  rfm69hcwRxDataBuffer[APV_RFM69HCW_REGISTER_SETUP_MAXIMUM_WIDTH],
                                rfm69hcwTxDataBuffer[APV_RFM69HCW_REGISTER_SETUP_MAXIMUM_WIDTH],
                                rfm69hcwRegisterCommand[APV_RFM69HCW_REGISTER_SETUP_MAXIMUM_WIDTH];

apv_rfm69hcwRegisterState_t     rfm69hcwRegisterState;

/******************************************************************************/
/* apvInitialiseRfm69Hcw() :                                                  */
/*  --> apvCoreTimerBlock : duration timers block                             */
/*  --> spiChipSelect     : SPI slave device chip select                      */
/*                                                                            */
/*                                                                            */
/******************************************************************************/

APV_ERROR_CODE apvInitialiseRfm69Hcw(apvCoreTimerBlock_t      *apvCoreTimerBlock,
                                     apvSPIFixedChipSelects_t  spiChipSelect)
  {
/******************************************************************************/

  volatile APV_ERROR_CODE                   rfm69hcwError        = APV_ERROR_CODE_NONE;

           APV_TYPE_UINT8                   apvSpi0FillLevel     = 0;
  volatile APV_TYPE_INT8                    registerCounter      = 0;
  volatile APV_TYPE_UINT16                  apvRfm69HcwDataWord  = 0;
           
           apv_rfm69hcwRegisterAddressSet_t registerSet          = APV_RFM69HCW_REG_FIFO,
                                            registerBase         = APV_RFM69HCW_REG_FIFO;

  volatile APV_TYPE_UINT32                  centreFrequency      = APV_RFM69HCM_UK_ISM_FREQUENCY_868_HEX;
           
  volatile APV_TYPE_UINT                    testCounter          = 0;
  
/******************************************************************************/

  // Set the matching SPI0 chip-select and operating mode for this peripheral
  rfm69hcwError = apvSpi0Cs1rfm69hcwAssignment();

  // Switch on the chip-enable line
  apvSwitchDigitalLines(APV_DIGITAL_RFM69HCW_CHIP_ENABLE_SELECT, 
                        true);
  
  // Write the Rfm69Hcw register set defaults into their "shadows"
  for (registerSet = APV_RFM69HCW_REG_FIFO; registerSet < APV_RFM69HCW_REGISTER_ADDRESS_SET; registerSet++)
    {
    apvRfm69HcwWriteRegisterDefault(registerSet);
    }

  // Switch on the peripheral I/O "D" channel clock
  rfm69hcwError = apvSwitchPeripheralClock(ID_PIOD,
                                           true);

  // Switch on the manual RESET line
  rfm69hcwError = apvSwitchDigitalLines(APV_DIGITAL_RFM69HCW_RESET_SELECT,
                                        true);

  // Force a chip RESET
  rfm69hcwError = apvRfm69HcwAtomicReset(apvCoreTimerBlock);

  // Switch off the manual RESET line
  rfm69hcwError = apvSwitchDigitalLines(APV_DIGITAL_RFM69HCW_RESET_SELECT,
                                        false);

  // Then try to detect the device
  apvRfm69HcwTimerFlag = false;
  
#if (1)
  rfm69hcwError = apvCreateDurationTimer( apvCoreTimerBlock,
                                         apvRfm69HcwStateTimer,               // callback function
                                        (void *)&apvRfm69HcwTimerFlag,        // timer expiry flag passed to callback function
                                         APV_DURATION_TIMER_TYPE_ONE_SHOT,    // single-shot
                                         //APV_EVENT_TIMER_INVERSE_NANOSECONDS, // one second period
                                         APV_RFM69HCW_EVENT_TIMER_1ms,        // one millisecond period
                                         APV_DURATION_TIMER_SOURCE_SYSTICK,
                                         APV_DURATION_TIMER_STATE_STOPPED,
                                        &apvRfm69HcwTimerIndex);

  // Restart the one-shot timer delay waiting for a character
  rfm69hcwError = apvReTriggerDurationTimer(apvCoreTimerBlock,
                                            apvRfm69HcwTimerIndex,
                                            // APV_EVENT_TIMER_INVERSE_NANOSECONDS
                                            APV_RFM69HCW_EVENT_TIMER_1ms);
#endif

  // Do the character read request - note the "READ_DATA" prefix character flag is rfm69hcw-only!
#if (0)
  rfm69hcwError = apvSPITransmitCharacter((APV_RFM69HCW_REGISTER_READ_DATA | APV_RFM69HCW_REG_VERSION),
                                          APV_RFM69HCW_NULL_CHARACTER,
                                           spiChipSelect,
                                           APV_SPI_LAST_TRANSFER_ACTIVE,
                                           true);
#else
#if (0)
  // Setup to read the rfm69hcw sx1231 chip version number
  //registerAccessLength = APV_RFM69HCW_REGISTER_SETUP_MINIMUM_WIDTH;
  registerBase         = APV_RFM69HCW_REG_VERSION;
#else  
  // Setup to read all three components of the carrier centre frequency
  rfm69hcwRegisterState.apv_rfm69hcwRegisterDataLength = APV_RFM69HCW_REG_CENTRE_FREQUENCY_WIDTH;
  //registerAccessLength = APV_RFM69HCW_REG_CENTRE_FREQUENCY_WIDTH;
  registerBase                                         = APV_RFM69HCW_REG_FRFMSB;
#endif

  rfm69hcwError = apvRfm69hcwReadRegisterStart( registerBase,
                                               &rfm69hcwRegisterState,
                                                apvRfm69HcwStateTimer,               // callback function,
                                               &rfm69HcwRegisterScheduleAccess,
                                                apvCoreTimerBlock,
                                               &apvRfm69HcwTimerIndex,
                                               &apvRfm69HcwTimerFlag,
                                                spiChipSelect);

#if (0)
  rfm69hcwError = apvRfm69hcwAccessRegister( registerBase,
                                            &apv_rfm69hcwRegisterDefinitions[0],
                                            &rfm69hcwTxDataBuffer[0],
                                            &registerAccessLength,
                                             APV_RFM69HCW_REGISTER_READ_DATA,
                                             spiChipSelect);
#endif
#endif

#if (1)

  // Look for the register access succeeding or timing out
  while((rfm69HcwRegisterScheduleAccess == APV_RFM69HCW_REGISTER_ACCESS_IN_PROGRESS_READ) &&
        (rfm69hcwError                  == APV_ERROR_CODE_NONE))
    {
    // TEST : monitoring the number of calls to "apvRfm69hcwReadRegisterEnd"
    testCounter = testCounter + 1;
    
    rfm69hcwError = apvRfm69hcwReadRegisterEnd(&rfm69HcwRegisterScheduleAccess,
                                               &rfm69hcwRegisterState,
                                                apvCoreTimerBlock,
                                               &apvRfm69HcwTimerIndex,
                                               &apvRfm69HcwTimerFlag,
                                                spiChipSelect);
    }

  // On a success read the centre frequency and compare it to the default
  if (rfm69hcwError == APV_ERROR_CODE_NONE)
    {
    // Read the centre frequency in MSB -> LSB order
    centreFrequency = 0;

    for (registerCounter = 0; registerCounter < (rfm69hcwRegisterState.apv_rfm69hcwRegisterDataLength + APV_RFM69HCW_REG_DEFAULT_WIDTH); registerCounter++)
      {
      // Scale and load the next byte of the centre frequency
      centreFrequency = centreFrequency << APV_RFM69HCM_REG_CENTRE_FREQUENCY_SHIFT;
      centreFrequency = centreFrequency + rfm69hcwRegisterState.apv_rfm69hcwRegisterData[registerCounter];
      }    
    }

/*    {
    volatile double cD = 0.0;
    volatile unsigned int cU = 0;

    cD = APV_RFM69HCM_UK_ISM_FREQUENCY_868_DECIMAL / APV_RFM69HCM_BASE_FREQUENCY_RESOLUTION;

    cU =((APV_TYPE_UINT32)(APV_RFM69HCM_UK_ISM_FREQUENCY_868_DECIMAL / APV_RFM69HCM_BASE_FREQUENCY_RESOLUTION));
    }


  {
  volatile APV_TYPE_DOUBLE d868 = (APV_RFM69HCM_UK_ISM_FREQUENCY_868_DECIMAL / APV_RFM69HCM_BASE_FREQUENCY_RESOLUTION);

  //#define APV_RFM69HCM_UK_ISM_FREQUENCY_868_DECIMAL   ((APV_TYPE_DOUBLE)868000000) // the EU/UK ISM frequencies
  //#define APV_RFM69HCM_UK_ISM_FREQUENCY_868_HEX       ((APV_TYPE_UINT32)(APV_RFM69HCM_UK_ISM_FREQUENCY_868_DECIMAL / APV_RFM69HCM_BASE_FREQUENCY_RESOLUTION))

  } */











  if ((centreFrequency != APV_RFM69HCM_BASE_FREQUENCY_915_HEX) && (centreFrequency != APV_RFM69HCM_UK_ISM_FREQUENCY_868_HEX))
    {
    while (true)
      ;
    }

  /******************************************************************************/
  /* Test the "write register" functions                                        */
  /******************************************************************************/

  // Load the new centre-frequency
  centreFrequency = APV_RFM69HCM_UK_ISM_FREQUENCY_868_HEX;

  {
  volatile APV_TYPE_UINT32 txBufferT = 0;
    
  for (registerCounter = (rfm69hcwRegisterState.apv_rfm69hcwRegisterDataLength - 1); registerCounter >= 0;  registerCounter--)
    {
    txBufferT = centreFrequency;
    txBufferT = txBufferT >> (APV_RFM69HCM_BASE_FREQUENCY_SUBMASK_SHIFT * registerCounter);
    txBufferT = txBufferT &  APV_RFM69HCM_BASE_FREQUENCY_SUBMASK;

    rfm69hcwTxDataBuffer[(rfm69hcwRegisterState.apv_rfm69hcwRegisterDataLength - 1) - registerCounter] = txBufferT;    
    }
  }
  
  rfm69hcwError = apvRfm69hcwWriteRegisterStart( registerBase,
                                                &rfm69hcwTxDataBuffer[0],
                                                &rfm69hcwRegisterState,
                                                 apvRfm69HcwStateTimer,               // callback function,
                                                &rfm69HcwRegisterScheduleAccess,
                                                 apvCoreTimerBlock,
                                                &apvRfm69HcwTimerIndex,
                                                &apvRfm69HcwTimerFlag,
                                                 spiChipSelect);

  // Look for the register access succeeding or timing out
  testCounter = 0;
  
  while((rfm69HcwRegisterScheduleAccess == APV_RFM69HCW_REGISTER_ACCESS_IN_PROGRESS_WRITE) &&
        (rfm69hcwError                  == APV_ERROR_CODE_NONE))
    {
    // TEST : monitoring the number of calls to "apvRfm69hcwReadRegisterEnd"
    testCounter = testCounter + 1;
    
    rfm69hcwError = apvRfm69hcwWriteRegisterEnd(&rfm69hcwRegisterState,
                                                &rfm69HcwRegisterScheduleAccess,
                                                 apvCoreTimerBlock,
                                                &apvRfm69HcwTimerIndex,
                                                &apvRfm69HcwTimerFlag,
                                                 spiChipSelect);
    }


#else
/******************************************************************************/
/* Default to expiry - this will be cancelled if the interrupt turns up first */
/******************************************************************************/

  apvRfm69HcwTimerExpiredFlag = true; 

/******************************************************************************/
/* Wait for the SPI0 receive interrupt or the duration timer expiry,          */
/* whichever happens first                                                    */
/******************************************************************************/

  while (apvRfm69HcwTimerFlag == false)
    {
    if (apvGetFIFOFillLevel(&apvSpi0ChipSelectFIFO[spiChipSelect],
                            &apvSpi0FillLevel,
                             true) == APV_ERROR_CODE_NONE)
      {
      if (apvSpi0FillLevel != 0)
        {
        apvRfm69HcwTimerExpiredFlag = false;
        }
      }
    }
  
  if (apvRfm69HcwTimerExpiredFlag == false)
    {
    apvRfm69HcwTimerExpiredFlag = true;

    // READ THE RETURNED VALUE AND COMPARE ?


    // Check the read buffer is not empty
    if (apvSpi0FillLevel == 0)
      {
      // This shouldn't even be possible, but...
      initialisationError = APV_ERROR_CODE_MESSAGE_BUFFER_FAULTY;
      }
    else
      {
      registerCounter = registerAccessLength + APV_RFM69HCW_REG_DEFAULT_WIDTH;
      
      // Unload the chip version-number byte - the second character of two 8-bit 
      // characters is the answer

      // Read the centre frequency in MSB -> LSB order
      centreFrequency      = 0;

      while (registerCounter > 0)
        {
        rfm69hcwError = apvSpiReceiveCharacter(&apvRfm69HcwDataWord,
                                               &spiChipSelect);

        // Scale and load the next byte of the centre frequency
        centreFrequency = centreFrequency << APV_RFM69HCM_REG_CENTRE_FREQUENCY_SHIFT;
        centreFrequency = centreFrequency + apvRfm69HcwDataWord;

        registerCounter = registerCounter - 1;
        }
      }
    }
  else
    {
    rfm69hcwError = APV_ERROR_CODE_EVENT_TIMEOUT_ERROR;
    }

  // Finished with the duration timer
  apvDestroyDurationTimer( apvCoreTimerBlock, 
                          &apvRfm69HcwTimerIndex);

  // Check the radio is attached and working
#if (0)  
  if (((APV_TYPE_UINT8)(apvRfm69HcwDataWord & APV_RFM69HCW_VERSION_MASK)) != APV_RFM69HCW_VERSION_NUMBER)
#else
  if (centreFrequency != APV_RFM69HCM_BASE_FREQUENCY_915_HEX)
#endif
    {
    while (true)
      ;
    }
#endif

/******************************************************************************/

  return(rfm69hcwError);

/******************************************************************************/
  } /* end of apvInitialiseRfm69Hcw                                           */

/******************************************************************************/
/* apvRfm69hcwReadRegisterStart() :                                           */
/*  --> rfm69hcwRegister                 : rfm69hcw register number           */
/*  --> rfm69hcwRegisterState            : register read state                */
/*  --> durationTimerCallBack            : callback function used by duration */
/*                                         timer to set a "timer expired"     */
/*                                         flag                               */
/*  --> rfm69HcwRegisterScheduleAccess   : flag a register access in progress */
/*                                         to detect completion               */
/*  --> timerBlock                       : duration timers block              */
/*  --> timerIndex                       : duration timer index               */
/*  --> timerFlag                        : duration timer event signalling    */
/*                                         flag                               */
/*  --> spiChipSelect                    : SPI slave device chip select       */
/*                                                                            */
/* - start a register read. Load a data buffer with the field starting        */
/*   address to read from and <NOP> characters equal to the length of the     */
/*   field being read (SPI read/write is always bit-synchronous). The read is */
/*   flagged as in progress and completed by an "...End" co-routine scheduled */
/*   by the "...AccessInProgress" flag. Set the starting state of the co-     */
/*   routine                                                                  */
/*                                                                            */
/******************************************************************************/

APV_ERROR_CODE apvRfm69hcwReadRegisterStart(apv_rfm69hcwRegisterAddressSet_t   rfm69HcwRegister,
                                            apv_rfm69hcwRegisterState_t       *rfm69HcwRegisterState,
                                            void                             (*durationTimerCallBack)(void *durationEventMessage),
                                            apv_rfm69hcwRegisterSchedule_t    *rfm69HcwRegisterScheduleAccess,
                                            apvCoreTimerBlock_t               *timerBlock,
                                            APV_TYPE_UINT32                   *timerIndex,
                                            APV_TYPE_BOOLEAN                  *timerFlag,
                                            apvSPIFixedChipSelects_t           spiChipSelect)
  {
/******************************************************************************/

  APV_ERROR_CODE rfm69hcwError    = APV_ERROR_CODE_NONE;
  // Register access lengths always include the address byte
  APV_TYPE_UINT  charactersLength = rfm69HcwRegisterState->apv_rfm69hcwRegisterDataLength + APV_RFM69HCW_REG_DEFAULT_WIDTH,
                 registerCounter  = 0;

/******************************************************************************/

   if ((durationTimerCallBack != NULL) && (rfm69HcwRegisterScheduleAccess != NULL) &&
       (timerBlock            != NULL) && (timerIndex                     != NULL) &&
       (*rfm69HcwRegisterScheduleAccess == APV_RFM69HCW_REGISTER_ACCESS_IN_PROGRESS_NONE))
     {
     // Setup the <address + data> buffer to receive "...DataLength" characters
     for (registerCounter = APV_RFM69HCW_REGISTER_COMMAND_OFFSET; registerCounter < charactersLength; registerCounter++)
       {
       rfm69hcwRegisterState.apv_rfm69hcwRegisterData[registerCounter] = APV_RFM69HCW_COMMAND_NOP;
       }
     
     rfm69hcwError = apvRfm69hcwAccessRegister( rfm69HcwRegister,
                                               &apv_rfm69hcwRegisterDefinitions[0],
                                                rfm69HcwRegisterState->apv_rfm69hcwRegisterData,
                                               &charactersLength,
                                                APV_RFM69HCW_REGISTER_READ_DATA,
                                                spiChipSelect);
  
    if (rfm69hcwError == APV_ERROR_CODE_NONE)
      {
      /******************************************************************************/
      /* The register "read" access has been started. Initialise the state of the   */
      /* "...End" function and create a countdown timer towait for the register     */
      /* "read" to complete or a failure                                            */
      /******************************************************************************/

       rfm69hcwRegisterState.apv_rfm69hcwRegisterAccessState = APV_RFM69HCW_REGISTER_ACCESS_STATE_START;
      *timerFlag                                             = false;
    
      rfm69hcwError = apvCreateDurationTimer(timerBlock,
                                             durationTimerCallBack,                 // callback function
                                             (APV_TYPE_VOID *)timerFlag,            // timer expiry flag passed to callback function
                                             APV_DURATION_TIMER_TYPE_ONE_SHOT,      // single-shot
                                             APV_RFM69HCW_EVENT_TIMER_5ms,          // five millisecond period
                                             APV_DURATION_TIMER_SOURCE_SYSTICK,
                                             APV_DURATION_TIMER_STATE_STOPPED,
                                             timerIndex);
  
      if (rfm69hcwError == APV_ERROR_CODE_NONE)
        {
        // Restart the one-shot timer delay waiting for a character
        rfm69hcwError = apvReTriggerDurationTimer( timerBlock,
                                                  *timerIndex,
                                                   APV_RFM69HCW_EVENT_TIMER_5ms);
        }
      }

                                                
    if (rfm69hcwError == APV_ERROR_CODE_NONE)
      {
      *rfm69HcwRegisterScheduleAccess = APV_RFM69HCW_REGISTER_ACCESS_IN_PROGRESS_READ;
      }
    }
  else
    {
    rfm69hcwError = APV_ERROR_CODE_NULL_PARAMETER;
    }
  
/******************************************************************************/

  return(rfm69hcwError);

/******************************************************************************/
  } /* end of apvRfm69hcwReadRegisterStart                                    */

/******************************************************************************/
/* apvRfm69hcwReadRegisterEnd() :                                             */
/*  --> rfm69HcwRegisterScheduleAccess   : flag a register access in progress */
/*                                         to detect completion               */
/*  --> rfm69hcwRegisterState            : register read state                */
/*  --> timerBlock                       : duration timers block              */
/*  --> timerIndex                       : duration timer index               */
/*  --> timerFlag                        : duration timer event signalling    */
/*                                         flag                               */
/*  --> spiChipSelect                    : SPI slave device chip select       */
/*                                                                            */
/* - return the characters of a SPI read from the FIFO                        */
/*                                                                            */
/******************************************************************************/
                                            
APV_ERROR_CODE apvRfm69hcwReadRegisterEnd(apv_rfm69hcwRegisterSchedule_t *rfm69HcwRegisterScheduleAccess,
                                          apv_rfm69hcwRegisterState_t    *rfm69HcwRegisterState,
                                          apvCoreTimerBlock_t            *timerBlock,
                                          APV_TYPE_UINT32                *timerIndex,
                                          APV_TYPE_BOOLEAN               *timerFlag,
                                          apvSPIFixedChipSelects_t        spiChipSelect)
  {
/******************************************************************************/
  
           APV_ERROR_CODE rfm69hcwError        = APV_ERROR_CODE_NONE;

  volatile APV_TYPE_UINT16 apvRfm69HcwDataWord = 0;

           APV_TYPE_UINT8 apvSpi0FillLevel     = 0;
         
           // Register access lengths always include the address byte
           APV_TYPE_UINT  charactersLength     = rfm69HcwRegisterState->apv_rfm69hcwRegisterDataLength + APV_RFM69HCW_REG_DEFAULT_WIDTH;

/******************************************************************************/

  if ((rfm69HcwRegisterScheduleAccess     != NULL) && (rfm69HcwRegisterState != NULL) &&
      (timerBlock                         != NULL) && (timerIndex            != NULL) && 
      (timerFlag                          != NULL) &&
      (*rfm69HcwRegisterScheduleAccess == APV_RFM69HCW_REGISTER_ACCESS_IN_PROGRESS_READ))
    {
    /******************************************************************************/
    /* The SPI FIFO reads are stateful :                                          */
    /******************************************************************************/

    switch(rfm69HcwRegisterState->apv_rfm69hcwRegisterAccessState)
      {
      case APV_RFM69HCW_REGISTER_ACCESS_STATE_START  : if (*timerFlag == false)
                                                         {
                                                         /******************************************************************************/
                                                         /* "...Index" counts down from the number of characters pulled from the FIFO  */
                                                         /* "...Count" counts up tot he number of characters required                  */
                                                         /******************************************************************************/
                                                         
                                                         rfm69hcwRegisterState.apv_rfm69hcwRegisterIndex = 0;
                                                         rfm69hcwRegisterState.apv_rfm69hcwRegisterCount = 0;
                                                         
                                                         // No timeout - when the FIFO level has data change state to pull the received characters
                                                         apvGetFIFOFillLevel(&apvSpi0ChipSelectFIFO[spiChipSelect],
                                                                             &apvSpi0FillLevel,
                                                                              true);

                                                         if (apvSpi0FillLevel > 0)
                                                           {
                                                           rfm69HcwRegisterState->apv_rfm69hcwRegisterIndex       = apvSpi0FillLevel;

                                                           rfm69HcwRegisterState->apv_rfm69hcwRegisterAccessState = APV_RFM69HCW_REGISTER_ACCESS_STATE_PULL;
                                                           }
                                                         }
                                                       else
                                                         {
                                                         // The FIFO read has timed-out, flush and exit
                                                         apvResetFIFO(&apvSpi0ChipSelectFIFO[spiChipSelect],
                                                                       true);
                                                         
                                                         rfm69HcwRegisterState->apv_rfm69hcwRegisterAccessState = APV_RFM69HCW_REGISTER_ACCESS_STATE_FAILED;
                                                         }
                                                       
                                                       break;

      case APV_RFM69HCW_REGISTER_ACCESS_STATE_PULL   : if (*timerFlag == false)
                                                         {
                                                         // Have enough characters been pulled ?
                                                         if (rfm69HcwRegisterState->apv_rfm69hcwRegisterCount < charactersLength)
                                                           {
                                                           // Pull the outstanding characters up to the data length limit
                                                           while ((rfm69HcwRegisterState->apv_rfm69hcwRegisterIndex > 0) && 
                                                                  (rfm69HcwRegisterState->apv_rfm69hcwRegisterCount < charactersLength))
                                                             {
                                                             // Get the next character
                                                             rfm69hcwError = apvSpiReceiveCharacter(&apvRfm69HcwDataWord,
                                                                                                    &spiChipSelect);

                                                             rfm69HcwRegisterState->apv_rfm69hcwRegisterData[rfm69HcwRegisterState->apv_rfm69hcwRegisterCount] = (APV_TYPE_UCHAR)apvRfm69HcwDataWord;

                                                             rfm69HcwRegisterState->apv_rfm69hcwRegisterIndex = rfm69HcwRegisterState->apv_rfm69hcwRegisterIndex - 1;
                                                             rfm69HcwRegisterState->apv_rfm69hcwRegisterCount = rfm69HcwRegisterState->apv_rfm69hcwRegisterCount + 1;
                                                             }

                                                           // Now are we finished ? If not try to pull some more characters
                                                           if (rfm69HcwRegisterState->apv_rfm69hcwRegisterCount < charactersLength)
                                                             {
                                                             apvGetFIFOFillLevel(&apvSpi0ChipSelectFIFO[spiChipSelect],
                                                                                 &apvSpi0FillLevel,
                                                                                  true);

                                                             rfm69HcwRegisterState->apv_rfm69hcwRegisterIndex = apvSpi0FillLevel;
                                                             }
                                                           }
                                                         else
                                                           {
                                                           // All the register characters have been pulled
                                                           rfm69HcwRegisterState->apv_rfm69hcwRegisterAccessState = APV_RFM69HCW_REGISTER_ACCESS_STATE_END;
                                                           }
                                                         }
                                                       else
                                                         {
                                                         // The FIFO read has timed-out, flush and exit
                                                         apvResetFIFO(&apvSpi0ChipSelectFIFO[spiChipSelect],
                                                                       true);
                                                         
                                                         rfm69HcwRegisterState->apv_rfm69hcwRegisterAccessState = APV_RFM69HCW_REGISTER_ACCESS_STATE_FAILED;
                                                         }      
                                                       break;

      case APV_RFM69HCW_REGISTER_ACCESS_STATE_FAILED : rfm69hcwError = APV_ERROR_CODE_EVENT_TIMEOUT_ERROR;
      case APV_RFM69HCW_REGISTER_ACCESS_STATE_END    : apvDestroyDurationTimer(timerBlock, 
                                                                               timerIndex);
                                                 
                                                       // Cancel the successful (or not!)"read"
                                                       *rfm69HcwRegisterScheduleAccess = APV_RFM69HCW_REGISTER_ACCESS_IN_PROGRESS_NONE;
                                                       break;

      default                                        :
                                                       break;
      }
    
  /******************************************************************************/
    }
  else
    {
    apvDestroyDurationTimer(timerBlock, 
                            timerIndex);

    // Cancel the unsuccessful "read" attempt
    *rfm69HcwRegisterScheduleAccess = APV_RFM69HCW_REGISTER_ACCESS_IN_PROGRESS_NONE;

     rfm69hcwError                  = APV_ERROR_CODE_PARAMETER_OUT_OF_RANGE;
    }


#if (0)      
  // Test for rfm69hcw register "read" in progress
  if (*rfm69HcwRegisterScheduleAccess == APV_RFM69HCW_REGISTER_ACCESS_IN_PROGRESS_READ)
    {
    // Has the register time-out expired ?
    if (*timerFlag == false)
      {
      if (apvGetFIFOFillLevel(&apvSpi0ChipSelectFIFO[spiChipSelect],
                              &apvSpi0FillLevel,
                               true) == APV_ERROR_CODE_NONE)
        { // Have all the expected characters been read ?
        if (apvSpi0FillLevel == charactersLength)
          {
          APV_TYPE_UINT16 apvRfm69HcwDataWord = 0;
          
          for (registerIndex = 0; registerIndex < charactersLength; registerIndex++)
            {
            rfm69hcwError = apvSpiReceiveCharacter(&apvRfm69HcwDataWord,
                                                   &spiChipSelect);

            // If the number of characters in the FIFO is not enough, leave in error
            if (rfm69hcwError != APV_ERROR_CODE_NONE)
              {
              break;
              }
            
            // Return the characters as <address> + <ascending register order>
            *(rfm69HcwRegisterState->apv_rfm69hcwRegisterData + registerIndex) = apvRfm69HcwDataWord;
            }

          // Cancel the "read", successful or otherwise
          *rfm69HcwRegisterScheduleAccess = APV_RFM69HCW_REGISTER_ACCESS_IN_PROGRESS_NONE;
          }
        // If not, come around for another try by leaving the "accessProgress" flag set
        }
      else
        { // FIFO has failed!
        apvDestroyDurationTimer(timerBlock, 
                                timerIndex);
   
        // Cancel the unsuccessful "read" attempt
        *rfm69HcwRegisterScheduleAccess = APV_RFM69HCW_REGISTER_ACCESS_IN_PROGRESS_NONE;
   
         rfm69hcwError                  = APV_ERROR_CODE_EVENT_TIMEOUT_ERROR;
        }                               
      }
    else
      { // Register time-out has expired, "read" has failed!
      // Finished with the duration timer
      apvDestroyDurationTimer(timerBlock, 
                              timerIndex);

      // Cancel the unsuccessful "read" attempt
      *rfm69HcwRegisterScheduleAccess = APV_RFM69HCW_REGISTER_ACCESS_IN_PROGRESS_NONE;

       rfm69hcwError                  = APV_ERROR_CODE_EVENT_TIMEOUT_ERROR;
      }
    }
#endif

/******************************************************************************/

  return(rfm69hcwError);

/******************************************************************************/
  } /* end of apvRfm69hcwReadRegisterEnd                                      */

/******************************************************************************/
/* apvRfm69hcwWriteRegisterStart() :                                          */
/*  --> rfm69hcwRegister                 : rfm69hcw register number           */
/*  --> rfm69HcwRegisterData             : 1 { <data character> } n           */
/*  --> rfm69hcwRegisterState            : register write state               */
/*  --> durationTimerCallBack            : callback function used by duration */
/*                                         timer to set a "timer expired"     */
/*                                         flag                               */
/*  --> rfm69HcwRegisterScheduleAccess   : flag a register access in progress */
/*                                         to detect completion               */
/*  --> timerBlock                       : duration timers block              */
/*  --> timerIndex                       : duration timer index               */
/*  --> timerFlag                        : duration timer event signalling    */
/*                                         flag                               */
/*  --> spiChipSelect                    : SPI slave device chip select       */
/*                                                                            */
/* - start a register write. Load a data buffer with the field starting       */
/*   address to write to and <DATA> characters equal to the length of the     */
/*   field being read (SPI read/write is always bit-synchronous). The write   */
/*   is flagged as in progress and completed by an "...End" co-routine        */
/*   scheduled by the "...AccessInProgress" flag. Set the starting state of   */
/*   the co-routine                                                           */
/*                                                                            */
/******************************************************************************/

APV_ERROR_CODE apvRfm69hcwWriteRegisterStart(apv_rfm69hcwRegisterAddressSet_t   rfm69HcwRegister,
                                             APV_TYPE_UCHAR                    *rfm69HcwRegisterData,
                                             apv_rfm69hcwRegisterState_t       *rfm69HcwRegisterState,
                                             void                             (*durationTimerCallBack)(void *durationEventMessage),
                                             apv_rfm69hcwRegisterSchedule_t    *rfm69HcwRegisterScheduleAccess,
                                             apvCoreTimerBlock_t               *timerBlock,
                                             APV_TYPE_UINT32                   *timerIndex,
                                             APV_TYPE_BOOLEAN                  *timerFlag,
                                             apvSPIFixedChipSelects_t           spiChipSelect)
  {
/******************************************************************************/

  APV_ERROR_CODE rfm69hcwError    = APV_ERROR_CODE_NONE;
  // Register access lengths always include the address byte
  APV_TYPE_UINT  charactersLength = rfm69HcwRegisterState->apv_rfm69hcwRegisterDataLength + APV_RFM69HCW_REG_DEFAULT_WIDTH,
                 registerCounter  = 0;

/******************************************************************************/

  if ((durationTimerCallBack != NULL) && (rfm69HcwRegisterScheduleAccess != NULL) &&
      (timerBlock            != NULL) && (timerIndex                     != NULL) &&
      (*rfm69HcwRegisterScheduleAccess == APV_RFM69HCW_REGISTER_ACCESS_IN_PROGRESS_NONE))
    {
     // Setup the <address + data> buffer to write "...DataLength" characters
     for (registerCounter = APV_RFM69HCW_REGISTER_DATA_OFFSET; registerCounter < charactersLength; registerCounter++)
       {
       rfm69hcwRegisterState.apv_rfm69hcwRegisterData[registerCounter] = *(rfm69HcwRegisterData + registerCounter - APV_RFM69HCW_REGISTER_DATA_OFFSET);
       }

    rfm69hcwError = apvRfm69hcwAccessRegister( rfm69HcwRegister,
                                              &apv_rfm69hcwRegisterDefinitions[0],
                                               rfm69HcwRegisterState->apv_rfm69hcwRegisterData,
                                              &charactersLength,
                                               APV_RFM69HCW_REGISTER_WRITE_DATA,
                                               spiChipSelect);

    if (rfm69hcwError == APV_ERROR_CODE_NONE)
      {
      /******************************************************************************/
      /* The register "write" access has been started. Initialise the state of the  */
      /* "...End" function and create a countdown timer to wait for the register    */
      /* "write" to complete or a failure                                           */
      /******************************************************************************/

       rfm69hcwRegisterState.apv_rfm69hcwRegisterAccessState = APV_RFM69HCW_REGISTER_ACCESS_STATE_START;      
      *timerFlag                                             = false;
    
      rfm69hcwError = apvCreateDurationTimer(timerBlock,
                                             durationTimerCallBack,                 // callback function
                                             (APV_TYPE_VOID *)timerFlag,            // timer expiry flag passed to callback function
                                             APV_DURATION_TIMER_TYPE_ONE_SHOT,      // single-shot
                                             APV_RFM69HCW_EVENT_TIMER_5ms,          // five millisecond period
                                             APV_DURATION_TIMER_SOURCE_SYSTICK,
                                             APV_DURATION_TIMER_STATE_STOPPED,
                                             timerIndex);
 
      if (rfm69hcwError == APV_ERROR_CODE_NONE)
        {
        // Restart the one-shot timer delay waiting for a character
        rfm69hcwError = apvReTriggerDurationTimer( timerBlock,
                                                  *timerIndex,
                                                   APV_RFM69HCW_EVENT_TIMER_5ms);
        }
      }
                                                
    if (rfm69hcwError == APV_ERROR_CODE_NONE)
      {
      *rfm69HcwRegisterScheduleAccess = APV_RFM69HCW_REGISTER_ACCESS_IN_PROGRESS_WRITE;
      }
    }
  else
    {
    rfm69hcwError = APV_ERROR_CODE_NULL_PARAMETER;
    }

/******************************************************************************/

  return(rfm69hcwError);

/******************************************************************************/
  } /* end of apvRfm69hcwWriteRegisterStart                                   */

/******************************************************************************/
/* apvRfm69hcwWriteRegisterEnd() :                                            */
/*  --> rfm69hcwRegisterState            : register write state               */
/*  --> rfm69HcwRegisterScheduleAccess   : flag a register access in progress */
/*                                         to detect completion               */
/*  --> timerBlock                       : duration timers block              */
/*  --> timerIndex                       : duration timer index               */
/*  --> timerFlag                        : duration timer event signalling    */
/*                                         flag                               */
/*  --> spiChipSelect                    : SPI slave device chip select       */
/*                                                                            */
/* - clear any characters in the SPI FIFO after a write. These do not need to */
/*   be saved                                                                 */
/*                                                                            */
/******************************************************************************/
                                            
APV_ERROR_CODE apvRfm69hcwWriteRegisterEnd(apv_rfm69hcwRegisterState_t    *rfm69HcwRegisterState,
                                           apv_rfm69hcwRegisterSchedule_t *rfm69HcwRegisterScheduleAccess,
                                           apvCoreTimerBlock_t            *timerBlock,
                                           APV_TYPE_UINT32                *timerIndex,
                                           APV_TYPE_BOOLEAN               *timerFlag,
                                           apvSPIFixedChipSelects_t        spiChipSelect)
  {
/******************************************************************************/

  volatile APV_TYPE_UINT16 apvRfm69HcwDataWord = 0;

           APV_ERROR_CODE  rfm69hcwError       = APV_ERROR_CODE_NONE;

/******************************************************************************/

  // Test for rfm69hcw register "write" in progress
  if ((rfm69HcwRegisterScheduleAccess     != NULL) && (rfm69HcwRegisterState != NULL) &&
      (timerBlock                         != NULL) && (timerIndex            != NULL) && 
      (timerFlag                          != NULL) &&
      (*rfm69HcwRegisterScheduleAccess == APV_RFM69HCW_REGISTER_ACCESS_IN_PROGRESS_WRITE))
    {
    /******************************************************************************/
    /* The SPI FIFO writes are stateful :                                         */
    /******************************************************************************/

    switch(rfm69HcwRegisterState->apv_rfm69hcwRegisterAccessState)
      {
      case APV_RFM69HCW_REGISTER_ACCESS_STATE_START  : if (*timerFlag == false)
                                                         {
                                                         /******************************************************************************/
                                                         /* "...Index" counts the number of "...Timer == false" iterations             */
                                                         /******************************************************************************/

                                                         rfm69hcwRegisterState.apv_rfm69hcwRegisterIndex = APV_RFM69HCW_REGISTER_ACCESS_TIMER_FAILURE;

                                                         // Time-out (using the scheduling rate of this function) the timer!
                                                         rfm69HcwRegisterState->apv_rfm69hcwRegisterAccessState = APV_RFM69HCW_REGISTER_ACCESS_STATE_PULL;
                                                         }
                                                       else
                                                         {
                                                         // All done, clear the write transaction
                                                         rfm69HcwRegisterState->apv_rfm69hcwRegisterAccessState = APV_RFM69HCW_REGISTER_ACCESS_STATE_END;
                                                         }
                                                       break;

      case APV_RFM69HCW_REGISTER_ACCESS_STATE_PULL   : if (*timerFlag == false)
                                                         {
                                                         // Time's up ?
                                                         rfm69hcwRegisterState.apv_rfm69hcwRegisterIndex = rfm69hcwRegisterState.apv_rfm69hcwRegisterIndex - 1;

                                                         if (rfm69hcwRegisterState.apv_rfm69hcwRegisterIndex == 0)
                                                           {
                                                           // Timer failure - very bad!
                                                           rfm69HcwRegisterState->apv_rfm69hcwRegisterAccessState = APV_RFM69HCW_REGISTER_ACCESS_STATE_FAILED;
                                                           rfm69hcwError                                          = APV_ERROR_CODE_EVENT_TIMEOUT_ERROR;
                                                           }
                                                         }
                                                       else
                                                         {
                                                         // All done, clear the write transaction
                                                         rfm69HcwRegisterState->apv_rfm69hcwRegisterAccessState = APV_RFM69HCW_REGISTER_ACCESS_STATE_END;
                                                         }
                                                       
                                                       break;

      case APV_RFM69HCW_REGISTER_ACCESS_STATE_END    : apvDestroyDurationTimer(timerBlock, 
                                                                               timerIndex);       
      case APV_RFM69HCW_REGISTER_ACCESS_STATE_FAILED : // Clear the FIFO!
                                                       apvGetFIFOFillLevel(&apvSpi0ChipSelectFIFO[spiChipSelect],
                                                                           &rfm69HcwRegisterState->apv_rfm69hcwRegisterCount,
                                                                            true);

                                                       while (rfm69HcwRegisterState->apv_rfm69hcwRegisterCount > 0)
                                                         {
                                                         rfm69hcwError = apvSpiReceiveCharacter(&apvRfm69HcwDataWord,
                                                                                                &spiChipSelect);
                                                         }

                                                       // Cancel the "write" attempt
                                                       *rfm69HcwRegisterScheduleAccess = APV_RFM69HCW_REGISTER_ACCESS_IN_PROGRESS_NONE;
                                                       break;

      default                                        : 
                                                       break;
      }

    /******************************************************************************/
    }
  else
    {
    apvDestroyDurationTimer(timerBlock, 
                            timerIndex);
  
    // Cancel the unsuccessful "write" attempt
    *rfm69HcwRegisterScheduleAccess = APV_RFM69HCW_REGISTER_ACCESS_IN_PROGRESS_NONE;
  
     rfm69hcwError                  = APV_ERROR_CODE_PARAMETER_OUT_OF_RANGE;
    }

/******************************************************************************/

  return(rfm69hcwError);

/******************************************************************************/
  } /* end of apvRfm69hcwWriteRegisterEnd                                     */

/******************************************************************************/
/* apvRfm69HcwWriteRegisterDefault() :                                        */
/*  --> apv_rfm69HcwRegisterNumber : Rfm69Hcw register number                 */
/*                                                                            */
/* - write the default value into a Rfm69Hcw register "shadow"                */
/*                                                                            */
/******************************************************************************/

APV_ERROR_CODE apvRfm69HcwWriteRegisterDefault(apv_rfm69hcwRegisterAddressSet_t apv_rfm69HcwRegisterNumber)
  {
/******************************************************************************/

  APV_ERROR_CODE defaultError = APV_ERROR_CODE_NONE;

/******************************************************************************/

  if (apv_rfm69HcwRegisterNumber >= APV_RFM69HCW_REGISTER_ADDRESS_SET)
    {
    defaultError = APV_ERROR_CODE_PARAMETER_OUT_OF_RANGE;
    }
  else
    {
    apv_rfm69hcwRegisters[apv_rfm69HcwRegisterNumber].apv_rfm69hcwRegister = apv_rfm69hcwRegisters[apv_rfm69HcwRegisterNumber].apv_rfm69hcwRegisterDefinition->apv_rfm69hcwRegisterDefault ;
    }

/******************************************************************************/

  return(defaultError);
  
/******************************************************************************/
  } /* end of apvRfm69HcwWriteRegisterDefault                                 */

/******************************************************************************/
/* apvRfm69hcwAccessRegister() :                                              */
/*  --> rfm69hcwRegister                 : rfm69hcw register number           */
/*  --> rfm69hcwRegisterDefinitions      : register characteristics           */
/*                                         definition array                   */
/*  --> rfm69hcwRegisterCharacters       : array of bytes describing the      */
/*                                         register read/write request. The   */
/*                                         data MUST start at array offset #1 */
/*  --> rfm69hcwRegisterCharactersLength : number of register characters to   */
/*                                         be written/returned INCLUDING the  */
/*                                         address byte                       */
/*  --> rfm69hcwRegisterReadWrite        : register access direction          */
/*  --> spiChipSelect                    : SPI chip-select to drive           */
/*                                                                            */
/* - read/write a rfm69hcw configuration register in the register map. This   */
/*   function sends a register read or write request over the SPI bus to the  */
/*   RF chip. In the case of a write request the function wholly satisfies    */
/*   the transaction. In the case of a read request the SPI receiveing FIFO   */
/*   must be further inspected to retrieve the result                         */
/*                                                                            */
/******************************************************************************/

APV_ERROR_CODE apvRfm69hcwAccessRegister(      apv_rfm69hcwRegisterAddressSet_t  rfm69HcwRegister,
                                         const apv_rfm69hcwRegisterDefinition_t *rfm69hcwRegisterDefinitions,
                                               APV_TYPE_UCHAR                   *rfm69hcwRegisterCharacters,
                                               APV_TYPE_UINT                    *rfm69hcwRegisterCharactersLength,
                                               apv_rfm69hcwRegisterReadWrite_t   rfm69hcwRegisterReadWrite,
                                               apvChipSelectRegisterInstance_t   spiChipSelect)
  {
/******************************************************************************/

  APV_ERROR_CODE       rfm69hcwError = APV_ERROR_CODE_NONE;

  APV_TYPE_UINT        bufferIndex   = 0;
  apvSPILastTransfer_t lastTransfer  = APV_SPI_LAST_TRANSFER_NONE;

/******************************************************************************/

 // Check the parameters ad nauseam
 if (( rfm69hcwRegisterDefinitions      == NULL ) || ( rfm69hcwRegisterCharacters == NULL ) || 
     ( rfm69hcwRegisterCharactersLength == NULL ) || 
     ((rfm69hcwRegisterReadWrite        != APV_RFM69HCW_REGISTER_READ_DATA) && (rfm69hcwRegisterReadWrite !=APV_RFM69HCW_REGISTER_WRITE_DATA )) ||
     ( spiChipSelect                    >= APV_SPI_CHIP_SELECT_REGISTER_SET ))
   {
   rfm69hcwError = APV_ERROR_CODE_EVENT_UNRECOGNISED_PARAMETER;
   }
 else
   {
   if (rfm69HcwRegister >= APV_RFM69HCW_REGISTER_ADDRESS_SET)
     {
     rfm69hcwError = APV_ERROR_CODE_PARAMETER_OUT_OF_RANGE;
     }
   else
     {
     if ((rfm69hcwRegisterDefinitions + rfm69HcwRegister)->apv_rfm69hcwRegisterInUse == FALSE)
       {
       rfm69hcwError = APV_ERROR_CODE_NULL_PARAMETER;
       }
     else
       {
       if (*rfm69hcwRegisterCharactersLength > APV_RFM69HCW_REGISTER_SETUP_MAXIMUM_WIDTH)
         {
         rfm69hcwError = APV_ERROR_CODE_PARAMETER_OUT_OF_RANGE;
         }
       else
         {
         // Build the access-address/command
         *(rfm69hcwRegisterCharacters + APV_RFM69HCW_REGISTER_COMMAND_OFFSET) = rfm69HcwRegister | rfm69hcwRegisterReadWrite;

         // Now send the register access request 
         for (bufferIndex = 0; bufferIndex < *rfm69hcwRegisterCharactersLength; bufferIndex++)
           {
           // The final byte in the payload buffer is marked by CSN rising
           if (bufferIndex == (*rfm69hcwRegisterCharactersLength - 1))
             {
             lastTransfer  = APV_SPI_LAST_TRANSFER_ACTIVE;
             }

           /******************************************************************************/
           /* A couple of notes on "apvSPITransmitCharacter()" :                         */
           /*  (i) strictly the character loaded onto the SPI FIFO by the function is :  */
           /*      (<parameter1> << 8) | <parameter2> which is a 16-bit number. In the   */
           /*      case of an 8-bit number the required byte must be loaded as           */
           /*      <parameter2>                                                          */
           /* (ii) <parameter5> is an instruction to prime the SPI transmit interrupt.   */
           /*      This is actually achieved by inspecting the "transmitter empty" flag  */
           /*      so this parameter is redundant                                        */
           /******************************************************************************/

           rfm69hcwError = apvSPITransmitCharacter( APV_RFM69HCW_NULL_CHARACTER,
                                                   *(rfm69hcwRegisterCharacters + bufferIndex),
                                                    //APV_RFM69HCW_NULL_CHARACTER,
                                                    spiChipSelect,
                                                    lastTransfer,
                                                    true);

           }
         }
       }
     }
   }

/******************************************************************************/

  return(rfm69hcwError);

/******************************************************************************/
  } /* end of apvRfm69hcwAccessRegister                                       */

/******************************************************************************/
/* apvRfm69HcwAtomicReset() :                                                 */
/*  --> apvCoreTimeBaseBlock : global timebase controlling structure          */
/*                                                                            */
/*  - carry out a chip RESET without yielding (as the "...Trigger" +          */
/*    "...Terminate"  pair do). The RESET needs :                             */
/*       (i) >100us assertion time                                            */
/*      (ii) > 5ms  dwell time after RESET                                    */
/*                                                                            */
/******************************************************************************/

APV_ERROR_CODE apvRfm69HcwAtomicReset(apvCoreTimerBlock_t *apvCoreTimeBaseBlock)
  {
/******************************************************************************/

         APV_ERROR_CODE   rfm69hcwError         =   APV_ERROR_CODE_NONE;

volatile APV_TYPE_BOOLEAN apvRfm69HcwDelay      = false;
         APV_TYPE_UINT32  apvRfm69HcwDelayIndex = 0;

/******************************************************************************/

  // RESET prephase        :
  // The RESET initial transition is LOW-TO-HIGH so the line needs to be LOW == '0' pre-RESET
  
  ApvPeripheralLineControlBlock_p[APV_PERIPHERAL_LINE_GROUP_D]->PIO_CODR = PIO_CODR_P0;

  rfm69hcwError = apvCreateDurationTimer(apvCoreTimeBaseBlock,
                                          apvRfm69HcwStateTimer,
                                         (void *)&apvRfm69HcwDelay,
                                          APV_DURATION_TIMER_TYPE_ONE_SHOT,
                                          APV_RFM69HCW_RESET_ASSERTION_DELAY_PERIOD,
                                          APV_DURATION_TIMER_SOURCE_SYSTICK,
                                          APV_DURATION_TIMER_STATE_RUNNING,
                                         &apvRfm69HcwDelayIndex);

  while(apvRfm69HcwDelay == false)
    ;

  rfm69hcwError = apvDestroyDurationTimer( apvCoreTimeBaseBlock,
                                          &apvRfm69HcwDelayIndex);


  // RESET assertion phase :
  // The RESET initial transition is LOW-TO-HIGH so the line needs to be HIGH == '1' at RESET
  apvRfm69HcwDelay = false;
  
  ApvPeripheralLineControlBlock_p[APV_PERIPHERAL_LINE_GROUP_D]->PIO_SODR = PIO_SODR_P0;

  rfm69hcwError = apvCreateDurationTimer(apvCoreTimeBaseBlock,
                                          apvRfm69HcwStateTimer,
                                         (void *)&apvRfm69HcwDelay,
                                          APV_DURATION_TIMER_TYPE_ONE_SHOT,
                                          APV_RFM69HCW_RESET_ASSERTION_DELAY_PERIOD,
                                          APV_DURATION_TIMER_SOURCE_SYSTICK,
                                          APV_DURATION_TIMER_STATE_RUNNING,
                                         &apvRfm69HcwDelayIndex);

  while(apvRfm69HcwDelay == false)
    ;

  rfm69hcwError = apvDestroyDurationTimer( apvCoreTimeBaseBlock,
                                          &apvRfm69HcwDelayIndex);

  // RESET dwell phase :
  // The RESET final transition is HIGH-TO-LOW so the line needs to be LOW == '0' after RESET
  apvRfm69HcwDelay = false;

  ApvPeripheralLineControlBlock_p[APV_PERIPHERAL_LINE_GROUP_D]->PIO_CODR = PIO_CODR_P0;

  // RESET (post-)dwell period
  rfm69hcwError = apvCreateDurationTimer(apvCoreTimeBaseBlock,
                                          apvRfm69HcwStateTimer,
                                         (void *)&apvRfm69HcwDelay,
                                          APV_DURATION_TIMER_TYPE_ONE_SHOT,
                                          APV_RFM69HCW_RESET_DWELL_DELAY_PERIOD,
                                          APV_DURATION_TIMER_SOURCE_SYSTICK,
                                          APV_DURATION_TIMER_STATE_RUNNING,
                                         &apvRfm69HcwDelayIndex);

  while(apvRfm69HcwDelay == false)
    ;

  rfm69hcwError = apvDestroyDurationTimer( apvCoreTimeBaseBlock,
                                          &apvRfm69HcwDelayIndex);

/******************************************************************************/

  return(rfm69hcwError);

/******************************************************************************/
  } /* end of apvRfm69HcwAtomicReset                                          */

/******************************************************************************/
/* apvTriggerRfm69HcwManualReset() :                                          */
/*                                                                            */
/* - kick-off an RFM69HCW chip reset period                                   */
/*                                                                            */
/******************************************************************************/

APV_ERROR_CODE apvTriggerRfm69HcwManualReset(APV_TYPE_VOID)
   {
/******************************************************************************/

  APV_ERROR_CODE resetError = APV_ERROR_CODE_NONE;

/******************************************************************************/

  // Exit if a RESET is in progress!
  if ((apvRfm69HcwManualResetFlag == false) && (apvRfm69HcwManualResetCounter == 0))
    {
    // Flag a chip manual reset is in progress and initialise the countdown
    apvRfm69HcwManualResetFlag    = true;
    apvRfm69HcwManualResetCounter = APV_RFM69HCW_MANUAL_RESET_COUNTDOWN;
  
    // Enable the reset line output
    ApvPeripheralLineControlBlock_p[APV_PERIPHERAL_LINE_GROUP_D]->PIO_OER  = PIO_OER_P0;
  
    // The RESET initial transition is LOW-TO-HIGH so the line needs to be HIGH == '1' at RESET
    ApvPeripheralLineControlBlock_p[APV_PERIPHERAL_LINE_GROUP_D]->PIO_SODR = PIO_SODR_P0;
    }
  else
    {
    resetError = APV_ERROR_CODE_CONFIGURATION_ERROR;
    }

/******************************************************************************/

  return(resetError);

/******************************************************************************/
  } /* end of apvTriggerRfm69HcwManualReset                                   */

/******************************************************************************/
/* apvTerminateRfm69HcwManualReset() :                                        */
/*  <--> terminationError : return the error state of the termination attempt */
/*                          or NULL                                           */
/*   <-- terminationState : [ true | false ]                                  */
/*                                                                            */
/* - terminate an RFM69HCW chip reset period NOT a cancellation - that is not */
/*   in the remit of the software, the chip will proceed anyway               */
/*                                                                            */
/******************************************************************************/

APV_TYPE_BOOLEAN apvTerminateRfm69HcwManualReset(APV_ERROR_CODE *terminationError)
   {
/******************************************************************************/

  APV_TYPE_BOOLEAN  terminationState  = false;
  
/******************************************************************************/

  if (terminationError != NULL)
    {
    *terminationError  = APV_ERROR_CODE_NONE; // just a placeholder for now
    }

  // Exit if a RESET is in progress!
  if ((apvRfm69HcwManualResetFlag == true) && (apvRfm69HcwManualResetCounter == 0))
    {
    // A chip manual reset has ended
    terminationState = true;
  
    // Disable the reset line output
    ApvPeripheralLineControlBlock_p[APV_PERIPHERAL_LINE_GROUP_D]->PIO_ODR  = PIO_ODR_P0;
  
    // The RESET end transition is HIGH-TO-LOW so the line needs to be LOW == '0'' after RESET
    ApvPeripheralLineControlBlock_p[APV_PERIPHERAL_LINE_GROUP_D]->PIO_CODR = PIO_CODR_P0;
    }
  else
    {
    terminationState = false;
    }

/******************************************************************************/

  return(terminationState);

/******************************************************************************/
  } /* end of apvTerminateRfm69HcwManualReset                                   */

/******************************************************************************/
/* apvRfm69HcwStateTimer() :                                                  */
/*  --> durationEventMessage : possible callback parameter - in this case a   */
/*                             boolean flag signalling timer expiry           */
/*                                                                            */
/* - device timing callback - set the "timeout" flag                          */
/*                                                                            */
/******************************************************************************/

APV_TYPE_VOID apvRfm69HcwStateTimer(APV_TYPE_VOID *durationEventMessage)
  {
/******************************************************************************/

  *(APV_TYPE_BOOLEAN *)durationEventMessage = true;

/******************************************************************************/
  } /* end of apvRfm69HcwStateTimer                                           */

/******************************************************************************/
/* apv_rfm69hcw.c                                                             */
/******************************************************************************/
/* (C) PulsingCoreSoftware Limited 2020 (C)                                   */
/******************************************************************************/
