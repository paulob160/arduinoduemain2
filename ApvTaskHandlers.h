/******************************************************************************/
/* (C) PulsingCoreSoftware Limited 2018 (C)                                   */
/******************************************************************************/
/*                                                                            */
/* ApvTaskHandlers.h                                                          */
/* 31.10.18                                                                   */
/* Paul O'Brien                                                               */
/*                                                                            */
/* - definition of the cooperative ("mosaic") scheduling structure            */
/*                                                                            */
/******************************************************************************/

#ifndef _APV_TASK_HANDLERS_H_
#define _APV_TASK_HANDLERS_H_

/******************************************************************************/
/* Include Files :                                                            */
/******************************************************************************/

#include "ApvError.h"
#include "ApvUtilities.h"
#include "ApvTaskPriorities.h"

/******************************************************************************/
/* Definitions :                                                              */
/******************************************************************************/

#define APV_MAXIMUM_TASK_CONTROL_BLOCKS      (32)
#define APV_TASK_SCRATCH_WORKSPACE_SIZE      (32) // bytes!

#define APV_MAXIMUM_TASK_SCHEDULING_TICKS  (1024) // note each tick is actually 1 millisecond so the whole 
                                                  // schedule is 1024 milliseconds

/******************************************************************************/
/* Type Definitions :                                                         */
/******************************************************************************/
/* Task scheduling types :                                                    */
/*  - APV_TASK_SCHEDULE_PERPETUAL : run the task at each scheduling tick that */
/*                                  is a multiple of the task event interval  */
/*                                  i.e. event interval == 10 { 10, 20, 30... */
/*                                  WRAPPING at the scheduling boundary e.g.  */
/*                                  { 1020, (1030 - 1024), (1040 - 1024) }    */
/*  - APV_TASK_SCHEDULE_REPEATING : run the task at the event time ONCE per   */
/*                                  scheduling boundary                       */
/*  - APV_TASK_SCHEDULE_SINGLE    : run the task ONCE ONLY at the first event */
/*                                  found in the earliest schedule            */
/*  - APV_TASK_SCHEDULE_NEVER     : ignore the task unless and until its      */
/*                                  scheduling scheme is changed              */
/******************************************************************************/

typedef enum apvTaskScheduleType_tTag
  {
  APV_TASK_SCHEDULE_NONE = 0,
  APV_TASK_SCHEDULE_PERPETUAL,
  APV_TASK_SCHEDULE_REPEATING,
  APV_TASK_SCHEDULE_SINGLE,
  APV_TASK_SCHEDULE_NEVER,
  APV_TASK_SCHEDULE_TYPES
  } apvTaskScheduleType_t;

/******************************************************************************/

typedef struct apvTaskControlBlock_tTag
  {
  boolean_t                      apvTaskControlBlockAllocated;
  apvTaskScheduleType_t          apvTaskScheduleType;
  apvTaskSchedulePriorityClass_t apvTaskSchedulePriorityClass;      // a high priority task CAN block a low priority task even if there is no 
                                                                    // preemption! If two tasks have the same event time or self-schedule the 
                                                                    // higher priority task will still run first! "Sole" priority tasks will be the 
                                                                    // only task to run in that slot; any other tasks will (i) have their event-time
                                                                    // incremented (next slot) or (ii) self-scheduled tasks will just not run until 
                                                                    // a clear slot occurs!
  APV_TASK_SCHEDULE_PRIORITY        apvTaskSchedulePriority;        // task priority 0 == highest 0xfe == lowest 0xff == reserved
  boolean_t                         apvTaskSelfSchedulingFlag;      // the task may run even if the event interval is not current - hence a 
                                                          // task can start on an interval and proceed through several internal states
  boolean_t                         apvTaskSchedulingEventComplete; // the task has run since the last scheduled event
  uint16_t                          apvTaskSchedulingEventInterval; // the interval the task shold be run at :
                                                                    //  - for APV_TASK_SCHEDULE_PERPETUAL this is a multiple
                                                                    //  - for APV_TASK_SCHEDULE_REPEATING this is a target
                                                                    //  - for APV_TASK_SCHEDULE_SINGLE    this is a once-only
  uint16_t                          apvTaskSchedulingEventNext;     // the next time the task should run :
                                                                    //  - for APV_TASK_SCHEDULE_PERPETUAL this increments intra-schedule
                                                                    //  - for APV_TASK_SCHEDULE_REPEATING this equals the inter-schedule target
                                                                    //  - for APV_TASK_SCHEDULE_SINGLE    this is a once-only
  APV_ERROR_CODE                  (*apvTaskFunction)(void      *taskParameters,
		                                                   void      *taskWorkspace,
                                                     boolean_t *taskSelfSchedulingFlag);
  void                             *apvTaskParameters;
  uint8_t                           apvTaskScratchWorkspace[APV_TASK_SCRATCH_WORKSPACE_SIZE];
  } apvTaskControlBlock_t;

/******************************************************************************/
/* Global Variable Declarations :                                             */
/******************************************************************************/

extern apvTaskControlBlock_t  apvTaskControlBlocks[APV_MAXIMUM_TASK_CONTROL_BLOCKS];
extern apvTaskControlBlock_t *apvTaskRunList[APV_MAXIMUM_TASK_CONTROL_BLOCKS];

/******************************************************************************/
/* Global Function Declarations :                                             */
/******************************************************************************/

extern APV_ERROR_CODE apvInitialiseTaskControlBlocks(apvTaskControlBlock_t *taskControlBlocks);
extern APV_ERROR_CODE apvInitialiseTaskControlBlock(apvTaskControlBlock_t *taskControlBlock);
extern APV_ERROR_CODE apvCreateTask(APV_ERROR_CODE        (*taskFunction)(void      *taskParameters,
		                                                                        void      *taskWorkspace,
                                                                          boolean_t *taskSelfSchedulingFlag),
                                    void                  *taskParameters,
                                    apvTaskScheduleType_t  taskScheduleType,
                                    uint16_t               taskSchedulingInterval);

/******************************************************************************/

#endif

/******************************************************************************/
/* (C) PulsingCoreSoftware Limited 2018 (C)                                   */
/******************************************************************************/
