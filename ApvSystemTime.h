/******************************************************************************/
/*                                                                            */
/* ApvSystemTime.h                                                            */
/* 03.05.18                                                                   */
/* Paul O'Brien                                                               */
/*                                                                            */
/* - holding file for the various system timer definitions                    */
/*                                                                            */
/******************************************************************************/

#ifndef _APV_SYSTEM_TIME_H_
#define _APV_SYSTEM_TIME_H_

/******************************************************************************/
/* Include Files :                                                            */
/******************************************************************************/

#include <stdint.h>
#include "ApvEventTimers.h"

/******************************************************************************/
/* Definitions :                                                              */
/******************************************************************************/

#define APV_EVENT_TIMER_GENERAL_PURPOSE_TIME_BASE (APV_EVENT_TIMER_TIMEBASE_MINIMUM * ((uint32_t)100000)) // 1000000 nanoseconds (1 millisecond)

#define APV_EVENT_TIMER_CHANNEL_TIME_BASE_MAXIMUM ((uint32_t)((int32_t)-1))

/******************************************************************************/
/* As the main loop runs at one millisecond intervals it can be subdivided    */
/* into coarse timing intervals using a bounded counter                       */
/******************************************************************************/

#define APV_ONE_MILLISECOND_COUNTER_MINIMUM       ((uint16_t)0)
#define APV_ONE_MILLISECOND_COUNTER_MAXIMUM       ((uint16_t)1000)

#define APV_ONE_SECOND_COUNTER_MINIMUM            ((uint16_t)0)
#define APV_ONE_SECOND_COUNTER_MAXIMUM            ((uint16_t)1)

// These task intervals are currently wholly arbitrary!
#define APV_ONE_MILLISECOND_LSM9DS1_TASK_TRIGGER  ((APV_ONE_MILLISECOND_COUNTER_MAXIMUM - APV_ONE_MILLISECOND_COUNTER_MINIMUM) >> 1)
#define APV_ONE_MILLISECOND_RFM69HCW_TASK_TRIGGER ((APV_ONE_MILLISECOND_COUNTER_MAXIMUM - APV_ONE_MILLISECOND_COUNTER_MINIMUM) >> 2)
#define APV_ONE_MILLISECOND_UBLOX6_TASK_TRIGGER   ((uint16_t)97)

/******************************************************************************/
/* Type Definitions :                                                         */
/******************************************************************************/

typedef enum apvEventTimerFlag_tTag
  {
  APV_EVENT_TIMER_FLAG_CLEAR = 0,
  APV_EVENT_TIMER_FLAG_SET,
  APV_EVENT_TIMER_FLAG_TYPES
  } apvEventTimerFlag_t;

typedef struct apvEventTimerHotOnes_tTag
  {
  uint32_t APV_EVENT_TIMER_CHANNEL_0_FLAG : 1;
  uint32_t APV_EVENT_TIMER_CHANNEL_1_FLAG : 1;
  uint32_t APV_EVENT_TIMER_CHANNEL_2_FLAG : 1;
  uint32_t APV_EVENT_TIMER_CHANNEL_3_FLAG : 1;
  uint32_t APV_EVENT_TIMER_CHANNEL_4_FLAG : 1;
  uint32_t APV_EVENT_TIMER_CHANNEL_5_FLAG : 1;
  uint32_t APV_EVENT_TIMER_CHANNEL_6_FLAG : 1;
  uint32_t APV_EVENT_TIMER_CHANNEL_7_FLAG : 1;
  uint32_t APV_EVENT_TIMER_CHANNEL_8_FLAG : 1;
  } apvEventTimerHotOnes_t;

typedef union apvEventTimersHotOneList_tTag
  {
  apvEventTimerHotOnes_t Flags;
  uint32_t               FlagPole;
  } apvEventTimersHotOneList_t;

/******************************************************************************/
/* Global Variable Declarations :                                             */
/******************************************************************************/

extern apvEventTimersBlock_t      apvEventTimerBlock[APV_EVENT_TIMERS];

extern uint32_t                   apvEventTimerGeneralPurposeTimeBaseTarget;

extern apvEventTimersHotOneList_t apvEventTimerHotShot;

/******************************************************************************/

#endif

/******************************************************************************/
/* (C) PulsingCoreSoftware Limited 2018 (C)                                   */
/******************************************************************************/