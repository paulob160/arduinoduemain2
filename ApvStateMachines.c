/******************************************************************************/
/* (C) PulsingCoreSoftware Limited 2020 (C)                                   */
/******************************************************************************/
/*                                                                            */
/* ApvStateMachines.c                                                         */
/* 25.03.18                                                                   */
/* Paul O'Brien                                                               */
/*                                                                            */
/* - a set of definitions and functions to handle generic state-machine       */
/*   states, variables, processing and transitions. All APV state-machines    */
/*   be defined as non-generic "clones" of these structures to allow a single */
/*   engine to process each state-machine requirement.                        */
/*                                                                            */
/*  State-machines' states must be defined in the form :                      */
/*                                                                            */
/*  apv<...>States_tTag                                                       */
/*   {                                                                        */
/*   APV_<...>_STATE_NULL = 0,                                                */
/*   APV_<...>_STATE_INITIALISATION,                                          */
/*        ...                                                                 */
/*   < domain state-machine specific states >                                 */
/*        ...                                                                 */
/*   APV_<...>_ERROR_REPORTER,                                                */
/*   APV_<...>_STATES // the number of states in this machine                 */
/*                                                                            */
/******************************************************************************/
/* Include Files :                                                            */
/******************************************************************************/

#include "ApvError.h"
#include "ApvStateMachines.h"
#include "ApvMessageHandling.h"
#include "ApvCommsUtilities.h"
#include "ApvSystemTime.h"
#include "NMEADecoder.h"
#include "NMEAUtilities.h"

/******************************************************************************/
/* Global Variable Definitions :                                              */
/******************************************************************************/

// The task scheduler runs at nultiples of this interval
uint16_t               apvNmeaDecoderCoreSchedule = APV_ONE_MILLISECOND_UBLOX6_TASK_TRIGGER;

// Local store for incoming characters fromthe uBlox GPS receiver
nmeaNumericalVariant_t nmeaDecoderCharacters[APV_COMMS_RING_BUFFER_MAXIMUM_LENGTH];

/******************************************************************************/
/* Function Definitions :                                                     */
/******************************************************************************/
/* apvExecuteStateMachine() :                                                 */
/*  --> apvDomainStateMachine : pointer to the concrete state-machine to be   */
/*                              executed                                      */
/*  <-- apvStateMachineErrors : error codes                                   */
/*                                                                            */
/* - using the defining table execute the next state of the machine and       */
/*   return the error status                                                  */
/*                                                                            */
/******************************************************************************/

APV_GENERIC_STATE_CODE apvExecuteStateMachine(apvGenericState_t *apvDomainStateMachine)
  {
/******************************************************************************/

  APV_GENERIC_STATE_CODE apvStateMachineCode = APV_STATE_MACHINE_CODE_NONE;

/******************************************************************************/

  // Execute the current and next states of the machine until the driving event 
  // has completed - which may be none, part or whole of the complete process
  while (apvStateMachineCode != APV_STATE_MACHINE_CODE_STOP)
    {
    apvStateMachineCode = (APV_GENERIC_STATE_CODE)(apvDomainStateMachine + apvDomainStateMachine->apvGenericStateVariables->apvGenericStateVariables[APV_GENERIC_STATE_VARIABLE_ACTIVE_STATE])->apvGenericStateAction(apvDomainStateMachine + apvDomainStateMachine->apvGenericStateVariables->apvGenericStateVariables[APV_GENERIC_STATE_VARIABLE_ACTIVE_STATE]);
    }

/******************************************************************************/

  return(apvStateMachineCode);

/******************************************************************************/
  } /* end of apvExecuteStateMachine                                          */

/******************************************************************************/
/* apvExecuteNMEAStateMachine() :                                             */
/*  --> nmeaPortRingBuffer : ring buffer directly connected to the serial     */
/*                           port                                             */
/*                                                                            */
/* - get any outstanding characters from the NMEA input port and run the NMEA */
/*   decoder state machine                                                    */
/******************************************************************************/

APV_ERROR_CODE apvExecuteNMEAStateMachine(apvRingBuffer_t *nmeaPortRingBuffer)
  {
/******************************************************************************/

  APV_ERROR_CODE  nmeaErrorCode = APV_ERROR_CODE_NONE;

  NMEA_GPS_UINT16 tokensReady   = 0;
  
/******************************************************************************/

  if ((nmeaErrorCode =  apvRingBufferReportFillState( nmeaPortRingBuffer,
                                                     &tokensReady,
                                                      true)) == APV_ERROR_CODE_NONE)
    {
    if (apvRingBufferUnLoad(nmeaPortRingBuffer,
                            APV_RING_BUFFER_TOKEN_TYPE_LONG_WORD,
                            (NMEA_GPS_UINT32 *)&nmeaDecoderCharacters[0],
                            tokensReady,
                            true) != 0)
        {
        }
    }

/******************************************************************************/

  return(nmeaErrorCode);

/******************************************************************************/
} /* end of apvExecuteNMEAStateMachine                                        */

/******************************************************************************/
/* (C) PulsingCoreSoftware Limited 2020 (C)                                   */
/******************************************************************************/